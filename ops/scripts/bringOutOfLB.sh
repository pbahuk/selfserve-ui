#!/bin/bash
echo "Bringing out of lb"
curDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
(cd $curDir/../.. && make dead)


servicesToBlockFile=/var/www/html/health/servicesToBlock.php
serviceName='selfserve'

if [ -f $servicesToBlockFile ]
then
    echo "Bringing service - $serviceName - out of LB"
    sed -i "s/^#'$serviceName'/'$serviceName'/g" $servicesToBlockFile
fi
