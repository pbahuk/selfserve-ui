#!/bin/bash
echo "Adding to LB"
curDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
(cd $curDir/../.. && make live)

servicesToBlockFile=/var/www/html/health/servicesToBlock.php
serviceName='selfserve'

if [ -f $servicesToBlockFile ]
then
    echo "Bringing service - $serviceName - into LB"
    sed -i "s/^'$serviceName'/#'$serviceName'/g" $servicesToBlockFile
fi

