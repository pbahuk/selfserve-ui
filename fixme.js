var fixme = require('fixme');
// All values below are Fixme default values unless otherwise overridden here. 
fixme({
  path:                 process.cwd(),
  ignored_directories:  ['node_modules/**', '.git/**', 'public/**', 'dist/**'],
  file_patterns:        ['**/*.js', 'Makefile', '**/*.sh','**/*.jsx','**/*.jsx','**/*.css']
});