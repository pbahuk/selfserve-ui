export PATH := /opt/rh/devtoolset-2/root/usr/bin:$(PATH)
export MANPATH := /opt/rh/devtoolset-2/root/usr/share/man:$(MANPATH)
export INFOPATH := /opt/rh/devtoolset-2/root/usr/share/info:$(INFOPATH)
export PCP_DIR := /opt/rh/devtoolset-2/root
export PERL5LIB := /opt/rh/devtoolset-2/root//usr/lib64/perl5/vendor_perl:/opt/rh/devtoolset-2/root/usr/lib/perl5:/opt/rh/devtoolset-2/root//usr/share/perl5/vendor_perl:$(PERL5LIB)

dev:
	make build && NODE_ENV=development NODE_SUBENV=fox7 npm start;
mpreprod:
	make build && NODE_ENV=development NODE_SUBENV=mpreprod npm start;
stage:
	make build && NODE_ENV=development NODE_SUBENV=stage npm start;
scmqa:
 	make build && NODE_ENV=development NODE_SUBENV=scmqa npm start;
sfqa:
 	make build && NODE_ENV=development NODE_SUBENV=sfqa npm start;
mjint:
	make build && NODE_ENV=development NODE_SUBENV=mjint npm start;
fox7:
	make build && NODE_ENV=development NODE_SUBENV=fox7 npm start;
prod:
	NODE_ENV=production make cdn && NODE_ENV=production npm run precompile && NODE_ENV=production npm start;
local-setup:
	rm -rf node_modules npm  --registry http://registry.myntra.com:8000 install && make build;
setup:
	rm -rf node_modules && NODE_ENV=production npm  --registry http://registry.myntra.com:8000 install;
install:
	NODE_ENV=production npm  --registry http://registry.myntra.com:8000 install && make build;
build:
	NODE_ENV=development NODE_SUBENV=fox7 npm run build;
cdn:
	NODE_ENV=production NODE_SUBENV=production npm run build && node ./src/utils/pushToCDN.js;
live:
	touch .live;
dead:
	rm -rf .live;
release:
	rm -rf node_modules && NODE_ENV=release NODE_SUBENV=fox7 npm  --registry http://registry.myntra.com:8000 install && NODE_ENV=release NODE_SUBENV=fox7 npm run build && node ./src/utils/pushToCDN.js && NODE_ENV=release NODE_SUBENV=fox7 pm2 start index.js -f --name selfserve;
soft-release:
	NODE_ENV=release NODE_SUBENV=fox7 npm run build && node ./src/utils/pushToCDN.js && NODE_ENV=release NODE_SUBENV=fox7 pm2 start index.js -f --name selfserve;
serve:
	NODE_ENV=production pm2 start index.js -f --name selfserve;
kill-serve:
	rm -f .live; sleep 10;
	pm2 delete selfserve || echo "WARN: mymyntra was not running on PM2.";
todo:
	npm run todo;
tar:
	rm -rf _nm.tgz && tar -czf _nm.tgz node_modules

.PHONY: tar