# SelfServe web
> The new mymyntra front-end repository.

***

## Getting Started
1. Install dependencies by running the `make setup` command.
2. Fire up a local server by running `make dev`.

## Running on production
1. Compile and push assets to cdn by running `make cdn`.
2. Fire up a production server by running `make prod`.

## The Technology Stack
1. Built using [REACT](http://facebook.github.io/react).
2. Build tasks have been written using [WEBPACK](https://webpack.github.io/).

## Todos
`make todo`
