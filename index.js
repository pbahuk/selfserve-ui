'use strict';
const env = process.env.NODE_ENV;
if (env === 'development') {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  require('babel-register');
  const hook = require('css-modules-require-hook');
  hook({
    generateScopedName: '[name]-[local]',
  });
}

// Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/*NEWRELIC*/
let  newrelic;
if (process.env.NEW_RELIC_ENABLE_MYMYNTRA_REACT) {
  newrelic = require('newrelic');
}

const express = require('express');
const logger = require('morgan');
const path = require('path');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();

if(newrelic){
  app.locals.newrelic = newrelic;
}

let routes;
if (env === 'development') {
  routes = require('./src/routes');
} else {
  routes = require('./dist/routes');
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('compression')());

if (env === 'development') {
  const wconfig = require('./webpack.config.js');
  const compiler = require('webpack')(wconfig);
  app.use(require('webpack-dev-middleware')(compiler,
    { noInfo: true,
      publicPath: wconfig.output.publicPath,
      watchOptions: {
        aggregateTimeout: 300,
        poll: true
      },
      stats: {
        colors: true,
        chunks: false
      }
    }));
  app.use(require('webpack-hot-middleware')(compiler));
}

app.use('/public', express.static(path.join(__dirname, 'public')));
app.set('port', 7710);
app.use('/healthcheck', require('./src/utils/healthcheck'));

routes.run(app);
const server = http.createServer(app);
server.listen(app.get('port'));
server.on('listening', () => console.log('Http server listening on port', app.get('port')));

module.exports = app;
