const ROUTES = {
  ROOT: 'http://mjint.myntra.com/',
  SECURE_ROOT: 'https://mjint.myntra.com/',

  giftcard: {
    root: 'http://giftcard.mjint.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'http://payments.mjint.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://loyalty.mjint.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://referral.mjint.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://coupon.mjint.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  mfg: {
    root: 'devapi.mjint.myntra.com/',
    path: 'mfg/v1'
  },
  crm: {
    root: 'http://crmservicerightnow.mjint.myntra.com/',
    path: 'crmservice-rightnow/crm/task'
  },
  switch: {
    root: 'http://switch.mjint.myntra.com/',
    path: 'config/'
  },

  // Order Realted.
  apify: {
    root: 'http://apify.mjint.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://apify.mjint.myntra.com/',
    path: 'v1/user/'
  },
  user: {
    root: 'http://gateway.mjint.myntra.com/',
    path: 'v1/user/'
  },
  oms: {
    root: 'http://oms.mjint.myntra.com/',
    path: 'myntra-oms-service/'
  },
  gateway: {
    root: 'http://gateway.mjint.myntra.com/',
    path: 'v1/'
  },
  armor: {
    root: 'http://armor.mjint.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://vinyl.mjint.myntra.com/',
    path: 'myntra-oms-service/'
  },
  orchestrator: {
    root: 'http://orchestrator.mjint.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://lms.mjint.myntra.com/',
    path: 'myntra-lms-service/'
  },
  rms: {
    root: 'http://rms.mjint.myntra.com/',
    port: 80,
    path: 'myntra-rms-service/'
  },
  style: {
    root: 'http://style.mjint.myntra.com/',
    path: 'style-service/'
  },
  pps: {
    root: 'pps.mjint.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  css: {
    root: 'http://css.mjint.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  related: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  },
  wishList: {
    root: 'http://devapi.mjint.myntra.com/',
    path: 'collections/me/wishlist/'
  },

  absolut: {
    root: 'http://absolut.mjint.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },

  // Address Related.
  address: {
    root: 'http://address.mjint.myntra.com/',
    path: 'myntra-address-service/'
  },
  patron: {
    root: 'http://serviceability.mjint.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  pincode: {
    root: 'http://mjint.myntra.com/',
    path: 'myntapi/'
  },
  topnav: {
    root: 'http://developer.myntra.com/',
    path: 'config/nav/v0/web/top'
  },
  search: {
    root: 'http://search.mjint.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  userProfiles: {
    root: 'http://usps.mjint.myntra.com/',
    path: 'user/'
  },
  signout: {
    path: 'web/myntapi/idp/auth/signout'
  }
};

export { ROUTES };
