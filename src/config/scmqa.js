const ROUTES = {
  ROOT: 'http://scmqa.myntra.com/',
  SECURE_ROOT: 'https://scmqa.myntra.com/',

  giftcard: {
    root: 'http://giftcard.scmqa.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'http://payments.scmqa.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://loyalty.scmqa.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://referral.scmqa.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://coupon.scmqa.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  mfg: {
    root: 'devapi.scmqa.myntra.com/',
    path: 'mfg/v1'
  },
  crm: {
    root: 'http://crmservicerightnow.scmqa.myntra.com/',
    path: 'crmservice-rightnow/crm/task'
  },
  switch: {
    root: 'http://switch.scmqa.myntra.com/',
    path: 'config/'
  },

  // Order Realted.
  apify: {
    root: 'http://apify.scmqa.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://apify.scmqa.myntra.com/',
    path: 'v1/user/'
  },
  gateway: {
    root: 'http://gateway.scmqa.myntra.com/',
    path: 'v1/'
  },
  user: {
    root: 'http://gateway.scmqa.myntra.com/',
    path: 'v1/user/'
  },
  oms: {
    root: 'http://oms.scmqa.myntra.com/',
    path: 'myntra-oms-service/'
  },
  armor: {
    root: 'http://armor.scmqa.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://vinyl.scmqa.myntra.com/',
    path: 'myntra-oms-service/'
  },
  orchestrator: {
    root: 'http://orchestrator.scmqa.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://lms.scmqa.myntra.com/',
    path: 'myntra-lms-service/'
  },
  rms: {
    root: 'http://rms.scmqa.myntra.com/',
    port: 80,
    path: 'myntra-rms-service/'
  },
  style: {
    root: 'http://style.scmqa.myntra.com/',
    path: 'style-service/'
  },
  pps: {
    root: 'pps.scmqa.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  css: {
    root: 'http://css.scmqa.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  patron: {
    root: 'http://serviceability.scmqa.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  related: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  },
  wishList: {
    root: 'http://devapi.scmqa.myntra.com/',
    path: 'collections/me/wishlist/'
  },
  absolut: {
    root: 'http://absolut.scmqa.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },


  // Address Related.
  address: {
    root: 'http://address.scmqa.myntra.com/',
    path: 'myntra-address-service/'
  },
  pincode: {
    root: 'http://scmqa.myntra.com/',
    path: 'myntapi/'
  },


  topnav: {
    root: 'http://developer.myntra.com/',
    path: 'config/nav/v0/web/top'
  },
  search: {
    root: 'http://search.scmqa.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  userProfiles: {
    root: 'http://usps.scmqa.myntra.com/',
    path: 'user/'
  },
  signout: {
    path: 'web/myntapi/idp/auth/signout'
  }
};

export { ROUTES };
