const ROUTES = {
  ROOT: 'http://stage.myntra.com/',
  SECURE_ROOT: 'https://stage.myntra.com/',

  giftcard: {
    root: 'http://giftcard.stage.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'http://payments.stage.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://loyalty.stage.myntra.com/',
    path: 'lp/loyalty/'
  },
  crm: {
    root: 'http://crmservicerightnow.stage.myntra.com/',
    path: 'crmservice-rightnow/'
  },
  referral: {
    root: 'http://referral.stage.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://coupon.stage.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  mfg: {
    root: 'devapi.stage.myntra.com/',
    path: 'mfg/v1'
  },
  switch: {
    root: 'http://switch.stage.myntra.com/',
    path: 'config/'
  },
  lgp: {
    root: 'http://developer.stage.myntra.com/',
    path: 'lgp/user/'
  },
  seller: {
    root: 'sellerservice.stage.myntra.com/',
    port: 80,
    path: 'myntra-seller-service/'
  },
  user: {
    root: 'http://gateway.stage.myntra.com/',
    path: 'v1/user/'
  },

  // Order Realted.
  apify: {
    root: 'http://apify.stage.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://apify.stage.myntra.com/',
    path: 'v1/user/'
  },
  gateway: {
    root: 'https://gateway.stage.myntra.com/',
    path: 'v1/'
  },
  oms: {
    root: 'http://oms.stage.myntra.com/',
    path: 'myntra-oms-service/'
  },
  armor: {
    root: 'http://armor.stage.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://vinyl.stage.myntra.com/',
    path: 'myntra-oms-service/'
  },
  orchestrator: {
    root: 'http://orchestrator.stage.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://lms.stage.myntra.com/',
    path: 'myntra-lms-service/'
  },
  rms: {
    root: 'http://rms.stage.myntra.com/',
    port: 80,
    path: 'myntra-rms-service/'
  },
  style: {
    root: 'http://style.stage.myntra.com/',
    path: 'style-service/'
  },
  pps: {
    root: 'pps.stage.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  absolut: {
    root: 'http://absolut.stage.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },
  css: {
    root: 'http://css.stage.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  patron: {
    root: 'http://serviceability.stage.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },

  wishList: {
    root: 'http://devapi.stage.myntra.com/',
    path: 'collections/me/wishlist/'
  },

  // Address Related.
  address: {
    root: 'http://address.stage.myntra.com/',
    path: 'myntra-address-service/'
  },
  pincode: {
    root: 'http://stage.myntra.com/',
    path: 'myntapi/'
  },


  topnav: {
    root: 'http://developer.myntra.com/',
    path: 'config/nav/v1/web/top'
  },
  search: {
    root: 'http://force.myntra.com/',
    path: 'force-service/v1/suggest/query'
  },
  payments: {
    root: 'http://payments.stage.myntra.com/',
    path: 'payments/v2/',
    client: 'qa',
    version: '1.0',
    key: 'qa'
  },

  // Order Features.
  userProfiles: {
    root: 'http://usps.stage.myntra.com/',
    path: 'user/'
  },
  ratings: {
    root: 'http://gateway.stage.myntra.com/',
    path: 'v1/ratings'
  },
  signout: {
    path: 'web/myntapi/idp/auth/signout'
  },

  // Style Plus Myntra Insider
  myntrainsider: {
    root: 'http://gateway.stage.myntra.com/',
    path: 'v1/styleplus/'
  },

  // writeReview related
  pdp: {
    root: 'http://api.myntra.com/',
    path: 'product/'
  },
  reviews: {
    root: 'http://gateway.stage.myntra.com/',
    path: 'v1/reviews'
  },

  // recommendations related
  lgpRecommended: {
    root: 'http://devapi.stage.myntra.com/',
    path: 'lgp/v2.9/stream/recommended'
  },
  related: {
    root: 'http://apify.stage.myntra.com/',
    path: 'product/'
  }
};

export { ROUTES };
