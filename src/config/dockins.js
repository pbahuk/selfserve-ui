let ROOT = 'http://fox7.myntra.com/';
let SECURE_ROOT = 'https://fox7.myntra.com/';
try {
  if (process.env.CLUSTER_NAME) {
    ROOT = `http://${process.env.CLUSTER_NAME}-browsehaproxy.dockins.myntra.com/`;
    SECURE_ROOT = `https://${process.env.CLUSTER_NAME}-browsehaproxy.dockins.myntra.com/`;
  }
} catch (ex) {
  console.error(ex);
}

const ROUTES = {
  ROOT,
  SECURE_ROOT,
  giftcard: {
    root: 'http://d7giftcard.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'http://d7payments.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  coupons: {
    root: 'http://d7coupon.myntra.com/',
    path: 'coupons/'
  },
  myntrapoints: {
    root: 'http://d7loyalty.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://d7referral.myntra.com/',
    path: 'rewards/'
  },
  topnav: {
    root: 'http://foxhadeveloper.myntra.com/',
    path: 'config/nav/v1/web/top'
  },
  userProfiles: {
    root: 'http://d7usps.myntra.com/',
    path: 'user/'
  },
  ratings: {
    root: 'http://d7gateway.myntra.com/',
    path: 'v1/ratings'
  },
  mfg: {
    root: 'm7devapi.mpreprod.myntra.com/',
    path: 'mfg/v1'
  },
  switch: {
    root: 'http://d7switch.myntra.com/',
    path: 'config/'
  },
  css: {
    root: 'http://d7css.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  lgp: {
    root: 'http://d7developer.myntra.com/',
    path: 'lgp/user/'
  },
  crm: {
    root: 'http://d7crmservicerightnow.myntra.com/',
    path: 'crmservice-rightnow/'
  },
  patron: {
    root: 'http://d7serviceability.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  orchestrator: {
    root: 'http://d7orchestrator.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://d7lms.myntra.com/',
    path: 'myntra-lms-service/'
  },
  wishList: {
    root: 'http://d7devapi.myntra.com/',
    path: 'collections/me/wishlist/'
  },

  pps: {
    root: 'd7pps.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  seller: {
    root: 'd7sellerservice.myntra.com/',
    port: 80,
    path: 'myntra-seller-service/'
  },
  search: {
    root: 'http://d7searchservice.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  searchdevapi: {
    root: 'http://d7searchdevapi.myntra.com/',
    path: 'search-service/searchservice/search/'
  },

  // Address Related.
  address: {
    root: 'http://d7address.myntra.com/',
    path: 'myntra-address-service/'
  },

  // Order Related.
  apify: {
    root: 'http://d7apify.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://d7apify.myntra.com/',
    path: 'v1/user/'
  },
  gateway: {
    root: 'http://d7gateway.myntra.com/',
    path: 'v1/'
  },
  user: {
    root: 'http://d7gateway.myntra.com/',
    path: 'v1/user/'
  },
  oms: {
    root: 'http://d7oms.myntra.com/',
    path: 'myntra-oms-service/'
  },
  armor: {
    root: 'http://d7armor.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://d7vinyl.myntra.com/',
    path: 'myntra-oms-service/'
  },
  absolut: {
    root: 'http://d7absolut.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },
  payments: {
    root: 'http://d7payments.myntra.com/',
    path: 'payments/v2/',
    client: 'qa',
    version: '1.0',
    key: 'qa'
  },

  // insider api
  myntrainsider: {
    root: 'http://d7gateway.myntra.com/',
    path: 'v1/styleplus/'
  },

  // writeReview related
  pdp: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  },
  reviews: {
    root: 'http://gateway.stage.myntra.com/',
    path: 'v1/reviews'
  },

  // recommendations related
  lgpRecommended: {
    root: 'http://d7developer.myntra.com/',
    path: 'lgp/v2.9/stream/recommended'
  },
  related: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  }

};

export { ROUTES };
