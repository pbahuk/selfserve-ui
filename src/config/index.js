const ENV = process.env.NODE_SUBENV;
const ROUTES = {
  ROOT: 'https://www.myntra.com/',
  SECURE_ROOT: 'https://www.myntra.com/',

  giftcard: {
    root: 'http://giftcard.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'https://www.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://loyaltyprogramservice.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://referral.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://pp-coupon.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  userProfiles: {
    root: 'http://usps.myntra.com/',
    path: 'user/'
  },
  ratings: {
    root: 'http://gateway.myntra.com/',
    path: 'v1/ratings'
  },
  mfg: {
    root: 'http://developer.myntra.com/',
    path: 'mfg/v1'
  },
  switch: {
    root: 'http://switch.myntra.com/',
    path: 'config/'
  },
  lgp: {
    root: 'http://developer.myntra.com/',
    path: 'lgp/user/'
  },

  // payments related
  payments: {
    root: 'http://payments.myntra.com/',
    path: 'payments/v2/',
    client: 'mymyntra',
    version: '1.0',
    key: '5dw4fjz6y'
  },

  // Order Related.
  apify: {
    root: 'http://api.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://api.myntra.com/',
    path: 'v1/user/'
  },
  gateway: {
    root: 'http://gateway.myntra.com/',
    path: 'v1/'
  },
  oms: {
    root: 'http://erpoms.myntra.com/',
    path: 'myntra-oms-service/'
  },
  armor: {
    root: 'http://erparmor.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://vinyl.myntra.com/',
    path: 'myntra-oms-service/'
  },
  orchestrator: {
    root: 'http://erporchestrator.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://erplogistics.myntra.com/',
    path: 'myntra-lms-service/'
  },
  rms: {
    root: 'http://erprms.myntra.com/',
    port: 80,
    path: 'myntra-rms-service/'
  },
  style: {
    root: 'http://styleservice.myntra.com/',
    path: 'style-service/'
  },
  pps: {
    root: 'pps-internal.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  absolut: {
    root: 'http://absolut.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },
  css: {
    root: 'http://serviceability.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  crm: {
    root: 'http://myntra.com/',
    path: 'crmservice-rightnow/'
  },
  patron: {
    root: 'http://patron.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  wishList: {
    root: 'http://developer.myntra.com/',
    path: 'collections/me/wishlist/'
  },

  // Address Related.
  address: {
    root: 'http://address.myntra.com/',
    path: 'myntra-address-service/'
  },
  pincode: {
    root: 'http://www.myntra.com/',
    path: 'myntapi/'
  },
  user: {
    root: 'http://gateway.myntra.com/',
    path: 'v1/user/'
  },

  // Header Related.
  topnav: {
    root: 'http://developer.myntra.com/',
    path: 'config/nav/v1/web/top'
  },
  search: {
    root: 'http://force.myntra.com/',
    path: 'force-service/v1/suggest/query'
  },
  searchdevapi: {
    root: 'http://charles.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  signout: {
    path: 'myntapi/idp/auth/signout'
  },
  instrumentation: {
    debug: false
  },
  myntrainsider: {
    root: 'http://gateway.myntra.com/',
    path: 'v1/styleplus/'
  },

  // writeReview related
  pdp: {
    root: 'http://api.myntra.com/',
    path: 'product/'
  },
  reviews: {
    root: 'http://gateway.myntra.com/',
    path: 'v1/reviews'
  },

  // recommendations related
  lgpRecommended: {
    root: 'http://developer.myntra.com/',
    path: 'lgp/v2.9/stream/recommended'
  },
  related: {
    root: 'http://api.myntra.com/',
    path: 'product/'
  }
};

const CONFIGURATION = {
  serverSideRenderingEnabled: false
};

let config = ROUTES;

function init() {
  try {
    const environment = require(`./${ENV}`).ROUTES;
    if (environment) {
      config = { ...config, ...environment };
    }
  } catch (err) {
    // console.log(ENV, ' environment not found. Picking default Environment', err);
  }
}

init();

export default (route) => {
  const isBrowser = (typeof window !== 'undefined');
  return ((isBrowser ? '/' : (config[route].root || config.ROOT)) + ((isBrowser) ? 'web/' : '') + (config[route].path || ''));
};

export { ROUTES, CONFIGURATION };

export function get(key) {
  return CONFIGURATION[key];
}

export function root() {
  return config.ROOT;
}

export function getRouteConfig(key) {
  return config[key];
}

export function apiConfig(key) {
  return config[key];
}

export function secureRoot() {
  return config.SECURE_ROOT;
}
