const ROUTES = {
  ROOT: 'http://m7.mpreprod.myntra.com/',
  SECURE_ROOT: 'https://secure-m7.mpreprod.myntra.com/',

  giftcard: {
    root: 'http://m7giftcard.mpreprod.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'https://secure-m7.mpreprod.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://m7loyaltyprogramservice.mpreprod.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://m7referral.mpreprod.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://m7coupon.mpreprod.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  mfg: {
    root: 'm7devapi.mpreprod.myntra.com/',
    path: 'mfg/v1'
  },
  switch: {
    root: 'http://m7switch.mpreprod.myntra.com/',
    path: 'config/'
  },
  lgp: {
    root: 'http://m7developer.mpreprod.myntra.com/',
    path: 'lgp/user/'
  },
  seller: {
    root: 'm7sellerservice.mpreprod.myntra.com/',
    port: 80,
    path: 'myntra-seller-service/'
  },
  // Order Related End Points
  apify: {
    root: 'http://m7apify.mpreprod.myntra.com/',
    path: 'user/'
  },
  armor: {
    root: 'http://m7armor.mpreprod.myntra.com/',
    path: 'myntra-armor-service/'
  },
  oms: {
    root: 'http://m7oms.mpreprod.myntra.com/',
    path: 'myntra-oms-service/'
  },
  vinyl: {
    root: 'http://m7vinyl.mpreprod.myntra.com/',
    path: 'myntra-oms-service/'
  },
  pps: {
    root: 'http://m7pps.mpreprod.myntra.com/',
    path: 'myntra-payment-plan-service/v1/'
  },
  css: {
    root: 'http://m7css.mpreprod.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  patron: {
    root: 'http://m7serviceability.mpreprod.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  wishList: {
    root: 'http://d7devapi.myntra.com/',
    path: 'collections/me/wishlist/'
  },

  // Address Related.
  address: {
    root: 'http://address.mpreprod.myntra.com/',
    path: 'myntra-address-service/'
  },
  pincode: {
    root: 'http://m7.mpreprod.myntra.com/',
    path: 'myntapi/'
  },

  // Header Footer related configs.
  topnav: {
    root: 'http://foxhadeveloper.myntra.com/',
    path: 'config/nav/v0/web/top'
  },
  search: {
    root: 'http://m7search.mpreprod.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  signout: {
    path: 'web/myntapi/idp/auth/signout'
  },

  // recommendations related
  lgpRecommended: {
    root: 'http://m7developer.mpreprod.myntra.com/',
    path: 'product/'
  },
  related: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  }
};

export { ROUTES };
