const ROUTES = {
  ROOT: 'http://sfqa.myntra.com/',
  SECURE_ROOT: 'https://sfqa.myntra.com/',

  giftcard: {
    root: 'http://giftcard.sfqa.myntra.com/',
    path: 'giftcard-service/'
  },
  savedcards: {
    root: 'http://payments.sfqa.myntra.com/',
    path: 'payments/paymentInstruments/'
  },
  myntrapoints: {
    root: 'http://loyalty.sfqa.myntra.com/',
    path: 'lp/loyalty/'
  },
  referral: {
    root: 'http://referral.sfqa.myntra.com/',
    path: 'rewards/'
  },
  coupons: {
    root: 'http://coupon.sfqa.myntra.com/',
    port: 80,
    path: 'coupons/'
  },
  mfg: {
    root: 'devapi.sfqa.myntra.com/',
    path: 'mfg/v1'
  },
  crm: {
    root: 'http://crmservicerightnow.sfqa.myntra.com/',
    path: 'crmservice-rightnow/crm/task'
  },
  switch: {
    root: 'http://switch.sfqa.myntra.com/',
    path: 'config/'
  },

  // Order Realted.
  apify: {
    root: 'http://apify.sfqa.myntra.com/',
    path: 'user/'
  },
  apifyV2: {
    root: 'http://apify.sfqa.myntra.com/',
    path: 'v1/user/'
  },
  gateway: {
    root: 'http://gateway.sfqa.myntra.com/',
    path: 'v1/'
  },
  user: {
    root: 'http://gateway.sfqa.myntra.com/',
    path: 'v1/user/'
  },
  oms: {
    root: 'http://oms.sfqa.myntra.com/',
    path: 'myntra-oms-service/'
  },
  armor: {
    root: 'http://armor.sfqa.myntra.com/',
    path: 'myntra-armor-service/'
  },
  vinyl: {
    root: 'http://vinyl.sfqa.myntra.com/',
    path: 'myntra-oms-service/'
  },
  orchestrator: {
    root: 'http://orchestrator.sfqa.myntra.com/',
    path: 'myntra-orchestrator-service'
  },
  lms: {
    root: 'http://lms.sfqa.myntra.com/',
    path: 'myntra-lms-service/'
  },
  rms: {
    root: 'http://rms.sfqa.myntra.com/',
    port: 80,
    path: 'myntra-rms-service/'
  },
  style: {
    root: 'http://style.sfqa.myntra.com/',
    path: 'style-service/'
  },
  pps: {
    root: 'pps.sfqa.myntra.com',
    path: '/myntra-payment-plan-service/v1/'
  },
  css: {
    root: 'http://css.sfqa.myntra.com/',
    port: 80,
    path: 'myntra-css-service/'
  },
  patron: {
    root: 'http://serviceability.sfqa.myntra.com/',
    port: 80,
    path: 'serviceability-service/'
  },
  related: {
    root: 'http://d7apify.myntra.com/',
    path: 'product/'
  },
  wishList: {
    root: 'http://devapi.sfqa.myntra.com/',
    path: 'collections/me/wishlist/'
  },
  absolut: {
    root: 'http://absolut.sfqa.myntra.com/',
    path: 'myntra-absolut-service/absolut/'
  },


  // Address Related.
  address: {
    root: 'http://address.sfqa.myntra.com/',
    path: 'myntra-address-service/'
  },
  pincode: {
    root: 'http://sfqa.myntra.com/',
    path: 'myntapi/'
  },


  topnav: {
    root: 'http://developer.myntra.com/',
    path: 'config/nav/v0/web/top'
  },
  search: {
    root: 'http://search.sfqa.myntra.com/',
    path: 'search-service/searchservice/search/'
  },
  userProfiles: {
    root: 'http://usps.sfqa.myntra.com/',
    path: 'user/'
  },
  signout: {
    path: 'web/myntapi/idp/auth/signout'
  }
};

export { ROUTES };
