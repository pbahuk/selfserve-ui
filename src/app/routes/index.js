import React from 'react';
import { Route, browserHistory } from 'react-router';
import Application from './../components/Application';
import Share from './../components/Share';
import OrderList from './../components/OrderList';
import OrderDetails from './../components/OrderDetails';
import ItemDetails from './../components/ItemDetails';
import Cancellation from './../components/Cancellation';
import PackageTracking from './../components/PackageTracking';
import Return from './../components/Return';
import BankAccountEdit from './../components/BankAccountEdit';
import Exchange from './../components/Exchange';
import Ratings from './../components/Ratings';
import WriteReview from './../components/WriteReview';

const routes = (
  <Route path="/" component={Application} history={browserHistory}>
    <Route path="/my/share" component={Share} />
    <Route path="/my/orders" component={OrderList} />
    <Route path="/my/order/details" component={OrderDetails} />
    <Route path="/my/archived/orders" component={OrderList} />
    <Route path="/my/archived/order/details" component={OrderDetails} />
    <Route path="/my/archived/item/details" component={ItemDetails} />
    <Route path="/my/item/details" component={ItemDetails} />
    <Route path="/my/cancel" component={Cancellation} />
    <Route path="/my/package/tracking" component={PackageTracking} />
    <Route path="/my/return" component={Return} />
    <Route path="/my/bankaccount/edit" component={BankAccountEdit} />
    <Route path="/my/exchange" component={Exchange} />
    <Route path="/my/ratings" component={Ratings} />
    <Route path="/my/writeReview/:styleid" component={WriteReview} />
    <Route path="/my/writeReview/:styleid/:reviewid" component={WriteReview} />
  </Route>
);

export default routes;
