const Uri = require('jsuri');
import at from 'v-at';
import Client from '../../utils/Client';
import config from '../../config';

export function isBrowser() {
  return typeof window !== 'undefined';
}

export function isMobile() {
  if (isBrowser()) {
    return isBrowser() && (at(window, '__myx_deviceType__') === 'mobile' || at(window, '__myx_deviceData__.isMobile'));
  } return false;
}

export function isIEBrowser() {
  const ua = window.navigator.userAgent;
  const msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    return true;
  }
  const trident = ua.indexOf('Trident/');
  if (trident > 0) {
    return true;
  }
  const edge = ua.indexOf('Edge/');
  if (edge > 0) {
    return true;
  }
  return false;
}

export function getQueryParameter(param) {
    const url = window.location.href;
    let name = param.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

export function fetchTopNav(key, callback) {
  Client.get(config(key)).then((res) => {
    if(res && res.body && res.body.data){
      callback(null, res.body.data);
    } else {
      callback("could not fetch navData", null);
    }
  }, (err) => {
    callback(err, null);
  });
}

export const vendorAuth = `
    <script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '182424375109898',
          xfbml      : true,
          version    : 'v2.5'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
`;

export function sanitize(url){
    if (url) { // undefined check needed as decodeURIComponent was sending it as string.
        // list of domains in regex. please note add $ at end each white listed domain. ( to avoid myntra.com.hacksite.com pattern)
        const whitelistedDomains = [
            /.*\.myntra\.com$/,
            /.*\.flipkart\.com$/
        ];

        const urlObj = new Uri(url);
        if (!urlObj.host()) {
          return new Uri().setPath(urlObj.path()).setQuery(urlObj.query()).toString(); 
        }
        for (let i = 0; i < whitelistedDomains.length; i++) {
            // if true the pass entire url
            if (whitelistedDomains[i].test(urlObj.host())) {
                return urlObj.toString();
            }
        }
    }
    return "/";
}

export function isApp(req) {
  if (req) {
    const deviceChannel = at(req, 'myx.deviceData.deviceChannel') || '';
    return deviceChannel.toLowerCase() === 'mobile_app';
  }
  return false;
}