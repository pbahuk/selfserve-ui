import lscache from 'lscache';
import bus from 'bus';

const mock = (str) => {
	// creates a fake response object from a given string
  const bodyData = typeof str === 'string' ? JSON.parse(str) : str;
  const res = {
    body: bodyData,
    _lscache: true
  };
  return res;
};

// expiry in minutes, optional
const cache = (fn, prefix, expiry) =>
  (query, callback) => {
    const cached = lscache.get(`${prefix || 'myx'}:${query}`);
    if (cached) {
      bus.emit('lscachify.hit', query);
      callback && callback(null, mock(cached));
    } else {
      bus.emit('lscachify.miss', query);
      return fn(query, (err, res) => {
        if (err) {
          return callback(err, res);
        }
        try {
          lscache.set(`${prefix || 'myx'}:${query}`, JSON.stringify(res.body), expiry || 2); // 2 minutes sound about right
        } catch (_e_) {
          console.error(_e_);
          // probable overflow, better clear the cache.
          bus.emit('lscachify.clear');
          window.localStorage.clear();
        }
        callback && callback(err, res);
        return false;
      });
    }
    return false;
  };

export default cache;
