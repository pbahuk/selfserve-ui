function isLocalStorageEnabled() {
  const testItem = 'test';
  try {
    if(!window.localStorage) {
      return false;
    }
    window.localStorage.setItem(testItem, {});
    window.localStorage.removeItem(testItem);
    return true;
  } catch (e) {
    return false;
  }
}

function setItem(key, value) {
  if (isLocalStorageEnabled()) {
    try {
      window.localStorage.setItem(key, value);
    } catch (e) {

    }
  }
}


function getItem(key) {
  if (isLocalStorageEnabled()) {
    try {
      return window.localStorage.getItem(key);
    } catch (e) {
      return null;
    }
  }
}

function removeItem(key) {
  if (isLocalStorageEnabled()) {
    try {
      window.localStorage.removeItem(key);
    } catch (e) {

    }
  }
}

export { isLocalStorageEnabled, setItem, getItem, removeItem };
