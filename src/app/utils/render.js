
import React from 'react';
import ReactDOMServer from 'react-dom/server';
const assets = require('../../../webpack-assets.json');
import assetify from '../../utils/assetify';
import { pump } from '../../utils/pump';
import { match, RouterContext } from 'react-router';
import routes from '../routes';
import _ from 'lodash';
import at from 'v-at';
import serialize from 'serialize-javascript';
import {getRouteConfig} from '../../config';
import { headerRender } from '../components/Header/server';
import { footerRender } from '../components/Footer/server';

const serveSideRendering = require('../../config').get('serverSideRenderingEnabled');
const instrumentEnv = getRouteConfig('instrumentation');

function renderHtml({ commonCssBundle, reactOutput, commonJsBundle, props, title, myx, header, footer, outline , raw , seo, isMobile, topnav, fontStyle}) {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>${title || 'Myntra'}</title>
        ${fontStyle}
        <link rel="stylesheet" href="${commonCssBundle}"/>
        <link rel="shortcut icon" href="https://cdn.myntassets.com/skin1/icons/favicon.ico">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
        <script>
          !function(){try{navigator.serviceWorker&&"function"==typeof navigator.serviceWorker.getRegistrations&&navigator.userAgent&&/MyntraRetailAndroid|MyntraRetailiPhone/g.test(navigator.userAgent)&&navigator.serviceWorker.getRegistrations().then(function(e){Array.from(e).forEach(function(e){e.unregister().then(function(){window.location&&window.location.reload()}).catch(function(e){console.error("service worker unregister failed",e)})})}).catch(function(e){console.error("service worker get registrations failed",e)})}catch(e){console.log("Exception in handling service worker ",e)}}();
        </script>
        
        <!-- Google Analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-1752831-18', 'auto');  // Replace with your property ID.
          ga('send', 'pageview');
          ga('require', 'ec');
        </script>
        <!-- End Google Analytics -->
      </head>
      <body>
        ${header}
        ${outline.pump()}
        <div id="mountRoot" class="${myx.deviceData.isMobile ? 'mobile': ''}" style = "min-height:1000px;${isMobile ? '' : 'margin-top:-2px;'}">${reactOutput}</div>
        ${raw}
        <div id="web-footerMount">
           ${footer}
        </div>
        <script>${props}</script>
        <script>
          window.__myx_seo__ = ${serialize(at(seo, 'footerData'))};
          window.__myx_navigationData__ = ${serialize(topnav)};
          window.__myx_instrumentation_env = ${serialize(instrumentEnv)}
        </script>
        <script src="${commonJsBundle}"></script>
      </body>
    </html>`;
}

export function getProtocol(req) {
  const hdr = req.get('http_x_forwarded_proto') || req.get('x-forwarded-proto');
  return hdr;
}

function dehydrateStateForBrowser(props) {
  if (!_.isNil(props)) {
    return props[_.head(Object.keys(props))];
  }
  return {};
}

function getFontStyles(protocol) {
  let prefix = '';
  if (protocol !== 'https') {
    prefix = 'http://myntra.myntassets.com';
  }

  return `<style>
    @font-face {
    font-family: 'Whitney';
    src: url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-Book.eot');
    src: url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-Book.eot?#iefix') format('embedded-opentype'),
      url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-Book.woff') format('woff'),
      url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-Book.ttf') format('truetype');
    font-weight: 400;
    font-style: normal;
  }

    @font-face {
    font-family: 'Whitney';
    src: url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-SemiBold.eot');
    src: url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-SemiBold.eot?#iefix') format('embedded-opentype'),
      url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-SemiBold.woff') format('woff'),
      url('https://cdn.myntassets.com/skin2/fonts/WhitneyHTF-SemiBold.ttf') format('truetype');
    font-weight: 500;
    font-style: normal;
  }

    @font-face {
    font-family: 'icomynt';
    src: url('https://cdn.myntassets.com/skin2/fonts/core_v3/icomynt_v1.woff') format('woff'),
      url('https://cdn.myntassets.com/skin2/fonts/core_v3/icomynt_v1.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  </style>`;
}

function render(req, res, props, title, outline) {
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    renderProps.location.dehydratedState = dehydrateStateForBrowser(props);
    const reactOutput = serveSideRendering ? ReactDOMServer.renderToString(<RouterContext {...renderProps} />) : '';
    const commonJsBundle = assetify(assets.main.js, getProtocol(req));
    const commonCssBundle = assetify(assets.main.css, getProtocol(req));
    const fontStyle = getFontStyles(getProtocol(req));

    const header = new Promise((resolve, reject) => {
      headerRender(req, (data) => {
        if (data) {
          resolve(data);
        }
        reject('Invalid Header HTML');
      });
    });
    header.then((data) => { // data : { navData: data, header: headerStr }
      return (data);
      const navData = at(data, 'navData.children');
      if (navData && !at(req, 'myx.deviceData.isMobile')) {
        const groups = navData.map((val) => ({ name: at(val, 'props.title'), linkUrl: at(val, 'props.url') }));
        footerRender(req, groups, (footerHtml) => {
          data.footer = footerHtml; // data : { navData: data, header: headerStr, footer : footerHtml }
          console.log('footerHtml ', footerHtml);
          return (data);
        });
      } else {
        footerRender(req, null, (footerHtml) => {
          data.footer = footerHtml; // data : { navData: data, header: headerStr, footer : footerHtml }
          console.log('footerHtml ', footerHtml);
          return (data);
        });
      }
    }, (reason) => {
      console.error('Reason ::::::::::::', reason);
    }).catch((err) => {
      console.error('ERROR  : ', err);
    });

    header.then((completeObj) => {
      res.send(renderHtml({
        commonCssBundle,
        reactOutput,
        commonJsBundle,
        props: `window.__myx = ${serialize(props || {})}`,
        title: title,
        fontStyle,
        myx: req.myx || {},
        header: completeObj.header,
        footer: completeObj.footer,
        outline: outline,
        raw: pump(req),
        seo: req.seo || {},
        isMobile: req.isMobile,
        topnav: completeObj.navData
      }));
    });
  });
}

export default render;
