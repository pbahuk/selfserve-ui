import Client from '../../../utils/Client';

const getCrossSellDetails = (productId, callback) => {
  Client.get(`/my/api/crossSellApi/fetchProducts/${productId}`)
    .end((err, res) => {
      if (err) {
        console.error('Error [Cross Sell get Data]:', err);
      }
      callback(err, res);
    });
};

export { getCrossSellDetails };