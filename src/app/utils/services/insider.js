import Client from '../../../utils/Client';
import { getUidx } from '../pageHelpers';

const getInsiderPoints = (callback) => {
  const uidx = getUidx();
  const headers = { 'm-uidx': uidx };
  Client.get('/my/api/insiderApi/fetchOrders', headers)
    .end((err, res) => {
      if (err) {
        console.error('Error [Insider Api get Data]:', err);
      }
      callback(err, res);
    });
}

const claimInsiderPoints = (itemId, callback) => {
  const uidx = getUidx();
  const headers = { 'm-uidx': uidx };
  Client.post(`/my/api/insiderApi/claimInsiderPoints/${itemId}`, {}, headers)
    .end((err, res) => {
      if (err) {
        console.error('Error [Insider Api get Data]:', err);
      }
      callback(err, res);
    });
}
export { getInsiderPoints, claimInsiderPoints };