import Client from '../../../utils/Client';


const getArnData = (ppsId, callback) => {
  Client.post('/my/api/payments/transaction', { ppsId })
  .end((err, res) => {
    if (err) {
      console.error('Error [Payments ArnDispaly Actions]:', err);
    }
    callback(err, res);
  });
};

const getAccDetails = ( neftId, callback) => {
    Client.post('/my/api/payments/getAccount', { neftId })
    .end((err, res) => {
      if (err) {
        console.error('Error [Payments getAccount Actions]:', err);
      }
      callback(err, res);
    });
};

const updateBankAccountDetail = (payload, callback) => {
  Client.post('/my/api/payments/updateBankAccountDetail', payload)
  .end((err, res) => {
    if (err) {
      console.error('Error [Payment Actions]:', err);
    }
    callback(err, res);
  });
}


export {
  getArnData,
  getAccDetails,
  updateBankAccountDetail
};