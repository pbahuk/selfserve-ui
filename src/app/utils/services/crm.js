import Client from '../../../utils/Client';
import { getUserToken, getUidx } from '../pageHelpers';
import { apiConfig } from '../../../config';

const getBankDetails = (callback) => {
  Client.get('/my/api/crmApi/getBankDetails')
    .end((err, res) => {
      if (err) {
        console.error('Error [CRM Actions]:', err);
      }
      callback(err, res);
    });
};

const getIfscDetails = (ifscCode,callback) => {
  Client.get(`/my/api/crmApi/getIfscDetails?ifsccode=${ifscCode}`,{},{ timeout: 2000})
    .end((err, res) => {
      if (err) {
        console.error('Error [CRM Actions]:', err);
      }
      callback(err, res);
    });
};

const postBankAccountDetail = (payload, callback) => {
  payload.USER_TOKEN = getUserToken();
  Client.post('/my/api/crmApi/postBankAccountDetail', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [CRM Actions]:', err);
      }
      callback(err, res);
    });
}

// commenting for future usage
/* const getAccDetails = (query, callback) => {
  const user = getUidx();
  const xMyntCtx = `storeid=2297;uidx=${user}`;
  const url = `${apiConfig('payments').root}${apiConfig('payments').path}bankAccount`;
  const headers = { 
    'x-mynt-ctx': xMyntCtx,
    client: apiConfig('payments').client,
    version: apiConfig('payments').version,
    'Access-Control-Allow-Origin': '*'
  };
  Client.get(url, headers)
  .query(query)
  .end((err, res) => {
    if(err) {
      console.error('Error [CRM Actions getAccDetails]:', err);
    }
    callback(err, res);
  });
} */


export { getBankDetails, postBankAccountDetail, getIfscDetails};
