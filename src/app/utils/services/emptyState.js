import Client from '../../../utils/Client';

const getEmptyStateProducts = (productId, callback) => {
  Client.get(`/my/api/emptyStateApi/fetchProducts/${productId}`)
    .end((err, res) => {
      if (err) {
        console.error('Error [Empty State get Data]:', err);
      }
      callback(err, res);
    });
};

export { getEmptyStateProducts };