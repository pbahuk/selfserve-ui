/***
 * Function of Service files: To make the API call to the server.
 * And to attach the CSRF token validated for every request.
 * Add the USER_TOKEN at the topmost level, will be deleted before passing on to the
 * actual services.
 */

import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const getOrders = (payload, callback) => {
  let url = '/my/api/ordersApi/getOrders';
  if(payload.archived) {
    url = '/my/api/ordersApi/getArchivedOrders';
  }
  delete payload.archived;
  Client
    .post(url, payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions]:', err);
      }
      callback(err, res);
    });
};

function getPackageDetails(packageId, callback) {
  Client.get(`/my/api/ordersApi/packageDetails?packageId=${packageId}`)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions]:', err);
      }
      callback(err, res);
    });
}

/***
 * POST Calls Segment, User Actions.
 */
const cancelOrder = (postData, callback) => {
  postData.USER_TOKEN = getUserToken();
  Client.post('/my/api/ordersApi/cancelOrder', postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions, Cancel Order]:', err);
      }
      callback(err, res);
    });
};

const cancelItems = (postData, callback) => {
  postData.USER_TOKEN = getUserToken();
  Client.post('/my/api/ordersApi/cancelItems', postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions, Cancel Order]:', err);
      }
      callback && callback(err, res);
    });
};

const cancelPacket = (postData, callback) => {
  Client.put('/my/api/ordersApi/cancelPacket', postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions, Cancel Packet]:', err);
      }
      callback(err, res);
    })
}

function markDelivered(payload, callback) {
  payload.USER_TOKEN = getUserToken();
  Client.post('/my/api/ordersApi/markDelivered', payload).end((err, resp) => {
    if (err) {
      console.error('Error [OMS Actions]:', err);
    } else {
      callback(resp);
    }
  });
}

function updateOrderAddress(postData, callback) {
  postData.USER_TOKEN = getUserToken();
  Client
    .post('/my/api/ordersApi/orderrelease/changeAddress', postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions]:', err);
      }
      callback(err, res);
    });
}

function gatewayUpdateOrderAddress(postData, callback) {
  postData.USER_TOKEN = getUserToken();
  Client
    .post('/my/api/ordersApi/orderrelease/changeAddress?gateway=true', postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [OMS Actions]:', err);
      }
      callback(err, res);
    });
}

export { getOrders, cancelOrder, cancelItems, cancelPacket, markDelivered, getPackageDetails, updateOrderAddress, gatewayUpdateOrderAddress };
