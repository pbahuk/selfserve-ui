import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const setRatings = (payload, callback) => {
  payload.USER_TOKEN = getUserToken();
  Client.post('/my/api/ratingsApi', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Ratings Actions]:', err);
      }
      callback(err, res);
    })
};


const getRatings = (payload, callback) => {
  Client.post('/my/api/ratingsApi/getRatings', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Ratings Actions]:', err);
      }
      callback(err, res);
    })
};

export { setRatings, getRatings };
