import Client from '../../../utils/Client';
import { getUidx, getUserToken } from '../pageHelpers';

const addToWishListOfUser = (productId, callback) => {
  const payload = { Uidx: getUidx(), USER_TOKEN: getUserToken() };
  Client.post(`/my/api/wishListApi/add/${productId}`, payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Add to Wishlist Post Data]:', err);
      }
      callback(err, res);
    });
};

const removeFromWishListOfUser = (productId, callback) => {
  const payload = { Uidx: getUidx(), USER_TOKEN: getUserToken() };
  Client.post(`/my/api/wishListApi/remove/${productId}`, payload)
      .end((err, res) => {
        if (err) {
          console.error('Error [remove from Wishlist Post Data]:', err);
        }
        callback(err, res);
      });
  };
  
const getWishtListForUser = (callback) => {
  const payload = { Uidx: getUidx() };
    Client.get('/my/api/wishListApi/summary/', payload)
      .end((err, res) => {
          if (err) {
            console.log('Error [get summary data of wishlist]:', err);
          }
          callback(err, res);
      })
}
export { addToWishListOfUser, removeFromWishListOfUser, getWishtListForUser };