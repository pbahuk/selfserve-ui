import Client from '../../../utils/Client';
import at from 'v-at';

const getSavedCards = (callback) => {
  Client.get('/my/api/savedcardsApi/getCards')
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions] get Saved Cards:', err);
      }
      callback(err, res);
    });
};

const deleteSavedCard = (postData, callback) => {
  const dataToSend = 'id='+ encodeURIComponent(postData.id) +'&csrf_token='+ encodeURIComponent(postData.csrf_token);

  Client.post('/my/api/savedcardsApi/deleteCard')
    .send(postData)
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions] delete Card:', err);
      }
      callback(err, res);
    });
}

const addCard = (data, callback) => {
  Client.post('/my/api/savedcardsApi/addCard')
    .send(data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions] add Card:', err);
      }
      callback(err, res);
    });
};

const editCard = (data, callback) => {
  Client.post('/my/api/savedcardsApi/editCard', data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions]:', err);
      }
      callback(err, res);
    });
};

export { getSavedCards, deleteSavedCard, addCard, editCard };
