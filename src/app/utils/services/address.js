/***
 * Function of Service files: To make the API call to the server.
 * And to attach the CSRF token validated for every request.
 */

import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const addAddress = (address, callback) => {
  address.USER_TOKEN = getUserToken();
  Client.post('/my/api/addressApi/addAddress', address)
    .end((err, res) => {
      if (err) {
        console.error('Error [Address Actions]:', err);
      }
      callback(err, res);
    });
};

const updateAddress = (address, callback) => {
  address.USER_TOKEN = getUserToken();
  Client.put('/my/api/addressApi/updateAddress', address)
    .end((err, res) => {
      if (err) {
        console.error('Error [Address Actions]:', err);
      }
      callback(err, res);
    });
};

const removeAddress = (addressId, callback) => {
  const payload = { addressId, USER_TOKEN: getUserToken() };
  Client.post('/my/api/addressApi/deleteAddress/', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Address Actions]:', err);
      }
      callback(err, res);
    });
};

const getAllAddress = (callback) => {
  Client.get('/my/api/addressApi/getAllAddress')
    .end((err, res) => {
      if (err) {
        console.error('Error [Address Actions]:', err);
      }
      callback(err, res);
    });
};

/***
 * Gateway: Address APIs, keeping the old functions to manage the Address, Order Details.
 * Both will have to present to be backward compatible.
 * Gateway routes nomenclature: /{Old my api route}?gateway=true
 */

 const gatewayGetAllAddress = (callback) => {
   Client.get(`/my/api/addressApi/getAllAddress?gateway=true`)
    .end((err, res) => {
      if (err) {
        console.error('Error [Gateway Address Actions]:', err);
      }
      callback(err, res);
    });
 }

const fetchPincodeDetails = (pincode, callback) => {
  Client.get(`/my/api/addressApi/fetchPincodeDetails/${pincode}`)
    .end((err, res) => {
      if (err) {
        console.error('Error [Address Actions]:', err);
      }
      callback(err, res);
    });
};

export { addAddress, updateAddress, removeAddress, getAllAddress, fetchPincodeDetails, gatewayGetAllAddress };
