import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const getGiftCardData = (cardType, offset, limit, callback) => {
  Client.post('/my/api/giftcardApi/getData', {
    type: cardType,
    offset,
    limit
  }).end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions]:', err);
      }
      callback(err, res);
    });
};

const addCardToAccount = (data, callback) => {
  // Attaching the user token to the req body.
  data.USER_TOKEN = getUserToken();
  Client.post('/my/api/giftcardApi/addCard', data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions]:', err);
      }
      callback(err, res);
    });
};

const getTransactionDetails = (data, callback) => {
  Client.post('/my/api/giftcardApi/getTransactionDetails', data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Giftcard Actions]:', err);
      }
      callback(err, res);
    });
};


const showMoreTopUpTransactions = (data, callback) => {
  Client.post('/my/api/giftcardApi/showMoreTransactions', data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Transaction Details]:', err);
      }
      callback(err, res);
    });
}

const resendEmail = (data, callback) => {
  data.USER_TOKEN = getUserToken();
  Client.post('/my/api/giftcardApi/resendGiftCardEmail', data)
    .end((err, res) => {
      if (err) {
        console.error('Error [Resend GiftCard Email]:', err);
      }
      callback(err, res);
    });
}

const getBalance = (callback) => {
  Client.get('/my/api/giftcardApi/getGCBalance')
    .end((err, res) => {
      if (err) {
        console.error('Error [Resend GiftCard getBalance]:', err);
      }
      callback(err, res);
    });
}

export { getGiftCardData, addCardToAccount, getTransactionDetails, showMoreTopUpTransactions, resendEmail, getBalance };

