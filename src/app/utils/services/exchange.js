import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const createExchange = (payload, callback) => {
  payload.USER_TOKEN = getUserToken();
  Client.post('/my/api/exchangeApi/createExchange/', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Exchange Actions]:', err);
      }
      callback(err, res);
    });
}

export { createExchange };
