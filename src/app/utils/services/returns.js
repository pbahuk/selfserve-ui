import Client from '../../../utils/Client';
import { getUserToken } from '../pageHelpers';

const createReturn = (payload, callback) => {
  payload.USER_TOKEN = getUserToken();
  Client.post('/my/api/returnsApi/createReturn/', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Returns Actions]:', err);
      }
      callback(err, res);
    });
}

export { createReturn };
