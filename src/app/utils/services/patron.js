import Client from '../../../utils/Client';
import at from 'v-at';

import { getPaymentMode } from '../pageHelpers';

const checkDeliveryServiceability = (data, callback) => {
  const payload = serviceabilityReqFormat(data, 'DELIVERY');
  Client.post('/my/api/patronApi/checkServiceability', payload)
  .end((err, res) => {
    if (err) {
      console.error('Error [Serviceability Actions]:', err);
    }
    callback(err, res);
  });
};

const checkServiceability = (data, callback) => {
  const payload = serviceabilityReqFormat(data);
  Client.post('/my/api/patronApi/checkServiceability', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Serviceability Actions]:', err);
      }
      callback(err, res);
    });
};

const checkExchangeServiceability = (data, callback) => {
  const payload = exchangeServiceabilityFormat(data);
  Client.post('/my/api/patronApi/checkServiceability', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Serviceability Actions]:', err);
      }
      callback(err, res);
    });
};


const checkReturnServiceability = (data, callback) => {
  const payload = returnServiceabilityReqFormat(data, 'RETURN');

  Client.post('/my/api/patronApi/checkServiceability', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [Return Serviceability Actions]:', err);
      }
      callback(err, res);
    });
}

const returnServiceabilityReqFormat = (data, serviceabilityType) => {
  const items = at(data, 'items') || []; 

  let itemsPayload = [];

  items.forEach((item) => {
    const dispatchWarehouseId = at(item, 'dispatchWarehouseId') || '';
    const eachItemPayload = {
      itemTypes: {
        itemType: [
          {
            dataType: "NUMBER",
            name: "codValue",
            value: at(item, 'payments.instruments.cod') / 100 || 0
          },
          {
            dataType: "NUMBER",
            name: "itemMrpValue",
            value: at(item, 'payments.amount') /100 || 0
          },
          {
            dataType: "BOOLEAN",
            name: "isHazmat",
            value: at(item, 'flags.isHazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isFragile",
            value: at(item, 'product.attributes.hazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isJewellery",
            value: at(item, 'product.attributes.jewellery') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isPersonalized",
            value: false
          },
          {
            dataType: "BOOLEAN",
            name: "isLarge",
            value: at(item, 'product.attributes.large') || false
          },
          {
            dataType: "BOOLEAN",
            name: "pickupEnabled",
            value: at(item, 'product.attributes.pickupEnabled') || false
          }
        ]
      },
      skuId: at(item, 'product.skuId'),
      leadTime: "0",
      warehouses: {
        warehouseId: Array.isArray(dispatchWarehouseId) ? dispatchWarehouseId : [dispatchWarehouseId]
      }
    };
    itemsPayload.push(eachItemPayload);
  });

  return {
    CourierServiceabilityRequest: {
      data: {
        order: {
          capacityCheckRequired: true,
          expressCapacityCheckRequired: true,
          fetchCouriers: true,
          items: {
            item: itemsPayload
          },
          paymentMode: 'on',
          pincode: data.pincode,
          serviceType: serviceabilityType || 'DELIVERY',
          shippingMethod: serviceabilityType === 'DELIVERY' ? 'NORMAL' : 'ALL'
        }
      }
    }
  };
}

const serviceabilityReqFormat = (data, serviceabilityType) => {
  const items = at(data, 'items') || [];
  const order = at(data, 'order') || {};
  let paymentInstruments = getPaymentMode(at(order, 'payments'));
  let isCOD = false;

  paymentInstruments.forEach((instrument) => {
    if (instrument.toLowerCase() === 'cod') {
      isCOD = true;
    }
  });

  let itemsPayload = [];

  items.forEach((item) => {
    const sizes = at(item, 'product.sizes') || [];
    const sizeOrdered = sizes.find((size) => size.skuId === at(item, 'product.skuId'));
    const warehouseIds = at(sizeOrdered, 'warehouses') || [];

    const eachItemPayload = {
      itemTypes: {
        itemType: [
          {
            dataType: "NUMBER",
            name: "codValue",
            value: `${at(item, 'payments.instruments.cod') / 100}` || '0'
          },
          {
            dataType: "NUMBER",
            name: "itemMrpValue",
            value: `${at(item, 'payments.amount') / 100}` || '0'
          },
          {
            dataType: "BOOLEAN",
            name: "isHazmat",
            value: at(item, 'flags.isHazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isFragile",
            value: at(item, 'product.attributes.hazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isJewellery",
            value: at(item, 'product.attributes.jewellery') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isPersonalized",
            value: false
          },
          {
            dataType: "BOOLEAN",
            name: "isLarge",
            value: at(item, 'product.attributes.large') || false
          },
          {
            dataType: "BOOLEAN",
            name: "pickupEnabled",
            value: at(item, 'product.attributes.pickupEnabled') || false
          }
        ]
      },
      skuId: `${at(item, 'product.skuId')}`,
      leadTime: "0",
      warehouses: {
        warehouseId: Array.isArray(warehouseIds) ? warehouseIds : [warehouseIds]
      }
    };
    itemsPayload.push(eachItemPayload);
  });

  let paymentMode;

  if (serviceabilityType === 'DELIVERY') {
    paymentMode = isCOD ? 'cod' : 'ALL';
  } else {
    paymentMode = 'on';
  }

  return {
    CourierServiceabilityRequest: {
      data: {
        order: {
          capacityCheckRequired: true,
          expressCapacityCheckRequired: true,
          fetchCouriers: true,
          items: {
            item: itemsPayload
          },
          paymentMode,
          pincode: data.pincode,
          serviceType: data.serviceType || 'DELIVERY',
          shippingMethod: serviceabilityType === 'DELIVERY' ? 'NORMAL' : 'ALL'
        }
      }
    }
  };
};


const exchangeServiceabilityFormat = (data) => {
  const items = at(data, 'items') || [];
  const { size = {} } = data;
  const warehouses = at(size, 'warehouses') || []

  let itemsPayload = [];

  items.forEach((item) => {
    const eachItemPayload = {
      orderReleaseId: at(item, 'orderReleaseId'),
      itemTypes: {
        itemType: [
          {
            dataType: "NUMBER",
            name: "codValue",
            value: at(item, 'payments.instruments.cod') / 100 || 0
          },
          {
            dataType: "NUMBER",
            name: "itemMrpValue",
            value: at(item, 'payments.amount') /100 || 0
          },
          {
            dataType: "BOOLEAN",
            name: "isHazmat",
            value: at(item, 'flags.isHazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isFragile",
            value: at(item, 'product.attributes.hazmat') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isJewellery",
            value: at(item, 'product.attributes.jewellery') || false
          },
          {
            dataType: "BOOLEAN",
            name: "isPersonalized",
            value: false
          },
          {
            dataType: "BOOLEAN",
            name: "isLarge",
            value: at(item, 'product.attributes.large') || false
          },
          {
            dataType: "BOOLEAN",
            name: "pickupEnabled",
            value: at(item, 'product.attributes.pickupEnabled') || false
          }
        ]
      },
      skuId: at(size, 'skuId'),
      leadTime: "0",
      warehouses: {
        warehouseId: Array.isArray(warehouses) ? warehouses : [warehouses]
      }
    };
    itemsPayload.push(eachItemPayload);
  });

  return {
    CourierServiceabilityRequest: {
      data: {
        order: {
          capacityCheckRequired: true,
          expressCapacityCheckRequired: true,
          fetchCouriers: true,
          items: {
            item: itemsPayload
          },
          paymentMode: 'on',
          pincode: data.pincode,
          serviceType: 'EXCHANGE',
          shippingMethod: 'ALL'
        }
      }
    }
  };
};

export {
  checkServiceability,
  checkDeliveryServiceability,
  checkReturnServiceability,
  checkExchangeServiceability
};
