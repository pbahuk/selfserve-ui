import Client from '../../../utils/Client';

function deleteProfile({profileId}, callback) {
  Client.post('/my/api/userprofileapi/deleteProfile', {
      profileId
     })
    .end((err, res) => {
      if (err) {
        console.error('Error in deleting profile: ', err);
        return;
      }
      callback(err, res);
    });
};

function updateProfile(payload, callback) {
  Client.put('/my/api/userprofileapi/updateProfile', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error in updating profile: ', err);
        return;
      }
      callback(err, res);
    });

}

function addProfile(payload, callback) {
  Client.post('/my/api/userprofileapi/saveProfile', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error in adding profile: ', err);
        return;
      }
      callback(err, res);
    });
}

function tagProfile(payload, callback) {
  Client.post('/my/api/userprofileapi/addOrderToProfile', payload)
  .end((err, res) => {
    if (err) {
      console.error('Error in tagging profile: ', err);
      return;
    }
    callback(err, res);
  });
}

function getProfileForProduct(payload, callback) {
  Client.post('/my/api/userprofileapi/getSizeProfileOrder', payload)
  .end((err, res) => {
    if (err) {
      console.error('Error in fetching profile: ', err);
      return;
    }
    callback(err, res);
  });
}

function getProfilesForUser(payload, callback) {
  Client.post('/my/api/userprofileapi/getProfiles', payload)
    .end((err, res) => {
      if (err) {
        console.error('Error in fetching profile: ', err);
      }
      callback(err, res);
    });
}

export { deleteProfile, updateProfile, addProfile, tagProfile, getProfileForProduct, getProfilesForUser };

