import Client from '../../../utils/Client';
import config from '../../../config';
import { secureRoot } from '../../../config';

function logoutUserAction(payload, callback) {
  const url = `${secureRoot()}web/auth/v1/signout`;
  Client
    .post(url, payload)
    .end((err, res) => {
      if (err) {
        console.error('Error [auth User Logout]:', err);
      }
      callback(err, res);
    });

}

export { logoutUserAction };
