import Client from '../../../utils/Client';
import get from 'lodash/get';
import at from 'v-at';

import { getPaymentMode } from '../pageHelpers';

const checkServiceability = (data, type ,callback) => {
  const payload = serviceabilityReqFormat(data, type);
  Client.post('/my/api/serviceability/checkServiceability', payload)
  .end((err, res) => {
    if (err) {
      console.error(`Error [ ${type} Serviceability Actions]:`, err);
    }
    callback(err, res);
  });
};

const serviceabilityReqFormat = (data, serviceabilityType) => {
  const items = get(data, 'items', []);
  const order = get(data, 'order', {});
  let paymentInstruments = getPaymentMode(at(order, 'payments'));
  let isCOD = false;
  paymentInstruments.forEach((instrument) => {
    if (instrument.toLowerCase() === 'cod') {
      isCOD = true;
    }
  });
  let itemsPayload = [];
  let shipmentId = '';
  items.forEach((item, key) => {
    let skuId = at(data, 'skuId') || (at(item, 'product.skuId') || 'false').toString();
    const warehouseIds = at(data, 'warehouseIds') || [`${at(item, 'dispatchWarehouseId')}`] || [];

    const eachItemPayload = {
      isFragile: (at(item, 'product.attributes.fragile') || 'false').toString(),
      isHazmat: (at(item, 'product.attributes.hazmat') || 'false').toString(),
      isJewellery: (at(item, 'product.attributes.jewellery') || 'false').toString(),
      isLarge: (at(item, 'product.attributes.large') || 'false').toString(),
      codEnabled: (at(item, 'product.attributes.codEnabled') || 'false').toString(),
      skuId: skuId,
      codValue: serviceabilityType === 'DELIVERY' ?(get(item, 'payments.instruments.cod', 0) / (100 * get(item, 'quantity', 1))).toString():"0",
      itemValue: (at(item, 'payments.amount') /100 || 0).toString(),
      availableInWarehouses: Array.isArray(warehouseIds) ? warehouseIds : [warehouseIds],
      openBoxPickupEnabled: (at(item, 'product.attributes.pickupEnabled') || 'false').toString(),
      tryAndBuyEnabled: (at(item, 'flags.isTryable') || 'false').toString(),
      isReturnable: (at(item, 'flags.returnable.isReturnable') || 'false').toString(),
      procurementTimeInDays: "0",
      isExchangeable: (at(item, 'flags.exchangeable.isExchangeable') || 'false').toString()
    };
    if(serviceabilityType === 'EXCHANGE' || serviceabilityType === 'RETURN') {
      shipmentId = `${get(item, 'orderReleaseId', '')}`;
    }
    itemsPayload.push(eachItemPayload);
  });
  let paymentMode;
  if (serviceabilityType === 'DELIVERY') {
    paymentMode = isCOD ? 'CASH_ON_DELIVERY' : 'ALL';
  } else {
    paymentMode = 'ONLINE';
  }
  return {
        items : itemsPayload,
        pincode: data.pincode,
        clientId: '2297',
        consolidationEnabled: "false",
        scheduleEnabled: (at(data, 'scheduleEnabled') || 'false').toString(),
        paymentMode: paymentMode,
        serviceType: data.serviceType || serviceabilityType,
        shippingMethod: (at(data, 'shippingMethod') || 'ALL').toString(),
        platform: 'MT',
        shipmentId
  }
};

export {
  checkServiceability
};
