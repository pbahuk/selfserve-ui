import Client from '../../../utils/Client';

const getSellerConfig = (sellerId, callback) => {
  Client.post('/my/api/sellerApi/sellerConfiguration/search/bySellerId/', {
      sellerId
    })
    .end((err, res) => {
      if (err) {
        console.error('Error [Seller Actions]:', err);
      }
      callback(err, res);
    });
};

export { getSellerConfig };