function get(key) {
  const features = typeof window !== 'undefined' && window.__myx_features__ || {};
  return features[key];
}

export default get;
