import { root, secureRoot } from '../config';

const absoluter = (url) => {
  if (typeof url !== 'undefined' && (url.substr(0, 2) !== '//' && url.indexOf('://') === -1)) {
    url = (url.indexOf('/my/') === -1 ? root() : secureRoot()) + (url.charAt(0) === '/' ? url.substr(1) : url);
  }
  return url;
};

export default absoluter;
