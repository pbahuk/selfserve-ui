//USE THIS ONLY ON CLIENT SIDE
import _ from 'lodash';

/* temporary read from __myx_env */
const isBrowser = (typeof window !== 'undefined');
const prefix = (isBrowser && __myx_env__ && __myx_env__.cookie) ? __myx_env__.cookie.prefix : '';
const domain = (isBrowser && __myx_env__ && __myx_env__.cookie) ? __myx_env__.cookie.domain : '.myntra.com'; 

const cookies = {
    get: function(key) {
        key = prefix + key;
        var result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie);
        return result ? result[1] : null;
    },

    set: function(key, value, days, path) {
        key = prefix + key;
        var cookie = encodeURIComponent(key) + '=' + encodeURIComponent(value);
        if (days) {
            var expDate = new Date(Date.now() + (days * 24 * 60 * 60 * 1000));
            cookie += '; expires=' + expDate.toGMTString();
        }
        if (!path) { path = '/'; }
        cookie += '; path=' + path;
        cookie += '; domain=' +  domain;
        document.cookie = cookie;
    },

    del: function(key) {
        this.set(key, '', -1);
    }
};

export default cookies;
