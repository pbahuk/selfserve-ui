import Madalytics from 'madalytics-web';

/**
 * data_set : payload -> screen -> data_set
 * customPayload : payload -> widget.
 */
export default {
  setPageContext: (name) => Madalytics.setPageContext({ name, url: window.location.pathname }),

  sendEvent: function(eventType, data_set = {}, customPayload = {}, variant = 'react') {
  	if (Madalytics && typeof Madalytics.send === 'function') {
      Madalytics.send(eventType, { variant, data_set }, customPayload);
    }
  }
};