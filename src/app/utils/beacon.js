import at from 'v-at';
import cookies from '@myntra/myx/lib/cookies';
import lscache from 'lscache';
import { isBrowser } from './index';
import Client from '../../utils/Client';
import bus from 'bus';
import lscachify from './lscachify';

export const CACHE_KEY = 'beacon:user-data';

export const updateGlobals = (beaconData) => {
  if (isBrowser()) {
    Object.keys(beaconData).forEach((keys) => {
      window[`__myx_${keys}__`] = beaconData[keys];
    });
  }
};

export const clearCache = () => {
  lscache.remove(CACHE_KEY);
  lscache.remove(`${CACHE_KEY}-cacheexpiration`);
};

const wrapper = lscachify((dummy, callback) => {
  Client.post(`/beacon/user-data${window.location.search}`).then((res) => {
    callback(null, res);
  }, (error) => {
    console.log('Could not fetch user-data', error);
    cookies.del('bc');
    callback(error, null);
  });
}, 'beacon', 5);

export const beacon = (callback) => {
  if (isBrowser() && window.__myx_servedGeneric__) {
    if (window.localStorage) {
      const expiry = window.localStorage.getItem('lscache-beacon:user-data-cacheexpiration');
      if (!expiry || cookies.get('bc')) {
        window.localStorage.clear();
      }
    }

    wrapper('user-data', (err, res) => {
      if (!err) {
        const beaconData = at(res, 'body');
        if (Object.keys(beaconData)) {
          bus.emit('beacon-data', res.body);
          updateGlobals(beaconData);
        }
        if (callback) {
          callback(err, beaconData);
        }
      }
    });
  }
};
