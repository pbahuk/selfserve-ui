function get(key) {
  const kvpairs = typeof window !== 'undefined' && window.__myx_kvpairs__ || {};
  return kvpairs[key];
}

export default get;
