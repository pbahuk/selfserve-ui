import kvpairs from './kvpairs';
import cookies from './cookies';
import at from 'v-at';

function inPriceRevealMode() {
  let priceRevealData = kvpairs('hrdr.pricereveal.data');
  let priceRevealMode = false;
  if (typeof priceRevealData === 'string') {
    try {
      priceRevealData = JSON.parse(priceRevealData);
      priceRevealMode = priceRevealData.enable === 'true';
    } catch (e) {
      console.log('price reveal data is not a proper json object');
    }
  } else if (typeof priceRevealData === 'object') {
    priceRevealMode = priceRevealData.enable === 'true';
  }
  return priceRevealMode;
}

function isLoggedIn() {
  return at(window, '__myx_session__.isLoggedIn');
}

function stsCookieExists() {
  return cookies.get('sts') || false;
}

function cookieExists(cname) {
  return cookies.get(cname) || false;
}

function isSlotEntryEnabled() {
  return cookieExists('stp') && !cookieExists('stb') && isLoggedIn();
}

function isShowSlotPopup() {
  return inPriceRevealMode() && !stsCookieExists() && cookieExists('stp');
}

export { isShowSlotPopup, isSlotEntryEnabled, inPriceRevealMode, isLoggedIn };
