import at from 'v-at';
import moment from 'moment';
import cookies from './cookies';
import pick from 'lodash/pick';
import extend from 'lodash/extend';
import uniqBy from 'lodash/uniqBy';


const isLoggedIn = () => {
  const session = window ? window.__myx_session__ : {};
  return (session && session.login && session.isLoggedIn);
};

const getUidx = () => {
  const session = window ? window.__myx_session__ : {};
  return session.login;
};

const getUserName = () => {
  let userName = '';
  const session = window ? window.__myx_session__ : {};
  if (at(session,'userfirstname')) {
    userName = at(session,'userfirstname') + ' ' + (at(session,'userlastname') ? at(session,'userlastname') : '');
  } else {
    userName = session.email;
  }
  return userName;
};

const getAt = () => {
  return cookies.get('at') || null;
}

const getUserToken = () => {
  let session = window ? window.__myx_session__ : {};
  return(at(session, 'USER_TOKEN'));
}

const getDeviceData = () => {
  const deviceData = window ? window.__myx_deviceData__ : {};
  let device = pick(deviceData, 'isDesktop', 'isMobile', 'isTablet');
  device = extend(device, {
    'isApp': at(deviceData, 'deviceChannel') === 'mobile_app',
    userAgent: navigator.userAgent
  });
  return device;
};

const getLoginPath = (route) => {
  route = route.charAt(0) === '/' ? route.substr(1) : route;
  const configData = window ? window.__myx_env__ : {};
  const root = configData.hosts && configData.hosts.root ? configData.hosts.root : 'http://www.myntra.com/';
  const referer = `${root}${route}`;
  return `/login?referer=${encodeURIComponent(referer)}`;
};

const getUserEmail = () => {
  const session = window ? window.__myx_session__ : {};
  return session.email;
};

const getGCconfigs = (key) => {
  const configs = window ? window.__myx_giftcardsConfig__: {};
  return configs[key];
};

const getKvpair = (key) => {
  const kvpairs = window ? window.__myx_kvpairs__: {};
  return kvpairs[key];
}

const getDate = (epochDate) => {
  let day = epochDate.getDate();
  const month = epochDate.getMonth();
  const year = epochDate.getFullYear();
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  if (day < 10) {
    day = `0${day}`;
  }
  return `${day} ${months[month]} ${year}`;
};

const getShortDate = (epochDate) => {
  let day = epochDate.getDate();
  const month = epochDate.getMonth();
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  if (day < 10) {
    day = `0${day}`;
  }
  return `${day} ${months[month]}`;
};

const getDateObject = (epochDate, timeComputationRequired) => {
  if (epochDate instanceof Date) {
    let date = epochDate.getDate();
    date = date < 10 ? `0${date}` : date;
    let month = epochDate.getMonth();
    const monthsInShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const daysInShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const monthInWords = monthsInShort[month];
    month = month < 10 ? `0${month}` : month;
    let time;

    // Time Related Computation
    if (timeComputationRequired) {
      let hours = epochDate.getHours();
      let minutes = epochDate.getMinutes();
      let seconds = epochDate.getSeconds();
      const suffix = hours >= 12 ? 'P.M' : 'A.M';

      hours = ((hours + 11) % 12 + 1);
      hours = hours.toString().length < 2 ? `0${hours}` : hours;
      minutes = minutes.toString().length < 2 ? `0${minutes}` : `${minutes}`;
      seconds = seconds.toString().length < 2 ? `0${seconds}` : `${seconds}`;
      time = {
        hours,
        minutes,
        seconds,
        suffix
      }
    }

    return {
      date,
      month,
      monthInWords,
      year: epochDate.getFullYear(),
      day: epochDate.getDay(),
      dayInWords: daysInShort[epochDate.getDay()],
      time
    };
  } else {
    return null;
  }
};

const gaEmitter = (eventCategory, eventAction, eventLabel, data) => {
  eventCategory = eventCategory || 'mymyntra_unidentified';
  eventLabel = eventLabel || '';

  if (typeof MyntApp !== 'undefined' &&  typeof MyntApp.mynacoSendEvent === 'function' && eventAction) {
    const gaEventAction = data || {
      'category' : eventCategory,
      'action': eventAction,
      'label': eventLabel
    };
    MyntApp.mynacoSendEvent(eventCategory, eventAction, JSON.stringify(gaEventAction));
  } else if (window && window.ga && eventAction) {
    window.ga('send', 'event', eventCategory, eventAction, eventLabel);
  }
};

const unescapeHTML = (str) => {
  const escapeChars = {
      lt: '<',
      gt: '>',
      quot: '"',
      amp: '&',
      apos: "'"
  };
  if (str == null) return '';
  return String(str).replace(/\&([^;]+);/g, function(entity, entityCode) {
    let match;

    if (entityCode in escapeChars) {
        return escapeChars[entityCode];
    } else if (match = entityCode.match(/^#x([\da-fA-F]+)$/)) {
        return String.fromCharCode(parseInt(match[1], 16));
    } else if (match = entityCode.match(/^#(\d+)$/)) {
        return String.fromCharCode(~~match[1]);
    } else {
        return entity;
    }
  });
};

const isEmptyObject = (obj) => {
  // because Object.keys(new Date()).length === 0;
  // we have to do some additional check
  return Object.keys(obj).length === 0 && obj.constructor === Object
};

const getPaymentMode = (payments) => {
  const instruments = at(payments, 'instruments');
  const paymentInstruments = [];

  for (const key in instruments) {
    if (instruments[key] > 0 && instruments.hasOwnProperty(key)) {
      paymentInstruments.push(key);
    }
  }
  return paymentInstruments;
};

const insertCharInString = (str, char, index) => {
  const chunks = [];
  for (let i = 0; i < str.length; i += index) {
    chunks.push(str.slice(i, i + index));
  }
  return chunks.join(char);
};

const currencyValue = (number, round) => {
  var _r = typeof round === 'undefined' ? true : round;
  var num = number < 0 ? 0.00 : number;
  num = (_r ? Math.round(num):parseFloat(num, 10).toFixed(2)).toString();
  var one = (num == 1) ? true : false,
      arr = [],
      digits,
      dp = '00',
      curr;
  if(!_r){
      var _dec = num.split('.');
       if(_dec.length > 1){
           num = _dec[0];
           dp = _dec[1];
       }
  }
  while (num) {
      digits = digits ? 2 : 3;
      arr.push(num.slice(-digits));
      num = num.slice(0, -digits);
  }

  curr = arr.reverse().join(',');

  return _r ? curr : curr+'.'+dp;
};

const getCancellationRefundAmount = (items, itemQuantityMapper) => {
  // Add KVPair, later.
  // Check for handling the case of getting the Single Item's refund amount.
  items = items.length ? items : [items];
  const nonRefundableInstruments = ['mynts', 'cod'];
  let refundAmount = 0;

  // Refund Amount calculation. Remove specific instruments if any. Will return in paise.
  items.forEach((item) => {
    const instruments = at(item, 'payments.instruments');
    let itemRefundAmount = 0;
    for (const key in instruments) {
      if (instruments.hasOwnProperty(key) && instruments[key] && nonRefundableInstruments.indexOf(key) === -1) {
        itemRefundAmount += instruments[key];
      }
    }
    refundAmount += itemRefundAmount / item.quantity * itemQuantityMapper[item.id].eventQuantity;
  });
  return refundAmount;
};

const getImageUrl = (url, height, width, quality=100) => {
  const productImageUrl = url.replace('($height)', height.toString())
      .replace('($qualityPercentage)', quality.toString())
      .replace('($width)', width.toString());
  return productImageUrl;
}

const getReturnRefundAmount = (item, itemQuantityMapper = {}) => {
  const nonRefundableInstruments = ['mynts'];
  const instruments = at(item, 'payments.instruments');
  let refundAmount = 0;
  // Make it coorect later.
  const quantity = 1;

  for(const key in instruments) {
    if (instruments.hasOwnProperty(key) && instruments[key] && nonRefundableInstruments.indexOf(key) === -1) {
      refundAmount += instruments[key];
    }
  }
  return (refundAmount / item.quantity * quantity);
};

const getPickupSlotString = (anySlot, selectedSlot) => {
  let pickupSlot = '';
  if(selectedSlot) {
    const dayFormat = `ddd, D MMM${!anySlot ?',':''}`;
    const slot = moment(selectedSlot.scheduleEnd);
    const today = moment(new Date());
    if(slot.diff(today)<0) {
      pickupSlot =  '';
    } else {
      const day =  moment(selectedSlot.scheduleStart).calendar(null, {
        lastWeek: dayFormat,
        lastDay: dayFormat,
        sameDay: !anySlot ? '[Today,]' : '[Today]',
        nextDay: !anySlot ? '[Tomorrow,]' : '[Tomorrow]',
        nextWeek: dayFormat,
        sameElse: dayFormat
      });
      const time = !anySlot ? ` ${moment(selectedSlot.scheduleStart).format('LT')}–${moment(selectedSlot.scheduleEnd).format('LT')}`:'';
      pickupSlot = day+time;
    }
  }

  return pickupSlot;
}

const sortUserProfiles = (gender,profiles) => {
  /*
  * To get same profile while getting unique profiles, sort profiles by pidx.
  */
   let sortedProfiles = profiles.sort((profile) => profile.pidx);
   /*
    * Get unique profiles by name
    * Sorts profiles based on gender. If product gender is male, all male profiles appear first and vice versa.
    */
   const uniqueProfiles = uniqBy(sortedProfiles, 'name') || [];
   sortedProfiles = uniqueProfiles.filter((profile) => {
     let genderFilter = 'male';
     if (gender.toLowerCase() === 'female') {
			genderFilter = 'female';
     }
     return profile.gender === genderFilter;
   });
   return sortedProfiles;
};
export {
  isLoggedIn,
  getUidx,
  getUserName,
  getUserToken,
  getDeviceData,
  getLoginPath,
  getUserEmail,
  getGCconfigs,
  getKvpair,
  getDate,
  getShortDate,
  getDateObject,
  gaEmitter,
  unescapeHTML,
  isEmptyObject,
  getPaymentMode,
  insertCharInString,
  currencyValue,
  getCancellationRefundAmount,
  getReturnRefundAmount,
  getImageUrl,
  getPickupSlotString,
  sortUserProfiles
};

