var WEBCOLORS = require('./HeaderFooter/Webcolors');
var BLUEBERRY = require('./blueberry');
var BLUE = require('./blue');
var BLACK = require('./black');

var COLORS = {
  'border_color': '#d4d5d9',
  'border_blueberry_5': '#F5F5F6',
  'border_blueberry_10': '#eaeaec',
  'border_shadow': '#888888',
  'shimmer': 'rgba(0,0,0,0.6)',
  'app_white': 'white',
  'app_green': '#14cda8',
  'app_blue': '#526cd0',
  'app_black': '#3e4152',
  'text_light_grey': '#7e818c',
  'app_error': '#ff5a5a',
  'text_bluberry_50': '#94969F',
  'text_blueberry_70': '#696b79',
  'text_blueberry_80': '#535665',
  'text_blueberry': '#282C3F',
  'notepadyellow': '#FFCCCB',
  'button_text': '#535766',
  'mango': '#f2c210',
  'text_dark_orange': '#f16565',
  'notepad_yellow': '#fafadc',
  'watermelon': '#ff3f6c',
  'salmon_100': '#ff6636',
  'verified_green' : '#2faa21',
  "red": "#ee5f73",
  'off_white': '#fffde5',
  'off_cream': '#fef9e5',
  'pale_rose': '#fde3f3',
  'lavender': '#dfefff',
  'light_mint_green': '#f4fff9',
  'white': '#ffffff',
  'redical_red': '#ff3e6c'
};

module.exports = Object.assign(COLORS, WEBCOLORS, BLUEBERRY, BLUE, BLACK);
