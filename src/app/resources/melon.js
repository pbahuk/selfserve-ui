module.exports = {
  "melon": "#FF3F6C",
  "melon_90": "#FF527B",
  "melon_80": "#FF668A",
  "melon_70": "#FF7998",
  "melon_60": "#FF8CA7",
  "melon_20": "#FFD9E2",
  "melon_10": "#FFECF1" 
}
