var BLACK = require('./black');
var MELON = require('./melon');
var ORANGE = require('./orange');
var MYNT = require('./mynt');
var BLUE = require('./blue');

var WEBCOLORS = {
  "blue": "#118BEB",
  "link": "#526CD0",
  "red": "#ee5f73",
  "green": "#20BD99",
  "green_light": "#11DAB1",
  "ultra_dark": "#282C3F",
  "light_pink": "#FEEDF6",
  "light_yellow": "#FCF0E2",
  "dark": "#696E79",
  "white_60": "rgba(255,255,255,0.6)",
  "medium": "#94989F",
  "medium_60": "#BFC1C5",
  "dark_pink": "#EA5E8B",
  "dark_pink_10": "#ff517b",
  "light": "#D5D6D9",
  "ultra_light": "#F5F5F6",
  "white": "#FFFFFF",
  "bberry": "#FAFBFC",
  "bberry_10": "#EAEAEC",
  "transparent": "rgba(0,0,0,0)"
};

module.exports = Object.assign(WEBCOLORS, BLACK, MELON, ORANGE, MYNT, BLUE);