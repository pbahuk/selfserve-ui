const fonts = {
  'tiny_xl': '10px',
  'small': '12px',
  'xl': '20px',
  'app_font': '14px',
  'app_font_large': '16px',
  'app_medium': '16px',
  'app_large': '18px',
  'medium': '24px',
  'large': '30px',
  'description': '12px'
};

module.exports = fonts;