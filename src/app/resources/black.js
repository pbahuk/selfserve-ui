module.exports = {
  "black": "#282C3F",
  "black_90": "#3E4152",
  "black_80": "#535766",
  "black_70": "#696B79",
  "black_60": "#7E818C",
  "black_50": "#94969F",
  "black_40": "#A9ABB3",
  "black_30": "#BFC0C6",
  "black_20": "#D4D5D9",
  "black_10": "#EAEAEC",
  'black_3': '#F8F8F9',
  "black_5": "#F5F5F6"
}