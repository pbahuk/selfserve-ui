module.exports = {
  'blue_100': '#526CD0',
  'blue_90': '#3466E8',
  'blue_20': '#DDE2F6',
  'blue_10': '#EEF1FB',
  'blue_30': '#A8B5E7',
  'very_light_blue': '#e6f0ff',
  'powder_blue': '#f9daff'
};
