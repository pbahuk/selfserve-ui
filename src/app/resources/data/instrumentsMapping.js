export default {
  mrp: 'MRP',

  // Online Instruments.
  neft: 'NetBanking',
  creditCard: 'Credit Card',
  debitCard: 'Debit Card',
  netBanking: 'Netbanking',
  emi: 'Emi',
  phonepe: 'Phone Pe',

  // Cash On Delivery
  cod: 'Cash On Delivery',

  // Myntra Instruments
  cashback: 'Cashback',
  wallet: 'Wallet',
  giftcard: 'Gift Card',
  loyaltyPoints: 'Loyalty Points',
  bankCashback: 'Bank CashBack',
  mynts: 'EORS Milestone Coupon',
  myntraCredit: 'Myntra Credit',
  paylater: 'Pay Later',

  // Discounts.
  coupon: 'Coupon Discount',
  product: 'Item Discount',

  // Other Charges placed on the order.
  gst: 'GST',
  cart: 'Special Discount',
  giftwrapping: 'Service Fee',
  shipping: 'Shipping Charges',
  tryandbuy: 'Try and Buy Charges'
};
