export default [
  {
    heading: 'Eligibility, Membership, Accrual',
    details: [
      `These terms and conditions are operational only in India and open to participation of all the registered members,
        resident of India of myntra, over and above the age of 18 years.`,
      'My Privilege program has been converted into Myntra Points Program. The same denomination is applicable for Myntra Points.'
    ]
  },
  {
    heading: 'General terms and conditions',
    details: [
      `Each member is responsible for remaining knowledgeable about the Myntra Program Terms and Conditions
        and the number of points in his or her account.`,
      `Myntra will send correspondence to active members to advise them of matters of interest, including
        notification of Myntra Program changes and Points Updates.`,
      'Myntra will not be liable or responsible for correspondence lost or delayed in the mail/e-mail.',
      `Myntra reserves the right to refuse, amend, vary or cancel membership of any Member without assigning
        any reason and without prior notification.`,
      `Any change in the name, address, or other information relating to the Member must be notified to Myntra
        via the Helpdesk/email by the Member, as soon as possible at support@myntra.com or call at +91-80-43541999	24 Hours a Day / 7 Days a Week.`,
      `Myntra reserves the right to add,modify,delete or otherwise change the Terms and Conditions without any
        approval, prior notice or reference to the Member.`,
      `In the event of dispute in connection with Myntra Program and the interpretation of Terms and Conditions,
        Myntra\'s decision shall be final and binding.`,
      'This Policy and these terms shall be read in conjunction with the standard legal policies of Myntra, including its Privacy policy.'
    ]
  }
];
