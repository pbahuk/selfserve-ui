import kvpairs from '../../utils/kvpairs';

const TrackingDisplayStates = {
  'PLACED': 'Placed',
  'PROCESSING': 'Processing',
  'DISPATCH_FROM_SOURCE': 'Processing',
  'RECEIVED_FOR_PACK': 'Packing',
  'PACKED': 'Packed',
  'SHIPPED': 'Shipped',
  'OUT_FOR_DELIVERY': 'Out for delivery',
  'DELIVERED': 'Delivered',
  'CANCELLED': 'Cancelled'
};

const TrackingStateMappings = kvpairs('mymyntra.tracking.statusMap') || {
  'ORDER_CREATION': TrackingDisplayStates.PLACED,
  'PACK_IN_SOURCE_WAREHOUSE': TrackingDisplayStates.PROCESSING,
  'DISPATCH_FROM_SOURCE_WAREHOUSE': TrackingDisplayStates.DISPATCH_FROM_SOURCE,
  'RECEIVED_IN_DISPATCH_WAREHOUSE': TrackingDisplayStates.RECEIVED_FOR_PACK,
  'PACK_IN_DISPATCH_WAREHOUSE': TrackingDisplayStates.PACKED,
  'SHIP_TO_CUSTOMER': TrackingDisplayStates.SHIPPED,
  'OUT_FOR_DELIVERY': TrackingDisplayStates.OUT_FOR_DELIVERY,
  'CANCELLED': TrackingDisplayStates.CANCELLED,
  'DELIVERED_TO_CUSTOMER': TrackingDisplayStates.DELIVERED
};

export { TrackingStateMappings };
