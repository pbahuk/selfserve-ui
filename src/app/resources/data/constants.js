export const crossSellTxtList = ['Trending Picks', 'Most Popular', 'Fresh Stock', 'Handpicked Styles', 'Trending Styles'];
export const emptyStateEventData = {
  widgetClickEvent: 'Return and cancellation - Empty Widget Click',
  widgetLoadEvent: 'Return and cancellation - Empty Widget Loaded',
  contentTypeShopMore: 'shop-more',
  contentTypeContinueShopping: 'continue-shopping',
  cardTypeComponentCard: 'COMPONENT_CARD'
}