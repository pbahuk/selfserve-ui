export default{
  sections: [
    {
      data: [
        {
          description: 'Check your order status',
          image: 'https://assets.myntassets.com/assets/images/2017/11/9/11510224464021-profile_Orders.png',
          redirectUrl: '/my/orders',
          title: 'Orders'
        },
        {
          description: 'All your curated product collections',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509113344443-profile_Collections.png',
          redirectUrl: '/wishlist',
          title: 'Collections & Wishlist'
        }
      ]
    },
    {
      data: [
        {
          description: 'Manage all your refunds & gift cards',
          image: 'https://assets.myntassets.com/assets/images/2017/12/1/11512134313695-profile_myntra_credit.png',
          redirectUrl: '/my/myntracredit',
          title: 'Myntra Credit'
        },
        {
          description: 'Earn points as you shop and use them in checkout',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509112977633-profile_MyntraPoints.png',
          redirectUrl: '/my/myntrapoints',
          title: 'Myntra Points'
        },
        {
          description: 'Your cashback rewards go here',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509113524201-profile_MyntraCredit.png',
          redirectUrl: '/my/cashback',
          title: 'Cashback'
        },
        {
          description: 'Save your cards for faster checkout',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509113019405-profile_Cards.png',
          redirectUrl: '/my/savedcards',
          title: 'Saved Cards'
        },
        {
          description: 'Save addresses for a hassle-free checkout',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509112077254-profile_Address.png',
          redirectUrl: '/my/address',
          title: 'Addresses'
        },
        {
          description: 'Manage coupons for additional discounts',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509113484291-profile_Coupons.png',
          redirectUrl: '/my/coupons',
          title: 'Coupons'
        }
      ]
    },
    {
      data: [
        {
          description: 'Change your profile details & password',
          image: 'https://assets.myntassets.com/assets/images/2017/10/27/11509112835028-profile_Edit.png',
          redirectUrl: '/my/profile/edit',
          title: 'Profile Details'
        }
      ]
    }
  ]
};
