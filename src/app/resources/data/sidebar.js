export default [
  {
    heading: '',
    links: [
      { name: 'Overview', refer: '/my/dashboard' }
    ]
  },
  {
    heading: 'ORDERS',
    links: [
      { name: 'Orders & Returns', refer: '/my/orders' }
    ]
  },
  {
    heading: 'CREDITS',
    links: [
      { name: 'Coupons', refer: '/my/coupons' },
      { name: 'Manage Gift Cards', refer: '/my/giftcards' },
      { name: 'Myntra Points', refer: '/my/myntrapoints' }
    ]
  },
  {
    heading: 'ACCOUNT',
    links: [
      { name: 'Profile', refer: '/my/profile' },
      { name: 'Saved Cards', refer: '/my/savedcards' },
      { name: 'Addresses', refer: '/my/address' },
      { name: 'Myntra Insider', refer: '/myntrainsider?cache=false' }
    ]
  }
];
