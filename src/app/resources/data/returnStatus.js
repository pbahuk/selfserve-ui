import kvpairs from '../../utils/kvpairs';

const returnStatusMap = kvpairs('mymyntra.return.statusMap') || {
  'D': {
    'name': 'Return Request Declined',
    'message': 'Your return request has been declined.'
  },
  'DEC': {
    'name': 'Return Request Declined',
    'message': 'Your return request has been declined.'
  },
  'DLC': {
    'name': 'Reshipped to You',
    'message': 'Your return request has been declined as it did not meet our return policy. The item will be shipped back to you.'
  },
  'DLS': {
    'name': 'Refund Processed',
    'message': 'Your refund for the return has been processed successfully. Please check your email for refund details.'
  },
  'L': {
    'name': 'Refund Processed',
    'message': 'Your refund for the return has been processed successfully. Please check your email for refund details.'
  },
  'LPI': {
    'name': 'Return Request Placed',
    'message': 'Your return request placed. Please check your email for details.'
  },
  'Q': {
    'name': 'Return Request Placed',
    'message': 'Your return request placed. Please check your email for details.'
  },
  'RFR': {
    'name': 'Return Request Placed',
    'message': 'Your return request placed. Please check your email for details.'
  },
  'RL': {
    'name': 'Return Picked Up/Received',
    'message': 'Your return has been picked up/received successfully. Please check your email for refund details.'
  },
  'RPC': {
    'name': 'Refund Processed',
    'message': 'Your return has been processed successfully. Please check your email for refund details.'
  },
  'RRC': {
    'name': 'Refund Processed',
    'message': 'Your return has been processed successfully. Please check your email for refund details.'
  },
  'RRSH': {
    'name': 'Reshipped to You',
    'message': 'Your return request has been declined as it did not meet our return policy. The item will be shipped back to you.'
  },
  'SHC': {
    'name': 'Reshipped to You',
    'message': 'Your return request has been declined as it did not meet our return policy. The item will be shipped back to you.'
  },
  'SHS': {
    'name': 'Refund Processed',
    'message': 'Your refund for the return has been processed successfully. Please check your email for refund details.'
  }
};

export { returnStatusMap };
