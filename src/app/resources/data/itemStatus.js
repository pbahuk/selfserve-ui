import kvpairs from '../../utils/kvpairs';

const itemStatusMap = kvpairs('mymyntra.item.statusMap') || {
  'PV': 'Order Received',
  'PP': 'Order Received',
  'Q': 'Order Received',
  'DEC': 'Declined',
  'OH': 'On Hold',
  'IC': 'Cancelled',
  'RFR': 'Ready For Release',
  'WP': 'Work In Progress',
  'PK': 'Packed',
  'RTO': 'Returned To Origin',
  'S': 'Shipped',
  'D': 'Delivered',
  'C': 'Delivered',
  'L': 'Lost'
};

export { itemStatusMap };
