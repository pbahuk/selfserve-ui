const calculateYears = () => {
  const currentYear = new Date().getFullYear();
  const years = [];

  for (let i = 0; i < 50; i++) {
    years.push({
      displayName: currentYear + i,
      value: currentYear + i
    });
  }
  return years;
};

export default {
  month: {
    title: 'Expiry Month',
    values: [
      { displayName: '01 (Jan)', value: '01 (Jan)' },
      { displayName: '02 (Feb)', value: '02 (Feb)' },
      { displayName: '03 (Mar)', value: '03 (Mar)' },
      { displayName: '04 (Apr)', value: '04 (Apr)' },
      { displayName: '05 (May)', value: '05 (May)' },
      { displayName: '06 (Jun)', value: '06 (Jun)' },
      { displayName: '07 (Jul)', value: '07 (Jul)' },
      { displayName: '08 (Aug)', value: '08 (Aug)' },
      { displayName: '09 (Sep)', value: '09 (Sep)' },
      { displayName: '10 (Oct)', value: '10 (Oct)' },
      { displayName: '11 (Nov)', value: '11 (Nov)' },
      { displayName: '12 (Dec)', value: '12 (Dec)' }
    ]
  },
  year: {
    title: 'Expiry Year',
    values: calculateYears()
  }
};

