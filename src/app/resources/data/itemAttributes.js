
const celebBrands = {
 '22952': 'NUSH',
 '13127': 'HRX by Hrithik Roshan',
 '8626': 'WROGN',
 '9834': 'AAY'
};

const articleTypes = {
 '83': 'Kurtas',
 '85': 'Shirts',
 '424': 'Suits',
 '66': 'Watches',
 '249': 'Jeggings',
 '543': 'Kurta Sets',
 '82': 'Jackets',
 '72': 'Shorts',
 '79': 'Dresses',
 '78': 'Trousers',
 '70': 'Jeans',
 '74': 'Skirts',
 '94': 'Formal Shoes',
 '0': 'Blazers',
 '51': 'Sunglasses',
 '93': 'Casual Shoes',
 '280': 'Flats',
 '279': 'Heels',
 '207': 'Jumpsuit',
 '46': 'Handbags',
 '90': 'Tshirts',
 '95': 'Sports Shoes',
 '43': 'Backpacks',
 '80': 'Blazers',
 '87': 'Sweatshirts',
 '88': 'Sweaters',
 '89': 'Tops',
 '162': 'Perfume and Body Mist'
};

const internationalBrands = {
 '16905': '7 For All Mankind',
 '13656': '99 HUNTS',
 '5995': 'AIGNER',
 '15546': 'ALCOTT',
 '37559': 'AMERICAN EAGLE OUTFITTERS',
 '7636': 'Antony Morato',
 '7778': 'Autograph',
 '18083': 'Automobili Lamborghini',
 '9599': 'BLEND',
 '21908': 'Blue Giraffe',
 '13717': 'Boohoo',
 '19120': 'BOSS Green',
 '12955': 'BOSS Orange',
 '1637': 'Bwitch',
 '21485': 'CONFLUENCE Crystals from SWAROVSKI',
 '21557': 'CR7',
 '7133': 'Desigual',
 '19605': 'DOROTHY PERKINS',
 '1097': 'ESPRIT',
 '7084': 'ESPRIT Collection',
 '1365': 'Ferrari',
 '1078': 'Forever New',
 '39881': 'Forever9teen',
 '7720': 'Fox',
 '10402': 'FunkyFish',
 '12947': 'G-STAR RAW',
 '77': 'Geox',
 '7584': 'Girls On Film',
 '8558': 'H.E. By Mango',
 '8710': 'Harley-Davidson',
 '15760': 'HEATWAVE',
 '10441': 'INDICODE',
 '14093': 'Jeep',
 '8724': 'Jn Joy',
 '47023': 'KVL',
 '7585': 'Little Mistress',
 '23591': 'Lonsdale',
 '15431': 'Lyla Loves',
 '7775': 'M&S Collection',
 '7646': 'MANGO',
 '8559': 'Mango Kids',
 '23816': 'MANGO MAN',
 '12925': 'MARC JACOBS',
 '7666': 'Marks & Spencer',
 '8478': 'Marks La Moda',
 '15540': 'Metersbonwe',
 '103': 'Michael Kors',
 '8459': 'New Look',
 '19469': 'next',
 '22919': 'OVS',
 '7583': 'Paper Dolls',
 '8460': 'Parfois',
 '9621': 'Polo Ralph Lauren',
 '1377': 'Prada',
 '28355': 'Quest',
 '15432': 'QUIZ',
 '10395': 'Qupid',
 '8454': 'Replay',
 '10162': 'Rinascimento',
 '1081': 's.Oliver',
 '1302': 'Satya Paul',
 '9043': 'SELA',
 '14037': 'Silvian Heach',
 '1319': 'Slazenger',
 '7550': 'Soludos',
 '16112': 'SWAROVSKI',
 '13940': 'Tokyo Laundry',
 '16197': 'Tom Tailor',
 '16111': 'TOMS'
};

const celebBrandMap = {
 'NUSH': 'Anushka approves! You\'ve made an excellent choice',
 'WROGN': 'Virat would wear that! That’s a great choice you\'ve made!',
 'HRX by Hrithik Roshan': 'You\'ll soon have something Hrithik also has. Excellent choice!',
 'AAY': 'Nice! You\'ve picked something Deepika would buy too'
};

const itemTagsText = {
  'INTL_BRAND': 'Wow! You bought some high-end styles from ',
  'CLASSY_CHOICE': 'Classy choice! You\'re going to love what you just bought!',
  'HIDDEN_TREASURE': 'Congratulations! You\'ve found a rare gem!',
  'SUPER_SAVER': 'Smart! You got the product at a big discount',
};

const itemThemeMap = {
  'PRODUCT_USP': 'Product USP',
  'PRICING': 'Pricing',
  'URGENCY': 'Urgency',
  'SERVICES': 'Services',
  'PRICING': 'Pricing'
}

const getInventoryCountTag = (count) => {
  if(count === 0)
    return (`You got lucky! This item is now out of stock`)
  return (`Lucky you! The item will be out of stock soon`)
}

const getUrgencyTag = (count) => {
  return (`Smart Choice! ${count} others have this in their cart!`);
};

const getFastDeliveryTag = (deliveryTime) => {
  return (`YAY! You're gonna get your order within ${deliveryTime} DAYS!`)
}

export { celebBrands, articleTypes, internationalBrands, celebBrandMap, itemTagsText, getUrgencyTag, getFastDeliveryTag, itemThemeMap, getInventoryCountTag };