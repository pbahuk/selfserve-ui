module.exports = {
  'blueberry_10': '#EAEAEC',
  'blueberry_20': '#D4D5D9',
  'blueberry_50': '#94969F',
  'blueberry_60': '#7e818c',
  'blueberry_70': '#696B79',
  'blueberry_80': '#535766',
  'blueberry_90': '#3E4152'
};