import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import _ from 'lodash';
import PageStyles from './../../resources/page.css';
import Notify from '../Reusables/Notify/notify';

// Functional Imports.
import Madalytics from '../../utils/trackMadalytics';
import { getUidx, getDeviceData } from '../../utils/pageHelpers';
import { getOrders } from '../../utils/services/oms.js';
import { getInsiderPoints } from '../../utils/services/insider';
import InsiderEnrollment from '../Reusables/InsiderEnrollment';
import features from '../../utils/features';
import kvpairs from '../../utils/kvpairs';

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';

// Order List related components.
import Order from './Order';
import Pagination from './Pagination';
import ArchivedButton from './ArchivedButton';
import SearchByOrder from './SearchByOrder';
import MobileVerification from './MobileVerification';

// FGs and KVPairs.
const ppsDisableFG = features('mymyntra.pps.disable') === 'true';
const archivedFG = features('mymyntra.pastorders.enable') === 'true';
const mymyntraDisableFG = features('hrdr.mymyntra.orders.disable') === 'true';
const unavailableMessage = kvpairs('mymyntra.features.unavailable.message');
const delayMessage = kvpairs('mymyntra.orders.delayMessage');
const mymyntraAppMessage = unavailableMessage || delayMessage;
const totalOrdersWithLooks = parseInt(kvpairs('mymyntra.orderwithlooks.number'), 10);

class OrderList extends React.Component {
  constructor(props) {
    super(props);
    // State Data.
    const queryParameters = at(this, 'props.location.query') || {};
    const deviceData = getDeviceData() || {};
    const isApp = deviceData.isApp || false;

    // Function bindings.
    this.updatePage = this.updatePage.bind(this);
    this.closeVerificationCard = this.closeVerificationCard.bind(this);
    this.toggleReviewSuccessDisplay = this.toggleReviewSuccessDisplay.bind(this);

    this.state = {
      page: parseInt(queryParameters.p, 10) || 1,
      loading: true,
      pageError: false,
      orders: [],
      totalOrdersCount: 0,
      archivedOrdersLink: false,
      mobileVerificationCard: queryParameters && queryParameters.showMobileVerification === 'true' && isApp || false,
      showReviewSuccess: false
    };
  }


  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    let pageError = false;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }

    window.addEventListener('popstate', this.fetchNewPage.bind(this));

    const archivedOrdersLink = at(serviceResponse, 'orderResponse.archivedOrdersLink') || false;
    const totalOrdersCount = at(serviceResponse, 'orderResponse.totalOrders');
    this.looksAb = at(serviceResponse, 'looksAb');
    this.savingsAB = at(serviceResponse, 'savingsAB') || 'disabled';
    this.removeCancelABValue = at(serviceResponse, 'removeCancelAB');
    this.myOrderInsiderABValue = at(serviceResponse, 'myOrderInsiderAB');
    this.itemsTagAB = at(serviceResponse, 'itemsTagAB') || 'disabled';
    let orders = at(serviceResponse, 'orderResponse.orders') || [];
    orders = _.isArray(orders) ? orders : [orders];
    this.archived = at(serviceResponse, 'archived');


    const profileData = at(serviceResponse, 'profileResponse.data.0') || {};
    this.profileList = [];

    if (profileData.profileList) {
      this.profileList = profileData.profileList.filter(({ isEnabled }) => (isEnabled));
    }

    Madalytics.sendEvent('ScreenLoad', {
      type: 'orders',
      data_set: {
        success: !pageError,
        userId: getUidx()
      }
    });

    this.setState({
      loading: false,
      pageError,
      totalOrdersCount,
      orders,
      archivedOrdersLink
    });
    if (this.myOrderInsiderABValue) {
      getInsiderPoints((err, res) => {
        if (!err) {
          const insiderResponse = res.body || {};
          const enrolmentStatus = insiderResponse.enrolmentStatus;
          const insiderOrdersPoints = insiderResponse.orders || [];
          const insiderOrdersPointsMap = insiderOrdersPoints.reduce((orderObj, order) => {
            orderObj[order.orderId] = order;
            return orderObj;
          }, {});
          if (enrolmentStatus === 'not_enrolled') {
            Madalytics.sendEvent(
              'widgetLoad',
              {
                type: 'my orders page - Insider login banner load'
              },
            );
            this.setState({
              showInsiderEnrollBanner: true
            });
          } else {
            this.setState({
              insiderOrdersPoints: insiderOrdersPointsMap
            });
          }
        }
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('popstate', this.fetchNewPage.bind(this));
  }

  fetchNewPage(e) {
    const newURL = (e.state || {}).path || '';
    const newPage = Number(newURL.split('=')[1]) || 1;
    this.fetchData(newPage);
  }

  updatePage(next) {
    const page = next ? this.state.page + 1 : this.state.page - 1;
    this.setState({
      page
    }, () => {
      if (window.history.pushState) {
        let newURL = window.location.href.split('?')[0];
        newURL = `${newURL}?p=${page}`;
        window.history.pushState({ path: newURL }, '', newURL);
      }
      this.fetchData(page);
    });
  }

  fetchData(page) {
    let pageError = false;
    const payload = {
      page,
      getStyle: 'true',
      getPayments: !ppsDisableFG ? 'true' : 'false',
      getTracking: 'true',
      getReturn: 'true',
      getGiftCard: 'false',
      archived: this.archived
    };

    this.setState({
      loading: true
    }, () => {
      window.scroll(0, 0);
    });

    getOrders(payload, (err, res) => {
      if (err) {
        pageError = true;
      } else if (at(res, 'body.status') && at(res, 'body.status.statusType') === 'ERROR') {
        pageError = true;
      }
      // To keep latest orders in the variable.
      window.__myx.omsServiceResponse = at(res, 'body');
      let orders = at(res, 'body.orders') || [];
      orders = _.isArray(orders) ? orders : [orders];
      const archivedOrdersLink = at(res, 'body.archivedOrdersLink') || false;

      this.setState({
        page,
        pageError,
        loading: false,
        totalOrdersCount: at(res, 'body.totalOrders'),
        orders,
        archivedOrdersLink
      });
    });
  }

  closeVerificationCard() {
    this.setState({
      mobileVerificationCard: false
    });
  }

  toggleReviewSuccessDisplay() {
    this.setState({
      showReviewSuccess: true
    }, () => {
      setTimeout(() => {
        this.setState({
          showReviewSuccess: false
        });
      }, 3000);
    });
  }

  renderOrders() {
    return this.state.orders.map((order, key) => {
      const pos = key + 1;
      const looksEnabled = (totalOrdersWithLooks === -1 || (pos <= totalOrdersWithLooks)) && this.looksAb;
      const insiderOrderPoints = at(this.state, `insiderOrdersPoints.${order.storeOrderId}.lineItems`);
      return (<Order
        key={key}
        order={order}
        page={this.state.page}
        looksEnabled={looksEnabled}
        profiles={this.profileList}
        savingsAB={this.savingsAB}
        itemsTagAB={this.itemsTagAB}
        archived={this.archived}
        insiderOrderPoints={insiderOrderPoints}
        displaySuccess={this.toggleReviewSuccessDisplay}
        removeCancelABValue={this.removeCancelABValue} />);
    });
  }

  render() {
    let page = null;

    if (this.state.loading) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <div className={PageStyles.loader} />
        </div>
      </div>);
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <ErrorCard
            message="Something Went Wrong"
            secondaryMessage="Please try again after some time."
            actionName="Retry"
            action={() => { location.reload(); }} />
          {archivedFG && this.state.archivedOrdersLink && <ArchivedButton />}
        </div>
      </div>);
    } else if (mymyntraDisableFG) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          {mymyntraAppMessage && <div className={PageStyles.pageMessage}>{mymyntraAppMessage}</div>}
          <SearchByOrder />
        </div>
      </div>);
    } else if (!this.state.orders.length) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          {this.state.mobileVerificationCard &&
            <MobileVerification
              closeVerificationCard={this.closeVerificationCard} />}
          <ErrorCard
            message="No Active Orders"
            secondaryMessage="There are no recent orders to show."
            actionName="Start Shopping"
            action={() => { window.location = '/'; }} />
            {archivedFG && this.state.archivedOrdersLink && <ArchivedButton />}
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        {this.state.showReviewSuccess &&
          <div className={PageStyles.reviewSuccessNotify}>
            <Notify
              backgroundColor={'#2bc1a1'}
              NotifyText={'Your review has been submitted successfully!'} />
          </div>}
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          {mymyntraAppMessage && <div className={PageStyles.pageMessage}>{mymyntraAppMessage}</div>}
          {this.state.mobileVerificationCard &&
            <MobileVerification
              closeVerificationCard={this.closeVerificationCard} />}
          {this.state.showInsiderEnrollBanner ? <InsiderEnrollment /> : null}
          {!mymyntraDisableFG ? this.renderOrders() : <SearchByOrder />}
          {archivedFG && this.state.archivedOrdersLink && <ArchivedButton />}
          {!mymyntraDisableFG && <Pagination
            page={this.state.page}
            batch={5}
            action={this.updatePage}
            totalCount={this.state.totalOrdersCount} />}
        </div>
      </div>);
    }

    return page;
  }
}

OrderList.PropTypes = {
  location: PropTypes.Object
};

export default OrderList;
