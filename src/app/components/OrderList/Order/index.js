import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Functional Imports.
import Styles from './order.css';
import { insertCharInString } from '../../../utils/pageHelpers';
import Madalytics from '../../../utils/trackMadalytics';

// Component Related.
import Item from '../../Reusables/Item';


class Order extends React.Component {

  redirectOrderDetails(storeOrderId) {
    Madalytics.sendEvent('orderDetailsClick', {
      'entity_type': 'order_details',
      'entity_id': storeOrderId
    }, null, 'react');
    let redirect = `/my/order/details?storeOrderId=${storeOrderId}`;
    if (this.props.archived) {
      redirect = `/my/archived/order/details?storeOrderId=${storeOrderId}`;
    }
    window.location = redirect;
  }

  renderItems() {
    const insiderOrderPoints = this.props.insiderOrderPoints || [];
    const insiderItemsPointsMap = insiderOrderPoints.reduce((itemObj, item) => {
      if (item.points !== 0) {
        itemObj[Number.parseInt(item.lineItemId, 10)] = item;
      }
      return itemObj;
    }, {});
    const order = this.props.order || {};
    const items = at(order, 'items');
    const type = 'item';
    const archived = this.props.archived;
    return items.map((item, key) =>
      <Item
        key={key}
        type={type}
        item={item}
        archived={archived}
        page={this.props.page}
        order={this.props.order}
        insiderItemsPoints={insiderItemsPointsMap[item.id]}
        looksEnabled={this.props.looksEnabled}
        profiles={this.props.profiles}
        savingsAB={this.props.savingsAB}
        itemsTagAB={this.props.itemsTagAB}
        displaySuccess={this.props.displaySuccess}
        removeCancelABValue={this.props.removeCancelABValue} />
    );
  }

  render() {
    const order = this.props.order || {};
    return (<div className={Styles.order}>
      <div className={Styles.orderInfo}>
        ORDER NO:
        <span className={Styles.orderNumber}> {insertCharInString((at(order, 'storeOrderId') || ''), '-', 7)}</span>
        <span className={Styles.orderDetailsButton} onClick={() => this.redirectOrderDetails(order.storeOrderId)}>
            Order Details
        </span>
      </div>
      {this.renderItems()}
    </div>);
  }
}

Order.propTypes = {
  order: PropTypes.object,
  returnsInfo: PropTypes.array,
  looksEnabled: PropTypes.bool,
  profiles: PropTypes.array,
  page: PropTypes.number,
  insiderOrderPoints: PropTypes.array,
  removeCancelABValue: PropTypes.string,
  savingsAB: PropTypes.string,
  itemsTagAB: PropTypes.string,
  archived: PropTypes.bool,
  displaySuccess: PropTypes.func
};

export default Order;
