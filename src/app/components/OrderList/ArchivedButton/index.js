import React from 'react';
import Styles from './archivedButton.css';

const ArchivedButton = () => (
  <div className={Styles.button} onClick={() => { window.location = '/my/archived/orders'; }}>
    Show My Older Orders
  </div>
);

export default ArchivedButton;
