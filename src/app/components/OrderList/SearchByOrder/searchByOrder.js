import React from 'react';
import Styles from './searchByOrder.css';
import Input from '../../Reusables/Forms/MyInput';
import { getDeviceData } from '../../../utils/pageHelpers';
import classnames from 'classnames/bind';

const bindClass = classnames.bind(Styles);

class SearchByOrder extends React.PureComponent {
  render() {
    const { props } = this;
    const device = getDeviceData();
    return (
      <div className={bindClass('container', { mobile: device.isMobile || device.isApp })}>
        <div className={Styles.header}>Search by Order Number</div>
        <div className={Styles.desc}>To know the details of your order, please enter the Order Number in the field below</div>
        <div className={Styles.tipText}><span className={Styles.tip}>Tip: </span>
        You can find your Order Number in the confirmation Email/SMS that was sent to you</div>
        <Input
          label="Enter order number"
          customised={false}
          type="text"
          name="orderNumber"
          value={props.value}
          errorMessage={props.error}
          onBlur={props.onBlur}
          onChange={props.onChange} />
        <button className={bindClass('button', 'confirm', { disabled: props.confirmDisabled })} onClick={props.onConfirm}> Search Order </button>
      </div>
    );
  }
}

export default SearchByOrder;

