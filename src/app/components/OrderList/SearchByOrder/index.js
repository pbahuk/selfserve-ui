import React from 'react';
import SearchByOrder from './searchByOrder';
import at from 'v-at';

const EMPTY_OBJ_READ_ONLY = {};
const ERROR_TEXT = 'Invalid input';

class SearchByOrderWrapper extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      orderField: {
        value: '',
        error: '',
        validation: /^[0-9]+$/,   // Only number
        label: 'Enter order number'
      },
      confirmDisabled: true
    };

    // Bind methods
    ['onChange', 'onConfirm']
      .forEach(method => { this[method] = this[method].bind(this); });
  }

  onConfirm() {
    if (!at(this, 'state.confirmDisabled')) {
      window.location = `/my/order/details?storeOrderId=${at(this, 'state.orderField.value')}`;
    }
  }

  onChange(target) {
    const orderField = at(this, 'state.orderField') || EMPTY_OBJ_READ_ONLY;
    let newOrderField;
    let confirmDisabled;

    // Regex validation
    if (!target.value.match(orderField.validation)) {
      newOrderField = { ...orderField, error: ERROR_TEXT, value: target.value };
      confirmDisabled = true;
    } else {
      newOrderField = { ...orderField, value: target.value, error: '' };
      confirmDisabled = false;
    }

    this.setState({
      orderField: newOrderField,
      confirmDisabled
    });
  }

  render() {
    const orderField = at(this, 'state.orderField') || EMPTY_OBJ_READ_ONLY;
    return (
      <SearchByOrder
        onBlur={this.onBlur}
        onChange={this.onChange}
        onConfirm={this.onConfirm}
        value={orderField.value}
        confirmDisabled={this.state.confirmDisabled}
        error={orderField.error} />
    );
  }
}

export default SearchByOrderWrapper;

