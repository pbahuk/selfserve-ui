import React from 'react';
import PropTypes from 'prop-types';
import Styles from './pagination.css';

const Pagination = (props) => {
  const currentPage = props.page;
  const maxPages = Math.ceil(props.totalCount / props.batch);

  return (<div className={Styles.pagination}>
    {currentPage > 1 &&
      <span className={Styles.link} onClick={() => { props.action(false); }}> &lt; Previous </span>}
    <span>
      Showing {currentPage === 1 ? currentPage : (currentPage - 1) * props.batch + 1} -
      {Math.min(props.totalCount, ((currentPage) * props.batch))} of {props.totalCount}
    </span>
    {currentPage < maxPages &&
      <span className={Styles.link} onClick={() => { props.action(true); }}> Next &gt; </span>}
  </div>);
};

Pagination.propTypes = {
  page: PropTypes.number,
  totalCount: PropTypes.number,
  batch: PropTypes.number,
  action: PropTypes.func
};

export default Pagination;
