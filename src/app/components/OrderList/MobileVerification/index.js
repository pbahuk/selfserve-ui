import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Functional Imports.
import Styles from './mobileVerification.css';
import { gaEmitter } from '../../../utils/pageHelpers';

// React Imports.
import Icon from '../../Reusables/Icon';
import MyInput from '../../Reusables/Forms/MyInput';

class MobileVerification extends React.Component {
  constructor(props) {
    super(props);

    // Function Binds.
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);

    // Initial Data.
    const customerMobileNumber = at(window, '__myx_session__.mobile') || '';
    gaEmitter('profile', 'mobile verf card load', 'my order');
    if (customerMobileNumber) {
      gaEmitter('mobile verification', 'card load', 'prefill');
    }

    this.state = {
      type: customerMobileNumber ? 'EDIT' : 'ADD',
      form: {
        mobile: {
          value: customerMobileNumber,
          error: '',
          maxLength: 10,
          validation: /^[789]\d{9}$/,
          label: 'Mobile Number'
        }
      }
    };
  }

  handleChange(target) {
    const { form = {} } = this.state;

    form[target.name] = { ...form[target.name], value: target.value };
    this.setState({
      form
    });
  }

  handleFocus(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], error: '' };

    this.setState({
      form
    });
  }

  handleBlur(target) {
    const form = this.state.form;

    if (target.value === '') {
      form[target.name] = { ...form[target.name], error: 'Required' };
    } else if (!target.value.match(form[target.name].validation)) {
      form[target.name] = { ...form[target.name], error: 'Please provide a valid Mobile Number' };
    }

    this.setState({
      form
    });
  }

  handleSubmit() {
    const { form = {} } = this.state;
    let inValidValues = false;

    for (const key in form) {
      if (form.hasOwnProperty(key)) {
        if (form[key].value === '') {
          form[key].error = 'Required';
          inValidValues = true;
        } else if (!form[key].value.match(form[key].validation)) {
          form[key].error = 'Invalid Mobile Number';
          inValidValues = true;
        }
      }
    }

    if (inValidValues) {
      this.setState({
        form
      });
    } else {
      gaEmitter('profile', 'mobile verf click', 'my order');
      window.location = `/verification/${form.mobile.value}`;
    }
  }

  closeCard() {
    gaEmitter('mobile verification', 'card close', 'my order');
    this.props.closeVerificationCard();
  }


  render() {
    const { type, form = {} } = this.state;

    return (<div className={Styles.card}>
      <div className={Styles.heading}> Verify Your Mobile Number </div>
      <div className={Styles.subHeading}> Get timely order updates and special offers </div>
      {type === 'EDIT' ?
        (<div className={Styles.inputWrapper}>
          <div className={Styles.input}>
            <MyInput
              type="tel"
              customStyle={Styles.inputStyle}
              customised={false}
              name="mobile"
              label={form.mobile.label}
              maxLength={form.mobile.maxLength}
              errorMessage={form.mobile.error}
              value={form.mobile.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
          </div>
          <div className={Styles.verifyButtonv1} onClick={() => { this.handleSubmit(); }}> Verify </div>
        </div>) :
        (<div
          className={Styles.verifyButtonv2}
          onClick={() => {
            gaEmitter('Order List', 'Click on Verify');
            window.location = '/verification/';
          }}> Verify
          <Icon name="back" className={Styles.arrow} />
        </div>)
      }
      <div className={Styles.closeCard} onClick={() => { this.closeCard(); }}>
        <Icon name="clear" customStyle={{ fontSize: '14px', cursor: 'pointer' }} />
      </div>
    </div>);
  }
}

MobileVerification.propTypes = {
  closeVerificationCard: PropTypes.func
};

export default MobileVerification;
