import React from 'react';
import PropTypes from 'prop-types';
import styles from './loader.css';

class Loader extends React.Component {
  render() {
    if (this.props.show) {
      return (
        <div className={styles.loaderContainer}>
          <div className={styles.spinnerContainer}>
            <div className={styles.spinner} />
          </div>
        </div>
      );
    }
    return null;
  }
}

Loader.propTypes = {
  show: PropTypes.bool.isRequired
};

Loader.defaultProps = {
  show: false
};

export default Loader;
