import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './cancellableItem.css';
import { currencyValue, getCancellationRefundAmount, getImageUrl } from '../../../utils/pageHelpers';

// React Component Imports.
import Icon from '../../Reusables/Icon';

class Item extends React.Component {
  constructor(props) {
    super(props);
    this.renderQuantityComponent = this.renderQuantityComponent.bind(this);
    this.state = {
    };
  }

  renderQuantityComponent() {
    /**
     * Item Qunatity: Different Scenarios.
     * 1. Free Item:
     *    Free Item Quantity will be updated as the main item's quantity is updated.
     *    Hence, EventQuantity / Item Quantity.
     * 2. Quantity Ordered is greater than 1 for the item.
     *    a. For Order Level Cancellation, every thing will be cancelled.
     *    b. Item Level Cancellation:
     *        Counter Provided to increase/ decrease the quantity.
     *        Checks, in place to not call the props function if case is not feasible.
     * 3. Order Level Cancellation, ordered quantity === 1
     *    Displaying the original quantity that was ordered.
     */
    const { itemQuantityMapper, pageView, item = {} } = this.props;
    const itemEventQuantity = itemQuantityMapper[item.id].eventQuantity;
    const isFreeItem = at(item, 'flags.free.isFree') || false;
    let quantityComponent = null;

    if (isFreeItem) {
      quantityComponent = (<span> | Qty: {itemEventQuantity < item.quantity ? `${itemEventQuantity} / ${item.quantity}` : `${itemEventQuantity}`} </span>);
    } else if (item.quantity > 1 && (pageView === 'itemCancellation' || pageView === 'packetCancellation')) {
      quantityComponent = (<div className={Styles.quantitySelector}>
        <span> Qty: </span>
        <div className={Styles.counter}>
          <span
            className={`${Styles.counterButton} ${itemEventQuantity === 1 ? Styles.disabled : ''}`}
            onClick={() => {
              if (itemEventQuantity !== 1) { this.props.changeItemEventQuantity(item.id, '-'); }
            }}> - </span>
          <span className={Styles.quantityCounter}> {itemEventQuantity} </span>
          <span
            className={`${Styles.counterButton} ${itemEventQuantity === item.quantity ? Styles.disabled : ''}`}
            onClick={() => {
              if (itemEventQuantity < item.quantity) { this.props.changeItemEventQuantity(item.id, '+'); }
            }}> + </span>
        </div>
        <span className={Styles.quantityMessage}> / {item.quantity} item{item.quantity > 1 ? 's' : ''} </span>
      </div>);
    } else {
      quantityComponent = (<span> | Qty: {item.quantity} </span>);
    }

    return quantityComponent;
  }

  render() {
    const item = this.props.item || {};
    const { itemQuantityMapper } = this.props;

    const productImages = item.product.images[0] || {};
    let productImageUrl = productImages.secureSrc || '';
    productImageUrl = getImageUrl(productImageUrl, 125, 161);

    // Product Info.
    const brandName = at(item, 'product.brand.name') || '';
    const productDisplayName = at(item, 'product.name') || '';
    let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
    productName = productName.length > 30 ? `${productName.substr(0, 30)}...` : productName;
    const sizes = at(item, 'product.sizes') || [];
    const size = sizes.find((option) => option.skuId === at(item, 'product.skuId'));

    return (<div className={Styles.item}>
      <div className={Styles['product-info']}>
        {<div className={Styles.thumbnail}>
          <img src={productImageUrl} />
          {at(item, 'flags.free.isFree') && <div className={Styles.isFree} />}
        </div>}
        <div className={Styles.info}>
          <span className={Styles.brand}> {brandName} </span>
          <div className={Styles.productName}> {productName} </div>
          <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
          {this.renderQuantityComponent()}
          {!this.props.ppsDisabled && <div className={Styles.price}>
            <Icon name="rupee500" customStyle={{ fontSize: '14px', fontWeight: '500' }} />
            {currencyValue(getCancellationRefundAmount(item, itemQuantityMapper) / 100, false)}
          </div>}
        </div>
      </div>
    </div>);
  }
}

Item.propTypes = {
  pageView: PropTypes.string,
  item: PropTypes.object,
  itemQuantityMapper: PropTypes.object,
  changeItemEventQuantity: PropTypes.func,
  ppsDisabled: PropTypes.bool
};

export default Item;
