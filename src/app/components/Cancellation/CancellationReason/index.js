import React from 'react';
import PropTypes from 'prop-types';

// Functional Imports.
import Styles from './cancellationReason.css';
import { getCancellationRefundAmount, currencyValue, getDeviceData } from '../../../utils/pageHelpers';

// React Component Imports.
import MyTextArea from './../../Reusables/Forms/MyTextArea';
import LineDropdown from './../../Reusables/Forms/LineDropdown';
import Icon from '../../Reusables/Icon';

const CancellationReason = (props) => {
  const { form = {} } = props;
  const { cancellableItems = [], cancellationReasons = [] } = props;
  const { userAgent } = getDeviceData();

  return (<div>
    <div className={Styles.card}>
      <div className={Styles.heading}>
        Please tell the reason for cancellation*
      </div>
      <div className={Styles.reasonMessage}>
        Please tell us correct reason for cancellation. This information is only used to improve our service
      </div>
      <LineDropdown
        name="cancellationReason"
        data={cancellationReasons}
        expanded={!form.cancellationReason.value}
        value={form.cancellationReason.value}
        error={form.cancellationReason.error}
        selectRadioOption={props.selectRadioOption} />
      <MyTextArea
        name="comment"
        value={form.comment.value}
        placeHolder={form.comment.placeHolder}
        handleChange={props.handleChange}
        handleBlur={props.handleBlur}
        error={form.comment.error} />
    </div>
    <div className={`${Styles.card} ${userAgent.indexOf('MyntraRetailAndroid') !== -1 ? Styles.android : ''}`}>
      <div className={Styles.refundInformation}>
        {!props.ppsDisabled && <div className={Styles.refundAmount}>
          <span> Total Refund Amount: </span>
          <span className={Styles.heading}>
            <Icon name="rupee500" customStyle={{ fontSize: '15px', fontWeight: '500' }} />
            {currencyValue(getCancellationRefundAmount(cancellableItems, form.itemQuantityMapper) / 100, false)}
          </span>
        </div>}
        <div>
          Upon confirmation, refund amount will be credited to original modes of payment.
          {props.ppsDisabled && <div>Refund amount details will be sent to your email.</div>}
        </div>
      </div>
    </div>
  </div>);
};

CancellationReason.propTypes = {
  form: PropTypes.object,
  selectRadioOption: PropTypes.func,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  cancellableItems: PropTypes.array,
  cancellationReasons: PropTypes.array,
  ppsDisabled: PropTypes.bool
};

export default CancellationReason;
