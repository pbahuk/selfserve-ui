import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Style Related Imports
import Styles from './nonCancellableItems.css';
import ItemViews from './../../Reusables/ItemViews';

const NonCancellableItems = (props) => (<div className={Styles.card}>
  <div className={Styles.heading}>
    You cannot cancel following item:
  </div>
  {props.items.map((item, key) => <ItemViews
    key={key}
    item={item}
    view="nonActionable"
    reason={at(item, 'flags.cancellable.remark') || 'Item is not cancellable'} />
  )}
</div>);

NonCancellableItems.propTypes = {
  items: PropTypes.array
};

export default NonCancellableItems;
