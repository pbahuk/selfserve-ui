import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

import Styles from './confirmation.css';

// React Component Imports.
import ItemViews from './../../Reusables/ItemViews';
import Madalytics from '../../../utils/trackMadalytics';
import EmptyState from '../../EmptyState';

const Confirmation = (props) => {
  const { cancelledItems, nonCancelledItems, emptyStateEnabled } = props;
  if (cancelledItems.length > 0) {
    const itemId = at(cancelledItems[0], 'product.id');
    Madalytics.sendEvent('ScreenLoad', {
      'type': 'My Orders - Cancellation-done',
      'entity_type': 'order',
      'entity_id': itemId
    }, null, 'react');
  }

  return (<div>
    {cancelledItems.length > 0 && <div className={`${Styles.itemsCard} ${emptyStateEnabled ? '' : Styles.marginBottom}`}>
      <div className={Styles.heading}> The following items have been cancelled: </div>
      {cancelledItems.map((item, key) => <ItemViews key={key} view="confirmed" item={item} />)}
      <div className={Styles.information}>
        <span className={Styles.bold}> Please note: </span>
        <span> This does not affect other items in the order </span>
      </div>
    </div>}
    {nonCancelledItems.length > 0 && <div className={`${Styles.itemsCard} ${emptyStateEnabled ? '' : Styles.marginBottom}`}>
      <div className={Styles.heading}> Following items could not be cancelled: </div>
      {nonCancelledItems.map((item, key) => <ItemViews key={key} view="confirmed" item={item} />)}
    </div>}
    {(!cancelledItems.length || !emptyStateEnabled) && <div className={Styles.button} onClick={() => { window.location = '/my/orders'; }}> Done </div>}
    {cancelledItems.length > 0 && emptyStateEnabled && <EmptyState product={cancelledItems[0].product} />}
  </div>);
};

Confirmation.propTypes = {
  emptyStateEnabled: PropTypes.bool,
  cancelledItems: PropTypes.array,
  nonCancelledItems: PropTypes.array
};

export default Confirmation;
