import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports
import Styles from './cancellableItems.css';
import CancellableItem from './../CancellableItem';
import { getCancellationRefundAmount, currencyValue } from '../../../utils/pageHelpers';

// React Component Imports.
import Icon from '../../Reusables/Icon';

let currentStoreOrderNo;

const StoreOrderWrapper = ({ item: { displayStoreOrderId }, children }) => {
  const block = (<div className={Styles.itemBlock}>
    {displayStoreOrderId !== currentStoreOrderNo ? <div><span className={Styles.displayOrderNo}>Order No: </span>{displayStoreOrderId}</div> : null}
    {children}
  </div>);

  currentStoreOrderNo = displayStoreOrderId;
  return block;
};

class CancellableItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  renderItems(items) {
    return items.map((item, key) =>
      <StoreOrderWrapper key={key} item={item}>
        <CancellableItem
          key={key}
          pageView={this.props.pageView}
          itemQuantityMapper={this.props.itemQuantityMapper}
          changeItemEventQuantity={this.props.changeItemEventQuantity}
          ppsDisabled={this.props.ppsDisabled}
          item={item} />
      </StoreOrderWrapper>);
  }

  render() {
    const items = this.props.items || [];
    const itemQuantityMapper = this.props.itemQuantityMapper;

    return (<div className={Styles.card}>
      <div className={Styles.heading}>
        You are cancelling {items.length} item{items.length > 1 ? 's' : ''} from the order:
      </div>
      {!this.props.ppsDisabled && <div className={Styles.subHeading}>
        Refund Total: <Icon name="rupee500" customStyle={{ fontSize: '13px', fontWeight: '500' }} />
        {currencyValue(getCancellationRefundAmount(items, itemQuantityMapper) / 100, false)}
      </div>}
      {this.renderItems(this.props.items)}
      <div className={Styles.helpMessage}>
        <span> Any myntra credit balance applied on this order will be reinstated. </span>
        <div> For more information, please view our
          <a className={Styles.link} href="/faqs#cancel"> cancellation policy </a>
        </div>
      </div>
    </div>);
  }
}

CancellableItems.propTypes = {
  items: PropTypes.array,
  pageView: PropTypes.string,
  itemQuantityMapper: PropTypes.object,
  changeItemEventQuantity: PropTypes.func,
  ppsDisabled: PropTypes.bool
};

StoreOrderWrapper.propTypes = {
  item: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ])
};

export default CancellableItems;
