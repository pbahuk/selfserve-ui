import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import async from 'async';

// Style Related Imports.
import Styles from './cancellationForm.css';

// Specific to this page.
import OrderInfo from '../../Reusables/OrderInfo';
import PacketCancellationInfo from '../PacketCancellationInfo';
import CancellableItems from '../CancellableItems';
import NonCancellableItems from '../NonCancellableItems';
import CancellationReason from '../CancellationReason';

// Service Calls.
import { cancelOrder, cancelItems, cancelPacket } from '../../../utils/services/oms';
import { getUidx, getDeviceData } from '../../../utils/pageHelpers';
import features from '../../../utils/features';

const ppsDisabled = features('mymyntra.pps.disable') === 'true';

/* CancellationForm: Component.
*** Holds the complete context of the form (Entries to be sent in the payload)
*** CancellableItems, NonCancellableItems, NonCancellableItems, CancellationReason, Buttons all comes under its context.
*** Can handle Order Cancellation, Item Level Cancellation (condition: pageView).
*** Order Cancellation:
***     1. Passing on the orderId in the request payload.
***     2. All the cancellableItems will be cancelled, with quantities ordered or quantity not cancelled.
***     3. Reasons: Order Level Cancellation Reason should be populated, handled by CancellationReason Component.
*** Item Cancellation:
***     1. Item Level Cancellation Reasons to be displayed.
***     2. Single Quantity Cancellation:
***         a. CancellableItems will render the item to be cancelled.
***         b. Quantity is always sent as 1, as set in form: quantity (1) by default. It will not be incremented from anywhere
***            if changeQuantityCounter is not called.
***     3. Multi Quantity Cancellation:
***         a. CancellableItems will render the item to be cancelled.
***         b. User can cancel more than 1 item, form state has to be updated. Hence, the function changeQuantityCounter
***             is only available to user if quantity is more than 1.
***         c. changeQuantityCounter: CancellationForm (form context) -> CancellableItems -> CancellableItem.
*** CancellationReason:
***     1. Form Context is passed to CancellationReason Component and it has to update the original context CancellationForm
***       about the change in the form's state.
***     2. CancellationForm (form context) -> CancellationReason -> MyTextArea , LineDropDown.
*** Buttons:
***     1. Cancel: Redirect to /my/orders url, will have to make it redirect to previous page. (TODO)
***     2. Confirm:
***         a. Calls handleFormSubmit: Validation of all the Required fields (key: required).
***         b. If validation clears, Initiates the cancellation process (pageView === 'orderView' ? Order : Item)
*/
class CancellationForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.selectRadioOption = this.selectRadioOption.bind(this);
    this.changeItemEventQuantity = this.changeItemEventQuantity.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.initiateCancellation = this.initiateCancellation.bind(this);

    // Setting Some states for the form.
    const pageView = this.props.pageView;
    const itemQuantityMapper = {};
    let freeItemPresentInView = false;
    let freeItemInOrder = false;
    this.props.cancellableItems.forEach((item) => {
      if (at(item, 'flags.free.isFree')) {
        freeItemInOrder = item;
      }
      itemQuantityMapper[item.id] = {
        eventQuantity: (pageView === 'itemCancellation' || pageView === 'packetCancellation') && item.quantity > 1 ? 1 : item.quantity,
        maxQuantity: item.quantity
      };
    });

    if (freeItemInOrder && (pageView === 'itemCancellation' || pageView === 'packetCancellation')) {
      const freeOriginalItem = this.props.cancellableItems.find((item) => item.id === at(freeItemInOrder, 'flags.free.associatedItem'));
      if (freeOriginalItem) {
        freeItemPresentInView = freeItemInOrder.id;
      }
    }

    this.state = {
      actionError: '',
      formInactive: false,
      formFilled: false,
      form: {
        itemQuantityMapper,
        freeItemPresentInView,
        quantity: {
          value: 1
        },
        cancellationReason: {
          value: '',
          displayValue: '',
          error: '',
          required: true
        },
        comment: {
          value: '',
          placeHolder: 'Additional Comments',
          validation: /^[a-zA-Z0-9!@#$%\^&* )(+=.,_-]*$/,
          error: '',
          required: false
        }
      }
    };
  }

  handleChange(target) {
    const form = this.state.form;
    const name = target.name;
    form[name].value = target.value;
    form[name].error = '';

    this.setState({
      form
    });
  }

  handleBlur(target) {
    const form = this.state.form;
    const name = target.name;
    form[name].value = target.value;

    if (!target.value.match(form[name].validation)) {
      form[name].error = 'Invalid Input';
    }

    this.setState({
      form
    });
  }

  selectRadioOption(target) {
    const form = this.state.form;
    const name = target.name;
    form[name].value = target.value;
    form[name].error = '';

    this.setState({
      form,
      formFilled: true
    });
  }

  changeItemEventQuantity(itemId, action) {
    const { form } = this.state;
    const itemQuantityMapper = form.itemQuantityMapper || {};
    let changingEventQuantity = true;
    const currentEventQuantity = itemQuantityMapper[itemId].eventQuantity;
    const maxQuantity = itemQuantityMapper[itemId].maxQuantity;

    if (action === '-' && currentEventQuantity === 1) {
      changingEventQuantity = false;
    } else if (action === '+' && currentEventQuantity === maxQuantity) {
      changingEventQuantity = false;
    } else {
      itemQuantityMapper[itemId].eventQuantity = action === '-' ? currentEventQuantity - 1 : currentEventQuantity + 1;
      if (form.freeItemPresentInView) {
        itemQuantityMapper[form.freeItemPresentInView].eventQuantity = action === '-' ? currentEventQuantity - 1 : currentEventQuantity + 1;
      }
    }

    form.itemQuantityMapper = itemQuantityMapper;
    if (changingEventQuantity) {
      this.setState({
        form
      });
    }
  }

  handleFormSubmit() {
    const { form } = this.state;
    let { actionError } = this.state;
    let invalidValues = false;

    for (const key in form) {
      if (form.hasOwnProperty(key)) {
        if (!form[key].value && form[key].required) {
          form[key].error = 'Required';
          invalidValues = true;
        } else if (form[key].error) {
          actionError = 'Please check the entries, some incorrect values are present';
          invalidValues = true;
        }
      }
    }

    if (invalidValues) {
      this.setState({
        form,
        actionError
      });
    } else {
      this.setState({
        actionError: ''
      }, () => { this.initiateCancellation(); });
    }
  }

  initiateCancellation() {
    const { form } = this.state;
    const orderId = this.props.order.id;
    const packet = this.props.packet || {};
    const cancellableItems = this.props.cancellableItems || [];
    const itemQuantityMapper = form.itemQuantityMapper || {};
    let postData = {};
    let actionError = '';
    let formInactive = false;

    // Making the form Inactive.
    this.setState({
      formInactive: true
    });

    if (this.props.pageView === 'orderCancellation') {
      // Order Level Cancellation.
      postData = {
        _storeOrderId: this.props.order.storeOrderId,
        orderUpdateEntry: {
          orderId,
          cancellationReasonId: form.cancellationReason.value,
          userLogin: getUidx(),
          comment: form.comment.value
        }
      };

      cancelOrder(postData, (err, res) => {
        formInactive = false;
        if (err) {
          console.error('[OMS Error: Cancel Order]', err);
          actionError = 'Something went wrong! Please try again';
        } else if (at(res, 'body.orderResponse.status.statusType') === 'ERROR') {
          actionError = 'You cannot cancel this item/order at this point as it is ready to be shipped.';
        } else {
          const cancelledItems = this.props.cancellableItems.map((item) => ({ ...item, ...{ quantity: itemQuantityMapper[item.id].eventQuantity } }));
          this.props.renderConfirmationComponent(cancelledItems, []);
        }
        this.setState({
          actionError,
          formInactive
        });
      });
    } else if (this.props.pageView === 'itemCancellation') {
      /** Item Level Cancellation:
       *  If the code comes here, means their is only item level cancellation.
       */
      const payloads = cancellableItems.map((cancellableItem) => {
        console.log('Construct payload');
        return {
          _storeOrderId: this.props.order.storeOrderId,
          releaseUpdateEntry: {
            orderId,
            releaseId: cancellableItem.orderReleaseId,
            cancellationReasonId: form.cancellationReason.value,
            userId: getUidx(),
            comment: form.comment.value,
            cancellationType: 'CCR',
            linesToBeCancelled: [
              {
                id: cancellableItem.id,
                quantity: itemQuantityMapper[cancellableItem.id].eventQuantity
              }
            ]
          }
        };
      });

      async.map(payloads, (payload, callback) => {
        callback(null, cancelItems(payload));
      }, (err, res) => {
        formInactive = false;
        if (err) {
          console.error('[OMS Error: Cancel Items]', err);
          actionError = 'Something went wrong! Please try again';
        } else if (at(res, '0.body.orderReleaseResponse.status.statusType') === 'ERROR') {
          actionError = 'You cannot cancel this item/order at this point as it is ready to be shipped.';
        } else {
          const cancelledItems = this.props.cancellableItems.map((item) => ({ ...item, ...{ quantity: itemQuantityMapper[item.id].eventQuantity } }));
          this.props.renderConfirmationComponent(cancelledItems, []);
        }
        this.setState({
          actionError,
          formInactive
        });
      });


      // Original Construct.
      // const lines = this.props.cancellableItems.map((item) => ({ id: item.id, quantity: itemQuantityMapper[item.id].eventQuantity }));

      // postData = {
      //   _storeOrderId: this.props.order.storeOrderId,
      //   releaseUpdateEntry: {
      //     orderId,
      //     releaseId: cancellableItems[0].orderReleaseId,
      //     cancellationReasonId: form.cancellationReason.value,
      //     userId: getUidx(),
      //     comment: form.comment.value,
      //     cancellationType: 'CCR',
      //     linesToBeCancelled: lines
      //   }
      // };

      // cancelItems(postData, (err, res) => {
      //   formInactive = false;
      //   if (err) {
      //     console.error('[OMS Error: Cancel Items]', err);
      //     actionError = 'Something went wrong! Please try again';
      //   } else if (at(res, 'body.orderReleaseResponse.status.statusType') === 'ERROR') {
      //     actionError = 'You cannot cancel this item/order at this point as it is ready to be shipped.';
      //   } else {
      //     const cancelledItems = this.props.cancellableItems.map((item) => ({ ...item, ...{ quantity: itemQuantityMapper[item.id].eventQuantity } }));
      //     this.props.renderConfirmationComponent(cancelledItems, []);
      //   }
      //   this.setState({
      //     actionError,
      //     formInactive
      //   });
      // });
    } else {
      postData = {
        packetId: Number.parseFloat(packet.id),
        actionCode: 'CANCEL_PACKET',
        cancellationReasonId: Number.parseInt(form.cancellationReason.value, 10),
        comment: 'Cancellation: Created by MyMyntra',
        doRefund: true,
        doNotify: true
      };

      cancelPacket(postData, (err, res) => {
        if (err) {
          console.error('[OMS Error: Cancel Packet]', err);
          actionError = 'You cannot cancel this item/order at this point as it is ready to be shipped.';
        } else {
          console.log('RES status:', res.statusCode);
          const cancelledItems = this.props.cancellableItems.map((item) => ({ ...item, ...{ quantity: itemQuantityMapper[item.id].eventQuantity } }));
          this.props.renderConfirmationComponent(cancelledItems, []);
        }
        this.setState({
          actionError,
          formInactive
        });
      });
    }
  }

  render() {
    const { pageView, order, packet, cancellableItems, nonCancellableItems, cancellationReasons } = this.props;
    const { form, formInactive, actionError } = this.state;
    const { userAgent } = getDeviceData();

    return (<div>
      <OrderInfo order={order} />
      {pageView === 'packetCancellation' &&
        <PacketCancellationInfo
          packet={packet}
          items={cancellableItems} />}
      {cancellableItems.length > 0 &&
        <CancellableItems
          items={cancellableItems}
          pageView={pageView}
          ppsDisabled={ppsDisabled}
          itemQuantityMapper={form.itemQuantityMapper}
          changeItemEventQuantity={this.changeItemEventQuantity} />}
      {nonCancellableItems.length > 0 &&
        <NonCancellableItems
          items={nonCancellableItems} />}
      {cancellableItems.length > 0 &&
        <CancellationReason
          form={form}
          handleChange={this.handleChange}
          handleBlur={this.handleBlur}
          ppsDisabled={ppsDisabled}
          selectRadioOption={this.selectRadioOption}
          cancellableItems={cancellableItems}
          cancellationReasons={cancellationReasons} />}
      {cancellableItems.length === 0 &&
        <a href="/my/orders" className={Styles.orderRedirectButton}> Go to My Orders </a>}
      {cancellableItems.length !== 0 &&
        <div className={`${Styles.buttons} ${userAgent.indexOf('MyntraRetailAndroid') !== -1 ? Styles.android : ''}`}>
          {actionError && <div className={Styles.actionError}> {actionError} </div>}
          <div className={Styles.button}>
            <div onClick={() => { window.history.back(); }} > Cancel </div>
          </div>
          <div
            className={`${Styles.button} ${!this.state.formFilled ? Styles.disabled : ''}`}
            onClick={() => { if (this.state.formFilled) { this.handleFormSubmit(); } }}> Confirm
            <div className={formInactive ? Styles.buttonLoader : ''} />
          </div>
        </div>}
    </div>);
  }
}

CancellationForm.propTypes = {
  pageView: PropTypes.string,
  order: PropTypes.object,
  packet: PropTypes.object,
  cancellationReasons: PropTypes.array,
  cancellableItems: PropTypes.array,
  nonCancellableItems: PropTypes.array,
  renderConfirmationComponent: PropTypes.func
};

export default CancellationForm;
