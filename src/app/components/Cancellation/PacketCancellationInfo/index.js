import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Style Related Imports.
import Styles from './packetCancellationInfo.css';

class PacketCancellationInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { items = [], packet: { items: packetItems = [] } = {} } = this.props;
    const totalRefund = packetItems.reduce((amount, item) =>
      (amount += get(item, 'payments.amount') / 100), 0);

    return (<div>
      {items.length > 1 ? <div className={Styles.packetCancellationInfo}>
        <div className={Styles.boldText}> You'll have to cancel {items.length} items as they're all packed together. Please review </div>
        <div className={Styles.infoText}>
          Following items are
          <span className={Styles.boldText}> now packed </span>
          together. At this point, you can cancel entire package and not individual items.
        </div>
        <div className={Styles.boldText}> Total: Rs. {totalRefund}</div>
      </div> : null}
    </div>);
  }
}

PacketCancellationInfo.propTypes = {
  items: PropTypes.array,
  packet: PropTypes.object
};

export default PacketCancellationInfo;
