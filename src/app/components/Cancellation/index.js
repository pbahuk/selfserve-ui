import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import PageStyles from './../../resources/page.css';

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';
import CCintervention from '../Reusables/CCintervention';
import Madalytics from '../../utils/trackMadalytics';

// Specific to this page.
import CancellationForm from './CancellationForm';
import Confirmation from './Confirmation';


/* Cancellation: Component (Completed).
*** Wrapper component to hold the Cancellation form and Confirmation Screen, after cancealltion is completed.
*** Loads the initial data and checks for if any error are present.
*** Also, segregates the items into cancellable and nonCancellable items.
*** Error, Confirmation, CancellationForm based on the different states.
*/
class Cancellation extends React.Component {
  constructor(props) {
    super(props);
    this.renderConfirmationComponent = this.renderConfirmationComponent.bind(this);

    this.state = {
      loading: true,
      pageError: false,
      pageView: '',
      order: {},
      packet: {},
      cancelledItems: [],
      nonCancelledItems: [],
      cancellationReasons: []
    };
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    let pageError = false;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }
    const order = at(serviceResponse, 'response.order');
    const cancellationReasons = at(serviceResponse, 'response.reasons');
    const packet = at(serviceResponse, 'response.packet');
    const type = at(serviceResponse, 'response.type');
    this.emptyStateEnabled = at(serviceResponse, 'emptyStateEnabled') === 'enabled';

    this.setState({
      loading: false,
      pageError,
      order,
      packet,
      pageView: type,
      cancellationReasons
    });
  }

  componentDidMount() {
    if (!this.state.cancelledItems.length) {
      const order = this.state.order || {};
      const itemId = Number.parseInt(this.props.location.query.itemId, 10);
      Madalytics.sendEvent('ScreenLoad', {
        'type': 'My Orders - Cancel-page',
        'entity_type': 'order',
        'entity_id': itemId,
        'entity_order_id': at(order, 'storeOrderId')
      }, null, 'react');
    }
  }

  sendMadalyticsEvent() {
    const order = this.state.order || {};
    Madalytics.sendEvent('widgetClick', {
      'type': 'My Orders - No-Cancel-call',
      'entity_type': 'order',
      'entity_id': Number.parseInt(this.props.location.query.itemId, 10),
      'entity_order_id': at(order, 'storeOrderId')
    }, null, 'react');
  }

  renderConfirmationComponent(cancelledItems, nonCancelledItems) {
    cancelledItems = cancelledItems.length ? cancelledItems : [cancelledItems];
    this.setState({
      cancelledItems,
      nonCancelledItems
    }, () => window.scrollTo(0, 0));
  }

  render() {
    let page = null;
    const order = this.state.order || {};
    const packet = this.state.packet || {};
    const items = at(order, 'items') || [];
    const cancellableItems = [];
    const nonCancellableItems = [];
    const itemId = Number.parseInt(this.props.location.query.itemId, 10);

    if (this.state.pageView === 'packetCancellation') {
      if (at(packet, 'flags.cancellable.isCancellable')) {
        cancellableItems.push(...packet.items);
      } else {
        const item = packet.items.find((eachItem) => Number.parseInt(eachItem.id, 10) === itemId);
        nonCancellableItems.push(item);
      }
    } else if (this.state.pageView === 'itemCancellation') {
      const item = items.find((eachItem) => eachItem.id === itemId);
      const freeItem = items.find((eachItem) => (at(eachItem, 'flags.free.isFree') && at(eachItem, 'flags.free.associatedItem') === itemId));

      if (at(item, 'flags.cancellable.isCancellable')) {
        cancellableItems.push(item);
        /** Special case of free Items, pushing the free item associated with the selected item
          * into the cancellableItems array.
        */
        if (freeItem && at(freeItem, 'flags.cancellable.isCancellable')) {
          cancellableItems.push(freeItem);
        }
      } else {
        nonCancellableItems.push(item);
      }
    } else {
      items.forEach((item) => {
        if (at(item, 'flags.cancellable.isCancellable')) {
          cancellableItems.push(item);
        } else {
          nonCancellableItems.push(item);
        }
      });
    }

    if (this.state.loading) {
      page = <div className={PageStyles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.component}>
          <ErrorCard
            message="Something Went Wrong"
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else if (this.state.cancelledItems.length) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <Confirmation
            emptyStateEnabled={this.emptyStateEnabled}
            cancelledItems={this.state.cancelledItems}
            nonCancelledItems={this.state.nonCancelledItems} />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <div onClick={this.sendMadalyticsEvent}><CCintervention type={'cancelling'} /></div>
          <CancellationForm
            pageView={this.state.pageView}
            order={this.state.order}
            packet={this.state.packet}
            cancellationReasons={this.state.cancellationReasons}
            cancellableItems={cancellableItems}
            nonCancellableItems={nonCancellableItems}
            renderConfirmationComponent={this.renderConfirmationComponent} />
        </div>
      </div>);
    }
    return page;
  }
}

Cancellation.propTypes = {
  location: PropTypes.object
};

export default Cancellation;
