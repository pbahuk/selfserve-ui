import React from 'react';
import PropTypes from 'prop-types';
import Styles from './editStatus.css';
import Icon from '../../Reusables/Icon';
import SVGImages from '../../Reusables/SVGImages';


class EditStatus extends React.Component {
  constructor(props) {
    super(props);
    this.initiateAction = this.initiateAction.bind(this);

    this.state = {
      loading: false
    };
  }

  initiateAction() {
    this.setState({
      loading: true
    }, () => {
      this.props.action();
    });
  }

  render() {
    const { componentType, message, secondaryMessage, actionName } = this.props;
    return (<div className={Styles.card}>
      {
        componentType === 'success' &&
          <Icon name="success_tick" className={Styles.icon} />
      }
      {
        componentType === 'expired' &&
          <SVGImages name="tokenExpired" className={Styles.image} />
      }
      <div className={Styles.primaryMessage}> {message}</div>
      {secondaryMessage && <div className={Styles.secondaryMessage}> {secondaryMessage} </div>}
      <div className={Styles.showButton} onClick={this.initiateAction}> {actionName}
        {
          this.state.loading && <div className={Styles.buttonLoader} />
        }
      </div>
    </div>);
  }
}

EditStatus.propTypes = {
  message: PropTypes.string,
  secondaryMessage: PropTypes.string,
  actionName: PropTypes.string,
  action: PropTypes.func,
  componentType: PropTypes.String
};

export default EditStatus;
