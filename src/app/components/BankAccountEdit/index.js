import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Reusable Components for page.
import SideBar from '../Reusables/SideBar';
import Error from '../Reusables/Error';
import EditStatus from './EditStatus';

// Global Styles
import PageStyles from './../../resources/page.css';

// Service Functions.
import { getAccDetails } from '../../utils/services/payments';

// Page Specific Components.
import BankAccountForm from './../Reusables/BankAccountForm';

const tokenExpiredCode = 987;


class BankAccountEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      pageError: false,
      bankAccounts: [],
      bankAccountFormActive: false,
      tokenExpired: false,
      tokenAccountSaved: false
    };
    this.updateBankAccountProcessComplete = this.updateBankAccountProcessComplete.bind(this);
  }

  componentWillMount() {
    const tokenResponse = get(window, '__myx.tokenResponse.response', null);
    let update = {
      loading: false,
      pageError: true
    };
    if (tokenResponse) {
      const status = get(tokenResponse, 'status');
      const tokenEntry = get(tokenResponse, 'tokenDetailsEntries[0]');

      if (get(status, 'statusType', '').toLowerCase() === 'success' && tokenEntry) {
        const neftId = get(tokenEntry, 'value.neftAccountId');
        if (neftId) {
          update = null;
          getAccDetails(neftId, this.getAccountDetailsCallback);
        }
      } else {
        console.log('Invalid Token');
        if (get(status, 'statusCode', null) === tokenExpiredCode) {
          update = {
            loading: false,
            tokenExpired: true
          };
        }
      }
    }
    if (update) {
      this.setState(update);
    }
  }

  getAccountDetailsCallback = (err, res) => {
    let update = {
      loading: false
    };
    try {
      if (err) {
        console.log('Errr : ', err);
        update = {
          loading: false,
          pageError: true
        };
      } else {
        const account = get(res, 'body.account', '');
        delete account.login;
        account.tokenId = this.props.location.query.tokenId; // this.props.location.query.tokenId
        if (account) {
          update.editAcc = account;
          update.bankAccountFormActive = true;
          console.log(account);
        }
      }
    } catch (e) {
      update = {
        loading: false,
        pageError: true
      };
    }
    this.setState(update);
    this.openBankAccountForm(update);
  }

  openBankAccountForm = (update) => {
    document.body.style.overflow = 'hidden';
    this.setState(update);
  }

  closeBankAccountForm = () => {
    document.body.style.overflow = 'auto';
    window.location = '/my/orders';
  }

  updateBankAccountProcessComplete() {
    this.setState({
      tokenAccountSaved: true,
      bankAccountFormActive: false
    });
  }

  render() {
    let page = null;
    const { tokenExpired, tokenAccountSaved, bankAccountFormActive } = this.state;
    if (this.state.loading) {
      page = (<div className={PageStyles.page}>
        <SideBar active="/my/bankaccounts" />
        <div className={PageStyles.fullWidthComponent}>
          <div className={PageStyles.loader} />
        </div>
      </div>);
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <SideBar active="my/bankaccounts" />
        <div className={PageStyles.component}>
          <Error
            message="Something Went Wrong"
            actionName="Retry"
            action={location.reload} />
        </div>
      </div>);
    } else {
      page = (<div className={`${PageStyles.page} ${tokenExpired || tokenAccountSaved ? PageStyles.white : ''}`}>
        <SideBar active="my/bankaccounts" />
        <div className={PageStyles.component}>
        {
          tokenExpired &&
            <EditStatus
              message="Link Expired!"
              secondaryMessage="The link has been expired, if you still need assistance please contact us."
              actionName="Contact Us"
              componentType="expired"
              action={() => { window.location = '/contactus'; }} />
        }
        {
          tokenAccountSaved &&
            <EditStatus
              message="Account details has been updated"
              secondaryMessage="Your refund will be processed in this updated bank account. It might take 5 to 7 days based on the bank working hours."
              actionName="Shop More"
              componentType="success"
              action={() => { window.location = '/'; }} />
        }
        </div>
        {
          bankAccountFormActive &&
            <BankAccountForm
              cancelAction={this.closeBankAccountForm}
              confirmAction={this.closeBankAccountForm}
              accountData={this.state.editAcc}
              updateBankAccountProcessComplete={this.updateBankAccountProcessComplete}
              mode="EDIT" />
        }
      </div>);
    }

    return page;
  }
}

BankAccountEdit.propTypes = {
  location: PropTypes.any
};

export default BankAccountEdit;
