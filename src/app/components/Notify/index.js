import React from 'react';
import at from 'v-at';
import styles from './notify.css';
import assign from 'lodash/assign';

class Notify extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      type: 'default',
      message: ''
    };
    this.hide = this.hide.bind(this);
  }

  getMessage() {
    if (this.state.type === 'error') {
      return (
        <div className={styles['error-message']}>
          <span className={styles.icon}>!</span>
          <p className={styles['icon-text']}>{this.state.message}</p>
        </div>
      );
    }
    let thumbnailHtml = null;
    let buttonHtml = null;
    let messageHtml = this.state.message;
    if (this.state.thumbnail) {
      thumbnailHtml = <img className={styles.thumbnail} src={this.state.thumbnail} />;
      messageHtml = <p className={styles['thumbnail-text']}>{this.state.message}</p>;
    }
    if (this.state.action) {
      buttonHtml = <button onClick={this.state.action.handler} className={styles.action}> {this.state.action.name}</button>;
    }
    return (
      <div className={styles['info-message']}>
        {thumbnailHtml}
        {messageHtml}
        {buttonHtml}
      </div>
    );
  }

  info(config) {
    const state = {
      type: 'info',
      show: true,
      thumbnail: null,
      position: null,
      action: null
    };
    assign(state, config);
    this.setState(state);
    setTimeout(() => {
      this.hide();
    }, 4000);
  }

  success(message) {
    this.setState({
      type: 'success',
      show: true,
      thumbnail: null,
      position: null,
      action: null,
      message
    });
  }

  error(message) {
    this.setState({
      type: 'error',
      show: true,
      thumbnail: null,
      position: null,
      action: null,
      message
    });
    setTimeout(() => {
      this.hide();
    }, 4000);
  }

  hide() {
    this.setState({
      show: false,
      thumbnail: null,
      position: null,
      action: null
    });
  }

  render() {
    if (at(this, 'state.show')) {
      let notifyClasses = styles[this.state.type];
      if (this.state.position === 'right') {
        notifyClasses = `${notifyClasses} ${styles['pull-right']}`;
      }
      if (this.state.thumbnail) {
        notifyClasses = `${notifyClasses} ${styles['text-left']}`;
      }
      return (
        <div className={styles.container} onClick={this.hide}>
          <div className={notifyClasses}>
            {this.getMessage()}
          </div>
        </div>
      );
    }
    return null;
  }
}

Notify.propTypes = {
  children: React.PropTypes.object,
  isPersistent: React.PropTypes.bool
};

export default Notify;
