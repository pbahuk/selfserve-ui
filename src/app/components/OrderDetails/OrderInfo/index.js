import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Functional Imports.
import Styles from './orderInfo.css';
import { getDate, currencyValue, insertCharInString, getPaymentMode } from '../../../utils/pageHelpers';
import features from '../../../utils/features';
import instrumentsMapping from '../../../resources/data/instrumentsMapping';

// React Components Import
import PriceBreakUp from '../../Reusables/PriceBreakUp';
import Icon from '../../Reusables/Icon';
import Address from '../Address';

const ppsDisabledFG = features('mymyntra.pps.disable') === 'true';
const ICON_STYLE = { fontSize: '16px', marginRight: '4px', verticalAlign: 'middle', color: '#535766' };

class OrderInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  render() {
    const order = this.props.order || {};
    const isGiftOrder = at(order, 'type') && at(order, 'type').toLowerCase() === 'giftcard' || false;
    const items = at(order, 'items') || [];
    let actualPrice = 0;
    items.forEach((item) => { actualPrice += item.unitPrice * item.quantity; });
    let paymentInstruments = getPaymentMode(order.payments);
    paymentInstruments = paymentInstruments.map((instrument) => instrumentsMapping[instrument]);
    return (<div className={Styles['order-details']}>
      <div className={Styles.block}>
        <div>
          <span className={Styles.label}> Placed On: </span>
          <span className={Styles.value}> {getDate(new Date(Number.parseInt(order.createdOn, 10)))} </span>
        </div>
        <div>
          <span className={Styles.label}> Order No: </span>
          <span className={Styles.value}> {insertCharInString(order.storeOrderId, '-', 7)} </span>
        </div>
        <div className={Styles.priceInfo}>
          <span className={Styles.label}>Price Details: </span>
          {!ppsDisabledFG ?
            <PriceBreakUp
              actualPrice={actualPrice}
              paymentsInfo={at(order, 'payments')} /> :
            <span className={Styles.price}>
              <Icon name="rupee500" customStyle={{ fontSize: '14px' }} /> {currencyValue(at(order, 'payments.amount') / 100, false)}
            </span>}
        </div>
      </div>
      <div className={Styles.block}>
        <div className={Styles.heading}>Updates sent to:</div>
        {at(order, 'address.user.mobile') ?
          <div className={Styles.customerInfo}>
            <Icon name="call" customStyle={ICON_STYLE} />
            <span>{at(order, 'address.user.mobile')}</span>
          </div> :
          null}
        <div className={Styles.customerInfo}>
          <Icon name="email" customStyle={ICON_STYLE} />
          <span>{at(order, 'address.user.email') || at(window, '__myx_session__.email')}</span>
        </div>
      </div>
      {!isGiftOrder && <Address
        order={order}
        address={at(order, 'address')}
        addressChangeable={at(order, 'flags.changeAddress.isAddressChangeable')} />}
      {paymentInstruments.length > 0 && <div className={Styles.block}>
        <div className={Styles.heading}> Payment Mode: </div>
        <div> {paymentInstruments.join(' | ')} </div>
      </div>}
    </div>);
  }
}
OrderInfo.propTypes = {
  order: PropTypes.object
};

export default OrderInfo;
