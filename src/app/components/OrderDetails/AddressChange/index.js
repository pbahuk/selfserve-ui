import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

// Functional Imports.
import Styles from './newAddressChange.css';
import { checkServiceability } from '../../../utils/services/serviceability';
import { updateOrderAddress } from '../../../utils/services/oms.js';
import { getUidx, getDeviceData } from '../../../utils/pageHelpers';

// React Component Imports.
import AddAddressModal from './../../Reusables/AddAddressModal';
import AddressAccordian from './../../Reusables/AddressAccordian';

import classnames from 'classnames/bind';

const bindClass = classnames.bind(Styles);

class AddressChange extends React.Component {
  constructor(props) {
    super(props);
    this.renderAddresses = this.renderAddresses.bind(this);
    this.checkServiceability = this.checkServiceability.bind(this);
    this.updateAddress = this.updateAddress.bind(this);
    this.closeAddressModal = this.closeAddressModal.bind(this);
    this.openAddAddressModal = this.openAddAddressModal.bind(this);
    this.editAddress = this.editAddress.bind(this);
    this.deleteAddress = this.deleteAddress.bind(this);

    this.state = {
      addAddressModal: false,
      addressModalMode: 'add',
      error: '',
      activeAddress: {
        id: (props.addresses[0] || {}).id,
        serviceable: 'inProgress'
      }
    };
  }

  componentDidMount() {
    this.checkServiceability(at(this, 'props.addresses.0'), { initialRequest: true });
  }

  componentWillReceiveProps(props) {
    this.checkServiceability(at(props, 'addresses.0'), { initialRequest: true });
  }

  openAddAddressModal(mode = 'add') {
    this.setState({
      addAddressModal: true,
      addressModalMode: mode
    });
  }

  closeAddressModal() {
    this.setState({
      addAddressModal: false
    });
  }

  editAddress() {
    this.openAddAddressModal('edit');
  }

  checkServiceability(address = {}, options = {}) {
    if (at(this, 'state.activeAddress.id') !== address.id || options.initialRequest) {
      const order = this.props.order;
      const isExchangeOrder = at(order, 'orderType') === 'ex';
      const serviceType = isExchangeOrder ? 'EXCHANGE' : 'DELIVERY';

      const data = {
        order,
        serviceType,
        pincode: at(address, 'pincode'),
        items: at(order, 'items'),
        shippingMethod: 'NORMAL'
      };

      checkServiceability(data, 'DELIVERY', (err, res) => {
        if (err) {
          console.error('LMS CheckServiceability Failure', err);
          this.setState({
            activeAddress: {
              id: address.id,
              serviceable: false
            }
          });
        } else {
          const serviceable = at(res, 'body.serviceable');

          this.setState({
            activeAddress: {
              id: address.id,
              serviceable
            }
          });
        }
      });

      this.setState({
        activeAddress: {
          id: address.id,
          serviceable: 'inProgress'
        }
      });
    }
  }

  updateAddress() {
    if (at(this, 'state.activeAddress.serviceable') === true) {
      const order = this.props.order;
      const activeAddress = at(this, 'state.activeAddress');
      const items = at(order, 'items') || [];
      const oneReleaseIdFromOrder = at(items[0], 'orderReleaseId') || '';
      const addresses = this.props.addresses;
      const address = addresses.find((eachAddress) => eachAddress.id === activeAddress.id);

      const addressPayload = {
        _storeOrderId: order.storeOrderId,
        addressChangeRequestEntry: {
          comment: 'Updated from mymyntra',
          userLogin: getUidx(),
          individualReleaseChange: false,
          releaseid: oneReleaseIdFromOrder,
          addressId: address.id,
          firstName: address.name,
          lastName: '',
          newAddress: address.address,
          country: address.countryCode,
          city: address.city,
          state: address.stateName,
          stateCode: address.stateCode,
          pincode: address.pincode,
          mobile: parseInt(address.mobile, 10),
          locality: address.locality
        }
      };

      updateOrderAddress(addressPayload, (err, res) => {
        if (err) {
          console.error('Address Change Failure', err);
          this.setState({
            error: 'Address could not updated. Please try again.'
          });
        } else {
          console.log('Response:::', res);
          this.props.closeModal();
          window.location.reload();
        }
      });
    }
  }

  deleteAddress() {
    this.props.deleteAddress(this.state.activeAddress, () => {
      this.checkServiceability(at(this, 'props.addresses.0'));
    });
  }

  renderAddresses(addresses) {
    return addresses.map((address) => <AddressAccordian
      type="changeAddress"
      key={address.id}
      active={address.id === this.state.activeAddress.id}
      action={this.checkServiceability}
      editAddress={this.editAddress}
      deleteAddress={this.deleteAddress}
      activeServiceable={at(this, 'state.activeAddress.serviceable')}
      errorMessage={this.state.error}
      address={address} />);
  }

  render() {
    const { addresses } = this.props;
    const { isDesktop } = getDeviceData();

    const currentAddress = addresses.find((eachAddress) => eachAddress.id === at(this, 'state.activeAddress.id'));

    return (<div className={Styles.addressChange}>
      <div className={Styles.shimmer} onClick={this.props.closeModal} />
      <div className={bindClass('card', { web: isDesktop, mobile: !isDesktop })}>
        {!isDesktop ? <div className={Styles.addNewAddress} onClick={this.openAddAddressModal}> + Add New Address </div> :
          <div className={Styles.modalHeader}>
            <div className={Styles.caHeader}>Change Address</div>
            <div className={Styles.addButton} onClick={this.openAddAddressModal}>Add new Address</div>
          </div>}
        <div className={Styles.addresses}>
          {!isEmpty(addresses) ? this.renderAddresses(addresses) :
            <div className={Styles.noAddresses}>No addresses added</div>}
        </div>
        <div className={Styles.buttons}>
          <div className={Styles.button} onClick={this.props.closeModal}> Cancel </div>
          <div
            className={bindClass(Styles.button, { disabled: at(this, 'state.activeAddress.serviceable') !== true })}
            onClick={this.updateAddress}> Select </div>
        </div>
      </div>
      {this.state.addAddressModal &&
        <AddAddressModal
          addressData={currentAddress}
          mode={this.state.addressModalMode}
          closeModal={this.closeAddressModal}
          loadAddresses={this.props.loadAddresses} />}
    </div>);
  }
}

AddressChange.propTypes = {
  order: PropTypes.object,
  addresses: PropTypes.array,
  loadAddresses: PropTypes.func,
  deleteAddress: PropTypes.func,
  closeModal: PropTypes.func
};

export default AddressChange;
