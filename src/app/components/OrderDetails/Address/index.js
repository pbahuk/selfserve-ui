import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import findIndex from 'lodash/findIndex';

// Style Related Imports.
import Styles from './address.css';
import classnames from 'classnames/bind';
const bindClassNames = classnames.bind(Styles);

// Functional Imports.
import features from '../../../utils/features';
import { unescapeHTML } from '../../../utils/pageHelpers';
import { getAllAddress, removeAddress } from '../../../utils/services/address.js';
const cancelFG = features('mymyntra.cancel.enable') === 'true';

// React Components Import.
import AddressChange from './../AddressChange';

class Address extends React.Component {
  constructor(props) {
    super(props);
    this.getAllAddress = this.getAllAddress.bind(this);
    this.deleteAddress = this.deleteAddress.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      changeAddresModal: false,
      addresses: []
    };
  }

  getAllAddress() {
    if (cancelFG) {
      getAllAddress((err, res) => {
        if (err || at(res, 'body.status.statusType') === 'ERROR') {
          console.error('[ERROR] OrderList fetching all addresses', err);
        } else {
          const addresses = this.sortAddresses(at(res, 'body.data'));
          document.body.style.overflow = 'hidden';
          this.setState({
            changeAddresModal: true,
            addresses
          });
        }
      });
    }
  }

  sortAddresses(addresses = []) {
    const index = findIndex(addresses, (address) => (address.defaultAddress));
    const defaultAddress = index !== -1 ? addresses.splice(index, 1) : null;

    if (defaultAddress !== null) { addresses.unshift(defaultAddress[0]); }

    return addresses;
  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.setState({
      changeAddresModal: false
    });
  }

  deleteAddress(address, callback) {
    removeAddress(address.id, (err) => {
      if (err) {
        console.error('error deleting address: ', err);
      } else {
        const newAddresses = this.state.addresses.filter(addr => addr.id !== address.id);
        this.setState({
          addresses: newAddresses
        });
        callback();
      }
    });
  }

  render() {
    const address = this.props.address || {};
    // Checking any one field from the address data.
    const addressFieldsPresent = at(address, 'locality');

    return (addressFieldsPresent ? (<div className={Styles.address}>
      <div className={Styles.shippingAddress}>
        <div className={Styles.heading}>Shipping Address:</div>
        <div style={{ fontWeight: '500' }}>{unescapeHTML(at(address, 'user.name'))}</div>
        <div>{unescapeHTML(at(address, 'streetAddress'))}</div>
        <div>{unescapeHTML(at(address, 'locality'))}, {unescapeHTML(at(address, 'city'))} - {address.pincode}</div>
      </div>
      {this.props.addressChangeable &&
        <div
          className={bindClassNames('button', { disabled: !cancelFG })}
          onClick={this.getAllAddress}>
          Change Address
        </div>}
      {this.state.changeAddresModal &&
        <AddressChange
          origin="orders"
          order={this.props.order}
          closeModal={this.closeModal}
          addresses={this.state.addresses}
          loadAddresses={this.getAllAddress}
          deleteAddress={this.deleteAddress} />}
    </div>) : null);
  }
}

Address.propTypes = {
  address: PropTypes.object,
  order: PropTypes.object,
  addressChangeable: PropTypes.bool
};

export default Address;

