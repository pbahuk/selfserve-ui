import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import PageStyles from './../../resources/page.css';

// Functional Imports.
import Madalytics from '../../utils/trackMadalytics';
import { getUidx } from '../../utils/pageHelpers';
import kvpairs from '../../utils/kvpairs';
const unavailableMessage = kvpairs('mymyntra.features.unavailable.message');

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';

// Component related imports
import OrderInfo from './OrderInfo';
import Item from './../Reusables/Item';

const otherItemTextStyle = {
  margin: '20px 0 5px 12px',
  fontSize: '15px',
  fontWeight: '500'
};

class OrderDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      pageError: false,
      order: {}
    };
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    let pageError = false;
    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }
    const order = at(serviceResponse, 'response.order') || {};
    this.item = Number(at(serviceResponse, 'item'));
    this.looksAb = at(serviceResponse, 'looksAb');
    this.savingsAB = at(serviceResponse, 'savingsAB') || 'disabled';
    this.removeCancelABValue = at(serviceResponse, 'removeCancelAB');
    this.itemsTagAB = at(serviceResponse, 'itemsTagAB') || 'disabled';
    this.type = 'orderDetails';
    this.archived = at(serviceResponse, 'archived');


    Madalytics.sendEvent('ScreenLoad', {
      type: 'My_Orders_order_details',
      data_set: {
        sucess: !pageError,
        userId: getUidx()
      }
    });

    this.setState({
      loading: false,
      pageError,
      order
    });
  }

  renderItems() {
    const items = at(this.state.order, 'items') || [];
    const pageType = this.type;

    return (
      <div>
        <div style={otherItemTextStyle}>Items in this order</div>
        {items.map((item, key) =>
          <Item
            key={key}
            type={pageType}
            item={item}
            order={this.state.order}
            savingsAB={this.savingsAB}
            selectedItem={this.item}
            removeCancelABValue={this.removeCancelABValue}
            itemsTagAB={this.itemsTagAB}
            archived={this.archived}
            looksAb={this.looksAb} />)}
      </div>
    );
  }

  render() {
    let page = null;

    if (this.state.loading) {
      page = <div className={PageStyles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.component}>
          <ErrorCard
            message="Something Went Wrong"
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          {unavailableMessage && <div className={`${PageStyles.pageMessage} ${PageStyles.odMessage}`}>{unavailableMessage}</div>}
          <OrderInfo order={this.state.order} />
          {this.renderItems()}
        </div>
      </div>);
    }

    return page;
  }
}

OrderDetails.propTypes = {
  location: PropTypes.object
};

export default OrderDetails;
