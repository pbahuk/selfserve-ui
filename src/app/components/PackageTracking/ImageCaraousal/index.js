import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './imageCaraousal.css';

class ImageCaraousal extends React.Component {
  constructor(props) {
    super(props);
    this.renderImages = this.renderImages.bind(this);
    this.state = {

    };
  }

  renderImages(images) {
    return images.map((image, key) => <img key={key} className={Styles.eachImage} src={image} />);
  }

  render() {
    return (<div className={Styles.imageCaraousal}>
      {this.renderImages(this.props.images)}
    </div>);
  }
}

ImageCaraousal.propTypes = {
  images: PropTypes.array
};

export default ImageCaraousal;
