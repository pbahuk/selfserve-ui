import React from 'react';

// Style Related Imports.
import PageStyles from '../../resources/page.css';

// Reused components everywhere.
import ErrorCard from '../Reusables/Error';

// Specific to the page.
import TrackingForm from './TrackingForm';
import Package from './Package';

import { isEmptyObject } from '../../utils/pageHelpers';

class PackageTracking extends React.Component {
  constructor(props) {
    super(props);
    this.setPackageDetails = this.setPackageDetails.bind(this);

    this.state = {
      loading: false,
      pageError: false,
      packet: {}
    };
  }

  setPackageDetails(packet) {
    console.log('Packet set in main Component', packet);
    this.setState({
      packet
    });
  }

  render() {
    let page = null;
    console.log('packet:::', this.state.packet);

    if (this.state.loading) {
      page = <div className={PageStyles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <div className={PageStyles.component}>
          <ErrorCard
            message="Something Went Wrong"
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else if (isEmptyObject(this.state.packet)) {
      page = (<div className={PageStyles.page}>
        <div className={PageStyles.fullWidthComponent}>
          <TrackingForm
            setPackageDetails={this.setPackageDetails} />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        <div className={PageStyles.fullWidthComponent}>
          <Package
            packet={this.state.packet} />
        </div>
      </div>);
    }

    return page;
  }
}

export default PackageTracking;
