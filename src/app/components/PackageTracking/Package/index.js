import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './package.css';

// React Component Imports.
import ImageCaraousal from '../ImageCaraousal';
import ItemTracker from '../../Reusables/ItemTracker';
import { getImageUrl } from '../../../utils/pageHelpers';

class Package extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { packet } = this.props;
    const items = at(packet, 'items') || [];
    const specialItem = items[0];
    console.log('items::::::', items);
    const itemImages = [];
    items.forEach((item) => {
      const product = at(item, 'product') || {};
      const productImages = product.images[0] || {};
      let productImageUrl = productImages.secureSrc || '';
      productImageUrl = getImageUrl(productImageUrl, 125, 161);

      itemImages.push(productImageUrl);
    });

    return (<div className={Styles.card}>
      <div className={Styles.packageDetails}>
        <div className={Styles.heading}> Package contains {items.length} items </div>
        <div> Courier Name: Myntra Logistics </div>
        <div> Tracking Number: ML987286275265 </div>
        <ImageCaraousal
          images={itemImages} />
      </div>
      <ItemTracker
        trackingInfo={specialItem.trackingInfo}
        detailsEnabled={items.length > 1} />
    </div>);
  }
}

Package.propTypes = {
  packet: PropTypes.object
};

export default Package;
