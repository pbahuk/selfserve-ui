import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

import Styles from './trackingForm.css';
import MyInput from '../../Reusables/Forms/MyInput';

// Service Calls.
import { getPackageDetails } from '../../../utils/services/oms';
// import { getUidxFromEmail } from '../../../utils/services/idea';

class TrackingForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      form: {
        email: {
          value: '',
          error: '',
          maxLength: 30,
          validation: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
          label: 'Email id *'
        }
      },
      formInActive: false,
      formValidated: false,
      formError: ''
    };
  }

  handleChange(target) {
    const { form } = this.state;
    form[target.name].value = target.value;

    this.setState({
      form
    });
  }

  handleFocus(target) {
    const { form } = this.state;
    form[target.name].error = '';

    this.setState({
      form,
      formValidated: false
    });
  }

  handleBlur(target) {
    const { form } = this.state;
    let { formValidated } = this.state;
    const value = target.value;

    if (!value.match(form[target.name].validation)) {
      form[target.name].error = 'Please enter a valid Email';
    } else {
      formValidated = true;
    }

    this.setState({
      form,
      formValidated
    });
  }

  handleSubmit() {
    const { form } = this.state;
    let { formError } = this.state;

    // Making the form Inactive.
    this.setState({
      formInActive: true
    });

    getPackageDetails(form.email.value, (err, res) => {
      if (err) {
        console.error('Error Fetching the package details');
        formError = 'Something went wrong';
      } else if (at(res, 'body.status.type') === 'ERROR') {
        formError = at(res, 'body.status.stausMessage') || 'Something went wrong';
      }
      const packet = at(res, 'body');
      console.log('packet:::::', packet);

      this.setState({
        formInActive: false,
        formError
      }, () => {
        this.props.setPackageDetails(packet);
      });
    });
  }

  render() {
    const { form, formValidated } = this.state;

    return (<div className={Styles.card}>
      <span className={Styles.heading}> Enter the email id using which the order is placed </span>
      <MyInput
        type="text"
        name="email"
        customised={false}
        inputSize="md"
        label={form.email.label}
        maxLength={form.email.maxLength}
        errorMessage={form.email.error}
        value={form.email.value}
        onChange={this.handleChange}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur} />
      <div
        className={formValidated ? Styles.button : `${Styles.button} ${Styles.disabled}`}
        onClick={this.handleSubmit}> Track Package
        <div className={this.state.formInActive ? Styles.buttonLoader : ''} />
      </div>
    </div>);
  }
}

TrackingForm.propTypes = {
  setPackageDetails: PropTypes.func
};

export default TrackingForm;
