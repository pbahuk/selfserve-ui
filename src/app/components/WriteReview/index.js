import React from 'react';
import Review from './writeReview';
import get from 'lodash/get';
import { isBrowser, isMobile } from '../../utils';
import styles from './writeReview.css';
import { securify } from '../../utils/securify';
import Notify from '../Reusables/Notify/notify';
const secure = securify();

const singleImageConfig = {
  width: 720,
  height: 960,
  q: 60
};

export function isApp() {
  if (isBrowser()) {
    const deviceChannel = get(window, '__myx_deviceData__.deviceChannel') || '';
    return deviceChannel.toLowerCase() === 'mobile_app';
  } return false;
}

function getProductImageUrl(imgObj = null, config = {}) {
  if (imgObj) {
    const secureSrc = get(imgObj, 'secureSrc');
    const imageFormula = secureSrc || get(imgObj, 'src');
    const url = imageFormula.replace('($width)', config.width).replace('($height)', config.height).replace('($qualityPercentage)', config.q);
    return secure(url);
  }
  return null;
}

const pdpData = isBrowser() ? get(window, '__myx.pdpData', {}) : {};
const src = get(pdpData, 'media.albums.0.images.0');
const pdtImageURL = getProductImageUrl(src, singleImageConfig);
const pdtTitle = get(pdpData, 'name');

class WriteReview extends React.Component {

  constructor(props) {
    super(props);
    this.displaySuccess = this.displaySuccess.bind(this);

    this.state = {
      showNotify: false
    };
  }

  componentWillMount() {
    const URL = window.location.href.split('?')[0];
    const URLComponents = URL.split('/');
    const URLlength = URLComponents.length;
    if (URLComponents[URLlength - 2] === 'writeReview' || URLComponents[URLlength - 2] === 'writereview') {
      this.setState({ styleId: URLComponents[URLlength - 1] });
      const ratingData = isBrowser() ? get(window, '__myx.userRatingData', {}) : {};
      const userRating = get(ratingData, 'userRatingMap.0.value.userRating.0.rating', -1);
      this.setState({ userRating });
    } else {
      this.setState({ reviewId: URLComponents[URLlength - 1] });
      this.setState({ styleId: URLComponents[URLlength - 2] });
      const reviewData = isBrowser() ? get(window, '__myx.reviewData', {}) : {};
      this.setState({
        reviewImg: get(reviewData, 'images', []),
        userRating: get(reviewData, 'userRating', -1),
        userReviewText: get(reviewData, 'review', ''),
        reviewStatus: get(reviewData, 'status', '')
      });
    }
  }

  componentDidMount() {
    if (isMobile()) {
      document.body.classList.add('no-scroll-background');
    }
  }

  componentWillUnmount() {
    if (isMobile()) {
      document.body.classList.remove('no-scroll-background');
    }
  }

  displaySuccess() {
    if (!isApp()) {
      this.setState({
        showNotify: true
      }, () => {
        setTimeout(() => {
          window.location.href = '/';
        }, 2000);
      });
    }
  }

  render() {
    const successNotifyText = 'Your review has been submitted successfully!';
    let pageWrapper = styles.pageWrapper;
    if (isMobile()) {
      pageWrapper = isApp() ? styles.pageWrapperApp : styles.pageWrapperMobile;
    }
    const { reviewStatus, reviewId, reviewImg, userReviewText, styleId, showNotify } = this.state;
    return (
      <div className={pageWrapper}>
        <Review
          pdtImageURL={pdtImageURL}
          pdtTitle={pdtTitle}
          styleId={styleId}
          userReviewText={userReviewText}
          reviewImg={reviewImg}
          reviewId={reviewId}
          reviewStatus={reviewStatus}
          writeReviewURL
          displaySuccess={this.displaySuccess}
          userRating={this.state.userRating} />
        {showNotify &&
          <div className={styles.errornotify}>
            <Notify
              backgroundColor={'#2bc1a1'}
              NotifyText={successNotifyText} />
          </div>}
      </div>
    );
  }
}

export default WriteReview;
