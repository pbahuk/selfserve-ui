import React from 'react';
import Review from './writeReview';
import get from 'lodash/get';
import Client from '../../../utils/Client';
import styles from './writeReview.css';
import { isMobile } from '../../utils';

class WriteReviewDesktop extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showNotify: false,
      errorNotifyText: 'Something went wrong. Please Try again.',
      reviewImg: [],
      userReviewText: '',
      status: ''
    };
  }

  componentWillMount() {
    if (this.props.reviewId) {
      const endPointUrl = '/my/api/reviewApi/fetchReview';
      const parameters = {
        id: this.props.reviewId
      };

      Client.post(endPointUrl, parameters).end((err, response) => {
        if (err) {
          if (err.status === 403) {
            window.location.href = `/login?referer=${window.location.pathname}`;
            return;
          }
          if (get(err, 'response.body.error.message')) {
            this.setState({
              errorNotifyText: get(err, 'response.body.error.message')
            });
          } else {
            this.setState({
              errorNotifyText: 'Something went wrong. Please Try again.'
            });
          }
        } else {
          this.setState({
            reviewImg: get(response, 'body.images', []),
            userReviewText: get(response, 'body.review', ''),
            status: get(response, 'body.status', '')
          });
        }
      });
    }
  }

  render() {
    const { pdtImageURL, pdtTitle, styleId, reviewId = '', userRating, displaySuccess, reviewStatus } = this.props;
    const { userReviewText, errorNotifyText, showNotify, reviewImg, status } = this.state;
    const wrapperClass = isMobile() ? styles.mobileViewWrapper : styles.backdropStyle;
    const subWrapper = isMobile() ? styles.mainContainerMobile : styles.mainContainer;
    return (
      <div className={wrapperClass} onClick={() => { if (!isMobile()) { this.props.showWriteReview(false); } }}>
        <div className={subWrapper}>
          <Review
            displaySuccess={displaySuccess}
            reviewStatus={reviewStatus}
            pdtImageURL={pdtImageURL}
            pdtTitle={pdtTitle}
            styleId={styleId}
            showWriteReview={this.props.showWriteReview}
            reviewId={reviewId}
            userRating={userRating}
            userReviewText={userReviewText}
            status={status}
            showNotify={showNotify}
            errorNotifyText={errorNotifyText}
            reviewImg={reviewImg} />
        </div>
      </div>
    );
  }
}

WriteReviewDesktop.propTypes = {
  userRating: React.PropTypes.number,
  pdtImageURL: React.PropTypes.string,
  pdtTitle: React.PropTypes.string,
  styleId: React.PropTypes.string,
  reviewId: React.PropTypes.string,
  showWriteReview: React.PropTypes.func,
  displaySuccess: React.PropTypes.func,
  reviewStatus: React.PropTypes.string
};

export default WriteReviewDesktop;
