import React from 'react';
import styles from './timeout.css';
import { isMobile } from '../../../utils';

const TimeoutModal = (props) => {
  const container = isMobile() ? styles.containerMobile : styles.container;
  return props.show ? (
    <div className={styles.backdropStyle} onClick={() => props.closeTimeoutModal()} >
      <div className={container} onClick={(e) => e.stopPropagation()}>
        <span style={{ paddingBottom: isMobile() ? '16px' : '30px' }} className={styles.title}>
        Looks like photos are taking time to upload. Would you like to submit your review without photos?</span>
        <div className={styles.subContainer}>
          <div className={styles.cancel}>
            <span className={styles.iconsTitle} onClick={() => props.closeTimeoutModal()} >Cancel</span>
          </div>
          <div className={styles.ok}>
            <span className={styles.iconsTitle} onClick={() => props.failedImgsetReview()} >Submit without photos</span>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

TimeoutModal.propTypes = {
  show: React.PropTypes.bool,
  failedImgsetReview: React.PropTypes.func,
  closeTimeoutModal: React.PropTypes.func
};


export default TimeoutModal;
