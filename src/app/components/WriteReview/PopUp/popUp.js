import React from 'react';
import styles from './popUp.css';
import Dropzone from 'react-dropzone';
class PopUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: false
    };
  }

  handleImageUpload(e) {
    debugger;
    console.log('popup', e.target.files[0]);
    const files = [];
    files.push(e.target.files[0]);
    this.props.onDrop(files);
    this.props.toggle();
  }

  render() {
    return this.props.show ? (
      <div className={styles.backdropStyle} onClick={this.props.toggle}>
        <div className={styles.photosOption} onClick={(e) => e.stopPropagation()}>
          <span className={styles.title}>Add Photos From</span>
          <div className={styles.subContainer}>
            <div className={styles.camera}>
              <label>
                <input onChange={(e) => this.handleImageUpload(e)} className={styles.hideDisplay} type="file" label="Camera" accept="image/*" capture />
                <span className={`myntraweb-review-sprite ${styles.cameraIcon}`} />
                <span className={styles.iconsTitle}>Camera</span>
              </label>
            </div>
            <div className={styles.gallery}>
              <Dropzone
                accept="image/*"
                onDrop={(e) => {
                  this.props.onDrop(e);
                  this.props.toggle();
                }}>
                {({ getRootProps, getInputProps }) => (
                  <div style={{ outline: 'none' }} {...getRootProps()}>
                    <input name="product" {...getInputProps()} />
                    <span className={`myntraweb-review-sprite ${styles.galleryMobileIcon}`} />
                    <span className={styles.iconsTitle}>Gallery</span>
                  </div>
                )}
              </Dropzone>
            </div>
          </div>
        </div>
      </div>
    ) : null;
  }
}

PopUp.propTypes = {
  show: React.PropTypes.bool,
  toggle: React.PropTypes.func,
  onDrop: React.PropTypes.func
};


export default PopUp;
