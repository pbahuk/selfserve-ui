/* global MyntApp:false */
import React from 'react';
import styles from './writeReview.css';
import SVGImages from '../Reusables/SVGImages/index';
import Dropzone from 'react-dropzone';
import PopUp from './PopUp/popUp.js';
import TimeoutModal from './PopUp/TimeoutModal';
import { isMobile, isBrowser } from '../../utils';
import Client from '../../../utils/Client';
import features from '../../utils/features';
import Loader from '../Loader/Loader.js';
import Notify from '../Reusables/Notify/notify';
import Madalytics from '../../utils/trackMadalytics';
import get from 'lodash/get';
import { isApp } from './index';
const agent = require('superagent');

const addPhotoEnable = features('mymyntra.reviewPhotos.enable') === true || false;
const isIOSApp = isBrowser() ? get(window, '__myx.isIOSApp') : false;
let newImgDamid = 0;
class Review extends React.Component {

  constructor(props) {
    super(props);
    const isloading = typeof props.reviewId !== 'undefined'
      && props.reviewId.length && (typeof props.userReviewText === 'undefined' || !props.userReviewText.length) || false;

    this.toggleShowPopUp = this.toggleShowPopUp.bind(this);
    this.toggleShowProductInfo = this.toggleShowProductInfo.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.showError = this.showError.bind(this);
    this.uploadImg = this.uploadImg.bind(this);
    this.submitReview = this.submitReview.bind(this);
    this.closeTimeoutModal = this.closeTimeoutModal.bind(this);
    this.failedImgsetReview = this.failedImgsetReview.bind(this);

    this.state = {
      review: props.userReviewText,
      reviewedImages: props.reviewImg || [],
      reviewImagesPreview: props.reviewImg || [],
      errorNotifyText: props.errorNotifyText,
      showPopUp: false,
      showProductInfo: true,
      newReviewImages: [],
      showAlert: false,
      maxCharLimit: false,
      setreview: true,
      showTimeoutModal: false,
      showNotify: false,
      isloading
    };
  }

  componentDidMount() {
    if (this.state.isloading) {
      setTimeout(() => {
        if (this.state.isloading) {
          this.setState({
            isloading: false
          }, () => {
            this.showError('Oops! Something went wrong. Please check your Internet connection');
          });
        }
      }, 30000);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      reviewedImages: nextProps.reviewImg,
      reviewImagesPreview: nextProps.reviewImg,
      review: nextProps.userReviewText,
      isloading: false
    });
  }

  onDrop(files) {
    const selectedFileLength = files.length;
    for (let i = selectedFileLength - 1; i >= 0; i--) {
      if (files[i].size > 5000000) {
        files.splice(i, 1);
      }
    }
    const toBeUploadedFileLength = files.length;
    if (toBeUploadedFileLength + this.state.reviewImagesPreview.length > 5) {
      this.setState({
        alertmsg: 'You can add max 5 photos',
        showAlert: true
      });
      return;
    }
    if (selectedFileLength - toBeUploadedFileLength > 0) {
      const photoOrPhotots = toBeUploadedFileLength > 1 ? 'photos' : 'photo';
      this.setState({
        alertmsg: `${toBeUploadedFileLength}/${selectedFileLength} ${photoOrPhotots} uploaded. Max. image size is 5MB`,
        showAlert: true
      });
    }
    const temp = files.map((file) => Object.assign(file, {
      url: URL.createObjectURL(file),
      damId: newImgDamid++
    }));
    this.setState((state) => ({
      newReviewImages: [...state.newReviewImages, ...files],
      reviewImagesPreview: [...state.reviewImagesPreview, ...temp]
    }));
  }

  onCancel(index) {
    const isuploaded = index < this.state.reviewedImages.length;
    let arr = [];
    if (isuploaded) {
      arr = [...this.state.reviewedImages];
      arr.splice(index, 1);
      this.setState({ reviewedImages: arr });
    } else {
      arr = [...this.state.newReviewImages];
      arr.splice(index - this.state.reviewedImages.length, 1);
      this.setState({ newReviewImages: arr });
    }
    arr = [...this.state.reviewImagesPreview];
    arr.splice(index, 1);
    this.setState({ reviewImagesPreview: arr });
  }

  getReviewHeader() {
    return !isApp() && !this.props.writeReviewURL ? (
      <div className={styles.header}>
        <span onClick={() => this.props.showWriteReview(false)} className={`myntraweb-sprite ${styles.closeSearch}`}></span>
        <span className={styles.headerTitle}>WRITE REVIEW</span>
      </div>
    ) : null;
  }

  getRating() {
    let index = 0;
    const star = [];
    while (index++ < 5) {
      if (index <= this.props.userRating) {
        star.push(<SVGImages key={index} name="filledStar" customStyle={{ transform: 'scale(0.5)', height: '22px', width: '22px' }} />);
      } else {
        star.push(<SVGImages key={index} name="emptyStar" customStyle={{ transform: 'scale(0.5)', height: '22px', width: '22px' }} />);
      }
    }
    return (
      <div className={styles.star}>
       {star}
      </div>
    );
  }

  getProductInfo() {
    return this.state.showProductInfo || !isMobile() ? (
      <div className={styles.productInfo}>
        <img
          src={this.props.pdtImageURL} />
        <div className={styles.info}>
          <div className={styles.pdtTitle}>
            {this.props.pdtTitle}
          </div>
          {this.getRating()}
        </div>
        {this.state.showNotify &&
          <div className={styles.errornotify}>
            <Notify
              backgroundColor={'#f16565'}
              NotifyText={this.state.errorNotifyText} />
          </div>}
      </div>
    ) : null;
  }

  getReviewImages() {
    let images = [...this.state.reviewImagesPreview];
    const imagesStyle = isMobile() ? styles.reviewImgMobile : styles.images;
    const cross = isMobile() ? styles.crossMobile : styles.cross;
    if (images.length > 5) {
      images = images.splice(5);
    }
    return images.map((image, index) =>
    (
      <div key={image.damId} className={imagesStyle}>
        <img
          src={image.url} />
        <span onClick={() => this.onCancel(index, image.damId)} className={`myntraweb-review-sprite ${cross}`}></span>
      </div>
    ));
  }

  getDesktopGallery() {
    return !isMobile() && addPhotoEnable ? (
      <span className={styles.gallery}>
        <Dropzone
          multiple
          accept="image/*"
          onDrop={this.onDrop}>
          {({ getRootProps, getInputProps }) => (
            <div style={{ outline: 'none' }} {...getRootProps()}>
              <input name="product" {...getInputProps()} />
              <div onClick={() => this.sendAddphotosMAEvents()} className={`myntraweb-review-sprite ${styles.galleryDesktop}`}></div>
            </div>
          )}
        </Dropzone>
      </span>
    ) : null;
  }

  getReviewData() {
    const textarea = isMobile() ? styles.textareaMobile : styles.textarea;
    const reviewData = isMobile() ? styles.reviewDataMobile : styles.reviewData;
    const flexWrap = isMobile() ? styles.flexWrapMobile : styles.flexWrap;
    const reviewWrapper = isMobile() ? styles.reviewWrapperMobile : styles.reviewWrapper;
    let maxPhotoLimit = styles.hide;
    let maxCharLimit = styles.hide;
    if (this.state.showAlert) {
      maxPhotoLimit = isMobile() ? styles.maxPhotoLimitMobile : styles.maxPhotoLimit;
      window.setTimeout(() => { this.setState({ showAlert: false }); }, 3000); // hide after 5s
    }
    if (this.state.maxCharLimit) {
      maxCharLimit = styles.maxCharLimit;
      window.setTimeout(() => { this.setState({ maxCharLimit: false }); }, 3000); // hide after 5s
    }
    return (
      <div className={reviewData}>
        <div className={reviewWrapper}>
          <div className={styles.textareaWrapper}>
            <div className={flexWrap}>
              <textarea
                onFocus={this.toggleShowProductInfo}
                onBlur={this.toggleShowProductInfo}
                placeholder="Please write product review here."
                onChange={(e) => this.handleChange(e)}
                value={this.state.review}
                className={textarea}>
              </textarea>
              <div className={maxCharLimit}>
                Maximum character limit exceeded (1500 characters allowed)
              </div>
            </div>
          </div>
          <div className={styles.imagesContainer}>
            {this.getDesktopGallery()}
            {!isMobile() && addPhotoEnable && !this.state.reviewImagesPreview.length &&
              <span className={`myntraweb-review-sprite ${styles.AddPhotos}`} ></span>}
            {this.getReviewImages()}
          </div>
          <div className={maxPhotoLimit}>
            {this.state.alertmsg}
          </div>
        </div>
      </div>
    );
  }

  getAddPhotos() {
    return isMobile() && addPhotoEnable ? (
      <div onClick={() => { this.toggleShowPopUp(); this.sendAddphotosMAEvents(); }} className={styles.addPhotosContainer}>
        <span className={`myntraweb-review-sprite ${styles.cameraIcon}`} />
        <span className={styles.addPhotos}>ADD PHOTOS</span>
        <span className={styles.maxPhotoinfo}>(Upto 5 photos)</span>
      </div>
    ) : null;
  }

  getSubmit() {
    const submit = isMobile() ? styles.submitMobile : styles.submit;
    return (
      <div className={styles.bottomContainer}>
        {this.getAddPhotos()}
        <div className={styles.submitContainer}>
          <span
            onClick={() => {
              this.sendSubmitMAEvents();
              if (this.state.review) {
                this.setState({ isloading: true });
                this.uploadImg()
                .then(this.submitReview)
                .catch(e => console.log('Image upload failed ', e));
              } else {
                this.setState({
                  alertmsg: 'Please enter a review!',
                  showAlert: true
                });
              }
            }} className={submit}>SUBMIT</span>
        </div>
      </div>
    );
  }

  sendAddphotosMAEvents() {
    Madalytics.setPageContext('Write Review');
    Madalytics.sendEvent('Write Review Page - Add Photos Click', {
      entity_type: 'product',
      entity_name: this.props.pdtTitle,
      entity_id: this.props.styleId
    },
    { user_data: this.props.reviewStatus,
      data_set: {
        event_type: 'widgetClick',
        event_category: 'Write Review Page'
      }
    }, 'web');
  }

  sendSubmitMAEvents() {
    Madalytics.setPageContext('Write Review');
    if (this.props.reviewId) {
      Madalytics.sendEvent('Write Review Page - Submit Editted Review', {
        entity_type: 'review',
        entity_name: this.state.review,
        entity_id: this.props.reviewId
      },
      { user_data: this.props.reviewStatus,
        data_set: {
          event_type: 'widgetClick',
          event_category: 'Write Review Page'
        }
      }, 'web');
    } else {
      Madalytics.sendEvent('Write Review Page - Submit Review', {
        entity_type: 'product',
        entity_name: this.props.pdtTitle,
        entity_id: this.props.styleId
      },
        {
          data_set: {
            event_type: 'widgetClick',
            event_category: 'Write Review Page'
          }
        }, 'web');
    }
  }

  showError(errText) {
    this.setState({
      showNotify: true,
      errorNotifyText: errText
    }, () => {
      setTimeout(() => {
        this.setState({
          showNotify: false
        });
      }, 3000);
    });
  }

  handleChange(e) {
    if (e.target.value.length > 1500) {
      if (e.target.defaultValue.length === 1500) {
        this.setState({
          maxCharLimit: true
        });
      } else {
        this.setState({
          maxCharLimit: true,
          review: e.target.value.substring(0, 1500)
        });
      }
    } else {
      this.setState({
        review: e.target.value
      });
      if (this.state.maxCharLimit) {
        this.setState({
          maxCharLimit: false
        });
      }
    }
  }

  submitReview(response) {
    if (!this.state.setreview) {
      return;
    }
    this.setState({ isloading: true });
    const reqObj = {
      'review': this.state.review,
      'images': response,
      'reviewId': this.props.reviewId,
      'styleId': this.props.styleId
    };
    const endPointUrl = '/my/api/reviewApi/setReview';

    Client.post(endPointUrl, reqObj).end((err, res) => {
      if (err) {
        this.setState({
          isloading: false
        });
        if (err.status === 403) {
          window.location.href = `/login?referer=${window.location.pathname}`;
          return;
        }
        if (get(err, 'response.body.error.message')) {
          this.showError(get(err, 'response.body.error.message'));
        } else {
          this.showError('Something went wrong. Please Try again.');
        }
      } else {
        console.log(res.body.message);
        this.setState({
          isloading: false
        });
        if (isApp()) {
          if (isIOSApp) {
            console.log('about to call showSnackBar for success, isIosApp->', isIOSApp);
            window.webkit.messageHandlers.showSnackBar.postMessage({ type: 2, message: 'review submitted successfully' });
          }
          if (typeof MyntApp !== 'undefined' && typeof MyntApp.showSnackBar === 'function') {
            MyntApp.showSnackBar(2, 'review submitted successfully');
          }
          if (typeof MyntApp !== 'undefined' && typeof MyntApp.forceCloseWebView === 'function') {
            MyntApp.forceCloseWebView();
          }
        } else if (!this.props.writeReviewURL) {
          this.props.showWriteReview(false);
        }
        this.props.displaySuccess();
      }
    });
  }

  uploadImg() {
    const endPointUrl = '/my/api/reviewApi/uploadImg';
    const ImgNumber = this.state.newReviewImages.length;
    const promises = [];
    const imgs = [...this.state.newReviewImages];
    for (let i = 0; i < ImgNumber; i++) {
      promises.push(new Promise((resolve, reject) => {
        let isUploaded = false;
        setTimeout(() => {
          if (!isUploaded) {
            reject('Image Upload TimeOut');
          }
        }, 30000);
        agent.post(endPointUrl).attach('product', imgs[i])
          .then(data => { isUploaded = true; resolve(data); })
          .catch(err => reject(err));
      }));
    }

    return Promise.all(promises)
      .then(responses => {
        const newlyAdded = [];
        for (const response of responses) {
          const obj = {
            'url': get(response, 'body.listData.0.imgs.CL.path', '').replace(/^http:\/\//i, 'https://'),
            'damId': get(response, 'body.listData.0.imgs.CL.id', '').toString(),
            'resolution': get(response, 'body.listData.0.resolution'),
            'aspectRatio': get(response, 'body.listData.0.aspectRatio')
          };
          newlyAdded.push(obj);
        }

        const imgDataToSend = [...this.state.reviewedImages, ...newlyAdded];
        return imgDataToSend;
      })
      .catch(err => {
        console.log('Error in uploding the images', err);
        this.setState({ isloading: false, showTimeoutModal: true, setreview: false });
        return err;
      });
  }

  toggleShowPopUp() {
    this.setState({
      showPopUp: !this.state.showPopUp
    });
  }

  toggleShowProductInfo() {
    this.setState({
      showProductInfo: !this.state.showProductInfo
    });
  }

  closeTimeoutModal() {
    this.setState({
      showTimeoutModal: false,
      setreview: true
    });
  }

  failedImgsetReview() {
    this.setState({
      showTimeoutModal: false,
      setreview: true
    }, () => {
      this.submitReview([...this.state.reviewedImages]);
    });
  }

  render() {
    return (
      <div className={styles.subContainer} onClick={(e) => e.stopPropagation()} >
        {this.getReviewHeader()}
        {this.getProductInfo()}
        {this.getReviewData()}
        {this.getSubmit()}
        <PopUp
          show={this.state.showPopUp}
          onDrop={this.onDrop}
          toggle={this.toggleShowPopUp} />
        <Loader show={this.state.isloading} />
        <TimeoutModal
          failedImgsetReview={this.failedImgsetReview}
          closeTimeoutModal={this.closeTimeoutModal}
          show={this.state.showTimeoutModal} />
      </div>
    );
  }
}

Review.propTypes = {
  userRating: React.PropTypes.number,
  pdtImageURL: React.PropTypes.string,
  pdtTitle: React.PropTypes.string,
  userReviewText: React.PropTypes.string,
  status: React.PropTypes.string,
  styleId: React.PropTypes.string,
  reviewId: React.PropTypes.string,
  reviewImg: React.PropTypes.array,
  showWriteReview: React.PropTypes.func,
  displaySuccess: React.PropTypes.func,
  showNotify: React.PropTypes.bool,
  errorNotifyText: React.PropTypes.string,
  reviewStatus: React.PropTypes.string,
  writeReviewURL: React.PropTypes.bool
};

export default Review;
