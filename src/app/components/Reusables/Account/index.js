import React from 'react';
import Styles from './account.css';
import { getUserName } from '../../../utils/pageHelpers';

class Account extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (<div className={Styles.account}>
      <div className={Styles.heading}> Account</div>
      <div>{getUserName()}</div>
    </div>);
  }
}

export default Account;
