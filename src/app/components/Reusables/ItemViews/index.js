import React from 'react';
import at from 'v-at';
import _ from 'lodash';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './itemViews.css';
import { currencyValue, getImageUrl } from '../../../utils/pageHelpers';

class ItemViews extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    const item = this.props.item || {};
    let itemComponent = null;
    const productImages = at(item, 'product.images.0') || {};
    let productImageUrl = productImages.secureSrc || '';

    // Product Info.
    const brandName = at(item, 'product.brand.name') || '';
    const productDisplayName = at(item, 'product.name') || '';
    let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
    productName = productName.length > 30 ? `${productName.substr(0, 30)}...` : productName;
    const size = _.find(at(item, 'product.sizes'), (option) => option.skuId === item.product.skuId);

    switch (this.props.view) {
      case 'actionable':
        productImageUrl = getImageUrl(productImageUrl, 125, 161);
        itemComponent = (<div className={`${Styles.item} ${Styles[this.props.view]}`}>
          <div className={Styles['product-info']}>
            <div className={Styles.thumbnail}>
              <img src={productImageUrl} />
            </div>
            <div className={Styles.info}>
              <span className={Styles.brand}> {brandName} </span>
              <div className={Styles.productName}> {productName} </div>
              <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
              <span> | Qty: {item.quantity}</span>
              <div className={Styles.price}>
                Rs. {currencyValue(at(item, 'payments.amount') / 100, false)}
              </div>
            </div>
          </div>
        </div>);
        break;

      case 'nonActionable':
        productImageUrl = getImageUrl(productImageUrl, 64, 48);
        itemComponent = (<div className={`${Styles.item} ${Styles[this.props.view]}`}>
          <div className={Styles['product-info']}>
            <div className={Styles.thumbnail}>
              <img src={productImageUrl} />
            </div>
            <div className={Styles.info}>
              <div className={Styles.productName}> {productName} </div>
              <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
              <span> | Qty: {item.quantity}</span>
              <div className={Styles.reason}> {this.props.reason} </div>
            </div>
          </div>
        </div>);
        break;

      case 'confirmed':
        productImageUrl = getImageUrl(productImageUrl, 64, 48);
        itemComponent = (<div className={`${Styles.item} ${Styles[this.props.view]}`}>
          <div className={Styles['product-info']}>
            <div className={Styles.thumbnail}>
              <img src={productImageUrl} />
            </div>
            <div className={Styles.info}>
              <span className={Styles.brand}> {brandName} </span>
              <div className={Styles.productName}> {productName} </div>
              <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
              <span> | Qty: {item.quantity}</span>
            </div>
          </div>
        </div>);
        break;

      default:
        itemComponent = null;
    }

    return itemComponent;
  }
}

ItemViews.propTypes = {
  view: PropTypes.string,
  item: PropTypes.object,
  reason: PropTypes.string
};

export default ItemViews;
