import PropTypes from 'prop-types';
import React from 'react';
import Icon from '../../Reusables/Icon';


class Info extends React.Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      show: false
    };
  }

  getNode = (node) => {
    this.node = node;
  }

  toggleInfo = (event) => {
    event.stopPropagation();
    const that = this;
    this.setState({
      show: !that.state.show
    }, () => {
      if (that.state.show) {
        const timeout = that.props.timeout && !isNaN(that.props.timeout) ? that.props.timeout : 5000;
        if (that.timer) {
          clearTimeout(that.timer);
        }
        if (timeout) {
          that.timer = setTimeout(() => {
            that.setState({ show: false });
          }, timeout);
        }
        document.addEventListener('mousedown', that.handleClickOut, false);
      }
    });
  }

  handleClickOut = (e) => {
    if (this.node && this.node.contains(e.target)) {
      return;
    }
    this.setState({ show: false });
    document.removeEventListener('mousedown', this.handleClickOut, false);
  }

  render() {
    const { text, style } = this.props;
    const { show } = this.state;
    return (
      <div className={`${style.iconWrapper} ${show ? style.tooltip : ''}`} ref={(node) => this.getNode(node)}>
        {
          show &&
            <div className={style.activePointsInfo}>
              <div className={style.triangle} />
              <div className={style.infoText}>
              {text}
              </div>
            </div>
        }
        <Icon
          name="info"
          className={
            style.icon
          }
          onClick={(event) => this.toggleInfo(event)} />
      </div>
    );
  }
}

Info.propTypes = {
  text: PropTypes.string,
  style: PropTypes.any,
  timeout: PropTypes.number
};

export default Info;
