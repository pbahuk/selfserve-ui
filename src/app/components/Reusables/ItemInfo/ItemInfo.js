import React, { PropTypes, PureComponent } from 'react';
import Styles from './itemInfo.css';
import Icon from '../Icon';
import SVGImages from '../SVGImages';

class ItemInfo extends PureComponent {
  render() {
    const {
      brandName,
      productImageUrl,
      productName,
      size,
      charges,
      qty,
      marketPrice,
      discount,
      insiderItemsPoints: { points = 0, status = '' }
    } = this.props.itemInfo;
    const noDiscount = parseInt(discount, 10) <= 1;
    return (
      <div className={Styles.container}>
        <div className={Styles.thumbnail}><img src={productImageUrl} /></div>
        <div className={Styles.info}>
          <span className={Styles.brand}> {brandName} </span>
          <div className={Styles.productName}> {productName} </div>
          <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
          <span> | Qty: {qty}</span>
          <span className={Styles.price}>
            <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{charges}
            {!noDiscount ? <span>
              <span className={Styles.productstrike}>
                <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{marketPrice}
              </span>
              <span className={Styles.productDiscountPercentage}> Saved
                <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{discount}
              </span>
            </span> : null}
          </span>
          <div className={Styles.claimInsiderPoints}>
            <div className={Styles.insiderIcon}>
              <SVGImages name={'insiderIcon'} customStyle={{ height: '14px', width: '20px' }} />
            </div>
            <div className={Styles.insiderPoints}>
              <span>{points} </span>
              Insider points {status}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ItemInfo.PropTypes = {
  itemInfo: PropTypes.object
};

export default ItemInfo;
