import React from 'react';
import PropTypes from 'prop-types';
import Styles from './miniPDP.css';
import Icon from '../../Reusables/Icon';
import Madalytics from '../../../utils/trackMadalytics';
import SVGImages from '../SVGImages';
import { getImageUrl } from '../../../utils/pageHelpers';
import COLORS from '../../../resources/colors';
import at from 'v-at';
class MiniPDP extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isWishListed: this.props.isWishListed
    };
    [
      'redirectToPDP',
      'renderWishListButton'
    ].forEach(method => (this[method] = this[method].bind(this)));
    this.timer = null;
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  redirectToPDP() {
    const { index, data, itemId, orderId } = this.props;
    const { name: productName, id: styleId, landingPageUrl } = data;
    const hPosition = index % 2;
    const vPosition = parseInt(index / 2, 10) + 1;
    Madalytics.sendEvent(
      'widgetClick',
      {
        type: 'Cross-sell_Item_details_click',
        data_set: { entity_type: 'order', entity_id: orderId }
      },
      {
        payload: {
          widget: { name: 'Cross-sell', type: 'list' },
          widget_items: {
            name: 'product_detail',
            type: 'list-item',
            h_position: hPosition,
            v_position: vPosition,
            entity_name: productName,
            entity_type: 'product',
            entity_id: styleId
          }
        },
        custom: {
          v1: itemId
        }
      }
    );
    window.location.href = `/${landingPageUrl}`;
  }
  renderWishListButton() {
    return (
      <button className={Styles.icon} onClick={() => this.props.updateWishList(this.props.data.id)}>
        <SVGImages name={this.props.isWishListed ? 'wishListed' : 'notWishListed'} customStyle={{ height: '18px', width: '13px' }} />
      </button>
    );
  }

  render() {
    const { data: { brand: { name }, price: { discounted, mrp, discount } }, height, width } = this.props;
    let productImageUrl = at(this.props.data, 'defaultImage.secureSrc');
    const fullProdName = at(this.props.data, 'name');
    productImageUrl = getImageUrl(productImageUrl, height, width);
    const productName = fullProdName && fullProdName.replace(name, '');
    return (<div className={Styles.itemContainer}>
      <div style={{ 'backgroundColor': COLORS.white, height }}>
        {width &&
          <img src={productImageUrl} className={Styles.image} onClick={this.redirectToPDP} />
        }
      </div>
      <div>
        <div className={Styles.wishList}>
          {this.renderWishListButton()}
        </div>
        <div className={Styles.info} onClick={this.redirectToPDP}>
          <ul className={Styles.list}>
            <li className={Styles.brandName}>
              <div className={Styles.textOverflow}>{name}</div>
            </li>
            <li className={Styles.name}>
              <div className={Styles.textOverflow}>{productName}</div>
            </li>
          </ul>
        </div>
        <div className={Styles.priceDiv}>
          <span className={Styles.price}>
            <Icon name="rupee500" customStyle={{ fontSize: '15px' }} />
            {discounted}
          </span>
          {discounted < mrp
            ? <span>
              <span className={Styles.mrp}>
                <Icon name="rupee500" customStyle={{ fontSize: '11px' }} />
                {mrp}
              </span>
              <span className={Styles.discount}>
                {discount.label}
              </span>
            </span>
            : null
          }
        </div>
      </div>
    </div>);
  }
}

MiniPDP.propTypes = {
  data: PropTypes.object,
  isWishListed: PropTypes.bool,
  handleToast: PropTypes.func,
  height: PropTypes.number,
  width: PropTypes.number,
  index: PropTypes.number,
  orderId: PropTypes.string,
  itemId: PropTypes.number,
  updateWishList: PropTypes.func
};

export default MiniPDP;
