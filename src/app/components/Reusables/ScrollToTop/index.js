import React from 'react';
import Styles from './Styles.css';
import SVGImages from '../SVGImages';

class ScrollToTop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      intervalId: 0,
      showScrollToTop: false
    };
    this.deviceType = window.__myx_deviceData__.deviceChannel;
    this.scrollStepInPx = '200';
    this.delayInMs = '5';
    this.scrollToTop = this.scrollToTop.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    if (window.pageYOffset > 400) {
      this.setState({
        showScrollToTop: true
      });
    } else {
      this.setState({
        showScrollToTop: false
      });
    }
  }

  scrollStep() {
    if (window.pageYOffset === 0) {
      clearInterval(this.state.intervalId);
    }
    window.scrollTo(0, window.pageYOffset - this.scrollStepInPx);
  }

  scrollToTop() {
    clearInterval(this.state.intervalId);
    const intervalId = setInterval(this.scrollStep.bind(this), this.delayInMs);
    this.setState({ intervalId });
  }
  render() {
    if (window.innerWidth < 780) {
      return (<div
        className={(this.state.showScrollToTop ? `${Styles.scroll} ` : ' ') + (this.deviceType === 'mobile' ? Styles.mobileDevice : ' ')}
        onClick={this.scrollToTop}>
        <SVGImages name="moveToTopArrow" customStyle={{ height: '10px', width: '10px' }} />
        <span className={Styles.moveToTop}>GO TO TOP</span>
      </div>);
    }
    return null;
  }
}

export default ScrollToTop;
