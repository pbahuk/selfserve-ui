import React from 'react';

// Style Related Imports.
import Styles from './selfShipInstructions.css';

class SelfShipInstructions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (<div className={Styles.selfShipInstructions}>
      <div className={Styles.heading}> Self-ship to us </div>
      <div> Since the choosen address is not eligible for pickup, you are requested to courier your purchase to us: </div>
      <ul>
        <li>
          Follow instructions as detailed
          <a href="/faqs#returns" > here </a>
        </li>
        <li> Courier your purchase to us </li>
        <li> Get full reimbursement for your courier expenses </li>
      </ul>
    </div>);
  }
}

export default SelfShipInstructions;
