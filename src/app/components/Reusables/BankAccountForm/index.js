import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Style Related Imports.
import Styles from './bankAccountForm.css';

// Functional Imports.
import { getUidx } from '../../../utils/pageHelpers';
import { postBankAccountDetail, getIfscDetails } from '../../../utils/services/crm';
import { updateBankAccountDetail } from '../../../utils/services/payments';
import features from './../../../utils/features';


// React Component Imports.
import MyInput from '../../Reusables/Forms/MyInput';
import Icon from '../../Reusables/Icon';
import { setTimeout } from 'timers';


const ICON_STYLE = { fontSize: '20px', color: '#535766' };
const editMode = 'EDIT';

class BankAccountForm extends React.Component {
  constructor(props) {
    super(props);

    // Functional Binds.
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleRadioClick = this.handleRadioClick.bind(this);
    this.submitBankAccountDetails = this.submitBankAccountDetails.bind(this);
    this.isValidForm = this.isValidForm.bind(this);
    this.closeAllInfo = this.closeAllInfo.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.addBankAccount = this.addBankAccount.bind(this);

    this.state = {
      form: {
        inactive: false,
        isDirty: false,
        error: '',
        fields: {
          // Change it to accountName as per the payload.
          customername: {
            value: '',
            error: '',
            maxLength: 100,
            validation: /^[a-zA-Z ]*$/,
            label: 'Customer Name (as per the bank records) *',
            key: 'accountName'
          },
          bankname: {
            value: '',
            error: '',
            maxLength: 1000,
            validation: /^[ A-Za-z0-9_$-.+!*'/()#\[\]]*$/,
            label: 'Bank Name *',
            key: 'bank'
          },
          ifsc: {
            value: '',
            error: '',
            maxLength: 11,
            validation: /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/,
            label: 'Bank IFSC Code *',
            key: 'ifsc'
          },
          accountnumber: {
            value: '',
            error: '',
            maxLength: 18,
            validation: /^[0-9]{9,18}$/,
            label: 'Account Number *',
            key: 'accountNumber'
          },
          reaccountnumber: {
            value: '',
            error: '',
            maxLength: 18,
            validation: /^[0-9]{9,18}$/,
            label: 'Re-enter Account Number *',
            key: 'accountNumber'
          },
          // Uppercase is mandatory in the request format.
          accountType: {
            value: '',
            error: '',
            label: 'Account Type *',
            options: ['SAVINGS', 'CURRENT'],
            key: 'accountType'
          },
          branch: {
            value: '',
            error: '',
            maxLength: 1000,
            validation: /^[ A-Za-z0-9_$-.+!*'/()#\[\]]*$/,
            label: 'Branch Name *',
            key: 'branch'
          }
        }
      },
      activeIfscInfo: false,
      ifscInfo: 'IFSC is an 11-digit alphanumeric code. It can be found on most cheque books and passbooks.',
      ifscStatus: null,
      checkingIfsc: false,
      ifscValidationFG: features('mymyntra.ifscvalidation.enable') === true

    };
  }

  componentWillMount() {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    this.mode = get(this, 'props.mode', '').toUpperCase();
    if (this.mode === editMode) {
      this.initialValidation(fields);
      this.setState({ form }, () => {
        this.isValidForm();
      });
    }
  }

  getIfscDetails = (ifscCode) => {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    const that = this;
    getIfscDetails(ifscCode, (err, res) => {
      const update = {
        checkingIfsc: false
      };
      if (!err) {
        const ifscResponse = at(res, 'body') || {};
        const message = at(ifscResponse, 'message') || '';
        let result = at(ifscResponse, 'status');
        if (result && result.toLowerCase() === 'success') {
          fields.ifsc = { ...fields.ifsc, ...{ error: '' } };
          fields.bankname = { ...fields.bankname, ...{ value: at(ifscResponse, 'bankName'), error: '' } };
          fields.branch = { ...fields.branch, ...{ value: at(ifscResponse, 'branchName'), error: '' } };
        } else if (result && result.toLowerCase() === 'failed' && message && message.toLowerCase().indexOf('ifscerror') !== -1) {
          fields.ifsc = { ...fields.ifsc, ...{ error: 'Please double check if IFSC code is correct' } };
        } else {
          result = null;
        }
        update.form = form;
        update.ifscStatus = result;
      }
      that.setState(update);
    });
    that.setState({ checkingIfsc: true, ifscStatus: null });
  }

  closeAllInfo() {
    this.setState({
      activeIfscInfo: false
    });
  }

  clearIfsc = (event) => {
    const { form = {} } = this.state;
    const { fields = {} } = form;

    fields.ifsc = { ...fields.ifsc, ...{ value: '', error: '' } };
    this.setState({
      form
    });
    event.stopPropagation();
  }

  toggleActiveIfscInfo = (event) => {
    this.setState((state) => ({ activeIfscInfo: !state.activeIfscInfo }));
    event.stopPropagation();
  }

  handleChange(target) {
    const { form = {}, ifscValidationFG } = this.state;
    const { fields = {} } = form;
    form.isDirty = true;
    const update = {
      form
    };
    if (target.name === 'ifsc') {
      update.ifscStatus = null;
      if (target.value.length <= 11) {
        fields[target.name] = { ...fields[target.name], ...{ value: target.value, error: '' } };
      }
      if (ifscValidationFG && target.value.match(fields[target.name].validation)) {
        this.getIfscDetails(target.value);
      }
    } else {
      fields[target.name] = { ...fields[target.name], ...{ value: target.value, error: '' } };
    }
    update.form = form;

    this.setState(update);
  }

  handleFocus(target) {
    const { form = {} } = this.state;
    const { fields = {} } = form;

    fields[target.name] = { ...fields[target.name], ...{ error: '' } };
    this.setState({
      form
    });
  }

  initialValidation = (fields) => {
    for (const field in fields) {
      if (fields.hasOwnProperty(field)) {
        this.validateFields(fields, field, at(this.props.accountData, fields[field].key));
      }
    }
  }

  validateFields(fields, key, val) {
    const { ifscValidationFG } = this.state;
    fields[key] = { ...fields[key], ...{ value: val, error: '' } };
    switch (key) {
      case 'accountnumber':
        if (!val) {
          fields[key] = { ...fields[key], ...{ error: 'Required' } };
        } else if (!val.match(fields[key].validation)) {
          fields[key] = { ...fields[key], ...{ error: 'Account number should be minimum 9 digits.' } };
        } else if (fields.reaccountnumber.value && fields[key].value === fields.reaccountnumber.value) {
          fields.reaccountnumber = { ...fields.reaccountnumber, ...{ error: '' } };
        }
        break;
      case 'ifsc' :
        if (!val) {
          fields[key] = { ...fields[key], ...{ error: 'Required' } };
        } else if (!val.match(fields[key].validation)) {
          fields[key] = { ...fields[key], ...{ error: 'Please provide a valid IFSC code.' } };
        }
        if (ifscValidationFG && val.match(fields[key].validation)) {
          this.getIfscDetails(val);
        }
        break;

      default:
        if (!val) {
          fields[key] = { ...fields[key], ...{ error: 'Required' } };
        } else if (!val.match(fields[key].validation)) {
          fields[key] = { ...fields[key], ...{ error: 'Please provide a valid input' } };
        }
    }
  }

  handleBlur(target) {
    const { form = {} } = this.state;
    const { fields = {} } = form;

    switch (target.name) {
      case 'accountnumber':
        if (!target.value) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Required' } };
        } else if (!target.value.match(fields[target.name].validation)) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Account number should be minimum 9 digits.' } };
        } else if (fields.reaccountnumber.value && fields[target.name].value === fields.reaccountnumber.value) {
          fields.reaccountnumber = { ...fields.reaccountnumber, ...{ error: '' } };
        }
        break;

      case 'reaccountnumber':
        if (!target.value) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Required' } };
        } else if (!target.value.match(fields[target.name].validation)) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Account number should be minimum 9 digits.' } };
        } else if (fields.accountnumber.value && fields[target.name].value !== fields.accountnumber.value) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Account number not matching' } };
        }
        break;

      case 'ifsc' :
        if (!target.value) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Required' } };
        } else if (!target.value.match(fields[target.name].validation)) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Please provide a valid IFSC code.' } };
        }
        break;

      default:
        if (!target.value) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Required' } };
        } else if (!target.value.match(fields[target.name].validation)) {
          fields[target.name] = { ...fields[target.name], ...{ error: 'Please provide a valid input' } };
        }
    }
    this.setState({
      form,
      activeIfscInfo: false
    });
  }

  handleRadioClick(event) {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    const target = event.currentTarget;
    const name = target.getAttribute('name');
    const value = target.getAttribute('value');
    form.isDirty = true;

    fields[name] = { ...fields[name], ...{ value, error: '' } };
    this.setState({
      form
    });
  }

  addBankAccount() {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    const that = this;
    const data = {
      neftAccountEntry: {
        login: getUidx(),
        accountName: fields.customername.value,
        bankName: fields.bankname.value,
        ifscCode: fields.ifsc.value,
        accountNumber: fields.accountnumber.value,
        accountType: fields.accountType.value,
        branch: fields.branch.value
      }
    };

    postBankAccountDetail(data, (err, res) => {
      if (err) {
        console.error('[Error BankAccount Submit]', err);
        form.error = 'Something Went Wrong';
      } else if (get(res, 'body.neftAccountResponse.status.statusType') === 'ERROR') {
        console.error(get(res, 'body.neftAccountResponse.status.statusMessage', 'Error Saving Bank Details'));
        form.error = 'Error Saving Bank Details';
      }

      if (form.error) {
        form.inactive = false;
        that.setState({
          form
        }, () => {
          setTimeout(() => {
            form.error = '';
            that.setState({
              form
            });
          }, 5000);
        });
      } else {
        that.props.addBankAccountProcessComplete();
      }
    });
  }

  updateBankAccount() {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    const that = this;
    const data = {
      accountName: fields.customername.value,
      ifsc: fields.ifsc.value,
      branch: fields.branch.value,
      bank: fields.bankname.value,
      accountType: fields.accountType.value,
      accountNumber: fields.accountnumber.value
    };
    const payload = { ...this.props.accountData, ...data };
    updateBankAccountDetail(payload, (err) => {
      if (err) {
        console.error('[Error BankAccount Submit]', err);
        form.error = 'Error Saving Bank Details';
        form.inactive = false;
        that.setState({
          form
        }, () => {
          setTimeout(() => {
            form.error = '';
            that.setState({
              form
            });
          }, 5000);
        });
      } else {
        that.props.updateBankAccountProcessComplete();
      }
    });
  }


  submitBankAccountDetails = () => {
    const { form = {} } = this.state;
    form.inactive = true;
    this.setState({
      form
    }, () => {
      if (this.mode === editMode) {
        this.updateBankAccount();
      } else {
        this.addBankAccount();
      }
    });
  }

  isValidForm() {
    const { form = {} } = this.state;
    const { fields = {} } = form;
    let isValidForm = true;
    if (!form.isDirty) {
      return false;
    }
    for (const key in fields) {
      if (fields.hasOwnProperty(key)) {
        if (key === 'ifsc') {
          if (!fields[key].value || !fields[key].value.match(fields[key].validation)) {
            isValidForm = false;
            break;
          }
        } else if (key === 'reaccountnumber') {
          console.log('reaccountnumber', fields[key].value !== fields.accountnumber.value);
          if (!fields[key].value.match(fields[key].validation) || fields[key].value !== fields.accountnumber.value) {
            isValidForm = false;
            break;
          }
        } else if (!fields[key].value || fields[key].error || !fields[key].value.match(fields[key].validation)) {
          isValidForm = false;
          break;
        }
      }
    }
    return isValidForm;
  }

  renderAccountTypes(formElement) {
    const accountTypes = formElement.options || [];

    return (<div className={Styles.accountTypesWrapper}>
      <div className={Styles.accountTypeLabel}> {formElement.label} </div>
        {accountTypes.map((accountType, key) => {
          const icon = formElement.value === accountType ?
            { name: 'radio_active', style: { ...ICON_STYLE, color: '#14cda8', fontSize: '24px' } } :
            { name: 'radio_inactive', style: { ...ICON_STYLE, fontSize: '24px' } };

          return (<div key={key} className={Styles.radioInput} onClick={this.handleRadioClick} name="accountType" value={accountType}>
            <span name="accountType" value={accountType} className={Styles.selector} >
              <Icon customStyle={icon.style} name={icon.name} />
            </span>

            <span className={Styles.accountType}>
              {accountType.toLowerCase()}
            </span>

          </div>);
        })}
    </div>);
  }

  renderIfscInput(fields, showInfo, status, clearIfsc, toggleActiveIfscInfo, activeIfscInfo) {
    const { ifscInfo } = this.state;
    let customView = null;
    if (showInfo) {
      customView = (<div className={`${Styles.iconWrapper} ${activeIfscInfo ? Styles.tooltip : ''}`}>
      {
          activeIfscInfo &&
            <div className={Styles.activePointsInfo}>
              <div className={Styles.triangle} />
              <div className={Styles.infoText}>
              {ifscInfo}
              </div>
            </div>
        }
        <Icon
          name="info"
          customStyle={{ fontSize: '24px', color: '#7e818c', float: 'right', marginTop: '-4px' }}
          onClick={(event) => toggleActiveIfscInfo(event)} />
      </div>);
    } else if (status === 'failed') {
      customView = (<div className={`${Styles.iconWrapper} ${Styles.noPadding}`}>
        <Icon
          name="close"
          customStyle={{ fontSize: '16px', color: '#f16565', float: 'right' }}
          onClick={(event) => clearIfsc(event)} />
      </div>);
    } else if (status === 'success') {
      customView = (<div className={Styles.iconWrapper}>
        <Icon
          name="tick1"
          className={Styles.tick} />
        <span className={Styles.verified} >VERIFIED</span>
      </div>);
    }

    return (<MyInput
      type="text"
      name="ifsc"
      label={fields.ifsc.label}
      inputSize="md"
      customised
      overlayStyle={this.state.checkingIfsc ? [Styles.buttonLoader] : []}
      maxLength={fields.ifsc.maxLength}
      errorMessage={fields.ifsc.error}
      value={fields.ifsc.value}
      onChange={this.handleChange}
      onFocus={this.handleFocus}
      onBlur={this.handleBlur}
      customClass={`${Styles.bold} ${Styles.ifsc}`}
      customView={customView} />);
  }

  render() {
    const { form = {}, activeIfscInfo, ifscStatus } = this.state;
    const { fields = {} } = form;
    const isValidForm = this.isValidForm();

    return (<div className={Styles.bankAccountForm} onClick={() => this.closeAllInfo()} >
      <div className={Styles.shimmer} />
      <div className={Styles.modal}>
        <div className={Styles.modalHeading}> {this.mode === editMode ? 'Edit Bank Account' : 'Add Bank Account'} </div>
        <div className={Styles.form}>
          <MyInput
            type="text"
            name="customername"
            label={fields.customername.label}
            inputSize="md"
            customised={false}
            maxLength={fields.customername.maxLength}
            errorMessage={fields.customername.error}
            value={fields.customername.value}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            customClass={Styles.bold} />
          <MyInput
            type="tel"
            name="accountnumber"
            label={fields.accountnumber.label}
            inputSize="md"
            customised={false}
            maxLength={fields.accountnumber.maxLength}
            errorMessage={fields.accountnumber.error}
            value={fields.accountnumber.value}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            customClass={Styles.bold} />
          <MyInput
            type="tel"
            name="reaccountnumber"
            label={fields.reaccountnumber.label}
            inputSize="md"
            customised={false}
            maxLength={fields.reaccountnumber.maxLength}
            errorMessage={fields.reaccountnumber.error}
            value={fields.reaccountnumber.value}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            customClass={Styles.bold} />
            {
              this.renderIfscInput(
                fields,
                !fields.ifsc.value || fields.ifsc.value.length === 0,
                ifscStatus,
                this.clearIfsc,
                this.toggleActiveIfscInfo,
                activeIfscInfo
              )
            }

          <MyInput
            type="text"
            name="bankname"
            label={fields.bankname.label}
            inputSize="md"
            customised={false}
            maxLength={fields.bankname.maxLength}
            errorMessage={fields.bankname.error}
            value={fields.bankname.value}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            customClass={Styles.bold} />
          <MyInput
            type="text"
            name="branch"
            label={fields.branch.label}
            inputSize="md"
            customised={false}
            maxLength={fields.branch.maxLength}
            errorMessage={fields.branch.error}
            value={fields.branch.value}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            customClass={Styles.bold} />
          {this.renderAccountTypes(fields.accountType)}
        </div>
        {
          form.error && <div className={Styles.errorToast} >
                  {form.error}
          </div>
        }
        <div className={Styles.buttonErrorWrapper}>
          <div className={Styles.buttons}>
            <div className={Styles.button} onClick={this.props.cancelAction}> Cancel </div>
            <div
              className={`${Styles.button} ${!isValidForm ? Styles.disabled : ''}`}
              onClick={() => { if (isValidForm && !form.inactive) { this.submitBankAccountDetails(); } }}>
              {this.mode === editMode ? 'Update' : 'Confirm'}
              <div className={form.inactive ? Styles.buttonLoader : ''} />
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}

BankAccountForm.propTypes = {
  cancelAction: PropTypes.func,
  confirmAction: PropTypes.func,
  addBankAccountProcessComplete: PropTypes.func,
  updateBankAccountProcessComplete: PropTypes.func,
  accountData: PropTypes.object,
  mode: PropTypes.string
};

export default BankAccountForm;
