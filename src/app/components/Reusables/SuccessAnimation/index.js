import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

import Icon from '../../Reusables/Icon';


// Style Related Imports.
import Styles from './successAnimation.css';

class SuccessAnimation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      display: true
    };
  }

  onRef = (ref) => {
    if (ref) {
      const tick = ReactDOM.findDOMNode(ref);
      tick.addEventListener('animationend', () => {
        this.setState({
          display: false
        });
        tick.removeEventListener('animationend', null);
      });
    }
  };

  render() {
    const { isApp, isAndroid } = this.props;
    const { display } = this.state;
    return (
      <div className={`${Styles.parent} ${display ? '' : Styles.hide}`}>
        <Icon name="success_tick" className={`${Styles.tick} ${isApp && Styles.tickApp} ${isAndroid && Styles.android}`} myRef={(ref) => this.onRef(ref)} />
        <div className={Styles.circle} />
      </div>
    );
  }


}

SuccessAnimation.propTypes = {
  isApp: PropTypes.bool,
  isAndroid: PropTypes.bool
};

export default SuccessAnimation;
