import PropTypes from 'prop-types';
import React from 'react';
import styles from './icon.css';

const Icon = (props) => (
  <span
    ref={(ref) => props.myRef && props.myRef(ref)} style={props.customStyle || {}}
    className={`${styles.icon} icon-${props.name} ${props.className || ''}`} onClick={props.onClick}>
  </span>
);

Icon.propTypes = {
  name: PropTypes.string,
  customStyle: PropTypes.object,
  className: PropTypes.string,
  onClick: PropTypes.func,
  myRef: PropTypes.func
};

export default Icon;
