import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './addressAccordian.css';
import classnames from 'classnames/bind';
const bindClass = classnames.bind(Styles);

// React Component Imports.
import Icon from '../../Reusables/Icon';
import SelfShipInstructions from '../SelfShipInstructions';
import { getDeviceData, unescapeHTML } from '../../../utils/pageHelpers';

const ICON_STYLE = { fontSize: '18px', color: '#535766' };

class AddressAccordian extends React.Component {
  constructor(props) {
    super(props);
    this.renderAddressButtons = this.renderAddressButtons.bind(this);

    this.state = {
    };
  }

  renderAddressButtons() {
    return (<div className={Styles.buttons}>
      <div className={Styles.button} onClick={this.props.editAddress}> EDIT </div>
      <div className={Styles.buttonDivider}></div>
      <div className={Styles.button} onClick={this.props.deleteAddress}> REMOVE </div>
    </div>);
  }

  render() {
    const { address, active, type } = this.props;
    const addressString = address.streetAddress || address.address;
    const addressTrimmed = !active && addressString.length > 30 ? `${addressString.substring(0, 30)}...` : addressString;
    let accordianComponent = null;


    if (type === 'myAddress') {
      accordianComponent = (
        <div className={`${Styles.addressAccordian} ${Styles[type]}`} onClick={() => this.props.action(address)}>
          <div className={Styles.address}>
            <div className={Styles.nameComponent}>
              <span className={Styles.name}>
                {(at(address, 'name') || at(address, 'user.name') || '').toLowerCase()}
              </span>
              {address.addressType && <span className={Styles.addressType}> {at(address, 'addressType')} </span>}
            </div>
            <div> {unescapeHTML(addressTrimmed)} </div>
            <div> {unescapeHTML(at(address, 'locality'))} </div>
            <div> {unescapeHTML(at(address, 'city'))} - {at(address, 'pincode')} </div>
            {active &&
              <div className={Styles.addressDetails}>
                <div>{at(address, 'stateName') || at(address, 'state.name')}</div>
                <div className={Styles.mobile}>Mobile: {at(address, 'user.mobile') || at(address, 'mobile')}</div>
                {!address.defaultAddress && <span className={Styles.makeDefaultText} onClick={this.props.makeDefaultAddress}>
                  Make This Default
                </span>}
              </div>}
          </div>
          {active && this.renderAddressButtons()}
        </div>);
    } else if (type === 'returnsChangeAddress' || type === 'exchangeChangeAddress') {
      const pickupServiceable = at(this, 'props.activeServiceable');
      const { isDesktop } = getDeviceData();
      const icon = active ?
        { name: 'radio_active', style: { ...ICON_STYLE, color: '#FF3F6C' } } :
        { name: 'radio_inactive', style: ICON_STYLE };

      accordianComponent = (
        <div
          className={bindClass(`${Styles.addressAccordian} ${Styles[type]}`, { web: isDesktop, mobile: !isDesktop })}
          onClick={() => { if (!this.props.active) { this.props.action(address); } }}>
          <div className={Styles.selector}>
            <Icon customStyle={icon.style} name={icon.name} />
          </div>
          <div className={`${Styles.address} ${active && !pickupServiceable ? Styles.nonServiceable : ''}`}>
            <div className={Styles.nameComponent}>
              <span className={Styles.name}>
                {(at(address, 'name') || at(address, 'user.name') || '').toLowerCase()}
              </span>
              {address.defaultAddress &&
                <span> (Default) </span>}
              {address.addressType &&
                <span className={Styles.addressType}> {at(address, 'addressType')} </span>}
            </div>
            <div> {unescapeHTML(addressTrimmed)} </div>
            <div> {unescapeHTML(at(address, 'locality'))}, {unescapeHTML(at(address, 'city'))} - {at(address, 'pincode')} </div>
            {active &&
              <div className={Styles.addressDetails}>
                <div>{at(address, 'stateName') || at(address, 'state.name')}</div>
                <div className={Styles.mobile}>Mobile: {at(address, 'user.mobile') || at(address, 'mobile')}</div>
                {!pickupServiceable && (type === 'returnsChangeAddress') &&
                  <div className={Styles.nonServiceableMessage}>
                    <SelfShipInstructions />
                  </div>}
                {!pickupServiceable && (type === 'exchangeChangeAddress') &&
                  <div className={Styles.nonServiceableMessage}>
                    Address not serviceable, Please select another address
                  </div>}
                {this.props.errorMessage &&
                  <div className={Styles.errorMessage}>
                    {this.props.errorMessage}
                  </div>}
              </div>}
          </div>
          {active && this.renderAddressButtons()}
        </div>);
    } else {
      const deliveryServiceable = at(this, 'props.activeServiceable');
      const { isDesktop } = getDeviceData();
      const icon = active ?
        { name: 'radio_active', style: { ...ICON_STYLE, color: '#14cda8' } } :
        { name: 'radio_inactive', style: ICON_STYLE };

      accordianComponent = (
        <div
          className={bindClass(`${Styles.addressAccordian} ${Styles[this.props.type]}`, { web: isDesktop, mobile: !isDesktop })}
          onClick={() => this.props.action(address)}>
          <div className={Styles.selector}>
            <Icon customStyle={icon.style} name={icon.name} />
          </div>
          <div className={`${Styles.address} ${active && !deliveryServiceable ? Styles.nonServiceable : ''}`}>
            <div className={Styles.nameComponent}>
              <span className={Styles.name}>
                {(at(address, 'name') || at(address, 'user.name') || '').toLowerCase()}
              </span>
              {address.defaultAddress &&
                <span> (Default) </span>}
              {address.addressType &&
                <span className={Styles.addressType}> {at(address, 'addressType')} </span>}
            </div>
            <div> {unescapeHTML(addressTrimmed)} </div>
            <div> {unescapeHTML(at(address, 'locality'))}, {unescapeHTML(at(address, 'city'))} - {at(address, 'pincode')} </div>
            {active &&
              <div className={Styles.addressDetails}>
                <div>{address.stateName}</div>
                <div className={Styles.mobile}>Mobile: {at(address, 'user.mobile') || at(address, 'mobile')}</div>
                {!deliveryServiceable &&
                  <div className={Styles.nonServiceableMessage}>
                    Address not serviceable, Please select another address
                  </div>}
                {this.props.errorMessage &&
                  <div className={Styles.nonServiceableMessage}>
                    {this.props.errorMessage}
                  </div>}
              </div>}
          </div>
          {active && this.renderAddressButtons()}
        </div>);
    }

    return accordianComponent;
  }
}

AddressAccordian.propTypes = {
  type: PropTypes.string,
  address: PropTypes.object,
  active: PropTypes.bool,
  editAddress: PropTypes.func,
  deleteAddress: PropTypes.func,
  makeDefaultAddress: PropTypes.func,
  action: PropTypes.func,
  errorMessage: PropTypes.string
};

export default AddressAccordian;
