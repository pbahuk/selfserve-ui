import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './svgImages.css';

const SVGImages = (props) => (
  <span
    style={props.customStyle || {}}
    className={`${Styles.svg} ${Styles[props.name]} ${props.className || ''}`}
    onClick={props.onClick} />
);

SVGImages.propTypes = {
  name: PropTypes.string,
  customStyle: PropTypes.object,
  onClick: PropTypes.func,
  className: PropTypes.string
};

export default SVGImages;
