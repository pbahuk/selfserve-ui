import React from 'react';
import PageStyles from './styles.css';
import PropTypes from 'prop-types';
import SVGImages from '../SVGImages';

const CCintervention = (props) => (<div className={PageStyles.helpcenter} onClick={() => { window.location = 'tel:+91-80-61561999'; }}>
  <div className={PageStyles.helpcenterIcon}>
    <SVGImages name={'phoneIcon'} customStyle={{ height: '35px', width: '35px' }} />
  </div>
  <div>
    <span className={PageStyles.helpcenterTitle}>NEED HELP?</span><br />
    <span className={PageStyles.helpcenterDesc}>Are you {props.type} the order due to some issue? Contact us, we will be able to help!</span>
  </div>
  <div className={PageStyles.helpcenterArrowIcon}>
    <SVGImages name={'ccInterventionArrow'} customStyle={{ width: '5px', height: '10px' }} />
  </div>
</div>);

CCintervention.propTypes = {
  type: PropTypes.text
};

export default CCintervention;
