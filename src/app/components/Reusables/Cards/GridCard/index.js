import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';

// Common components
import Image from '../../../Reusables/Image';

// Styles
import Styles from './gridCard.css';

// always make sure to have equal width as per screen size

const GridCard = ({ cardData = {}, triggerClickEvent, getSecureUrl }) => {
  const windowWidth = Math.floor((window.innerWidth - 5 * 12) / 4);
  return (<div className={Styles.cardContents}>
      {get(cardData, 'props.grid', []).map((data, index) => {
        const clickEventData = {
          vPos: index % 4,
          hPos: Math.floor(index / 4),
          data,
          contentType: cardData.contentType,
          cardType: cardData.type,
          entityName: data.link,
          entityId: data.id
        };
        // add on click in 'a' tag
        return (
          data.src &&
          data.link && (
            <a
              href={data.link}
              key={data.link}
              onClick={() => triggerClickEvent(clickEventData)} >
              <div className={Styles.productContainer}>
                <Image
                  src={getSecureUrl(
                    get(data, 'src', ''),
                    windowWidth,
                    windowWidth,
                    'fill'
                  )}
                  width={windowWidth}
                  height={windowWidth}
                  showBackgroundColor={false} />
              </div>
            </a>
          )
        );
      })}
  </div>
);
};

GridCard.propTypes = {
  cardData: PropTypes.object,
  triggerClickEvent: PropTypes.func,
  getSecureUrl: PropTypes.func
};
export default GridCard;
