import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';

// Common Components
import Image from '../../../Reusables/Image';
import Icon from '../../Icon';

// Local Components
import CardCarousel from './CardCarousel';

// Styles
import Styles from './plainCard.css';

const IMAGE_WIDTH = 150;
const IMAGE_HEIGHT = 200;

const ProductBlock = ({ cardData = {}, triggerClickEvent, getSecureUrl }) =>
  (<div className={Styles.contents}>
      {get(cardData, 'props.products', []).map((data, index) => {
        const clickEventData = {
          vPos: index,
          hPos: 0,
          data,
          contentType: cardData.contentType,
          cardType: cardData.type,
          entityName: data.stylename,
          entityId: data.styleid
        };
        return (
          data.styleid &&
          get(data, 'image.src') && (
            <a
              href={`/${data.styleid}`}
              onClick={() => triggerClickEvent(clickEventData)}
              key={data.styleid}>
              <div className={Styles.productContainer}>
                <Image
                  src={getSecureUrl(get(data, 'image.src'), IMAGE_WIDTH)}
                  width={IMAGE_WIDTH}
                  height={IMAGE_HEIGHT}
                  showBackgroundColor="true" />
                <div className={Styles.productData}>
                  <h3 className={Styles.brandName}>
                    {get(data, 'brands_facet')}
                  </h3>
                  <h4 className={Styles.productName}>
                    {get(data, 'stylename')}
                  </h4>
                  <div className={Styles.priceContainer}>
                    <span className={Styles.price}>
                      <Icon name="rupee" className={Styles.rupeeIcon} />
                      <span className={Styles.iconRupee} />
                      {get(data, 'discounted_price')}
                    </span>
                    {data.discount ? (
                      <span className={Styles.originalPrice}>
                        <Icon name="rupee" className={Styles.rupeeIcon} />
                        <span className={Styles.iconRupee} />
                        {get(data, 'price')}
                      </span>
                    ) : (
                      ''
                    )}
                    {data.discount ? (
                      <span className={Styles.discPercent}>
                        {get(data, 'discount_label')}
                      </span>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
              </div>
            </a>
          )
        );
      })}
  </div>
);

const PlainCard = ({ cardData = {}, triggerClickEvent, getSecureUrl }) => {
  const coverImg = get(cardData, "props['cover-image-url']", '');
  const carousalProps = {
    coverImg,
    title: get(cardData, "props['cover-title']", ''),
    subTitle: get(cardData, "props['cover-subtitle']", ''),
    opacityFactor: !!coverImg
  };

  return (
    <CardCarousel {...carousalProps}>
      <ProductBlock
        cardData={cardData}
        triggerClickEvent={triggerClickEvent}
        getSecureUrl={getSecureUrl} />
    </CardCarousel>
  );
};

PlainCard.propTypes = {
  cardData: PropTypes.object,
  triggerClickEvent: PropTypes.func,
  getSecureUrl: PropTypes.func
};

ProductBlock.propTypes = {
  cardData: PropTypes.object,
  triggerClickEvent: PropTypes.func,
  getSecureUrl: PropTypes.func
};
export default PlainCard;
