import React from 'react';
import PropTypes from 'prop-types';
import { currencyValue } from '../../../utils/pageHelpers';
import at from 'v-at';
import Styles from './priceBreakUp.css';
import mapping from '../../../resources/data/instrumentsMapping';
import Icon from '../../Reusables/Icon';

class PriceBreakUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  renderInfo() {
    let component = null;
    const paymentsInfo = this.props.paymentsInfo || {};
    const discountsInfo = at(paymentsInfo, 'discounts') || {};
    const chargesInfo = at(paymentsInfo, 'charges');
    const instrumentsInfo = at(paymentsInfo, 'instruments');
    const discounts = [];
    const charges = [];
    const instruments = [];
    for (const key in discountsInfo) {
      if (discountsInfo.hasOwnProperty(key) && discountsInfo[key]) {
        discounts.push({
          key: mapping[key],
          value: discountsInfo[key]
        });
      }
    }

    for (const key in chargesInfo) {
      if (chargesInfo.hasOwnProperty(key) && chargesInfo[key]) {
        charges.push({
          key: mapping[key],
          value: chargesInfo[key]
        });
      }
    }
    for (const key in instrumentsInfo) {
      if (instrumentsInfo.hasOwnProperty(key) && instrumentsInfo[key]) {
        instruments.push({
          key: mapping[key],
          value: instrumentsInfo[key]
        });
      }
    }

    component = (<div className={Styles.component}>
      <div><span className={Styles.key}>MRP:</span>
        <Icon name="rupee300" className={Styles.rupeeIcon} />
        {currencyValue(this.props.actualPrice, false)}
      </div>
      {charges.map((charge, index) =>
        <div key={index}>
          <span className={Styles.key}> {charge.key}: </span>
          <span className={Styles.value}>
            <Icon name="rupee300" className={Styles.rupeeIcon} />
            {currencyValue(charge.value / 100, false)}
          </span>
        </div>
      )}
      {discounts.map((discount, index) =>
        <div key={index}>
          <span className={Styles.key}> {discount.key}: </span>
          <span className={Styles.value}>(-)
            <Icon name="rupee300" className={Styles.rupeeIcon} />
            {currencyValue(discount.value / 100, false)}
          </span>
        </div>
      )}
      {instruments.map((instrument, index) =>
        ((instrument.key === 'Loyalty Points' ||
          instrument.key === 'Bank CashBack' ||
          instrument.key === 'Cashback') ?
         (<div key={index}>
           <span className={Styles.key}> {instrument.key}: </span>
           <span className={Styles.value}>(-)
             <Icon name="rupee300" className={Styles.rupeeIcon} />
            {currencyValue(instrument.value / 100, false)}
           </span>
         </div>)
        : (<div key={index}>
          <span className={Styles.key}> {instrument.key}: </span>
          <span className={Styles.value}>
            <Icon name="rupee300" className={Styles.rupeeIcon} />
            {currencyValue(instrument.value / 100, false)}
          </span>
        </div>))
      )}
      <div className={Styles.finalAmount}>
        <span className={Styles.key}>Total:</span>
        <Icon name="rupee300" className={Styles.rupeeIcon} />
        {instrumentsInfo ?
          currencyValue((
          paymentsInfo.amount -
          instrumentsInfo.cashback -
          instrumentsInfo.loyaltyPoints -
          instrumentsInfo.bankCashback) / 100, false)
          : currencyValue(paymentsInfo.amount / 100, false)
        }
      </div>
    </div>);
    return component;
  }

  render() {
    return (<div className={Styles.priceBreakupExpanded}>
      <div className={Styles.topBorder}></div>
      <div className={Styles.details}>
        {this.renderInfo()}
      </div>
    </div>);
  }
}

PriceBreakUp.propTypes = {
  actualPrice: PropTypes.number,
  paymentsInfo: PropTypes.object
};

export default PriceBreakUp;
