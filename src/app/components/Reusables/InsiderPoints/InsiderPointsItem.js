import React from 'react';
import PropTypes from 'prop-types';

import Madalytics from '../../../utils/trackMadalytics';

import Styles from './InsiderPointsItem.css';
import SVGImages from '../SVGImages';
import Tooltip from '../Tooltip';
import ItemInfo from '../ItemInfo/ItemInfo';
import Dialog from '../Dialog';
import { claimInsiderPoints } from '../../../utils/services/insider';
import MobileToast from '../MobileToast';

import statusDisplayedMapping from './statusDisplayedMapping';

class InsiderPointsItem extends React.Component {

  constructor(props) {
    super(props);
    [
      'setInsiderDialogueContent',
      'redirectMyntraInsider',
      'setInsiderPointStatusMessage',
      'showInsiderDialogue',
      'closeInsiderDialog',
      'collectInsiderPoints'
    ].forEach(method => (this[method] = this[method].bind(this)));
    this.state = {
      insiderPointsEarned: false,
      showInsiderDialogue: false,
      loadProceed: false
    };
    this.dialogContent = null;
  }

  componentWillMount() {
    const { itemInfo: { itemId, orderId, insiderItemsPoints: { points, canClaim, status } } } = this.props;
    Madalytics.sendEvent(
      'widgetLoad',
      {
        type: 'My Orders Item Details - Insider points status widget load',
        data_set: {
          payload: { screen: { name: 'My_Orders_Item_details', data_set: {
            entity_id: orderId,
            item_id: itemId
          } } }
        }
      },
      { payload: { widget: {
        name: 'Insider points',
        type: 'list-item',
        custom: {
          v1: itemId.toString(),
          v2: canClaim.toString(),
          v3: points.toString(),
          v4: status.toString()
        }
      } } },
    );
  }

  setInsiderDialogueContent(itemInfo) {
    this.dialogContent = (<div>
      <ItemInfo itemInfo={itemInfo} />
      You will <strong>not be able to return or exchange</strong>, this item.<br></br>Do you wish to proceed?
    </div>);
  }

  setInsiderPointStatusMessage(status, canClaim) {
    let statusMessage = '';
    if (status === 'due' && canClaim && !this.state.insiderPointsEarned) {
      statusMessage = 'You can collect your Insider points now. Doing so will make this item non-returnable/non-exchangeable.';
    } else if (status === 'credited' || this.state.insiderPointsEarned) {
      statusMessage = 'Return/Exchange not available as Insider points were collected.';
    } else if (status === 'due') {
      statusMessage = 'You will be able to collect these Insider points once the item is delivered.';
    }
    return statusMessage;
  }

  showInsiderDialogue(itemInfo) {
    this.setInsiderDialogueContent(itemInfo);
    const { orderId = '', itemId = '', insiderItemsPoints: { points = '' } } = itemInfo;
    this.setState({
      showInsiderDialogue: true
    });

    Madalytics.sendEvent(
      'widgetLoad',
      {
        type: 'My Orders Item Details - Insider points claim half card load',
        data_set: {
          payload: { screen: { name: 'My_Orders_Item_details', data_set: {
            entity_id: orderId.toString()
          } } }
        }

      },
      { payload: { widget: {
        name: 'Insider points',
        type: 'list-item',
        custom: {
          v1: itemId.toString(),
          v2: points.toString()
        }
      } } },
    );
  }

  closeInsiderDialog() {
    this.setState({ showInsiderDialogue: false });
  }

  collectInsiderPoints() {
    const { itemInfo: { itemId, orderId, brandName, articleType, insiderItemsPoints: { points } } } = this.props;
    this.setState({
      loadProceed: true
    });
    claimInsiderPoints(itemId, (err, res) => {
      const resBody = res.body || {};
      const claimedSuccessfully = !err && resBody.message === 'Success';
      if (claimedSuccessfully) {
        this.setState({
          insiderPointsEarned: true,
          showInsiderDialogue: false,
          loadProceed: false
        }, () => {
          this.props.insiderPointsCollected(points, brandName, articleType);
        });
      } else {
        this.setState({
          showErrorToast: 'Something went wrong, Please try again later!',
          loadProceed: false
        });
        setTimeout(() => {
          this.setState({
            showErrorToast: null
          });
        }, 3000);
      }
      Madalytics.sendEvent(
        'widgetClick',
        {
          type: 'My Orders Item Details - Insider points claim half card item click',
          data_set: {
            payload: { screen: { name: 'My_Orders_Item_details', data_set: {
              entity_type: 'order',
              entity_id: orderId.toString()
            } } }
          }
        },
        { payload: { widget: {
          name: 'Insider points',
          type: 'list-item',
          custom: {
            v1: itemId.toString(),
            v2: 'proceed',
            v3: claimedSuccessfully ? 'success' : 'failure'
          }
        } } },
      );
    });
  }

  redirectMyntraInsider = () => {
    window.location = '/myntrainsider';
  };

  render() {
    const {
      type,
      showButtonsItemDetails,
      itemInfo: { insiderItemsPoints: { points, canClaim, status } },
      itemInfo = {}
    } = this.props;
    const statusMessage = this.setInsiderPointStatusMessage(status, canClaim);
    let htmlContent = null;
    if (points) {
      htmlContent = (<div>
        <div className={`${Styles.claimInsiderPoints} ${showButtonsItemDetails ? Styles.bottomBorder : null}`}>
          <div className={Styles.insiderIcon}>
            <SVGImages name={'insiderIcon'} customStyle={{ height: '14px', width: '20px' }} />
          </div>
          <div className={Styles.insiderPointsStatusInfo}>
            <div className={Styles.insiderPoints}>
              <span>{points} </span>
              Insider points {this.state.insiderPointsEarned ? statusDisplayedMapping.credited : statusDisplayedMapping[status]}
            </div>
            <div className={Styles.insiderPointsHelp}>
              {statusMessage !== '' ? <Tooltip message={statusMessage}>
                <SVGImages name={'info'} customStyle={{ height: '20px', width: '20px' }} />
              </Tooltip> : null}
            </div>
          </div>
          {canClaim && !this.state.insiderPointsEarned
            ? <div className={Styles.insiderPointsAction} onClick={() => this.showInsiderDialogue(itemInfo)}>
              <div>Collect Points</div>
            </div>
            : null
          }
          {type === 'item' && status === 'credited' || this.state.insiderPointsEarned
          ? <a href="/myntrainsider" className={Styles.insiderPointsAction}>
            <div>Know More</div>
          </a>
          : null
          }
        </div>
        {this.state.showInsiderDialogue && this.dialogContent && <Dialog
          showItemDetails
          customStyles={{ height: 'auto' }}
          title={`Collect ${points} Insider points`}
          content={this.dialogContent}
          buttons={[
            { displayName: 'Cancel', action: 'cancel' },
            { displayName: 'Proceed', action: 'proceed', customClass: this.state.loadProceed ? ['buttonLight', 'buttonLoader'] : 'buttonBlue' }
          ]}
          onOverlayClick={this.closeInsiderDialog}
          actions={{ cancel: this.closeInsiderDialog, proceed: () => this.collectInsiderPoints(itemInfo.itemId) }} />}
        {this.state.showErrorToast && <MobileToast text={this.state.showErrorToast} error />}
      </div>);
    }
    return htmlContent;
  }

}

InsiderPointsItem.propTypes = {
  itemInfo: PropTypes.object,
  showButtonsItemDetails: PropTypes.bool,
  type: PropTypes.string,
  insiderPointsCollected: PropTypes.func
};

export default InsiderPointsItem;
