import React from 'react';
import PropTypes from 'prop-types';

import Styles from './tooltip.css';

class Tooltip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayTooltip: false
    };
    [
      'toggleTooltip'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  toggleTooltip() {
    this.setState({ displayTooltip: !this.state.displayTooltip });
  }

  render() {
    return (
      <span
        className={Styles.tooltip}
        onMouseLeave={this.toggleTooltip}
        onClick={this.toggleTooltip} >
        {this.state.displayTooltip &&
          <div className={Styles.tooltipBubble}>
            <div className={Styles.tooltipMessage}>{this.props.message}</div>
          </div>
        }
        <span
          className={Styles.tooltipTrigger} >
          {this.props.children}
        </span>
      </span>
    );
  }}

Tooltip.propTypes = {
  message: PropTypes.string,
  children: PropTypes.object
};

export default Tooltip;
