import React from 'react';
import PageStyles from './styles.css';
import Madalytics from '../../../utils/trackMadalytics';

const insiderBannerClick = () => {
  Madalytics.sendEvent(
    'widgetClick',
    {
      type: 'my orders page - Insider login banner click'
    },
  );
  window.location = '/myntrainsider';
};

const InsiderEnrollment = () => (<div className={PageStyles.insiderBanner}>
  <div className={PageStyles.header}>
    <span className={PageStyles.title}>MYNTRA INSIDER</span><br />
    <span className={PageStyles.description}>Earn 10 insider points for every Rs 100 purchase </span>
  </div>
  <div className={PageStyles.insiderButton} onClick={insiderBannerClick}>
    Enroll Now
  </div>
</div>);

export default InsiderEnrollment;
