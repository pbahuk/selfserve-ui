import React, { PropTypes } from 'react';
import capitalize from 'lodash/capitalize';

import Input from '../../Reusables/Forms/MyInput';
import Icon from '../../Reusables/Icon';

const ICON_STYLE = {
  marginRight: '4px',
  fontSize: '19px'
};
const ProfileCreationDialog = ({ nameField, Styles, unisexProduct, gender, onGenderClick, handleChange }) => (
  <div>
    <Input
      type="text"
      name="name"
      inputSize="md"
      customised={false}
      label={nameField.label}
      errorMessage={nameField.error}
      value={nameField.value}
      onChange={handleChange} />
    <div className={Styles.genderBlock}>
      <div className={Styles.genderHeader}>Gender</div>
      {unisexProduct ?
        ['male', 'female'].map((g) => {
          const icon = g === gender ? {
            name: 'radio_active',
            style: { ...ICON_STYLE, color: '#ff3f6c' }
          } : {
            name: 'radio_inactive',
            style: ICON_STYLE
          };
          return (
            <label id={g} className={Styles.genderLabel} onClick={onGenderClick}>
              <Icon customStyle={icon.style} name={icon.name} />
              <span className={Styles.radioText}>{capitalize(g)}</span>
            </label>
          );
        }) : <div className={Styles.autoSelectGender}>
            {capitalize(gender)}<span className={Styles.autoSelectText}>(Auto selected based on product)</span></div>
      }
    </div>
  </div>
);

ProfileCreationDialog.propTypes = {
  nameField: PropTypes.object,
  Styles: PropTypes.object,
  unisexProduct: PropTypes.bool,
  gender: PropTypes.string,
  onGenderClick: PropTypes.func,
  handleChange: PropTypes.func
};

export default ProfileCreationDialog;
