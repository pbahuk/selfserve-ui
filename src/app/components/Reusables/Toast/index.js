import React from 'react';
import PropTypes from 'prop-types';
import Styles from './Styles.css';

const Toast = (props) => (
  <button className={props.success ? Styles.sucessToast : Styles.errorToast}>
    <span className={Styles.text}>{props.text}
      <span className={Styles.boldText}>{props.boldText}</span>
    </span>
  </button>
);

Toast.propTypes = {
  text: PropTypes.string,
  boldText: PropTypes.string,
  success: PropTypes.bool
};


export default Toast;

