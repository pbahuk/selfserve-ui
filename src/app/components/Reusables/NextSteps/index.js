import React from 'react';
import PropTypes from 'prop-types';

// Style Related imports.
import Styles from './nextSteps.css';

class NextSteps extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderInstructions = this.renderInstructions.bind(this);
  }

  renderInstructions() {
    let instructionsComponent = null;
    switch (this.props.type) {
      case 'pickup':
        instructionsComponent = (<div className={Styles.instructions}>
          <div className={Styles.textRow}> Our agent will pick up the items with the tags intact. </div>
          <div className={Styles.textRow}> We will check the item for usage & quality </div>
          <div className={Styles.textRow}> After quality & usage check the amount is refunded. </div>
        </div>);
        break;

      case 'self-ship':
        instructionsComponent = (<div className={Styles.instructions}>
          <div className={Styles.textRow}> On a sheet of paper, write Order no. and Return no. and place it with the item in the package </div>
          <div className={Styles.textRow}> Courier the item to the nearest Myntra warehouse,
            <a className={Styles.link} href="/faqs#returns"> FIND WAREHOUSE ID </a> </div>
          <div className={Styles.textRow}> We will check the item for usage & quality </div>
          <div className={Styles.textRow}> After quality & usage check the amount is refunded. </div>
        </div>);
        break;

      case 'exchange':
        instructionsComponent = (<div className={Styles.instructions}>
          <div className={Styles.textRow}> Repack the item(s) with original product packaging and accompanying tags in the provided return bag.</div>
          <div className={Styles.textRow}> Hand over the package to the delivery agent and get the new package from him.</div>
        </div>);
        break;

      default:
        instructionsComponent = (<div className={Styles.instructions}>
          <div className={Styles.textRow}> Please provide your instructions. </div>
        </div>);
    }
    return instructionsComponent;
  }

  render() {
    const { props: { type } } = this;
    return (<div className={Styles.nextSteps}>
      <div className={Styles.heading}> Next Steps </div>
      <div className={`${Styles.instructionsWrapper} ${Styles[this.props.type]}`}>
        {type === 'exchange' ?
          <div>Follow the steps below to prepare your return package for courier to the Myntra warehouse</div> :
          <div className={Styles.emailInstruction}> Please check your email for pickup details </div>}
        <div className={Styles.image} />
        {this.renderInstructions()}
      </div>
    </div>);
  }
}

NextSteps.propTypes = {
  type: PropTypes.string
};

export default NextSteps;
