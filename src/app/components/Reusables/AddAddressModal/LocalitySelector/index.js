import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Input from '../../Forms/MyInput';
import Icon from '../../Icon';
import { getDeviceData } from '../../../../utils/pageHelpers';

import Styles from './localitySelector.css';
import classnames from 'classnames/bind';

const bindClass = classnames.bind(Styles);

const ICON_STYLE = { marginRight: '7px', verticalAlign: 'middle', fontSize: '18px', color: '#535766' };

class LocalitySelector extends React.Component {
  renderLoc(loc) {
    const icon = this.props.selectedLoc === loc ? {
      name: 'radio_active',
      style: { ...ICON_STYLE, color: '#FF3F6C' }
    } : {
      name: 'radio_inactive',
      style: ICON_STYLE
    };

    return (
      <div
        id={loc}
        key={loc}
        className={Styles.locLabel}
        onClick={this.props.onLocClick}>
        <Icon customStyle={icon.style} name={icon.name} />
        <span className={Styles.locText}>{loc}</span>
      </div>);
  }

  render() {
    const { localities } = this.props;
    const { isDesktop } = getDeviceData();
    return (<div>
      <div className={bindClass('shimmer', { web: isDesktop, mobile: !isDesktop })} onClick={this.props.hide} />
      <div className={bindClass('halfCard', { web: isDesktop, mobile: !isDesktop })}>
        <div className={Styles.locFields}>
          <div className={Styles.locHeader}>{isEmpty(localities) ? 'Locality' : 'Locality Selection'}</div>
          <div className={Styles.localities}>
          {localities.map(loc => (
            this.renderLoc(loc)
          ))}
          </div>
          <Input
            type="text"
            name="others"
            customised={false}
            label="Others"
            value={this.props.otherLoc}
            customClass={Styles.locationInput}
            onChange={this.props.otherLocChange} />
        </div>
        <div className={Styles.buttons}>
          <div className={Styles.button} onClick={this.props.hide}>Cancel</div>
          <div className={Styles.button} onClick={this.props.confirmLocation}>Confirm</div>
        </div>
      </div>
    </div>);
  }
}

LocalitySelector.propTypes = {
  selectedLoc: PropTypes.string,
  otherLoc: PropTypes.string,
  confirmLocation: PropTypes.func,
  onLocClick: PropTypes.func,
  otherLocChange: PropTypes.func,
  hide: PropTypes.func,
  localities: PropTypes.array
};

export default LocalitySelector;
