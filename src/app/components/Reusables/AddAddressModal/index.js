import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import at from 'v-at';
import isEmpty from 'lodash/isEmpty';
import forEach from 'lodash/forEach';
import { addAddress, updateAddress, fetchPincodeDetails } from '../../../utils/services/address';
import { getDeviceData } from '../../../utils/pageHelpers';

// Styles Related Imports
import Styles from './addAddressModal.css';
const bindClass = classnames.bind(Styles);

// React Component Imports
import MyInput from './../../Reusables/Forms/MyInput';
import Icon from '../../Reusables/Icon';
import LocalitySelector from './LocalitySelector';

const ICON_STYLE = {
  marginRight: '7px',
  verticalAlign: 'middle',
  fontSize: '18px',
  color: '#535766'
};
const ICON_SELECTED_STYLE = {
  color: '#FF3F6C'
};

const notAvailableDays = ['SATURDAY', 'SUNDAY'];

class AddAddressModal extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.changeSelectedOptions = this.changeSelectedOptions.bind(this);
    this.selectDefault = this.selectDefault.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.openLocalitySelector = this.openLocalitySelector.bind(this);
    this.closeLocalitySelector = this.closeLocalitySelector.bind(this);
    this.confirmLocation = this.confirmLocation.bind(this);
    this.onLocClick = this.onLocClick.bind(this);
    this.otherLocChange = this.otherLocChange.bind(this);
    // Radio Button Functions.
    this.renderOptions = this.renderOptions.bind(this);
    this.renderCheckBoxOptions = this.renderCheckBoxOptions.bind(this);
    this.isValidForm = this.isValidForm.bind(this);

    this.state = {
      activePincode: false,
      localitySelector: false,
      localities: [],
      selectedLoc: '',
      formError: '',
      formInactive: false,
      form: {
        name: {
          value: '',
          error: '',
          maxLength: 30,
          validation: /^[a-zA-Z ]*$/,
          label: 'Name *',
          required: true
        },
        mobile: {
          value: '',
          error: '',
          maxLength: 10,
          validation: /^[6-9][0-9]{9}$/,
          label: 'Mobile *',
          required: true
        },
        pincode: {
          value: '',
          error: '',
          maxLength: 6,
          validation: /^(\d)?\d{6}$/,
          label: 'Pincode *',
          required: true
        },
        state: {
          value: '',
          code: '',
          error: '',
          maxLength: 6,
          validation: /^[a-zA-Z ]*$/,
          label: 'State *',
          required: true
        },
        address: {
          value: '',
          error: '',
          maxLength: 100,
          validation: /^[ A-Za-z0-9_$-.+!*'/()#]*$/,
          label: 'Address (House No, Building, Street, Area) *',
          required: true
        },
        locality: {
          value: '',
          error: '',
          maxLength: 20,
          validation: /^[a-zA-Z ()]*$/,
          label: 'Locality/ Town *',
          required: true
        },
        city: {
          value: '',
          error: '',
          maxLength: 20,
          validation: /^[a-zA-Z ]*$/,
          label: 'City/ District *',
          required: true
        },
        addressType: {
          value: '',
          error: '',
          options: ['Home', 'Office'],
          label: 'Type of Address *',
          required: true
        },
        notAvailableDays: {
          value: notAvailableDays.slice(),
          error: '',
          options: ['Saturday', 'Sunday'],
          label: 'Is your office open on weekends?'
        },
        default: {
          value: false,
          error: '',
          label: 'Make this as my default address'
        },
        otherLoc: {
          value: '',
          error: ''
        }
      }
    };
  }

  componentWillMount() {
    const { addressData, mode } = this.props;
    const form = { ...this.state.form };

    if (!isEmpty(addressData) && mode === 'edit') {
      form.name.value = at(addressData, 'user.name') || addressData.name;
      form.mobile.value = at(addressData, 'user.mobile') || addressData.mobile;
      form.pincode.value = addressData.pincode;
      form.state.value = at(addressData, 'state.name') || addressData.stateName;
      form.state.code = at(addressData, 'state.code') || addressData.stateCode;
      form.address.value = addressData.streetAddress || addressData.address;
      form.locality.value = addressData.locality;
      form.city.value = addressData.city;
      form.addressType.value = addressData.addressType;
      form.notAvailableDays.value = addressData.notAvailableDays || [];
      form.default.value = addressData.defaultAddress || addressData.isDefault;
    }

    this.setState({
      form,
      selectedLoc: form.locality.value
    });
  }

  onLocClick(e) {
    const target = e.currentTarget;
    this.setState({
      selectedLoc: target.id
    });
  }

  handleFocus(target) {
    const { form } = this.state;
    form[target.name] = { ...form[target.name], error: '' };

    this.setState({
      form
    });
  }

  handleChange(target) {
    const { form } = this.state;

    // Special case for handling Pincode change.
    if (target.name === 'pincode' && target.value.length === 6) {
      this.fetchPincodeDetails(target.value);
    }

    form[target.name] = { ...form[target.name], value: target.value, error: '' };
    this.setState({
      form
    });
  }

  handleBlur(target) {
    const form = this.state.form;

    switch (target.name) {
      case 'pincode': {
        if (target.value === '') {
          form[target.name] = { ...form[target.name], error: 'Required' };
        } else if (!target.value.match(form[target.name].validation)) {
          form[target.name] = { ...form[target.name], error: 'Please provide a valid input' };
        } else {
          this.fetchPincodeDetails(target.value);
        }
        break;
      }

      default:
        if (target.value === '') {
          form[target.name] = { ...form[target.name], error: 'Required' };
        } else if (!target.value.match(form[target.name].validation)) {
          form[target.name] = { ...form[target.name], error: 'Please provide a valid input' };
        }
    }
    this.setState({
      form
    });
  }

  openLocalitySelector(target) {
    if (target.name === 'locality') {
      if (this.props.mode === 'edit') {
        this.fetchPincodeDetails(at(this, 'state.form.pincode.value'));
      }
      this.setState({
        localitySelector: true
      });
    }
  }

  closeLocalitySelector() {
    this.setState({
      localitySelector: false
    });
  }

  confirmLocation() {
    const form = { ...this.state.form };
    const location = this.state.selectedLoc;
    form.locality = { ...form.locality, value: location, error: isEmpty(location) ? 'Required' : '' };

    this.setState({
      form,
      localitySelector: false
    });
  }

  otherLocChange(target) {
    const form = { ...this.state.form };
    form.otherLoc.value = target.value;
    this.setState({
      selectedLoc: target.value,
      form
    });
  }

  fetchPincodeDetails(pincode) {
    const { form } = this.state;
    this.setState({
      activePincode: true
    });
    fetchPincodeDetails(pincode, (err, res) => {
      const pincodeDetails = at(res, 'body');
      if (err) {
        console.error('Error [Fetching Pincode Details]', err);
        form.pincode.error = 'Something went wrong! Please try again';
      } else if (at(pincodeDetails, 'city') === '' || at(pincodeDetails, 'status') === 'ERROR') {
        form.pincode.error = 'We do not service this pincode';

        // Clearing State and city values.
        form.city.value = '';
        form.state.value = '';
      } else {
        form.city.value = at(pincodeDetails, 'city');
        form.state.value = at(pincodeDetails, 'stateName');
        form.state.code = at(pincodeDetails, 'state');

        // Clear errors
        form.city.error = '';
        form.state.error = '';
      }
      this.setState({
        activePincode: false,
        localities: at(pincodeDetails, 'locality') || [],
        form
      });
    });
  }

  createPayload() {
    const { form } = this.state;
    let address = {
      userId: at(window, '__myx_session__.login'),
      defaultAddress: form.default.value,
      name: form.name.value,
      address: form.address.value,
      city: form.city.value,
      stateCode: form.state.code,
      stateName: form.state.value,
      countryCode: 'IN',
      countryName: 'India',
      pincode: form.pincode.value,
      email: at(window, '__myx_session__.email'),
      mobile: form.mobile.value,
      locality: form.locality.value,
      addressType: (form.addressType.value || '').toUpperCase(),
      notAvailableDays: form.notAvailableDays.value
    };

    if (this.props.mode === 'edit') {
      address = { ...address, id: this.props.addressData.id };
    }

    return address;
  }

  handleSubmit() {
    const { form } = this.state;
    let saveDisabled = false;
    const saveAddress = this.props.mode === 'edit' ? updateAddress : addAddress;
    const newForm = { ...form };
    forEach(form, (field, key) => {
      if (field.required && isEmpty(field.value)) {
        saveDisabled = true;
        newForm[key].error = 'Required';
      } else {
        newForm[key].error = '';
      }
    });

    if (saveDisabled) {
      this.setState({
        form: newForm
      });
    } else {
      const payload = this.createPayload();
      this.setState({
        formInactive: true
      }, () => {
        saveAddress(payload, (err) => {
          if (err) {
            console.error('error saving address: ', err);
            this.setState({ formError: 'Address could not be saved. Please try again.', formInactive: false });
          } else {
            this.props.closeModal();
            this.props.loadAddresses();
          }
        });
      });
    }
  }

  handleOptionChange(e) {
    const form = { ...this.state.form };
    form.addressType = { ...form.addressType, value: e.currentTarget.id, error: '' };
    if (form.addressType.value === 'Home') {
      form.notAvailableDays.value = notAvailableDays.slice();
    }

    this.setState({
      form
    });
  }

  changeSelectedOptions(e) {
    const form = { ...this.state.form };
    const value = (e.currentTarget.id || '').toUpperCase();
    const naDays = form.notAvailableDays.value;
    const index = naDays.indexOf(value);
    if (index !== -1) {
      naDays.splice(index, 1);
    } else {
      naDays.push(value);
    }

    form.notAvailableDays.value = naDays;
    this.setState({
      form
    });
  }

  selectDefault() {
    const form = { ...this.state.form };
    form.default.value = !form.default.value;

    this.setState({
      form
    });
  }

  isValidForm(form) {
    let isValidForm = true;
    for (const key in form) {
      if (form.hasOwnProperty(key)) {
        if (form[key].value === '' && form[key].required) {
          isValidForm = false;
          break;
        } else if (form[key].error) {
          isValidForm = false;
          break;
        }
      }
    }
    return isValidForm;
  }

  renderOptions(options) {
    const { form } = this.state;
    return options.map((option, key) => {
      const icon = (at(form, 'addressType.value') || '').toLowerCase() === option.toLowerCase() ? {
        name: 'radio_active',
        style: { ...ICON_STYLE, ...ICON_SELECTED_STYLE }
      } : {
        name: 'radio_inactive',
        style: ICON_STYLE
      };
      return (
        <li key={key} className={Styles.inputRow}>
          <label className={Styles.customLabel} onClick={this.handleOptionChange} id={option}>
            <Icon customStyle={icon.style} name={icon.name} />
            <span className={Styles.radioText}>{option}</span>
          </label>
        </li>);
    });
  }

  renderCheckBoxOptions(options) {
    const { form } = this.state;
    return options.map((option, key) => {
      const icon = (form.notAvailableDays.value).indexOf(option.toUpperCase()) === -1 ? {
        name: 'checkbox_active',
        style: { ...ICON_STYLE, ...ICON_SELECTED_STYLE }
      } : {
        name: 'checkbox_inactive',
        style: ICON_STYLE
      };
      return (
        <li key={key} className={Styles.inputRow}>
          <label className={`${Styles.customLabel} ${Styles.checkBoxLabel}`} onClick={this.changeSelectedOptions} id={option}>
            <Icon customStyle={icon.style} name={icon.name} />
            <span className={Styles.checkboxText}>{`Open on ${option}`}</span>
          </label>
        </li>);
    });
  }

  render() {
    const { form } = this.state;
    const { isDesktop } = getDeviceData();
    const { mode = 'add' } = this.props;
    const isValidForm = this.isValidForm(form);

    return (<div>
      <div className={Styles.shimmer} onClick={this.props.closeModal} />
      <div className={bindClass('form', { web: isDesktop, mobile: !isDesktop })}>
        <div className={Styles.modalHeading}> {mode === 'add' ? 'Add New Address' : 'Edit Address'}</div>
        <div className={Styles.cardFields}>
          <div className={Styles.card}>
            <MyInput
              type="text"
              name="name"
              inputSize="md"
              customClass={Styles.addressInput}
              customised={false}
              label={form.name.label}
              maxLength={form.name.maxLength}
              errorMessage={form.name.error}
              value={form.name.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
            <MyInput
              type="tel"
              name="mobile"
              inputSize="md"
              customClass={Styles.addressInput}
              customised={false}
              label={form.mobile.label}
              maxLength={form.mobile.maxLength}
              errorMessage={form.mobile.error}
              value={form.mobile.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
          </div>
          <div className={Styles.card}>
            <div className={Styles.inlineBlock}>
              <div className={Styles.eachBlock}>
                <MyInput
                  type="tel"
                  name="pincode"
                  inputSize="md"
                  customClass={Styles.addressInput}
                  customised
                  overlayStyle={this.state.activePincode ? [Styles.buttonLoader] : []}
                  label={form.pincode.label}
                  maxLength={form.pincode.maxLength}
                  errorMessage={form.pincode.error}
                  value={form.pincode.value}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur} />
              </div>
              <div className={Styles.eachBlock}>
                <MyInput
                  type="text"
                  name="state"
                  inputSize="md"
                  customClass={Styles.addressInput}
                  disabled
                  customised={false}
                  label={form.state.label}
                  maxLength={form.state.maxLength}
                  errorMessage={form.state.error}
                  value={form.state.value}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur} />
              </div>
            </div>
            <MyInput
              type="text"
              name="address"
              inputSize="md"
              customClass={Styles.addressInput}
              customised={false}
              label={form.address.label}
              maxLength={form.address.maxLength}
              errorMessage={form.address.error}
              value={form.address.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
            <MyInput
              type="text"
              name="locality"
              inputSize="md"
              customClass={Styles.addressInput}
              customised={false}
              readOnly
              onClick={this.openLocalitySelector}
              label={form.locality.label}
              maxLength={form.locality.maxLength}
              errorMessage={form.locality.error}
              value={form.locality.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
            <MyInput
              type="text"
              name="city"
              inputSize="md"
              customClass={Styles.addressInput}
              customised={false}
              disabled
              label={form.city.label}
              maxLength={form.city.maxLength}
              errorMessage={form.city.error}
              value={form.city.value}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur} />
          </div>
          <div className={Styles.card}>
            <div className={Styles.extraFieldLabel}> {form.addressType.label} <span className={Styles.errMessage}>{form.addressType.error}</span></div>
            <ul className={Styles.radioOptions}>
              {this.renderOptions(form.addressType.options)}
            </ul>
            <div className={(form.addressType.value || '').toLowerCase() === 'home' && Styles.hide}>
              <div className={Styles.extraFieldLabel}> {form.notAvailableDays.label} </div>
              <ul className={Styles.checkBoxOptions}>
                {this.renderCheckBoxOptions(form.notAvailableDays.options)}
              </ul>
            </div>
            <div className={Styles.horizontalSeparator}></div>
            <label className={Styles.checkBoxLabel} onClick={this.selectDefault}>
              <Icon
                customStyle={form.default.value ? { ...ICON_STYLE, ...ICON_SELECTED_STYLE } : ICON_STYLE}
                name={form.default.value ? 'checkbox_active' : 'checkbox_inactive'} />
              <span className={Styles.checkboxText}> {form.default.label} </span>
            </label>
            {this.state.formError && <div className={Styles.saveError}>{this.state.formError}</div>}
          </div>
        </div>
        <div className={Styles.buttons}>
          <div className={Styles.button} onClick={this.props.closeModal}> Cancel </div>
          <div
            className={`${Styles.button} ${!isValidForm ? Styles.disabled : ''}`}
            onClick={() => { if (isValidForm && !this.state.formInactive) { this.handleSubmit(); } }}>
            Save
            <div className={this.state.formInactive ? Styles.buttonLoader : ''} />
          </div>
        </div>
      </div>
      {this.state.localitySelector && <LocalitySelector
        selectedLoc={this.state.selectedLoc}
        otherLoc={at(this, 'state.form.otherLoc.value')}
        onLocClick={this.onLocClick}
        confirmLocation={this.confirmLocation}
        hide={this.closeLocalitySelector}
        otherLocChange={this.otherLocChange}
        localities={this.state.localities} />}
    </div>);
  }
}

AddAddressModal.propTypes = {
  show: PropTypes.bool,
  addressData: PropTypes.object,
  mode: PropTypes.string,
  loadAddresses: PropTypes.func,
  closeModal: PropTypes.func
};

export default AddAddressModal;
