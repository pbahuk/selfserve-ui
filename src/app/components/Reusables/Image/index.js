import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Lazy from 'react-lazy-load';

import {
  getCloudinaryImage,
  getRandomBackgroundColors
} from '../../../../utils/imageUtils';

import Styles from './image.css';

class Image extends PureComponent {
  render() {
    const { src, height, width, showBackgroundColor } = this.props;
    const imageSrc = getCloudinaryImage(src, width, height);
    const baseImage = (
      <div
        style={{
          background: showBackgroundColor
            ? getRandomBackgroundColors()
            : 'none',
          height: isNaN(height) ? height : `${height}px`,
          width: isNaN(width) ? width : `${width}px`
        }} >
        <Lazy
          height={height}
          offsetVertical={300}
          width={width}
          debounce={false}
          once >
          <picture className={Styles.imgResponsive} style={{ width: '100%' }}>
            <source srcSet={imageSrc} type="image/webp" />
            <img
              src={imageSrc}
              className={Styles.imgResponsive}
              style={{ width: '100%' }} />
          </picture>
        </Lazy>
      </div>
    );
    return baseImage;
  }
}

Image.propTypes = {
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  src: PropTypes.string
};

Image.defaultProps = {
  showBackgroundColor: true
};

export default Image;
