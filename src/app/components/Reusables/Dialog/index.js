import React, { PropTypes, PureComponent } from 'react';
import Styles from './dialog.css';
import classNames from 'classnames/bind';
const bindClass = classNames.bind(Styles);
class Dialog extends PureComponent {
  getActionButton(button, index) {
    const customStyle = this.props.buttons[index].customClass || '';
    return (
      <div
        key={index}
        className={Styles.buttonBlock}
        onClick={this.props.actions[button.action]}>
        <div className={bindClass('button', `button${index}`, customStyle)}>
          {button.displayName}
        </div>
        {index === 0 && <div className={Styles.buttonDivider}></div>}
      </div>
    );
  }

  render() {
    const props = this.props;
    return (
      <div>
        <div className={Styles.overlay} onClick={props.onOverlayClick}></div>
        <div className={Styles.dialogContainer} style={props.customStyles || {}}>
          <div className={Styles.title}>{props.title}</div>
          <div className={Styles.content}>
            {props.content}
          </div>
          <div className={Styles.buttonsContainer}>
            {props.buttons.map((button, index) => (this.getActionButton(button, index)))}
          </div>
        </div>
      </div>
    );
  }
}

Dialog.proptypes = {
  title: PropTypes.string,
  content: PropTypes.content,
  customStyles: PropTypes.object,
  buttons: PropTypes.arrayOf(PropTypes.shape({
    [PropTypes.string]: [PropTypes.string],   // key -> 'displayName', val -> Text to display
    [PropTypes.string]: [PropTypes.string]    // key -> action, val -> identifier for action
  })),
  actions: PropTypes.shape({
    [PropTypes.string]: PropTypes.func    // key -> identifier for action, val -> callback for the action
  })
};

Dialog.defaultProps = {
  onOverlayClick: () => {}
};

export default Dialog;
