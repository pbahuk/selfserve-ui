import React from 'react';
import PropTypes from 'prop-types';
import Styles from './tracker.css';

class Tracker extends React.Component {

  getCurrentStateLabelPosition(fillWidth, labelWidth) {
    const screenWidth = window.screen && window.screen.width;
    if (screenWidth && this.props.currentStateLabel && this.isMobileOnly()) {
      const labelHalfWidth = labelWidth / 2;
      const trackerWidth = screenWidth - 65 - 70 - (12 * 2);
      let leftPos = Math.floor(fillWidth / 100 * trackerWidth);
      leftPos = leftPos - labelHalfWidth;
      if (leftPos >= trackerWidth - labelWidth) {
        leftPos = trackerWidth - labelWidth;
      } else if (leftPos < 0) {
        leftPos = 0;
      }
      return `${leftPos}px`;
    }
    return `calc(${fillWidth}% - ${labelWidth / 2}px`;
  }

  isMobileOnly() {
    return window.__myx_deviceData__.isMobile && !window.__myx_deviceData__.isTablet;
  }

  render() {
    let fillWidth = this.props.isComplete ? 100 :
      Math.min(99, Math.ceil(100 / (this.props.finalValue - this.props.initialValue) * (this.props.currentValue - this.props.initialValue)));
    if (fillWidth !== 100 && this.props.offset &&
      this.props.currentValue !== this.props.initialValue) {
      fillWidth = fillWidth + this.props.offset;
      const minFillWidth = this.props.offset;
      const maxFillWidth = 100 - this.props.offset;
      fillWidth = Math.max(fillWidth, minFillWidth);
      fillWidth = Math.min(fillWidth, maxFillWidth);
    }
    const styleProp = {
      height: '2px',
      width: `${fillWidth}%`
    };
    const dottedStyleProp = {
      height: '2px',
      width: `${Math.min(100 - fillWidth, 10)}%`,
      left: `${fillWidth}%`
    };
    const filledStyleProp = {
      left: `${fillWidth}%`
    };
    const labelWidth = Math.min(145, Math.ceil(145 / 25 * this.props.currentStateLabel.length) + 15);
    const currentStateLabelProp = {
      left: this.getCurrentStateLabelPosition(fillWidth, labelWidth),
      width: this.isMobileOnly() ? `${labelWidth}px` : 'auto'
    };
    return (<div className={Styles.tracker}>
      <div className={`${Styles.circleMarker} ${Styles.initial} ${Styles.achieved}`} />
      <div className={Styles.progressBar}>
        <div className={Styles.trackingLine} style={styleProp} />
        <div className={Styles.dottedTrackingLine} style={dottedStyleProp} />
        {!this.props.isComplete && !isNaN(fillWidth) && fillWidth > (this.props.offset || 0) &&
          <div className={`${Styles.circleMarker} ${Styles.filled}`} style={filledStyleProp} />}
        {this.props.currentStateLabel && !this.props.isComplete && !isNaN(fillWidth) && fillWidth > (this.props.offset || 0) &&
          <div className={Styles.currentStateLabel} style={currentStateLabelProp}>{this.props.currentStateLabel}</div>}
      </div>
      <div
        className={`${Styles.circleMarker} ${Styles.final}
          ${this.props.currentValue === this.props.finalValue || this.props.isComplete ? Styles.achieved : ''}`} />
    </div>);
  }
}

export default Tracker;

Tracker.propTypes = {
  currentValue: PropTypes.number,
  finalValue: PropTypes.number,
  initialValue: PropTypes.number,
  isComplete: PropTypes.bool,
  currentStateLabel: PropTypes.string,
  offset: PropTypes.number
};
