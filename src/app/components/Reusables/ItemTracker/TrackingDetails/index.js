import React from 'react';
import PropTypes from 'prop-types';
import Styles from './trackingDetails.css';
import at from 'v-at';
import { getDateObject } from './../../../../utils/pageHelpers';

class TrackingDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      initialEntriesCount: 3,
      shownEntriesCount: 0,
      showMore: false,
      trackingHistorySorted: null,
      trackingHistoryUiElements: null
    };
  }

  componentWillMount() {
    this.initTrackingHistoryUi();
  }

  onMoreClick = () => {
    const shownEntriesCount = this.state.shownEntriesCount + this.state.initialEntriesCount;
    const trackingHistoryToBeShown = this.state.trackingHistorySorted.slice(this.state.shownEntriesCount, shownEntriesCount);
    const trackingHistoryUiElements = trackingHistoryToBeShown.map((trackingState, i) => this.getTrackingEntryDiv(trackingState, i));
    this.setState({ showMore: this.showMore(shownEntriesCount),
      trackingHistoryUiElements: [...this.state.trackingHistoryUiElements, ...trackingHistoryUiElements],
      shownEntriesCount });
  }

  getTrackingEntryDiv(trackingState, i) {
    const actualDateObject = getDateObject(new Date(parseInt(at(trackingState, 'actualEventTime'), 10)));
    const comment = at(trackingState, 'comment') ||
          ((at(trackingState, 'eventType') === 'ORDER_CREATION') && 'Order Placed') || '';
    if (comment) {
      return (
        <div className={Styles.entry} key={`entry${this.state.shownEntriesCount + i}`}>
          <div className={Styles.date}>{`${actualDateObject.date} ${actualDateObject.monthInWords}`}</div>
          <div className={Styles.details}>{comment}</div>
        </div>
      );
    }
    return null;
  }

  showMore(shownEntriesCount, trackingHistoryEntries) {
    return (trackingHistoryEntries || this.state.trackingHistorySorted || []).length > (shownEntriesCount || this.state.shownEntriesCount);
  }

  initTrackingHistoryUi() {
    const trackingStates = at(this.props, 'trackingDetails.states') || [];
    const trackingHistory = trackingStates.filter((trackingState) =>
      trackingState.hasOwnProperty('actualEventTime') && trackingState.actualEventTime !== '');
    const trackingHistorySorted = trackingHistory.sort((state1, state2) => parseInt(state2.actualEventTime, 10) - parseInt(state1.actualEventTime, 10));
    const trackingHistoryToBeShown = trackingHistorySorted.slice(0, this.state.initialEntriesCount);
    const trackingHistoryUiElements = trackingHistoryToBeShown.map((trackingState, i) => this.getTrackingEntryDiv(trackingState, i));
    this.setState({ trackingHistorySorted,
      showMore: this.showMore(this.state.initialEntriesCount, trackingHistorySorted),
      trackingHistoryUiElements,
      shownEntriesCount: this.state.initialEntriesCount });
  }

  render() {
    const trackingStates = at(this.props, 'trackingDetails.states') || [];
    if (this.props.expanded && trackingStates.length > 0) {
      const courierName = at(this.props, 'trackingDetails.courier.name');
      const courierCode = at(this.props, 'trackingDetails.courier.code');
      const trackingNumber = at(this.props, 'trackingDetails.number');
      return (
        <div>
          {((courierName || courierCode) && trackingNumber) &&
            (
            <div className={Styles.courierInfoContainer}>
              <div>
                <span className={Styles.infoHeading}>Courier Name: </span>
                <span className={Styles.infoDetails}> {courierName || courierCode}</span>
              </div>
              <div>
                <span className={Styles.infoHeading}>Tracking No: </span>
                <span className={Styles.infoDetails}>{trackingNumber}</span>
              </div>
            </div>
            )
          }
          <div className={Styles.container}>
            {this.state.trackingHistoryUiElements}
            {
              this.state.showMore &&
              (
                <div className={Styles.entry}>
                  <div className={`${Styles.date} ${Styles.more}`} onClick={this.onMoreClick}>More</div>
                  <div className={`${Styles.details} ${Styles.moredetails}`}>More Details..</div>
                </div>
              )
            }
          </div>
        </div>
      );
    }
    return null;
  }
}

TrackingDetails.propTypes = {
  trackingDetails: PropTypes.object,
  expanded: PropTypes.bool
};

export default TrackingDetails;
