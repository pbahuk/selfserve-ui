import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Style and Functional Imports.
import Styles from './itemTracker.css';
import { getDateObject } from './../../../utils/pageHelpers';
import { TrackingStateMappings } from './../../../resources/data/trackingStates';

// Component Related Imports.
import Tracker from './Tracker';
import TrackingDetails from './TrackingDetails';
import Icon from '../Icon';

class ItemTracker extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showTrackingDetails: this.props.detailsEnabled && !(this.props.collapseTrackerABValue)
    };
  }

  getTrackerData(otsResponse = []) {
    let lastUpdatedState = 'Placed';
    let lastUpdationDate = '';
    // otsResponse = otsResponse.filter((trackingState) => trackingState.trackingUpdateLevel === 'customer');
    const statesInfo = {};
    try {
      otsResponse.forEach((trackingState) => {
        let { actualEventTime, expectedEventTime } = trackingState;
        const { eventType } = trackingState;
        const trackingDisplayState = TrackingStateMappings[eventType];
        if (trackingDisplayState) {
          actualEventTime = actualEventTime ? parseInt(actualEventTime, 10) : '';
          expectedEventTime = expectedEventTime ? parseInt(expectedEventTime, 10) : '';
          const actualDate = (actualEventTime && new Date(actualEventTime).getTime()) || '';
          const expectedDate = (expectedEventTime && new Date(expectedEventTime).getTime()) || '';
          if (!lastUpdationDate || lastUpdationDate < actualDate) {
            lastUpdationDate = actualDate;
            lastUpdatedState = trackingDisplayState;
          }
          if (statesInfo.hasOwnProperty(trackingDisplayState)) {
            const currentState = statesInfo[trackingDisplayState];
            if (actualDate > currentState.actualDate) {
              currentState.actualDate = actualDate;
            } else if (expectedDate > currentState.expectedDate) {
              currentState.expectedDate = expectedDate;
            }
          } else {
            statesInfo[trackingDisplayState] = {
              actualDate,
              expectedDate
            };
          }
        }
      });
    } catch (e) {
      console.error(e);
    }
    return {
      lastUpdatedState,
      statesInfo
    };
  }

  getDeliveryDelayedMessage() {
    return (
      <span>
        <span>
          *Delivery Delayed:&nbsp;&nbsp;
        </span>
        <span>
          We are sorry for the delay in delivery. Please bear with us while we are trying to deliver your item at the earliest.
        </span>
      </span>
    );
  }

  isDeliveryDelayed(deliveryExpectedDate, deliveryActualDate) {
    return !deliveryActualDate && new Date().getTime() > new Date(parseInt(deliveryExpectedDate, 10)).setHours(24, 0, 0, 0);
  }

  showTracker(trackingInfo = {}) {
    const { statesInfo = {} } = trackingInfo;
    // Atleast placed and delivery expected states shoud be there
    if (Object.keys(statesInfo).length >= 2) {
      // Show if it was delivered today
      const deliveryDate = at(trackingInfo, 'statesInfo.Delivered.actualDate');
      return deliveryDate ? new Date().getTime() < new Date(parseInt(deliveryDate, 10)).setHours(24, 0, 0, 0) : true;
    }
    return false;
  }

  toggleTrackingDetailsDisplay = () => {
    this.setState({ showTrackingDetails: !this.state.showTrackingDetails });
  }

  render() {
    const trackingStates = at(this.props, 'trackingInfo.states') || [];
    if (trackingStates.length > 0) {
      const trackingInfo = this.getTrackerData(trackingStates);

      if (this.showTracker(trackingInfo)) {
        const placedActualDate = at(trackingInfo, 'statesInfo.Placed.actualDate');
        const placedDateObject = placedActualDate && getDateObject(new Date(placedActualDate));

        const deliveryExpectedDate = at(trackingInfo, 'statesInfo.Delivered.expectedDate');
        const deliveryActualDate = at(trackingInfo, 'statesInfo.Delivered.actualDate');
        const deliveryDateObject = getDateObject(new Date(deliveryActualDate || deliveryExpectedDate));

        const lastUpdatedState = at(trackingInfo, 'lastUpdatedState');
        const lastUpdatedStateDate = at(trackingInfo, `statesInfo.${lastUpdatedState}`).actualDate;
        const lastUpdatedDateObject = lastUpdatedStateDate && getDateObject(new Date(lastUpdatedStateDate));

        if (placedActualDate && (deliveryExpectedDate || deliveryActualDate) && lastUpdatedState && lastUpdatedStateDate) {
          const isDeliveryDelayed = this.isDeliveryDelayed(deliveryExpectedDate, deliveryActualDate);
          return (
            <div>
              <div className={Styles.trackingComponent}>
                <div
                  className={`${Styles.fixedState} ${lastUpdatedState === 'Placed' ? Styles.activeState : Styles.inactiveState}`}>
                  <div className={Styles.state}>
                    Ordered
                  </div>
                  <div> {`${placedDateObject.date} ${placedDateObject.monthInWords}`} </div>
                </div>
                <Tracker
                  initialValue={placedActualDate}
                  finalValue={deliveryActualDate || deliveryExpectedDate}
                  currentValue={lastUpdatedStateDate}
                  isComplete={!!deliveryActualDate || false}
                  offset={10}
                  currentStateLabel={`${at(trackingInfo, 'lastUpdatedState')} (${lastUpdatedDateObject.date} ${lastUpdatedDateObject.monthInWords})`} />
                <div
                  className={`${Styles.fixedState} ${Styles.fixedEndState} ${
                    lastUpdatedState === 'Delivered' ? Styles.activeState : Styles.inactiveState}`}>
                  <div className={Styles.state}>
                    {deliveryActualDate ? 'Delivered' : 'Delivery'}
                  </div>
                  <div className={isDeliveryDelayed ? Styles.delayed : ''}>
                    {deliveryActualDate ? 'Today' : `${deliveryDateObject.date} ${deliveryDateObject.monthInWords}${isDeliveryDelayed && '*' || ''}`}
                  </div>
                </div>
                {
                  this.props.detailsEnabled &&
                    <div className={Styles.arrowHolder} onClick={this.toggleTrackingDetailsDisplay}>
                      <Icon name={this.state.showTrackingDetails ? 'chevron_up' : 'chevron_down'} customStyle={{ fontSize: '20px' }} />
                    </div>
                }
                {this.state.showTrackingDetails &&
                  <div className={Styles.trackingHistory}>
                    <TrackingDetails
                      trackingDetails={this.props.trackingInfo}
                      expanded={this.state.showTrackingDetails} />
                  </div>}
              </div>
              {
                !this.props.detailsEnabled && isDeliveryDelayed &&
                  <div className={Styles.message}>{this.getDeliveryDelayedMessage()}</div>
              }
            </div>);
        }
      }
    }
    return null;
  }
}

ItemTracker.propTypes = {
  trackingInfo: PropTypes.object,
  detailsEnabled: PropTypes.bool,
  collapseTrackerABValue: PropTypes.bool
};

export default ItemTracker;
