import React from 'react';
import PropTypes from 'prop-types';
import Styles from './mobileToast.css';

const MobileToast = ({ text, error = false }) => (
  <div className={error ? Styles.errorToast : Styles.successToast}> {text} </div>
);

MobileToast.propTypes = {
  text: PropTypes.string,
  error: PropTypes.bool
};


export default MobileToast;

