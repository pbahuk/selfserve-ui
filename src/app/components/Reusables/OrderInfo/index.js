import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Style and Functional Imports.
import Styles from './orderInfo.css';
import instrumentsMapping from '../../../resources/data/instrumentsMapping';
import { getPaymentMode, getDate, insertCharInString } from '../../../utils/pageHelpers';

const OrderInfo = (props) => {
  const order = props.order || {};
  let paymentInstruments = getPaymentMode(at(order, 'payments'));
  paymentInstruments = paymentInstruments.map((instrument) => instrumentsMapping[instrument]);

  return (<div className={Styles.orderInfo}>
    <div>
      <span className={Styles.label}> Placed On: </span>
      <span> {getDate(new Date(Number.parseInt(order.createdOn, 10)))} </span>
    </div>
    <div>
      <span className={Styles.label}> Order No: </span>
      <span> {insertCharInString(order.storeOrderId, '-', 7)} </span>
    </div>
    {paymentInstruments.length > 0 ?
      (<div>
        <span className={Styles.label}> Payment Mode: </span>
        <span> {paymentInstruments.join(' | ')} </span>
      </div>) :
      null}
  </div>);
};

OrderInfo.propTypes = {
  order: PropTypes.object
};

export default OrderInfo;
