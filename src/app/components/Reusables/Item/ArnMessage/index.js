import React, { PropTypes } from 'react';
import get from 'lodash/get';

// Style related imports
import infoStyles from './infoStyle.css';
import Styles from './arnMessage.css';

// React Components Imports.
import Info from './../../Info';

import { getArnData } from './../../../../utils/services/payments';


class ArnMessage extends React.Component {

  state = {
    data: null,
    loading: false
  };

  componentWillMount() {
    const { item } = this.props;
    const ppsId = get(item, 'return.refund.id', null);
    if (ppsId) {
      this.setState({ loading: true }, () => {
        getArnData(ppsId, (err, res) => {
          const data = get(res, 'body.data[0]instrumentDetails', null);
          if (err) {
            console.error('[Arn Error: Getting ARN]', err);
          }
          this.setState({ loading: false, data });
        });
      });
    }
  }

  render() {
    const { data } = this.state;
    const { returnMessage } = this.props;
    if (this.state.loading) {
      return (<div className={Styles.arnMessage}><div className={Styles.loader} /></div>);
    } else if (data && data.arn) {
      return (<div className={Styles.arnMessage}>
        Your refund has been processed successfully. The bank reference number is <span>{data.arn}</span>
        <Info
          timeout={15000}
          text={'In case you do not receive the refund in 5 days, ' +
          'please reach out to your bank’s customer support team ' +
          '(using the number printed on your card) and raise a “chargeback dispute” with the provided reference number. ' +
          'Please note that you will have to call your bank customer care center and not visit the branch for this query.'} style={infoStyles} />
      </div>);
    } else if (returnMessage) {
      return (
        <div className={Styles.arnMessage}> <span>Please Note: </span>{returnMessage} </div>
      );
    }

    return null;
  }

}

ArnMessage.propTypes = {
  item: PropTypes.object,
  returnMessage: PropTypes.string
};

export default ArnMessage;
