// React Components Imports.
import at from 'v-at';
import kvpairs from '../../../../utils/kvpairs';

// Functional Imports.
import {
  celebBrands,
  articleTypes,
  internationalBrands,
  celebBrandMap,
  itemTagsText,
  getUrgencyTag,
  getFastDeliveryTag,
  getInventoryCountTag,
  itemThemeMap
} from '../../../../resources/data/itemAttributes';

const DAY_DURATION_MILLIS = 24 * 60 * 60 * 1000;

const isFromCurrentSeason = (season) => {
  if (!season) {
    return false;
  }
  const currentDateObject = new Date();
  const currentDate = currentDateObject.getDate();
  const currentMonth = currentDateObject.getMonth();
	/**
	 * SEASONS:
	 * SS or Spring/Summer - 16th January to 30th June (~5.5 months)
	 * FW or Fall/Winter - 1st July to 15th January (~6.5 months)
	 *
	 * Current season is 'SS' IF:
	 * 	- current date lies between 16th January - 31st January (both inclusive) OR
	 * 	- current month lies between February and June (both inclusive)
	 * ELSE current season is 'FW'
	 */
  const currentSeason
		= currentMonth === 0 && currentDate >= 16 || currentMonth >= 1 && currentMonth <= 5
		? 'SS'
    : 'FW';
  const styleFromCurrentSeason = currentSeason === 'SS'
		? season === 'summer' || season === 'spring'
    : season === 'fall' || season === 'winter';
  return styleFromCurrentSeason;
};

const getTimeDifference = (time1, time2, convertor = 1) => {
  if (!time1 || !time2) {
    return null;
  }
  const time1Parsed = parseInt(time1, 10) * 1000;
  const time2Parsed = parseInt(time2, 10) * 1000;
  return (new Date(time1Parsed) - new Date(time2Parsed)) / convertor;
};

const getDeliveryTime = (orderTime, item) => {
  const status = at(item, 'status');
  const deliveredTime = at(status, 'code') === 'D' && at(status, 'updatedOn');
  const delay = getTimeDifference(deliveredTime, orderTime, DAY_DURATION_MILLIS);
  return delay;
};

const getInventoryCount = (item) => {
  const skuId = at(item, 'product.skuId');
  const sizes = at(item, 'product.sizes');
  let invCnt = 0;
  sizes.forEach(size => {
    if (at(size, 'skuId') === skuId) {
      invCnt = at(size, 'inventory');
    } }
  );
  return invCnt;
};

const getItemAttributeData = (item, orderTime) => {
  const quantity = at(item, 'quantity');
  const itemPrice = at(item, 'unitPrice') * quantity * 100;
  const discountedPrice = at(item, 'payments.amount');
  const invCnt = getInventoryCount(item);
  const deliveryDays = getDeliveryTime(orderTime, item);
  const config = kvpairs('mymyntra.itemSpeciality.config') || {};
  let priority = null;
  let theme = null;
  const { inventoryCount, deliveryTime } = config;
  const catalogDate = new Date(parseInt(at(item, 'product.catalogDate'), 10) * 1000) / DAY_DURATION_MILLIS;
  const currDate = new Date() / 86400000;
  const urgencySkuCount = parseInt(at(item, 'product.urgencySkuCount'), 10);
  const { product } = item || {};
  let text = '';
  if (!product) {
    return text;
  }
  const { brandId, articleId, fashionType, season } = product;
  const intlBrand = internationalBrands[brandId] || '';
  const celebBrand = celebBrands[brandId] || '';
  const articleType = articleTypes[articleId] || '';
  if (articleType && intlBrand) {
    text = itemTagsText.INTL_BRAND + intlBrand;
    priority = 0;
    theme = itemThemeMap.PRODUCT_USP;
  } else if (celebBrand) {
    text = celebBrandMap[celebBrand];
    priority = 1;
    theme = itemThemeMap.PRODUCT_USP;
  } else if (isFromCurrentSeason(season) && discountedPrice / itemPrice <= 0.60 && currDate - catalogDate <= 60) {
    text = itemTagsText.HIDDEN_TREASURE;
    priority = 2;
    theme = itemThemeMap.PRICING;
  } else if (urgencySkuCount > 20) {
    text = getUrgencyTag(urgencySkuCount);
    priority = 3;
    theme = itemThemeMap.URGENCY;
  } else if (invCnt <= inventoryCount) {
    text = getInventoryCountTag(invCnt);
    priority = 4;
    theme = itemThemeMap.URGENCY;
  } else if (deliveryDays && deliveryDays <= deliveryTime) {
    text = getFastDeliveryTag(deliveryTime);
    priority = 5;
    theme = itemThemeMap.SERVICES;
  } else if (articleType && fashionType === 'Core') {
    text = itemTagsText.CLASSY_CHOICE;
    priority = 6;
    theme = itemThemeMap.PRODUCT_USP;
  } else if (itemPrice && discountedPrice / itemPrice <= 0.50) {
    text = itemTagsText.SUPER_SAVER;
    priority = 7;
    theme = itemThemeMap.PRICING;
  }
  return ({ text, priority, theme });
};

const isValidItem = (item) => {
  const isReturnable = at(item, 'flags.returnable.isReturnable');
  const isNotCancelled = at(item, 'flags.cancellable.isCancellable');
  const isNotExchanged = at(item, 'flags.exchangeable.isExchangeable');
  const quantity = at(item, 'quantity');
  const config = kvpairs('mymyntra.itemSpeciality.config') || {};
  const { minIsp } = config;
  const isp = quantity && (item.payments.amount / quantity);
  if ((isReturnable || isNotCancelled || isNotExchanged) && isp >= minIsp) {
    return true;
  }
  return false;
};

export {
  getItemAttributeData,
  isValidItem
};

