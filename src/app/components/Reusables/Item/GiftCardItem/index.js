import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import { currencyValue, getDateObject } from '../../../../utils/pageHelpers';

// Style Related Imports.
import Styles from './giftCardItem.css';

// React Component Imports.
import Icon from '../../../Reusables/Icon';

const GiftCardItem = (props) => {
  let giftCardComponent = null;
  const giftCardDetails = props.data || {};
  let imageUrl = at(giftCardDetails, 'image.secureSrc');
  imageUrl = imageUrl.replace('h_($height),q_($qualityPercentage),w_($width)/', '');
  const dateObject = getDateObject(new Date(at(giftCardDetails, 'expiry')), false);

  if (imageUrl) {
    giftCardComponent = (<div className={Styles.giftCardItem}>
      <div className={Styles.cardDetails}>
        <img src={imageUrl} />
        <div className={Styles.details}>
          <div>
            <Icon name="rupee500" customStyle={{ fontSize: '16px', fontWeight: '600' }} />
            {!at(giftCardDetails, 'amount') ?
              <span> Amount, Will Get Updated </span> :
              <span className={Styles.cardValue}> {currencyValue(at(giftCardDetails, 'amount'))} </span>}
          </div>
          <div className={Styles.cardExpiry}>
            EXPIRY DATE: {!at(giftCardDetails, 'expiry') ? 'Will Get Updated' : `${dateObject.date} ${dateObject.monthInWords} ${dateObject.year}`}
          </div>
        </div>
        <div className={Styles.myntraLogo} />
      </div>
      <div className={Styles.buttons}>
        <div
          className={Styles.button}
          onClick={() => { window.location = `/my/share?gcid=${at(giftCardDetails, 'id')}&share=true`; }}>
          Share
        </div>
        <a className={Styles.button} href={`${imageUrl}?__myx_bootup=true`} download> Download </a>
      </div>
    </div>);
  }

  return giftCardComponent;
};

GiftCardItem.propTypes = {
  data: PropTypes.object
};

export default GiftCardItem;
