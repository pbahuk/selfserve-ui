import React, { Component, PropTypes } from 'react';
import ProfileSuggestions from './profileSuggestions';
import { addProfile, tagProfile, getProfileForProduct } from '../../../../utils/services/userProfileService.js';
import Madalytics from '../../../../utils/trackMadalytics';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

const UNISEX = 'Unisex';
const genderMap = {
  Men: 'male',
  Women: 'female'
};

class ProfileSuggestionsWrapper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allProfiles: [],
      selectedProfile: '',
      infoCardOpen: false,
      saveInProgess: false,
      fetchInProgress: true,
      profileTagged: false,
      gender: '',
      form: {
        name: {
          value: '',
          error: '',
          label: 'Name',
          required: true
        }
      }
    };

    ['onProfileClick', 'onGenderClick', 'saveProfile', 'openSizeInfoPage', 'handleChange', 'hideInfoCard']
      .forEach(method => (this[method] = this[method].bind(this)));
  }

  componentWillMount() {
    const { product } = this.props;
    const gender = get(product, 'gender') || 'Men';
    this.unisexProduct = gender === UNISEX;
    this.setState({
      allProfiles: this.props.profiles,
      gender: this.unisexProduct ? 'male' : genderMap[gender]    // Defaulting to male for gender toggle if unisex
    });
  }

  componentDidMount() {
    const { product, orderId } = this.props;
    const payload = {
      uidx: get(window, '__myx_session__.login'),
      skuId: product.skuId,
      styleId: product.id,
      orderId
    };

    // Fetching profile info for the product. If it exists, just show the tagged profile, else show suggestor
    getProfileForProduct(payload, (err, resp) => {
      if (err) {
        console.log('error in fetching profile for product: ', err);
        this.setState({
          fetchInProgress: false
        });
      } else {
        const oResponse = ((JSON.parse(resp.body.text) || {}).data || [])[0] || {};
        if (oResponse.pidx) {
          this.setState({
            profileTagged: true,
            selectedProfile: oResponse.pidx,
            fetchInProgress: false
          });
        }
        this.setState({
          fetchInProgress: false
        });
      }
    });
  }

  onProfileClick(e) {
    const id = e.currentTarget.id;

    // If clicked on 'Myself' or 'Others' button, open half card for profile creation
    if (id === 'myself' || id === 'others') {
      this.openInfoCard(id === 'myself' ? get(window, '__myx_session__.userfirstname') : '');
    } else if (get(e, 'currentTarget.dataset.disabled') === 'false') {
      this.tagProfileToProduct(id);
      this.setState({
        selectedProfile: id
      });
    }
  }

  onGenderClick(e) {
    const id = e.currentTarget.id;
    this.setState({
      gender: id
    });
  }

  validateDetails() {
    const { form } = this.state;

    if (isEmpty(form.name.value)) {
      form.name = { ...form.name, error: 'Name is required' };
      this.setState({
        form
      });
      return false;
    }

    return true;
  }

  saveProfile() {
    const { form, allProfiles, gender } = this.state;
    if (this.validateDetails()) {
      const payload = {
        name: form.name.value,
        gender,
        userId: get(window, '__myx_session__.login')
      };
      this.setState({
        saveInProgess: true
      });

      addProfile(payload, (err, resp) => {
        if (err) {
          console.log('error in saving profile: ', err);
          this.setState({
            saveInProgess: false
          });
        } else {
          const profileData = ((JSON.parse(resp.body.text) || {}).data || [])[0];
          const newProfile = get(profileData, 'profileList.0');
          this.tagProfileToProduct(newProfile.pidx);
          form.name = { ...form.name, value: '' };
          this.setState({
            allProfiles: [...allProfiles, newProfile],
            selectedProfile: newProfile.pidx,
            saveInProgess: false,
            hideInfoCard: true,
            infoCardOpen: false,
            form
          });
          Madalytics.sendEvent('create_size_profile');
        }
      });
    }
  }

  handleChange(target) {
    const { form } = this.state;
    form[target.name] = { ...form[target.name], value: target.value, error: '' };
    this.setState({
      form
    });
  }

  tagProfileToProduct(id) {
    const { orderId, product } = this.props;
    const payload = {
      uidx: get(window, '__myx_session__.login'),
      pidx: id,
      skuId: product.skuId,
      styleId: product.id,
      orderId,
      gender: product.gender,
      articleType: product.articleType
    };

    if (sessionStorage) {
      try {
        // Save the product in session, so that suggestor will appear as long as session is alive
        let profileToProductList = sessionStorage.getItem('profileToProduct') || '';
        profileToProductList = profileToProductList.split(',');
        const key = `${product.id}_${product.skuId}_${orderId}`;
        if (profileToProductList.indexOf(key) === -1) {
          profileToProductList.push(key);
        }
        sessionStorage.setItem('profileToProduct', profileToProductList);
      } catch (e) {
        console.log('session storage error: ', e);
      }
    }

    tagProfile(payload, (err) => {
      if (err) {
        console.log('error in tagging profile: ', err);
      }
    });

    Madalytics.sendEvent('tag_size_profile', { entity_type: 'product', entity_id: product.id }, { custom: { v1: id } });
  }

  openInfoCard() {
    this.setState({
      infoCardOpen: true
    });
  }

  hideInfoCard() {
    this.setState({
      infoCardOpen: false
    });
  }

  openSizeInfoPage() {
    const previousPage = get(window, '__myx.omsServiceResponse.previousPage');
    let page = null;
    if (previousPage) {
      page = previousPage + 1;
    }
    window.location = `/my/sizingInfo?pidx=${this.state.selectedProfile}&mode=create&referrer=orders&page=${page}`;
  }

  render() {
    const { product, orderId } = this.props;
    let showSuggestions = !this.state.profileTagged;
    let profileToProductList;
    if (sessionStorage && !showSuggestions) {
      try {
        // Check if product is there in session. If it is there, show suggestor.
        profileToProductList = sessionStorage.getItem('profileToProduct');
        profileToProductList = profileToProductList ? profileToProductList.split(',') : [];
        const key = `${product.id}_${product.skuId}_${orderId}`;
        if (profileToProductList.indexOf(key) !== -1) {
          showSuggestions = true;
        }
      } catch (e) {
        console.log('Session storage error: ', e);
      }
    }

    const moreProps = {};
    ['unisexProduct', 'onGenderClick', 'onProfileClick', 'saveProfile', 'openSizeInfoPage', 'hideInfoCard', 'handleChange']
      .forEach(prop => (moreProps[prop] = this[prop]));

    return (
      <div>
        {!this.state.fetchInProgress ? <ProfileSuggestions
          profiles={this.state.allProfiles}
          selectedProfile={this.state.selectedProfile}
          profileTagged={this.state.profileTagged}
          gender={this.state.gender}
          showSuggestions={showSuggestions}
          infoCardOpen={this.state.infoCardOpen}
          nameField={this.state.form.name}
          infoCardSaveDisplay={this.state.saveInProgess ? 'Saving...' : 'Save'}
          {...moreProps} /> : null}
      </div>);
  }
}

ProfileSuggestionsWrapper.propTypes = {
  profiles: PropTypes.array,
  product: PropTypes.object,
  orderId: PropTypes.string,
  gender: PropTypes.string
};

export default ProfileSuggestionsWrapper;
