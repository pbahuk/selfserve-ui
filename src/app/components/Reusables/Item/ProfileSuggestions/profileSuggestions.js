import React, { PureComponent, PropTypes } from 'react';

// Reusables
import InfoCard from '../../../Reusables/Dialog';
import ProfileCreationDialog from '../../..//Reusables/ProfileCreationDialog';

// Libraries
import get from 'lodash/get';
import classnames from 'classnames/bind';

// Styles
import Styles from './profileSuggestions.css';

import { sortUserProfiles } from './../../../../utils/pageHelpers';

const bindClass = classnames.bind(Styles);

class ProfileSuggestions extends PureComponent {
  render() {
    const { profiles, profileTagged, showSuggestions, selectedProfile, gender, unisexProduct, onProfileClick, hideInfoCard, saveProfile } = this.props;
    const sortedProfiles = sortUserProfiles(gender, profiles);
    const selPr = sortedProfiles.find(profile => (profile.pidx === selectedProfile));
    return (
      <div className={Styles.block}>
        {showSuggestions || (profileTagged && !selPr) ? <div className={Styles.container}>
          <div className={Styles.headerText}>Bought this product for</div>
          <div className={Styles.allProfiles}>{sortedProfiles.map(profile => (
            <div
              key={profile.pidx}
              id={profile.pidx}
              className={bindClass('profile', {
                selected: profile.pidx === selectedProfile, disabled: !(profile.gender === gender || unisexProduct)
              })}
              data-disabled={!(profile.gender === gender || unisexProduct)}
              onClick={onProfileClick}>
              {profile.name}
            </div>)
          )}
            {profiles.length === 0 ? <div id="myself" className={Styles.profile} onClick={onProfileClick}>
              {get(window, '__myx_session__.userfirstname') || 'Myself'}
            </div> : null}
            <div id="others" className={Styles.profile} onClick={onProfileClick}>
              Others
            </div>
          </div>
          {(selPr && !selPr.complete) && <div className={Styles.moreDetails}>
            <div className={Styles.completeInfoText}>We don't have complete size information for <span style={{ color: '#282c3f' }}>{selPr.name}</span></div>
            <div className={Styles.addDetails} onClick={this.props.openSizeInfoPage}>Add details</div>
          </div>}
          {this.props.infoCardOpen &&
            <InfoCard
              title="Add Details"
              content={<ProfileCreationDialog {...this.props} Styles={Styles} />}
              customStyles={{ height: '252px' }}
              onOverlayClick={hideInfoCard}
              buttons={[{ displayName: 'Cancel', action: 'cancel' }, { displayName: this.props.infoCardSaveDisplay, action: 'save' }]}
              actions={{ cancel: hideInfoCard, save: saveProfile }} />}
        </div> : <div className={Styles.boughtforProfileText}>Bought for <span style={{ color: '#282c3f', fontWeight: '500' }}>{selPr.name}</span></div>}
      </div>
    );
  }
}

ProfileSuggestions.propTypes = {
  profiles: PropTypes.array,
  selectedProfile: PropTypes.string,
  profileTagged: PropTypes.bool,
  gender: PropTypes.string,
  onGenderClick: PropTypes.func,
  showSuggestions: PropTypes.bool,
  onProfileClick: PropTypes.func,
  saveProfile: PropTypes.func,
  openSizeInfoPage: PropTypes.func,
  infoCardOpen: PropTypes.bool,
  nameField: PropTypes.object,
  handleChange: PropTypes.func,
  hideInfoCard: PropTypes.func,
  infoCardSaveDisplay: PropTypes.string
};

export default ProfileSuggestions;
