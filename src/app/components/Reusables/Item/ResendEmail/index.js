import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

import Styles from './resendEmail.css';
import MyInput from '../../../Reusables/Forms/MyInput';
import { resendEmail } from '../../../../utils/services/giftcard';
import { setTimeout } from 'timers';

class ResendEmail extends React.Component {
  constructor(props) {
    super(props);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      form: {
        email: {
          value: this.props.defaultEmail ? this.props.defaultEmail : '',
          error: '',
          maxLength: 50,
          validation: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
          label: 'Recipient Email'
        }
      },
      formInactive: false,
      formError: '',
      confirmationMessage: ''
    };
  }

  handleBlur(target) {
    const { form = {} } = this.state;

    if (target.value === '') {
      form[target.name] = { ...form[target.name], error: 'Required' };
    } else if (!target.value.match(form[target.name].validation)) {
      form[target.name] = { ...form[target.name], error: 'Please provide a valid email' };
    }

    this.setState({
      form
    });
  }

  handleFocus(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], error: '' };

    this.setState({
      form,
      formError: ''
    });
  }

  handleChange(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], value: target.value };

    this.setState({
      form
    });
  }

  handleSubmit() {
    const { storeOrderId, item } = this.props;
    const { form = {} } = this.state;
    let formError = '';
    let confirmationMessage = '';
    let inValidValues = false;

    for (const key in form) {
      if (form.hasOwnProperty(key)) {
        if (form[key].value === '') {
          form[key].error = 'Required';
          inValidValues = true;
        } else if (form[key].error) {
          inValidValues = true;
        }
      }
    }

    if (inValidValues) {
      this.setState({
        form
      });
    } else {
      const data = {
        storeOrderId,
        skuId: at(item, 'product.skuId'),
        releaseId: at(item, 'orderReleaseId'),
        email: form.email.value
      };

      this.setState({
        formInactive: true,
        formError: ''
      }, () => {
        resendEmail(data, (err, res) => {
          if (err) {
            console.error('[GiftCard Error: Resending Email]', err);
            formError = 'Something Went Wrong, Please try again after some time.';
          } else {
            if (at(res, 'body.error') === 'forbidden') {
              formError = 'Invalid Order Id';
            } else if (at(res, 'body.status.statusType') === 'ERROR') {
              formError = at(res, 'body.status.statusMessage') || 'Something Went Wrong, Please try again after some time.';
            } else {
              confirmationMessage = `Email successfully sent to ${form.email.value}`;
            }
          }
          if (!confirmationMessage) {
            this.setState({
              formInactive: false,
              formError
            });
          } else {
            this.setState({
              formInactive: false,
              confirmationMessage
            }, () => {
              setTimeout(() => {
                this.setState({
                  confirmationMessage: ''
                });
              }, 3000);
            });
          }
        });
      });
    }
  }

  render() {
    const { form = {} } = this.state;

    return (<div className={Styles.resendEmail}>
      <div className={Styles.info}> Recipient didn't recieve the email ? </div>
      <MyInput
        type="text"
        name="email"
        inputSize="sm"
        customised={false}
        customClass={Styles.emailInput}
        label={form.email.label}
        maxLength={form.email.maxLength}
        errorMessage={form.email.error}
        value={form.email.value}
        onChange={this.handleChange}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur} />
      <div className={Styles.button} onClick={this.handleSubmit}>
        Resend
        <div className={this.state.formInactive ? Styles.buttonLoader : ''} />
      </div>
      {this.state.formError && <div className={Styles.error}> {this.state.formError} </div>}
      {this.state.confirmationMessage && <div className={Styles.confirmation}> {this.state.confirmationMessage} </div>}
    </div>);
  }
}

ResendEmail.propTypes = {
  storeOrderId: PropTypes.string,
  defaultEmail: PropTypes.string,
  item: PropTypes.object
};

export default ResendEmail;
