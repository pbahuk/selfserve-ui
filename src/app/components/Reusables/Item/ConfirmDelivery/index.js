import React, { PropTypes } from 'react';
import ConfirmDelivery from './confirmDelivery';
import { markDelivered } from '../../../../utils/services/oms';
import at from 'v-at';
import { getDeviceData } from '../../../../utils/pageHelpers';

const ERROR_TEXT = 'Incorrect barcode. Please try again.';
const EMPTY_OBJ_READ_ONLY = {};

class ConDelWrapper extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      barcodeField: {
        error: '',
        validation: /^[0-9a-zA-Z_!@#$%^&*-]+$/,   // Alphanumeric
      },
      confirmDisabled: false,
      confirmText: 'Confirm'
    };
    this.isDesktop = getDeviceData().isDesktop;

    // Bind methods
    ['onBlur', 'onChange', 'setRef', 'onClickScanner', 'onConfirm', 'onCancel']
      .forEach(method => { this[method] = this[method].bind(this); });
  }

  componentDidMount() {
    document.body.style.overflow = 'hidden';
    !this.isDesktop && (document.body.style.position = 'fixed');
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto';
    !this.isDesktop && (document.body.style.position = 'initial');
  }

  onClickScanner() {
    if (typeof MyntApp !== 'undefined' && typeof MyntApp.barcodeActivity === 'function') {
      MyntApp.barcodeActivity();
    } else {
      window.location.href = 'myntra-bridge://scan-Barcode';
    }
  }

  onConfirm() {
    const barcodeField = at(this, 'state.barcodeField');
    if (!this.state.confirmDisabled) {
      const payload = {
        barcode: this.inputField.value,
        warehouseId: at(this, 'props.warehouseId'),
        shipmentId: at(this, 'props.shipmentId'),
        packetId: at(this, 'props.packetId'),
        storeOrderId: at(this, 'props.storeOrderId')
      };

      this.setState({ confirmText: 'Confirming...', confirmDisabled: true });

      markDelivered(payload, (resp) => {
        if (at(resp, 'body.status') === 'error') {
          const newBarcodeField = { ...at(this, 'state.barcodeField'), error: at(resp, 'body.message') };
          this.setState({
            barcodeField: newBarcodeField,
            confirmDisabled: false,
            confirmText: 'Confirm'
          });
        } else {
          this.props.hide(true);
        }
      });
    }
  }

  onCancel() {
    this.props.hide(false);
  }

  onBlur() {}

  onChange(e) {
    const target = e.target;
    const barcodeField = at(this, 'state.barcodeField') || EMPTY_OBJ_READ_ONLY;
    let newBarcodeField;

    // Regex validation
    if (!target.value.match(barcodeField.validation)) {
      newBarcodeField = { ...barcodeField, error: ERROR_TEXT };
    } else {
      newBarcodeField = { ...barcodeField, error: '' };
    }
    this.setState({
      barcodeField: newBarcodeField
    });
  }

  setRef(target) {
    this.inputField = target;
  }

  render() {
    const barcodeField = at(this, 'state.barcodeField') || EMPTY_OBJ_READ_ONLY;
    return (
      <ConfirmDelivery
        onBlur={this.onBlur}
        onChange={this.onChange}
        setRef={this.setRef}
        error={barcodeField.error}
        confirmDisabled={at(this, 'state.confirmDisabled')}
        confirmText={at(this, 'state.confirmText')}
        onClickScanner={this.onClickScanner}
        onCancel={this.onCancel}
        onConfirm={this.onConfirm} />
    );
  }
}

ConDelWrapper.propTypes = {
  shipmentId: PropTypes.number,
  storeOrderId: PropTypes.string,
  packetId: PropTypes.string,
  warehouseId: PropTypes.string,
  hide: PropTypes.func
};

export default ConDelWrapper;
