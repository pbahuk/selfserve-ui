import React, { PropTypes, PureComponent } from 'react';
import semver from 'semver';
import Input from '../../Forms/MyInput';
import Icon from '../../Icon';
import Styles from './confirmDelivery.css';
import classNames from 'classnames/bind';
import { getDeviceData } from '../../../../utils/pageHelpers';
const bindClass = classNames.bind(Styles);

const qrIconStyle = {
  'fontSize': '28px',
  'color': '#526cd0'
};

class ConfirmDelivery extends PureComponent {
  render() {
    const { props } = this;
    const { isApp, isDesktop } = getDeviceData();
    let toShowScanner = isApp;
    if (navigator.userAgent.indexOf('MyntraRetailiPhone') !== -1) {
      const ua = navigator.userAgent;
      const iosVersion = ua.substring(ua.indexOf('MyntraRetailiPhone') + 19, ua.indexOf('(', ua.indexOf('MyntraRetailiPhone'))).trim();
      if (!semver.valid(iosVersion)) {
        toShowScanner = false;
      } else if(semver.lt(iosVersion, '2.3.0')) {
        toShowScanner = false;
      }
    }
    if ((typeof(MyntApp) !== 'undefined' && MyntApp.getAppVersion() <= 110075)) {
      toShowScanner = false;
    }

    return (
      <div>
        <div className={Styles.shimmer}></div>
        <div className={bindClass('container', { web: isDesktop, mobile: !isDesktop })}>
          <div className={bindClass('inputField', { showScanner: toShowScanner })}>
            <div className={Styles.inputHeader}>Enter or scan barcode</div>
            <input className={bindClass('input', { errorInput: props.error })} ref={props.setRef} type="tel" name="barcode" id="barcode" onChange={props.onChange}/>
            {props.error && <div className={Styles.errorText}>{props.error}</div>}
          </div>
          {toShowScanner && <div className={Styles.scanner} onClick={props.onClickScanner} >
            <Icon name="qr" customStyle={qrIconStyle} />
          </div>}
          <div className={Styles.content}>
            <div className={Styles.contentHeader}>Where can I find the barcode ?</div>
            <img
              className={Styles.contentImage}
              src="https://assets.myntassets.com/assets/images/2017/11/2/11509604395872-barcode.png" />
            <div className={Styles.contentText}>
              The item barcode is a 12 digit numeric value and starts with 100 or 101.
              You can find the barcode sticker on the outer transparent packaging of the product.
              In-case of footwear, the barcode will be on the box.
            </div>
          </div>
          <div className={Styles.buttons}>
            <button className={bindClass('button', 'cancel')} onClick={props.onCancel}> Cancel </button>
            <button className={bindClass('button', 'confirm', { disabled: props.confirmDisabled })} onClick={props.onConfirm}> {this.props.confirmText} </button>
          </div>
        </div>
      </div>
    );
  }
}

ConfirmDelivery.propTypes = {
  value: PropTypes.string,
  error: PropTypes.string,
  onBlue: PropTypes.func,
  onChange: PropTypes.func,
  maxLength: PropTypes.number,
  onCancel: PropTypes.func,
  onConfirm: PropTypes.func,
  confirmDisabled: PropTypes.bool,
  confirmText: PropTypes.string
};

export default ConfirmDelivery;
