import React from 'react';
import at from 'v-at';
import find from 'lodash/find';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import semver from 'semver';
import features from '../../../utils/features';
import kvpairs from '../../../utils/kvpairs';
import { returnStatusMap } from '../../../resources/data/returnStatus';
import { itemStatusMap } from '../../../resources/data/itemStatus';
import COLORS from '../../../resources/colors';
import WriteReviewDesktop from '../../WriteReview/writeReviewModal';
// Style Related Imports.
import Styles from './item.css';
import classNames from 'classnames/bind';
const bindedClassName = classNames.bind(Styles);

// React Components Imports.
import PriceBreakUp from './../PriceBreakUp';
import ItemTracker from './../ItemTracker';
import ConfirmDelivery from './ConfirmDelivery';
import Rating from './Rating';
import GiftCardItem from './GiftCardItem';
import ProfileSuggestions from './ProfileSuggestions';
import Icon from '../Icon';
import ResendEmail from './ResendEmail';
import { currencyValue, getDeviceData, getDateObject, gaEmitter, getPickupSlotString, getImageUrl } from '../../../utils/pageHelpers';
import Madalytics from '../../../utils/trackMadalytics';
import SVGImages from '../SVGImages';
import ArnMessage from './ArnMessage';
import MobileToast from '../MobileToast';

// Functional Imports.
import { isValidItem, getItemAttributeData } from './helpers';
import { getRatings } from './../../../utils/services/ratings';
import InsiderPointsItem from '../InsiderPoints/InsiderPointsItem';

const returnsEnableFG = features('mymyntra.return.enable') === 'true';
const cancelEnableFG = features('mymyntra.cancel.enable') === 'true';
const ppsDisabledFG = features('mymyntra.pps.disable') === 'true';
const uspFG = features('userSizeProfilesEnabled') === 'true';
const mymyntraUspFG = features('mymyntra.userSizeProfilesEnabled') === 'true';
const markDeliveryFG = features('order.markdelivered.enable') === 'true';
const oldRoute = features('returns.tab.enable') === 'true';
const valueReturnDiscount = kvpairs('mymyntra.valueReturnDiscount') || 100;
const refundStates = ['SHS', 'RRC', 'RPC', 'L', 'DLS'];

class Item extends React.Component {
  constructor(props) {
    super(props);
    [
      'showConfirmDelivery',
      'hideConfirmDelivery',
      'generateItemTags',
      'handleRedirect',
      'showStoryButton',
      'showWriteReview',
      'getUpdatedRating',
      'getUpdatestatus',
      'insiderPointsCollected'
    ].forEach(method => (this[method] = this[method].bind(this)));

    this.state = {
      showConfirmDelivery: false,
      markDelDone: false,
      showWriteReview: false,
      updatedRating: null,
      showSuccessToast: null
    };
  }

  componentDidMount() {
    const { order = {}, item = {} } = this.props;
    this.triggerItemSpecialityTagLoadEvent(item, order);
  }

  getStatusUpdatedDate(item) {
    let itemStatusUpdatedDate = at(item, 'status.updatedOn') || '';
    if (itemStatusUpdatedDate) {
      itemStatusUpdatedDate = getDateObject(new Date(Number.parseInt(itemStatusUpdatedDate, 10)));
      return `(${itemStatusUpdatedDate.monthInWords} ${itemStatusUpdatedDate.date}, ${itemStatusUpdatedDate.year})`;
    }
    return '';
  }

  getUpdatedRating() {
    const styleId = get(this, 'props.item.product.id', null);
    if (styleId) {
      const payload = {
        styleIds: [styleId]
      };
      getRatings(payload, (error, response) => {
        response = get(response, 'body');
        const status = get(response, 'status.statusCode');
        if (status === 200) {
          const ratings = get(response, 'userRatingMap');
          const rating = ratings.find((e) => e.key === styleId);
          this.setState({
            updatedRating: get(rating, 'value.userRating', null)
          });
        }
      });
    }
  }

  getUpdatestatus() {
    this.getUpdatedRating();
    this.props.displaySuccess();
  }

  triggerItemSpecialityTagLoadEvent(item, order) {
    const product = at(item, 'product') || {};
    const itemSpecialityData = getItemAttributeData(item, order.createdOn);
    if (this.props.itemsTagAB === 'enabled' && isValidItem(item) && itemSpecialityData.text) {
      Madalytics.sendEvent(
        'widgetLoad',
        {
          type: 'item Speciality Tag Loaded',
          data_set: {
            payload: {
              screen: {
                name: this.props.type,
                data_set: {
                  entity_type: 'product',
                  entity_name: product.name,
                  entity_id: product.id
                }
              }
            }
          }
        },
        {
          payload: {
            widget_items: {
              name: itemSpecialityData.text
            }
          },
          custom: {
            v1: itemSpecialityData.priority,
            v2: itemSpecialityData.theme
          }
        }
      );
    }
  }

  generateItemTags(item, order) {
    const tags = [];
    const itemFlags = {
      isExpress: at(item, 'shippingMethod') === 'XPRESS',
      isValueShipped: at(item, 'shippingMethod') === 'VALUE_SHIPPING',
      isTryable: at(item, 'flags.isTryable'),
      isFree: at(item, 'flags.free.isFree'),
      isGlobal: at(item, 'product.attributes.globalStore'),
      isExchangeOrder: at(order, 'type') && at(order, 'type').toLowerCase() === 'exchange',
      isPreOrder: at(item, 'flags.isPreOrder')
    };

    for (const key in itemFlags) {
      if (itemFlags.hasOwnProperty(key) && itemFlags[key]) { tags.push(key); }
    }
    return tags;
  }

  handleRedirect(type, storeOrderId, orderId, item) {
    const itemId = at(item, 'id') || '';
    const product = at(item, 'product') || {};
    let packetId;
    let packetQuery;

    switch (type) {
      case 'cancel':
        Madalytics.sendEvent('createCancelItemLevelInitiate', {
          'entity_name': product.name,
          'entity_type': 'product_cancel',
          'entity_id': product.id
        }, null, 'react');
        packetId = at(item, 'packetId');
        packetQuery = packetId ? `&packetId=${packetId}` : '';
        window.location = `/my/${type}?storeOrderId=${storeOrderId}&itemId=${itemId}${packetQuery}`;
        break;

      case 'return':
        Madalytics.sendEvent('createReturnInitiate', {
          'entity_name': product.name,
          'entity_type': 'product_return',
          'entity_id': product.id
        }, null, 'react');
        window.location = `/my/${type}?storeOrderId=${storeOrderId}&itemId=${itemId}&orderid=${orderId}&itemid=${itemId}`;
        break;

      case 'exchange':
        Madalytics.sendEvent('createExchangeInitiate', {
          'entity_name': product.name,
          'entity_type': 'product_exchange',
          'entity_id': product.id
        }, null, 'react');
        window.location = oldRoute ?
          `/my/orders/${type}?orderid=${orderId}&itemid=${itemId}` :
          `/my/${type}?storeOrderId=${storeOrderId}&itemId=${itemId}`;
        break;

      case 'looks':
        Madalytics.sendEvent('myOrdersTryLooks', {
          'entity_name': product.name,
          'entity_type': 'product_looks',
          'entity_id': product.id
        }, null, 'react');
        window.location = `/looks/style/${product.id}`;
        break;

      case 'story':
        gaEmitter('profile', 'share story', 'MyMyntra');
        window.location = `sharestory://url=www.myntra.com/${product.id}`;
        break;

      case 'itemDetails':
        if (this.props.archived) {
          window.location = `/my/archived/item/details?storeOrderId=${storeOrderId}&itemId=${itemId}`;
        } else {
          window.location = `/my/item/details?storeOrderId=${storeOrderId}&itemId=${itemId}`;
        }
        break;
      default:
        console.error('Error, No Button of the type present');
    }
  }

  calculateSavings(item) {
    const amountsInfo = at(item, 'payments.amount');
    const instrumentsInfo = at(item, 'payments.instruments');
    const chargesInfo = at(item, 'payments.charges');
    const unitPrice = at(item, 'unitPrice');
    const quantity = at(item, 'quantity');

    let marketPrice = 0;
    let charges = 0;
    let discount = 0;
    let discountInt = 0;

    if (!instrumentsInfo && !chargesInfo) {
      marketPrice = unitPrice * quantity;
      charges = amountsInfo / 100;
    } else if (!instrumentsInfo) {
      marketPrice = unitPrice * quantity;
      charges = amountsInfo / 100;
    } else if (!chargesInfo) {
      marketPrice = unitPrice * quantity / 100;
      const chargesInfoKeys = [
        instrumentsInfo.cod,
        instrumentsInfo.creditCard,
        instrumentsInfo.debitCard,
        instrumentsInfo.emi,
        instrumentsInfo.giftcard,
        instrumentsInfo.myntraCredit,
        instrumentsInfo.neft,
        instrumentsInfo.netBanking,
        instrumentsInfo.phonepe,
        instrumentsInfo.wallet,
        instrumentsInfo.paylater
      ];
      chargesInfoKeys.map((key) => (charges += Math.abs(key) / 100));
    } else if (instrumentsInfo && chargesInfo) {
      marketPrice = unitPrice * quantity;
      const chargesInfoKeys = [
        instrumentsInfo.cod,
        instrumentsInfo.creditCard,
        instrumentsInfo.debitCard,
        instrumentsInfo.emi,
        instrumentsInfo.giftcard,
        instrumentsInfo.myntraCredit,
        instrumentsInfo.neft,
        instrumentsInfo.netBanking,
        instrumentsInfo.phonepe,
        instrumentsInfo.wallet,
        instrumentsInfo.paylater
      ];
      chargesInfoKeys.map((key) => (charges += Math.abs(key) / 100));
    }
    discount = marketPrice - (charges || amountsInfo / 100);
    marketPrice = currencyValue(marketPrice, true);
    charges = currencyValue(charges || amountsInfo / 100, false);
    discountInt = discount;
    discount = currencyValue(discount, true);
    return [marketPrice, discountInt, discount, charges];
  }

  hideConfirmDelivery(markDelDone) {
    this.setState({
      showConfirmDelivery: false,
      markDelDone
    });

    if (markDelDone) {
      window.location.href = `/my/orders?p=${this.props.page}`;
    }
  }

  showWriteReview(flag) {
    this.setState({
      showWriteReview: flag
    }, () => {
      if (flag) {
        document.body.classList.add('no-scroll-background');
      } else {
        document.body.classList.remove('no-scroll-background');
      }
    });
  }

  showConfirmDelivery() {
    this.setState({
      showConfirmDelivery: true
    });
  }

  showStoryButton(itemStatusCode) {
    const ua = navigator.userAgent;
    if ((itemStatusCode === 'D' || itemStatusCode === 'C') && ua.indexOf('MyntraRetailAndroid') !== -1) {
      let androidVersion = ua.substring(ua.indexOf('MyntraRetailAndroid') + 20, ua.indexOf('(', ua.indexOf('MyntraRetailAndroid'))).trim();
      androidVersion = semver.valid(semver.coerce(androidVersion));
      if (semver.gt(androidVersion, '3.27.0')) {
        return true;
      }
    }
    return false;
  }

  insiderPointsCollected(points, brandName, articleType) {
    this.setState({
      insiderPointsEarned: true,
      showSuccessToast: `${points} points collected for ${brandName} ${articleType}`
    });
    setTimeout(() => {
      this.setState({
        showSuccessToast: null
      });
    }, 6000);
  }

  renderConfirmDeliveryLink() {
    return (<div className={Styles.cdLinkComponent}>
      <div className={Styles.cdSeparator}></div>
      <div className={Styles.cdHeader}>Want to return/exchange item(s)?</div>
      <div className={Styles.cdText}><span
        onClick={this.showConfirmDelivery}
        className={Styles.cdLink}>Confirm Delivery</span> of your order to create a return request</div>
    </div>);
  }

  renderReturnsExchangeButton({ type, storeOrderId, orderId, item }) {
    return (<div
      className={bindedClassName('button', { disabled: !returnsEnableFG })}
      onClick={() => {
        if (returnsEnableFG) { this.handleRedirect(type, storeOrderId, orderId, item); }
      }}>
      {type === 'exchange' ? 'Exchange' : 'Return'}
    </div>);
  }

  renderStatus(showStatus, allQuantitiesReturned, statusClass, itemStatus, showStatusDate, itemReturnsInfo, item) {
    return (
      <span>
        {(showStatus || allQuantitiesReturned) &&
        (<div className={statusClass}>
          {itemStatus}
          <span> {showStatusDate && !itemReturnsInfo ? this.getStatusUpdatedDate(item) : ''} </span> </div>)}
      </span>
    );
  }

  render() {
    const { order = {}, item = {}, type, insiderItemsPoints } = this.props;
    const insiderPoints = { ...insiderItemsPoints };
    const itemId = item.id;
    const articleType = at(item, 'product.articleType') || '';
    const bagDiscount = at(item, 'payments.discounts.cart');
    const valueReturnFlag = bagDiscount && (bagDiscount / 100) > valueReturnDiscount || false;
    let { customizedMessage } = item;
    const product = at(item, 'product') || {};
    const storeOrderId = at(order, 'storeOrderId');
    const orderId = at(order, 'id');

    const paymentInfo = at(item, 'payments');
    const unitPrice = at(item, 'unitPrice');
    const quantity = at(item, 'quantity');

    let removeCancelABValue = 'disabled';
    if (valueReturnFlag) {
      removeCancelABValue = 'enabled';
    } else {
      if (this.props.removeCancelABValue && this.props.removeCancelABValue.indexOf('enabled') === 0) {
        const abHours = parseInt(this.props.removeCancelABValue.split('_')[1], 10);
        if (isNaN(abHours)) {
          removeCancelABValue = 'enabled';
        } else {
          const orderHours = (new Date().valueOf() - parseInt(at(order, 'createdOn'), 10)) / 36e5;
          if (abHours < orderHours) {
            removeCancelABValue = 'enabled';
          }
        }
      }
    }

    if (customizedMessage) {
      try {
        customizedMessage = JSON.parse(customizedMessage);
      } catch (e) {
        console.error('not a proper JSON string');
      }
    }

    // Special Cases. Item Tags.
    let itemMessage = '';
    const giftCardOrder = at(order, 'type') && at(order, 'type').toLowerCase() === 'giftcard';
    const tags = this.generateItemTags(item, order);
    const { isApp, isMobile } = getDeviceData();
    const looksEnabled = isApp && this.props.looksEnabled && product.hasLooks;

    // Product Info.
    const productId = product.id || '';
    let productImages = product.images && product.images.length ? product.images[0] : {};
    let topup = false;
    if (at(customizedMessage, 'cardProgramGroupName') === 'Myntra TopUp eGift Cards' && product.images[1]) {
      topup = true;
      productImages = product.images[1];
    } else if (at(customizedMessage, 'cardProgramGroupName') === 'Myntra Prepaid Return Refund eGift Cards' && product.images[2]) {
      topup = true;
      productImages = product.images[2];
    }
    let productImageUrl = productImages.secureSrc || '';
    productImageUrl = getImageUrl(productImageUrl, 125, 161);
    const brandName = at(item, 'product.brand.name') || '';
    const productDisplayName = at(item, 'product.name') || '';
    let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
    const size = find(at(item, 'product.sizes'), (option) => option.skuId === item.product.skuId);
    // Status Class Names, and any computation based on status.
    const itemStatusCode = at(item, 'status.code');
    const isItemDetails = (type === 'itemDetails');
    const archived = (type === 'archived');
    let itemStatus = itemStatusMap[itemStatusCode] || '';
    const deliveredStatus = ['D', 'C', 'PV', 'PP', 'Q'];
    const statusClass = bindedClassName(
      'status',
      { statusDetails: isItemDetails },
      { delivered: deliveredStatus.indexOf(itemStatusCode) >= 0 },
      { deliveredBlack: deliveredStatus.indexOf(itemStatusCode) >= 0 },
      { cancelled: ['IC', 'RTO', 'L', 'DEC'].indexOf(itemStatusCode) >= 0 });
    const trackerNotShownStates = ['RTO', 'IC', 'L', 'DEC', 'OH']; // Delivered is handled in ItemTracker
    const statusShownStates = ['PV', 'PP', 'Q', 'RTO', 'D', 'C', 'IC', 'DEC', 'L', 'OH']; // C- complete, IC - Cancelled
    const statusDateShownStates = ['D', 'C', 'IC'];
    const showTracker = trackerNotShownStates.indexOf(itemStatusCode) < 0 && !giftCardOrder;
    const showStatus = statusShownStates.indexOf(itemStatusCode) >= 0;
    const showStatusDate = statusDateShownStates.indexOf(itemStatusCode) >= 0;


    // Return Exchange Related Computations.
    let allQuantitiesReturned = false;
    let returnMessage = '';
    let pickupSlot = '';
    let showArnMessage = false;
    const itemReturnsInfo = at(item, 'return') || null;
    const returnNonReintiableStates = ['DEC', 'DLC'];

    if (itemReturnsInfo) {
      const returnedQuantity = at(itemReturnsInfo, 'quantity') || 1;
      if (returnedQuantity >= quantity) {
        const returnCode = at(itemReturnsInfo, 'status.code');
        itemStatus = (returnStatusMap[returnCode] || {}).name || at(itemReturnsInfo, 'status.displayName');
        insiderPoints.canClaim = false; // Set canClaim for insider points as false if return is initiated
        if (type !== 'item') {
          const returnInfo = get(item, 'return', null);
          if (returnInfo && returnsEnableFG) {
            if (returnCode && refundStates.indexOf(returnCode) > -1) {
              showArnMessage = true;
            }
          }
        }
        returnMessage = (returnStatusMap[returnCode] || {}).message || at(itemReturnsInfo, 'status.message');
        pickupSlot = (returnCode === 'LPI' || returnCode === 'Q' || returnCode === 'RFR') &&
                        getPickupSlotString(at(itemReturnsInfo, 'anySlot'), at(itemReturnsInfo, 'pickupSlotSelected'));
        // Return can be re-initiated, if it is DEC: Declined, DLC: Delivered to customer.
        if (returnNonReintiableStates.indexOf(returnCode) === -1) {
          allQuantitiesReturned = true;
        }
      }
    }

    /**
     * Detailed Description: Flags.
     */
    if (itemStatusCode === 'L') {
      itemMessage = 'Your order could not be delivered due to technical issue and was cancelled. We are sorry for the inconvenience.';
    }
    let isItemCancellable = at(item, 'flags.cancellable.isCancellable') && !giftCardOrder && !this.state.insiderPointsEarned;
    if (at(item, 'flags.free.isFree') && isItemCancellable) {
      isItemCancellable = false;
      itemMessage = 'Free gift can\'t be cancelled independently, please cancel the original item to cancel the free gift';
    }
    let isItemReturnable = at(item, 'flags.returnable.isReturnable') && !allQuantitiesReturned && !this.state.insiderPointsEarned;
    if (at(item, 'flags.free.isFree') && isItemReturnable) {
      isItemReturnable = false;
      itemMessage = 'Free gift can\'t be returned independently, please return the original item to return the free gift';
    }
    const isItemExchangeable = at(item, 'flags.exchangeable.isExchangeable') && !allQuantitiesReturned && !this.state.insiderPointsEarned;
    const resendEmailOption = giftCardOrder && (at(item, 'status.code') === 'C' || at(item, 'status.code') === 'D') && !topup && type === 'itemDetails';
    const storyOption = this.showStoryButton(itemStatusCode);

    const ageGroup = at(product, 'ageGroup');
    const masterCategory = at(product, 'masterCategory');

    const profileSuggestionsEnabled = isMobile && uspFG && mymyntraUspFG &&
      ((ageGroup === 'Adults-Men' || ageGroup === 'Adults-Women' || ageGroup === 'Adults-Unisex') &&
      (masterCategory === 'Apparel' || masterCategory === 'Footwear')) &&
      (!isItemDetails) && (type !== 'orderDetails');

    const [marketPrice = 0, discountInt = 0, discount = 0, charges = 0] = this.calculateSavings(item);

    const showButtonsItemDetails = (type === 'itemDetails') &&
      (isItemReturnable || isItemExchangeable || (removeCancelABValue !== 'enabled' && isItemCancellable) || storyOption || looksEnabled);

    // Ratings Calculations.
    const ratings = this.state.updatedRating || at(item, 'product.ratings') || [];
    const typeOneRating = ratings.find((rating) => at(rating, 'ratingType') === 1);
    const ratingsEnabled = at(item, 'product.ratings') && typeOneRating.rating !== undefined;
    const revertible = (isItemReturnable || isItemExchangeable || isItemCancellable);
    const itemTagText = getItemAttributeData(item, order.createdOn).text;
    const itemInfo = {
      orderId,
      itemId,
      brandName,
      articleType,
      charges,
      discount,
      marketPrice,
      size,
      qty: item.quantity,
      productName,
      productImageUrl,
      insiderItemsPoints: insiderPoints
    };
    return (<div className={bindedClassName('itemContainer', { animate: at(this, 'props.selectedItem') === item.id })}>
        {
          ((this.props.itemsTagAB === 'enabled') && isValidItem(item) && itemTagText &&
           (
            <div className={Styles.label}>
              <span className={Styles.attributeIcon}><SVGImages name="itemSpecialityIcon" customStyle={{ height: '16px', width: '16px' }} /></span>
              <span className={Styles.attributeTag}> {itemTagText} </span>
            </div>
           ))
        }
      <div id={item.id} className={Styles.item}>
        <div
          className={Styles.productInfo}
          style={(!isItemDetails && !archived) ? { cursor: 'pointer' } : {}}
          onClick={() => {
            if (type === 'archived') {
              this.handleRedirect('archived', storeOrderId, orderId, item);
            } else if (!isItemDetails) {
              this.handleRedirect('itemDetails', storeOrderId, orderId, item);
            }
          }}>
          <div
            className={Styles.thumbnail}
            onClick={() => {
              if (!giftCardOrder) {
                window.location.href = `/${productId}`;
              }
            }}>
            <img src={productImageUrl} />
          </div>
          <div className={Styles.info}>
            <span className={Styles.brand}> {brandName} </span>
            <div className={Styles.productName}> {productName} </div>
            <span className={Styles.size}> Size: {size && size.label ? size.label : ''} </span>
            <span> | Qty: {item.quantity}</span>
            <div className={Styles.priceInfo}>
              {(isItemDetails) && !ppsDisabledFG
                ? <PriceBreakUp
                  actualPrice={quantity ? unitPrice * quantity : 0}
                  paymentsInfo={paymentInfo} />
                : <span className={Styles.price}>
                  <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{charges}
                    {this.props.savingsAB === 'enabled' && discountInt >= 500 && revertible &&
                      <span><span className={Styles.productstrike}>
                        <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{marketPrice}
                      </span>
                        <span className={Styles.productDiscountPercentage}> Saved
                          <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{discount}
                        </span>
                      </span>
                   }
                   {this.props.savingsAB !== 'enabled' && discountInt > 1 && itemStatus !== 'Cancelled' &&
                     <span><span className={Styles.productstrike}>
                       <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{marketPrice}
                     </span>
                       <span className={Styles.productDiscountPercentage}> Saved
                         <Icon name="rupee500" customStyle={{ fontSize: '14px' }} />{discount}
                       </span>
                     </span>
                   }
                </span>
              }
            </div>
          {!isItemDetails &&
            this.renderStatus(showStatus, allQuantitiesReturned, statusClass, itemStatus, showStatusDate, itemReturnsInfo, item)}
          </div>
          {tags.length > 0 &&
            tags.map((tag, index) => (<div
              key={index}
              style={{ marginTop: `${index * 20}px` }}
              className={bindedClassName('tag', tag)}>
            </div>))}
          {(!isItemDetails) &&
            <div className={Styles.rightArrow}>
              <Icon name="chevron_right" customStyle={{ fontSize: '18px', fontWeight: 800, color: COLORS.blueberry_90 }} />
            </div>}
        </div>
        {insiderItemsPoints
        ? <InsiderPointsItem
          itemInfo={itemInfo}
          insiderPointsCollected={this.insiderPointsCollected}
          showButtonsItemDetails={showButtonsItemDetails}
          type={type} />
        : null
        }
        {(isItemDetails)
          ? <div>
          {showButtonsItemDetails &&
            <div className={Styles.itemButtons}>
              {removeCancelABValue !== 'enabled' && isItemCancellable &&
                <div
                  className={bindedClassName('button', { disabled: !cancelEnableFG })}
                  onClick={() => {
                    if (cancelEnableFG) { this.handleRedirect('cancel', storeOrderId, orderId, item); }
                  }}>
                Cancel Item
                </div>}
              {isItemExchangeable && this.renderReturnsExchangeButton({ storeOrderId, orderId, item, type: 'exchange' })}
              {isItemReturnable && this.renderReturnsExchangeButton({ storeOrderId, orderId, item, type: 'return' })}
              {storyOption &&
                <div className={Styles.button} onClick={() => this.handleRedirect('story', storeOrderId, orderId, item)}>
                  Share Story
                </div>}
              {looksEnabled &&
                <div className={bindedClassName('button')} onClick={() => { this.handleRedirect('looks', '', '', item); }}>
                  Try Looks
                </div>}
            </div>
          }
          </div>
          : <div>
            {(storyOption || looksEnabled) &&
              <div className={Styles.itemButtons}>
                {storyOption &&
                  <div className={Styles.button} onClick={() => this.handleRedirect('story', storeOrderId, orderId, item)}>
                    Share Story
                  </div>}
                {looksEnabled &&
                  <div className={bindedClassName('button')} onClick={() => { this.handleRedirect('looks', '', '', item); }}>
                    Try Looks
                  </div>}
              </div>
            }
          </div>
        }
        {resendEmailOption &&
          <ResendEmail
            storeOrderId={storeOrderId}
            defaultEmail={at(order, 'address.user.email') ? at(order, 'address.user.email') : ''}
            item={item} />}
        {showTracker && !itemReturnsInfo && this.props.type !== 'orderDetails' &&
          <ItemTracker
            trackingInfo={item.tracking || {}}
            collapseTrackerABValue={this.props.collapseTrackerABValue}
            detailsEnabled={type === 'itemDetails'} />}
        {itemStatusCode === 'S' && markDeliveryFG && !this.state.markDelDone &&
          this.renderConfirmDeliveryLink()}
        {this.state.showConfirmDelivery &&
          <ConfirmDelivery
            shipmentId={at(item, 'orderReleaseId')}
            packetId={at(item, 'packetId')}
            warehouseId={at(size, 'warehouses.0')}
            storeOrderId={storeOrderId}
            hide={this.hideConfirmDelivery} />}
        {profileSuggestionsEnabled && <ProfileSuggestions profiles={this.props.profiles} orderId={storeOrderId} product={product} />}

        {

        (pickupSlot || returnMessage || showArnMessage) &&
          <div className={Styles.messageHolder} >
            {pickupSlot && <div className={Styles.pickupMessage}> <span>Pickup Date: </span>{pickupSlot} </div>}
            {
              showArnMessage ? <ArnMessage item={item} returnMessage={returnMessage} />
              : returnMessage && <div className={Styles.pickupMessage}> <span>Please Note: </span>{returnMessage} </div>
            }
          </div>
        }


        {itemMessage &&
          <div className={Styles.itemMessage}> {itemMessage} </div>}
        {ratingsEnabled &&
          <Rating
            context={isItemDetails ? 'Order Details' : 'Order List'}
            productName={at(item, 'product.name')}
            storeOrderId={storeOrderId}
            styleId={at(item, 'product.id')}
            rating={typeOneRating.rating}
            reviewId={typeOneRating.reviewId}
            reviewStatus={typeOneRating.reviewStatus}
            showWriteReview={this.showWriteReview}
            updateRating={this.getUpdatedRating} />}
      </div>
      {giftCardOrder && at(item, 'giftcard.image') && isItemDetails &&
        <GiftCardItem data={at(item, 'giftcard')} />}

      {ratingsEnabled && this.state.showWriteReview &&
        <WriteReviewDesktop
          displaySuccess={this.getUpdatestatus}
          userRating={typeOneRating.rating}
          pdtImageURL={productImageUrl}
          pdtTitle={at(item, 'product.name')}
          styleId={at(item, 'product.id').toString()}
          showWriteReview={this.showWriteReview}
          reviewStatus={typeOneRating.reviewStatus}
          reviewId={typeOneRating.reviewId} />}
      {this.state.showSuccessToast && <MobileToast text={this.state.showSuccessToast} />}
    </div>);
  }
}

Item.propTypes = {
  type: PropTypes.string,
  archived: PropTypes.bool,
  item: PropTypes.object,
  page: PropTypes.number,
  order: PropTypes.object,
  selectedItem: PropTypes.number,
  looksEnabled: PropTypes.bool,
  showCancelReturnsButton: PropTypes.bool,
  profiles: PropTypes.array,
  insiderItemsPoints: PropTypes.object,
  removeCancelABValue: PropTypes.string,
  savingsAB: PropTypes.string,
  itemsTagAB: PropTypes.string,
  collapseTrackerABValue: PropTypes.bool,
  getItemAttributeData: PropTypes.func,
  displaySuccess: PropTypes.func
};

export default Item;
