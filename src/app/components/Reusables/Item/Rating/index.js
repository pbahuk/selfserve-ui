import React from 'react';
import PropTypes from 'prop-types';
import Styles from './rating.css';
// React Component Imports.
import MyRating from '../../Forms/MyRating';
import SVGImages from '../../../Reusables/SVGImages';
import { isApp } from '../../../WriteReview/index';
// Helpers Function
import { getUidx } from '../../../../utils/pageHelpers';
import { setRatings } from '../../../../utils/services/ratings';
import Madalytics from '../../../../utils/trackMadalytics';
import Notify from '../../../Notify';
// sendEvent: function(eventType, data_set = {}, customPayload = {}, variant = 'react')
/**
   * data_set : payload -> screen -> data_set
   * customPayload : payload -> widget.
   */
class Rating extends React.PureComponent {
  componentDidMount() {
    const { context = 'Orders', styleId, productName, storeOrderId } = this.props;

    Madalytics.setPageContext(context);
    Madalytics.sendEvent('widgetLoad', {
      entity_type: 'order',
      entity_id: storeOrderId
    },
    { widget: {
      name: 'Ratings',
      type: 'input',
      data_set: {
        data: [{
          entity_type: 'product',
          entity_name: productName,
          entity_id: styleId
        }]
      }
    } }, 'web');
  }

  onChange = (value) => {
    const { context = 'Orders', styleId, productName, storeOrderId } = this.props;
    const payload = {
      uidx: getUidx(),
      styleId: this.props.styleId,
      rating: value,
      timestamp: (new Date().getTime()).toString(),
      type: 1
    };
    setRatings(payload, (err, res) => {
      if (err || res.statusCode !== 200) {
        this.refs.notify.info({
          message: 'Error rating products, try again later',
          position: 'right'
        });
      } else {
        this.props.updateRating();
      }

      Madalytics.setPageContext(context);
      Madalytics.sendEvent('widgetClick', {
        entity_type: 'order',
        entity_id: storeOrderId,
        entity_result: err || res.statusCode !== 200 ? 'failure' : 'successs'
      },
      { name: 'Ratings',
        type: 'input',
        data_set: {
          data: [{
            entity_type: 'product',
            entity_name: productName,
            entity_id: styleId
          }]
        }
      }, 'web');
    });
  }

  handleReviewClick() {
    if (isApp()) {
      const targetURL = this.props.reviewId ?
        `${window.location.origin}/my/writeReview/${this.props.styleId}/${this.props.reviewId}` :
        `${window.location.origin}/my/writeReview/${this.props.styleId}`;
      window.location.assign(targetURL);
    } else {
      this.props.showWriteReview(true);
    }
  }

  render() {
    const { rating, reviewStatus, reviewId } = this.props;
    const reviewPendingStates = ['IMAGE_PENDING', 'PENDING'];
    const reviewEditStates = (reviewId && ['ACTIVE', 'REVIEW_REJECTED', 'IMAGE_REJECTED']) || [];
    const reviewWriteStates = (rating > 0 && ['NOT_PRESENT']) || [];


    return (<div className={Styles.rating}>
      <div className={Styles.rateBox}>
        <MyRating
          start={0}
          stop={5}
          step={1}
          rating={rating}
          changeRatings={this.onChange}
          emptySymbol={<SVGImages name="emptyStar" customStyle={{ height: '22px', width: '40px' }} />}
          fullSymbol={<SVGImages name="filledStar" customStyle={{ height: '22px', width: '40px' }} />} />
      </div>
       {
         reviewPendingStates.indexOf(reviewStatus) > -1 && <div className={`${Styles.reviewText}`}>Review under Moderation</div>
       }
       {
          reviewEditStates.indexOf(reviewStatus) > -1 && <div
            onClick={() => this.handleReviewClick()}
            className={`${Styles.reviewText} ${Styles.active}`}>
          Edit Review</div>
       }
       {
         reviewWriteStates.indexOf(reviewStatus) > -1 &&
           <div onClick={() => this.handleReviewClick()} className={`${Styles.reviewText} ${Styles.active}`}>
           Write Review</div>
       }

      <Notify ref="notify" />
    </div>);
  }
}

Rating.propTypes = {
  context: PropTypes.string,
  productName: PropTypes.string,
  storeOrderId: PropTypes.number,
  styleId: PropTypes.number,
  rating: PropTypes.number,
  reviewStatus: PropTypes.string,
  reviewId: PropTypes.string,
  showWriteReview: PropTypes.func,
  updateRating: PropTypes.func
};

export default Rating;
