import React from 'react';
import PropTypes from 'prop-types';
import Styles from './error.css';

class Error extends React.Component {
  constructor(props) {
    super(props);
    this.initiateAction = this.initiateAction.bind(this);

    this.state = {
      loading: false
    };
  }

  initiateAction() {
    this.setState({
      loading: true
    }, () => {
      this.props.action();
    });
  }

  render() {
    return (<div className={Styles.card}>
      <div className={Styles.primaryMessage}> {this.props.message}</div>
      {this.props.secondaryMessage && <div className={Styles.secondaryMessage}> {this.props.secondaryMessage} </div>}
      <div className={Styles.errorImage} />
      <div className={Styles.showButton} onClick={this.initiateAction}> {this.props.actionName}
        <div className={this.state.loading ? Styles.buttonLoader : ''} />
      </div>
    </div>);
  }
}

Error.propTypes = {
  message: PropTypes.string,
  secondaryMessage: PropTypes.string,
  actionName: PropTypes.string,
  action: PropTypes.func
};

export default Error;
