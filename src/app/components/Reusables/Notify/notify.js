import React from 'react';
import PropTypes from 'prop-types';

// Style and Functional Imports.
import styles from './notify.css';

const Notify = (props) => {
  const mainContainer = styles.mainContainer;
  return (
    <div className={mainContainer} style={{ backgroundColor: props.backgroundColor }}>
      {props.NotifyText}
    </div>
  );
};

Notify.propTypes = {
  backgroundColor: PropTypes.string,
  NotifyText: PropTypes.string
};

export default Notify;
