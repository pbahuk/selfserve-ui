import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './lineDropdown.css';
import Icon from '../../Icon';

class LineDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.changeDropdownState = this.changeDropdownState.bind(this);
    this.selectOption = this.selectOption.bind(this);

    this.state = {
      expanded: this.props.expanded,
      data: this.props.data
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.setState({
        data: nextProps.data
      });
    }
  }

  changeDropdownState() {
    this.setState({
      expanded: true
    });
  }

  selectOption(event) {
    const context = this;
    event.persist();
    this.setState({
      expanded: false
    }, () => { context.props.selectRadioOption(event.target); });
  }

  renderOptions() {
    let optionsDOM = null;

    if (this.props.value && !this.state.expanded) {
      const selectedValue = this.state.data.find((option) => option.id === Number(this.props.value));
      optionsDOM = (<li>
        {selectedValue.displayName}
      </li>);
    } else {
      optionsDOM = (this.state.data.map((option, key) => <li key={key} className={Styles.inputRow}>
        <label className={Styles.customLabel}>
          <input
            type="radio"
            name={this.props.name}
            className={Styles.customRadio}
            onChange={this.selectOption}
            defaultChecked={option.id === this.props.value}
            id={option.code}
            value={option.id} />
          {option.displayName}
        </label>
      </li>));
    }
    return optionsDOM;
  }

  render() {
    return (<div className={Styles.lineDropdownWrapper}>
      {this.props.label && <div className={Styles.linelabel}> {this.props.label} </div>}
      <ul className={`${Styles.lineDropdown} ${this.state.expanded ? '' : Styles.closed}`} onClick={this.changeDropdownState}>
        {this.renderOptions()}
        {!this.state.expanded ? <Icon className={Styles.dropDownIcon} name="dropdown_down" customStyle={{ fontSize: '16px' }} /> : null}
      </ul>
      {this.props.error && <div className={this.props.error ? `${Styles.error} ${Styles.visible}` : null}>
        {this.props.error}
      </div>}
    </div>);
  }
}

LineDropdown.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  data: PropTypes.array,
  value: PropTypes.string,
  expanded: PropTypes.bool,
  selectRadioOption: PropTypes.func,
  error: PropTypes.string
};

export default LineDropdown;
