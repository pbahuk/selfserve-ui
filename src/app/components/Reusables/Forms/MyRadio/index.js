import React from 'react';
import PropTypes from 'prop-types';

// Styles Related Imports
import Styles from './myRadio.css';

/* Sample Call
  <MyRadio
    group="refundmode"
    defaultSelected="myntracredit"
    options={[
      {
        label: 'Myntra Credit',
        key: 'myntracredit',
        description: 'After receiving this item, {refund_amount} will be instantly refunded to your Myntra Credit.'
      }
    ]}
    onChange={this.onChange} />
*/


class MyRadio extends React.Component {
  constructor(props) {
    super(props);
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  handleOptionChange(event) {
    this.props.onChange(event.target);
  }

  renderOptions() {
    return this.props.options.map((option, key) =>
      <li
        key={key}
        className={`${Styles.inputRow} ${!this.props.selectable ? Styles.deselect : ''}`}>
        <label className={Styles.customLabel}>
          <input
            type="radio"
            className={Styles.customRadio}
            name={this.props.group}
            value={option.key}
            onClick={this.handleOptionChange}
            defaultChecked={this.props.defaultSelected === option.key} />
          <i />
          {option.label}
        </label>
        {option.defaultDisplay}
        {this.props.defaultSelected === option.key &&
          option.description &&
          <div className={Styles.description}>
            {option.description}
            {option.component}
          </div>}
      </li>);
  }

  render() {
    return (<ul className={Styles.radioBlock}>
      {this.renderOptions()}
    </ul>);
  }
}

MyRadio.propTypes = {
  defaultSelected: PropTypes.string,
  options: PropTypes.array,
  group: PropTypes.string,
  onChange: PropTypes.func,
  selectable: PropTypes.bool
};

export default MyRadio;
