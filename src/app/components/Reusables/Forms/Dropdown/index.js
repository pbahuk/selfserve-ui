import React from 'react';
import PropTypes from 'prop-types';

// Style Related imports
import Styles from './dropdown.css';
import dropDownmapping from './../../../../resources/data/dropDownMapping.js';

class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    const dropDownData = dropDownmapping[this.props.type] ? dropDownmapping[this.props.type] : {};
    this.selectOption = this.selectOption.bind(this);

    this.state = {
      show: props.show,
      title: dropDownData.title,
      data: dropDownData.values
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.show) {
      return;
    }
    const dropDownData = dropDownmapping[nextProps.type] ? dropDownmapping[nextProps.type] : {};

    this.setState({
      show: nextProps.show,
      title: dropDownData.title,
      data: dropDownData.values
    });
  }

  selectOption(event) {
    this.props.selectedValue(this.props.type, event.target.value);
  }

  // Expects data to be in array format.
  renderOptions(data) {
    return data.map((option, key) => <li key={key} className={Styles.inputRow}>
      <label className={Styles.customLabel}>
        <input
          className={Styles.customRadio}
          name={this.props.type}
          type="radio"
          onChange={this.selectOption}
          id={option.displayName}
          defaultChecked={option.value === this.props.defaultSelected}
          value={option.value} />
        <i />
        {option.displayName}
      </label>
    </li>);
  }

  render() {
    const dropDown = this.props.show ? (<div>
      <div className={Styles.shimmer} onClick={() => this.props.closeModal()} />
      <div className={Styles.card}>
        <div className={Styles.dropDownTitle}> {this.state.title} </div>
        <ul className={Styles.dropDownList}>
          {this.renderOptions(this.state.data)}
        </ul>
      </div>
    </div>) : null;

    return dropDown;
  }
}

Dropdown.propTypes = {
  type: PropTypes.string,
  show: PropTypes.bool,
  title: PropTypes.string,
  closeModal: PropTypes.func,
  selectedValue: PropTypes.func,
  defaultSelected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

export default Dropdown;
