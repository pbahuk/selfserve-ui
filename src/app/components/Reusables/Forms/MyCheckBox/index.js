import React from 'react';
import PropTypes from 'prop-types';

// Style Related imports
import Styles from './myCheckBox.css';

/* Sample Call
  <MyCheckBox
    group="fruits"
    defaultSelected={['apple']}
    options={['apple', 'banana', 'orange']} />
*/

class MyCheckBox extends React.Component {
  constructor(props) {
    super(props);
    this.changeSelectedOptions = this.changeSelectedOptions.bind(this);

    this.state = {
      selectedOptions: this.props.defaultSelected || []
    };
  }

  changeSelectedOptions(event) {
    const clickedValue = event.target.value;
    let selectedOptions = this.state.selectedOptions;

    if (selectedOptions.indexOf(clickedValue) > -1) {
      selectedOptions = selectedOptions.filter((selectedOption) => selectedOption !== clickedValue);
    } else {
      selectedOptions.push(clickedValue);
    }

    this.props.onSelect(event.target);
    this.setState({
      selectedOptions
    });
  }

  renderOptions() {
    return this.props.options.map((option, key) => <li key={key} className={Styles.inputRow}>
      <label>
        <input
          className={Styles.customCheckBox}
          type="checkbox"
          name={this.props.group}
          onChange={this.changeSelectedOptions}
          defaultChecked={this.state.selectedOptions.indexOf(option) > -1}
          value={option} />
        <i />
        {this.props.optionMap[option]}
      </label>
    </li>);
  }

  render() {
    return (<ul className={Styles.checkBoxComponent}>
      {this.renderOptions()}
    </ul>);
  }
}

MyCheckBox.propTypes = {
  group: PropTypes.string,
  defaultSelected: PropTypes.array,
  options: PropTypes.array,
  optionMap: PropTypes.object,
  onSelect: PropTypes.func
};

export default MyCheckBox;
