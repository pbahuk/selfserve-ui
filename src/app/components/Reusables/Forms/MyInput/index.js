import React from 'react';
import PropTypes from 'prop-types';
import Styles from './myInput.css';
import classNames from 'classnames/bind';

const bindedClassName = classNames.bind(Styles);
let ID_KEY = 1;

function createUniqueId() {
  return `input_${ID_KEY++}`;
}

class MyInput extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onChange(event) {
    const target = event.target;
    this.props.onChange(target);
  }

  onFocus(event) {
    const target = event.target;
    if (this.props.onFocus) {
      this.props.onFocus(target);
    }
  }

  onBlur(event) {
    const target = event.target;
    if (this.props.onBlur) {
      this.props.onBlur(target);
    }
  }

  onClick(event) {
    const target = event.target;
    if (this.props.onClick) {
      this.props.onClick(target);
    }
  }

  render() {
    const inputClass = bindedClassName(
      'input',
      { inputError: this.props.errorMessage });
    const errorClass = bindedClassName(
      'error',
      { visible: this.props.errorMessage });
    const inputInfo = this.props.customised !== false ?
      (<div className={this.props.overlayStyle.join(' ')} />) : null;
    const customView = this.props.customView ? this.props.customView : '';
    const opts = {};
    if (this.props.disabled) {
      opts.disabled = 'disabled';
    } else if (this.props.readOnly) {
      opts.readOnly = 'readOnly';
    }
    const inputSize = this.props.inputSize || 'lg';

    return (
      <div className={`${Styles.inputRow} ${Styles[inputSize]} ${this.props.customClass || ''}`}>
        <input
          id={this.props.id || createUniqueId()}
          type={this.props.type}
          name={this.props.name}
          value={this.props.value}
          onClick={this.onClick}
          onFocus={this.onFocus}
          onChange={this.onChange}
          onBlur={this.onBlur}
          maxLength={this.props.maxLength}
          className={inputClass}
          {...opts} />
        <label
          htmlFor={this.props.name}
          className={this.props.value ? Styles.labelTop : Styles.label}>
          {this.props.label}
        </label>
        {inputInfo}
        {
            customView
        }
        <div className={errorClass}>
          {this.props.errorMessage}
        </div>
      </div>);
  }
}

MyInput.propTypes = {
  customised: PropTypes.bool,
  id: PropTypes.string,
  type: PropTypes.string.isRequired,
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  inputSize: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  onFocus: PropTypes.func,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onClick: PropTypes.func,
  maxLength: PropTypes.number,
  label: PropTypes.string,
  errorMessage: PropTypes.string,
  overlayStyle: PropTypes.array,
  customClass: PropTypes.string,
  customView: PropTypes.element
};

export default MyInput;
