import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './myRating.css';

class MyRating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: this.props.rating,
      interacting: false,
      expectedRating: this.props.rating
    };

    ['onClick', 'onMouseOver', 'onMouseLeave'].forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  onClick(value) {
    if (value !== this.state.rating) {
      this.setState({
        rating: value
      }, () => {
        this.props.changeRatings(value);
      });
    }
  }

  onMouseOver(value) {
    this.setState({
      interacting: true,
      expectedRating: value
    });
  }

  onMouseLeave() {
    this.setState({
      interacting: false,
      expectedRating: this.state.rating
    });
  }

  render() {
    const { start, stop, step } = this.props;
    const rating = this.state.interacting ? this.state.expectedRating : this.state.rating;
    const symbols = [];

    for (let i = 1, j = 1; i <= Math.floor((stop - start) / step); i++, j++) {
      const value = i * step;
      const element = j <= rating ? this.props.fullSymbol : this.props.emptySymbol;

      symbols.push(<span
        key={i}
        className={Styles.imageWrapper}
        onClick={() => this.onClick(value)}
        onMouseOver={() => this.onMouseOver(value)}>
        {element}
      </span>);
    }

    return (<div className={Styles.inline} onMouseLeave={this.onMouseLeave} >
      {symbols}
    </div>);
  }
}

MyRating.propTypes = {
  start: PropTypes.number,
  stop: PropTypes.number,
  step: PropTypes.number,
  rating: PropTypes.number,
  readOnly: PropTypes.bool,
  emptySymbol: PropTypes.element,
  fullSymbol: PropTypes.element,
  changeRatings: PropTypes.func
};

export default MyRating;
