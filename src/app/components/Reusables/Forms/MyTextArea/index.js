import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports
import Styles from './myTextArea.css';
import classNames from 'classnames/bind';
const bindedClassName = classNames.bind(Styles);


class MyTextArea extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.state = {};
  }

  handleChange(event) {
    this.props.handleChange(event.target);
  }

  handleBlur(event) {
    this.props.handleBlur(event.target);
  }

  render() {
    const errorClass = bindedClassName(
      'error',
      { visible: this.props.error });

    return (<div className={Styles.inputRow}>
      <textarea
        className={Styles.customTextarea}
        placeholder={this.props.placeHolder}
        name={this.props.name}
        onChange={this.handleChange}
        onBlur={this.handleBlur}
        value={this.props.value} />
      <div className={errorClass}>
        {this.props.error}
      </div>
    </div>);
  }
}

MyTextArea.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  placeHolder: PropTypes.string,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  error: PropTypes.string
};

export default MyTextArea;
