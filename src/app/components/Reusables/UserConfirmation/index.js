import React from 'react';
import PropTypes from 'prop-types';

import MyCheckbox from '../Forms/MyCheckBox';
import { getDeviceData } from '../../../utils/pageHelpers';

// Style Related Imports.
import Styles from './userConfirmation.css';

const checkOptions = ['confirmText'];
const optionMap = { confirmText: 'I confirm that the product is unused with original tags intact' };

const UserConfirmation = (props) => {
  const { userAgent } = getDeviceData();
  return (<div
    className={
    `${Styles.userConfirmation}
    ${props.error ? Styles.error : ''}
    ${userAgent.indexOf('MyntraRetailAndroid') !== -1 ? Styles.android : ''}`
    }>
    <MyCheckbox
      group="userConfirmation"
      options={checkOptions}
      optionMap={optionMap}
      onSelect={props.onSelect} />
  </div>);
};

UserConfirmation.propTypes = {
  onSelect: PropTypes.func,
  error: PropTypes.bool
};

export default UserConfirmation;
