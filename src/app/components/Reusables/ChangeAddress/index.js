import React from 'react';
import at from 'v-at';
import findIndex from 'lodash/findIndex';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './changeAddress.css';

// Functional Imports.
import { getAllAddress, removeAddress } from '../../../utils/services/address';

// React Component Imports.
import AddressAccordian from '../../Reusables/AddressAccordian';
import AddAddressModal from '../../Reusables/AddAddressModal';
const pickupServiceableTypes = ['OPEN_BOX_PICKUP', 'CLOSED_BOX_PICKUP'];

class ChangeAddress extends React.Component {
  constructor(props) {
    super(props);

    this.renderAddresses = this.renderAddresses.bind(this);
    this.changeActiveAddress = this.changeActiveAddress.bind(this);
    this.checkActiveAddressServiceability = this.checkActiveAddressServiceability.bind(this);
    this.getAllAddress = this.getAllAddress.bind(this);
    this.openAddAddressModal = this.openAddAddressModal.bind(this);
    this.closeAddressModal = this.closeAddressModal.bind(this);
    this.editAddress = this.editAddress.bind(this);
    this.deleteAddress = this.deleteAddress.bind(this);
    this.updateAddress = this.updateAddress.bind(this);

    this.state = {
      loading: true,
      modalError: false,
      addAddressModal: false,
      selectDisabled: true,
      addressModalMode: 'add',
      addresses: [],
      error: '',
      activeAddress: {
        id: null,
        serviceable: 'inProgress'
      }
    };
  }

  componentWillMount() {
    this.getAllAddress();
  }

  getAllAddress() {
    let { modalError } = this.state;
    this.setState({
      selectDisabled: true
    });
    getAllAddress((err, res) => {
      if (err) {
        console.error('[ERROR] Change Address fetching all addresses', err);
        modalError = true;

        this.setState({
          modalError
        });
      } else {
        const addresses = this.sortAddresses(at(res, 'body.data'));
        const addressToSelect = addresses.find(address => address.id === at(this, 'props.preferredAddress.id'));
        const activeAddress = { ...this.state.activeAddress, ...{ address: addressToSelect || addresses[0] } };

        this.setState({
          loading: false,
          addresses,
          activeAddress
        }, () => {
          this.checkActiveAddressServiceability();
        });
      }
    });
  }

  openAddAddressModal(mode = 'add') {
    this.setState({
      addAddressModal: true,
      addressModalMode: mode
    });
  }

  closeAddressModal() {
    this.setState({
      addAddressModal: false
    });
  }

  editAddress() {
    this.openAddAddressModal('edit');
  }

  changeActiveAddress(address = {}) {
    this.setState({
      activeAddress: {
        address,
        serviceable: 'inProgress'
      },
      selectDisabled: true
    }, () => {
      this.checkActiveAddressServiceability();
    });
  }

  checkActiveAddressServiceability() {
    const activeAddress = at(this, 'state.activeAddress.address') || at(this, 'props.preferredAddress');
    const { props } = this;

    props.checkServiceability(activeAddress.pincode, (err1, res1) => {
      if (err1) {
        console.error('CSS CheckServiceability Failure', err1);
      }
      const itemServiceabilityEntries = at(res1, 'body.itemServiceabilityEntries') || [];
      let serviceabilityEntries = itemServiceabilityEntries.length ? itemServiceabilityEntries[0] : {};
      serviceabilityEntries = at(serviceabilityEntries, 'serviceabilityEntries') || [];
      serviceabilityEntries = serviceabilityEntries[0];
      const serviceType = at(serviceabilityEntries, 'serviceType');
      this.serviceType = serviceType;
      console.log('serviceType::::::', serviceType, pickupServiceableTypes);
      let pickupServiceable = pickupServiceableTypes.indexOf(serviceType) > -1;
      if (serviceType === 'EXCHANGE') {
        pickupServiceable = at(res1, 'body.serviceable');
      }
      this.setState({
        activeAddress: {
          address: activeAddress,
          serviceable: pickupServiceable
        },
        selectDisabled: props.origin !== 'returnsChangeAddress' ? !pickupServiceable : false
      });
    });
  }

  updateAddress() {
    const { selectDisabled, activeAddress } = this.state;
    if (!selectDisabled && activeAddress.address) {
      this.props.updateAddress(at(this, 'state.activeAddress.address'));
      if (this.props.updateReturnData) {
        this.props.updateReturnData({ serviceType: this.serviceType });
      }
    }
  }

  deleteAddress() {
    const address = at(this, 'state.activeAddress.address');
    removeAddress(address.id, (err) => {
      if (err) {
        console.error('error deleting address: ', err);
      } else {
        const newAddresses = this.state.addresses.filter(addr => addr.id !== address.id);
        this.setState({
          addresses: newAddresses
        }, () => {
          this.changeActiveAddress(at(this, 'state.addresses.0'));
        });
      }
    });
  }

  sortAddresses(addresses = []) {
    let prefferedAddress = null;
    if (at(this, 'props.preferredAddress.id')) {
      const index = findIndex(addresses, (address) => (address.defaultAddress));
      prefferedAddress = index !== -1 ? addresses.splice(index, 1) : null;
    } else {
      const index = findIndex(addresses, (address) => (address.defaultAddress));
      prefferedAddress = index !== -1 ? addresses.splice(index, 1) : null;
    }

    if (prefferedAddress !== null) { addresses.unshift(prefferedAddress[0]); }

    return addresses;
  }

  renderAddresses(addresses) {
    return addresses.map((address) => <AddressAccordian
      type={this.props.origin}
      key={address.id}
      active={address.id === this.state.activeAddress.address.id}
      action={this.changeActiveAddress}
      editAddress={this.editAddress}
      deleteAddress={this.deleteAddress}
      activeServiceable={at(this, 'state.activeAddress.serviceable')}
      errorMessage={this.state.error}
      address={address} />);
  }

  render() {
    let modalAddressComponent = null;

    if (this.state.loading) {
      modalAddressComponent = <div className={Styles.loader} />;
    } else if (this.state.modalError) {
      modalAddressComponent = (<div className={Styles.modalError}>
        Error fetching addresses, please try again after some time.
      </div>);
    } else if (this.state.addresses.length === 0) {
      modalAddressComponent = (<div className={Styles.noAddresses}>
        No added address, please add address for changing the address.
      </div>);
    } else {
      modalAddressComponent = (<div className={Styles.renderAddress}>
        {this.renderAddresses(this.state.addresses)}
      </div>);
    }

    return (<div className={Styles.changeAddress}>
      <div className={Styles.shimmer} onClick={this.props.closeChangeAddressModal} />
      <div className={Styles.card}>
        <div className={Styles.addNewAddress} onClick={() => this.openAddAddressModal('add')}> + Add New Address </div>
        <div className={Styles.modalHeader}>
          <div className={Styles.caHeader}>Change Address</div>
          <div className={Styles.addButton} onClick={() => this.openAddAddressModal('add')}> Add new Address </div>
        </div>
        {modalAddressComponent}
        <div className={Styles.buttons}>
          <div className={Styles.button} onClick={this.props.closeChangeAddressModal}> Cancel </div>
          <div
            className={`${Styles.button}
              ${this.state.selectDisabled ? Styles.disabled : ''}`}
            onClick={() => this.updateAddress()}> Select </div>
        </div>
      </div>
      {this.state.addAddressModal &&
        <AddAddressModal
          addressData={this.state.activeAddress.address}
          mode={this.state.addressModalMode}
          closeModal={this.closeAddressModal}
          loadAddresses={this.getAllAddress} />}
    </div>);
  }
}

ChangeAddress.propTypes = {
  closeChangeAddressModal: PropTypes.func,
  preferredAddress: PropTypes.object,
  checkServiceability: PropTypes.func,
  origin: PropTypes.string,
  updateAddress: PropTypes.func,
  updateReturnData: PropTypes.func
};

export default ChangeAddress;
