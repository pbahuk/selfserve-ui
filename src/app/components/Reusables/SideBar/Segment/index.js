import React from 'react';
import PropTypes from 'prop-types';
import Styles from './segment.css';
import features from '../../../../utils/features';
const myntraCreditEnable = (features('myntracredit.enable') || '').toLowerCase() === 'true' || false;

class Segment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  generateLinks(links) {
    return links.map((link, key) => {
      if (link.refer === '/my/giftcards' && myntraCreditEnable) {
        link.refer = '/my/myntracredit';
        link.name = 'Myntra Credit';
      }
      return (<a key={key} href={link.refer} className={link.refer.indexOf(this.props.active) > -1 ? Styles.activeLink : Styles.link}> {link.name} </a>);
    });
  }

  render() {
    return (<div className={Styles.segment}>
      <div className={this.props.heading ? Styles.heading : ''}>
        {this.props.heading}
      </div>
      {this.generateLinks(this.props.links)}
    </div>);
  }
}

Segment.propTypes = {
  heading: PropTypes.string,
  links: PropTypes.array,
  active: PropTypes.string
};

export default Segment;
