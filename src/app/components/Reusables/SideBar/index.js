import React from 'react';
import PropTypes from 'prop-types';
import Style from './sidebar.css';
import Segment from './Segment';
import data from '../../../resources/data/sidebar';

class SideBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderSegments() {
    const segments = data.map((segment, key) =>
      <Segment key={key} heading={segment.heading} links={segment.links} active={this.props.active} />
    );
    return segments;
  }

  render() {
    return (<div className={Style.sidebar}>
      {this.renderSegments()}
    </div>);
  }
}

SideBar.propTypes = {
  active: PropTypes.string
};

export default SideBar;

