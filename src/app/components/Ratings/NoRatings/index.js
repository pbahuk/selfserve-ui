import React from 'react';

// Style Related Imports.
import Styles from './noRatings.css';
import SVGImages from '../../Reusables/SVGImages';

const NoRatings = () => (<div className={Styles.noRatingsCard}>
  <div className={Styles.imageWrapper}>
    <SVGImages name="circularTick" customStyle={{ height: '40px', width: '41px' }} />
  </div>
  <span className={Styles.primaryMessage}> You have rated all your purchases </span>
  <a href="/">
    <div className={Styles.button}> Go to Home </div>
  </a>
</div>);

export default NoRatings;
