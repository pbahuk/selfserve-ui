import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

import PageStyles from './../../resources/page.css';
import Styles from './ratings.css';

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';

// Page Related Component imports.
import ProductRating from './ProductRating';
import NoRatings from './NoRatings';

// Functional Imports.
import { setRatings } from '../../utils/services/ratings';
import { getUidx } from '../../utils/pageHelpers';
import Madalytics from '../../utils/trackMadalytics';
import Notify from '../Reusables/Notify/notify';

class Ratings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      pageError: false,
      products: [],
      selectedStyleId: Number.parseInt(props.location.query.styleId, 10),
      selectedStyleRating: Number.parseInt(props.location.query.rating, 10),
      showReviewSuccess: false
    };

    this.renderRatings = this.renderRatings.bind(this);
    this.toggleReviewSuccessDisplay = this.toggleReviewSuccessDisplay.bind(this);
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.ratingsResponse');
    const { selectedStyleId, selectedStyleRating } = this.state;
    let pageError = false;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }
    const products = at(serviceResponse, 'response.data') || [];
    const isProductEligible = products.map(product => at(product, 'cmsData.id')).indexOf(selectedStyleId);

    if (selectedStyleId && selectedStyleRating && isProductEligible > -1) {
      const payload = {
        uidx: getUidx(),
        styleId: selectedStyleId,
        rating: selectedStyleRating,
        timestamp: (new Date().getTime()).toString(),
        type: 1
      };

      setRatings(payload, (err, res) => {
        if (err || res.statusCode !== 200) {
          console.error('[Ratings Error: Setting Ratings]', err);
        }
        this.setState({
          loading: false,
          pageError,
          products
        });
      });
    } else {
      this.setState({
        loading: false,
        pageError,
        products
      });
    }
  }

  componentDidMount() {
    Madalytics.setPageContext('My Ratings');
    Madalytics.sendEvent('screenLoad', {
      entity_type: 'uidx',
      entity_id: getUidx()
    },
    { widget: {
      name: 'Ratings',
      type: 'input',
      data_set: {
        data: [{
          entity_type: 'product',
          entity_name: 'User My Rating page',
          entity_id: this.state.products.map((product) => at(product, 'cmsData.id'))
        }]
      }
    } }, 'web');
  }

  toggleReviewSuccessDisplay() {
    this.setState({
      showReviewSuccess: true
    }, () => {
      setTimeout(() => {
        this.setState({
          showReviewSuccess: false
        });
      }, 3000);
    });
  }

  renderRatings(products, rating = 0) {
    return products.map((product, key) => <ProductRating
      key={key}
      rating={rating}
      displaySuccess={this.toggleReviewSuccessDisplay}
      product={product} />);
  }

  render() {
    let page = null;
    const { products = [], selectedStyleId = null, selectedStyleRating = 0 } = this.state;
    const selectedProduct = products.filter((product) => at(product, 'cmsData.id') === selectedStyleId);
    const remainingProducts = products.filter((product) => at(product, 'cmsData.id') !== selectedStyleId);

    if (this.state.loading) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/ratings" />
        <div className={PageStyles.fullWidthComponent}>
          <div className={PageStyles.loader} />
        </div>
      </div>);
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/ratings" />
        <div className={PageStyles.fullWidthComponent}>
          <ErrorCard
            message="Something Went Wrong"
            secondaryMessage="Please try again after some time."
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else if (!products.length) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/ratings" />
        <div className={PageStyles.fullWidthComponent}>
          <NoRatings />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        {this.state.showReviewSuccess &&
          <div className={PageStyles.reviewSuccessNotify}>
            <Notify
              backgroundColor={'#2bc1a1'}
              NotifyText={'Your review has been submitted successfully!'} />
          </div>}
        <Account />
        <SideBar active="my/ratings" />
        <div className={PageStyles.fullWidthComponent}>
          {selectedProduct.length > 0 && <div>
            <div className={Styles.sectionHeading}> Rate the product </div>
            {this.renderRatings(selectedProduct, selectedStyleRating)}
          </div>}
          {remainingProducts.length > 0 && <div>
            <div className={`${Styles.sectionHeading} ${Styles.secondHeading}`}>
              {selectedProduct.length ? 'Please help us in rating other products also' : 'Rate the products'}
            </div>
            {this.renderRatings(remainingProducts)}
          </div>}
        </div>
      </div>);
    }

    return page;
  }
}

Ratings.propTypes = {
  location: PropTypes.object
};

export default Ratings;
