import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './productRating.css';
import MyRating from '../../Reusables/Forms/MyRating';
import SVGImages from '../../Reusables/SVGImages';
import Notify from '../../Notify';

// Helpers Function
import { getUidx, getImageUrl } from '../../../utils/pageHelpers';
import { setRatings } from '../../../utils/services/ratings';
import Madalytics from '../../../utils/trackMadalytics';
// import { getImageUrl } from '../../../utils/pageHelpers';
import get from 'lodash/get';
import { getRatings } from './../../../utils/services/ratings';
import { isApp } from '../../WriteReview/index';
import WriteReviewDesktop from '../../WriteReview/writeReviewModal';


class ProductRating extends React.PureComponent {
  constructor(props) {
    super(props);
    this.getUpdatedRating = this.getUpdatedRating.bind(this);
    this.showWriteReview = this.showWriteReview.bind(this);
    this.handleReviewClick = this.handleReviewClick.bind(this);
    this.state = {
      writeReview: false,
      updatedRating: {},
      showWriteReview: false
    };
  }
  onChange = (value) => {
    const { product = {} } = this.props;
    const payload = {
      uidx: getUidx(),
      styleId: get(product, 'cmsData.id'),
      rating: value,
      timestamp: (new Date().getTime()).toString(),
      type: 1
    };

    setRatings(payload, (err, res) => {
      this.setState({ writeReview: true });
      if (err || res.statusCode !== 200) {
        this.refs.notify.info({
          message: 'Error rating product, try again later',
          position: 'right'
        });
      } else {
        // Madalytics event.
        this.getUpdatedRating();
        Madalytics.setPageContext('My Ratings');
        Madalytics.sendEvent('widgetClick', {
          entity_type: 'uidx',
          entity_id: getUidx(),
          entity_result: err || res.statusCode !== 200 ? 'failure' : 'successs'
        },
        { widget: {
          name: 'Ratings',
          type: 'input',
          data_set: {
            data: [{
              entity_type: 'product',
              entity_name: get(product, 'cmsData.name'),
              entity_id: get(product, 'cmsData.id')
            }]
          }
        } }, 'web');
      }
    });
  }

  getUpdatedRating() {
    const styleId = get(this, 'props.product.cmsData.id', null);
    if (styleId) {
      const payload = {
        styleIds: [styleId]
      };
      getRatings(payload, (error, response) => {
        response = get(response, 'body');
        const status = get(response, 'status.statusCode');
        if (status === 200) {
          const ratings = get(response, 'userRatingMap');
          const rating = ratings.find((e) => e.key === styleId);
          this.setState({
            updatedRating: get(rating, 'value.userRating.0', null)
          });
        }
      });
    }
  }

  handleReviewClick(styleId, reviewId) {
    if (isApp()) {
      const targetURL = reviewId ?
      `${window.location.origin}/my/writeReview/${styleId}/${reviewId}` :
      `${window.location.origin}/my/writeReview/${styleId}`;
      window.location.assign(targetURL);
    } else {
      this.showWriteReview(true);
    }
  }

  showWriteReview(flag) {
    this.setState({
      showWriteReview: flag
    }, () => {
      if (flag) {
        document.body.classList.add('no-scroll-background');
      } else {
        document.body.classList.remove('no-scroll-background');
      }
    });
  }

  render() {
    const { product = {}, rating = 0 } = this.props;
    const styleId = get(product, 'cmsData.id');
    const { updatedRating } = this.state;
    const { reviewId, reviewStatus } = updatedRating;
    const currentRating = updatedRating.rating || rating;
    const reviewPendingStates = ['IMAGE_PENDING', 'PENDING'];
    const reviewEditStates = (reviewId && ['ACTIVE', 'REVIEW_REJECTED', 'IMAGE_REJECTED']) || [];
    const reviewWriteStates = (currentRating > 0 && ['NOT_PRESENT']) || [];

    const imageResolutionBaseUrl =
    `${get(product, 'cmsData.styleImages.default.securedDomain')}${get(product, 'cmsData.styleImages.default.resolutionFormula')}`;
    const productImageUrl = getImageUrl(imageResolutionBaseUrl, 80, 60);

    // Product Info.
    const brandName = get(product, 'cmsData.brandName') || '';
    const productDisplayName = get(product, 'cmsData.productDisplayName') || '';
    let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
    productName = productName.length > 30 ? `${productName.substr(0, 30)}...` : productName;

    return (<div className={Styles.productRatingCard}>
      <div className={Styles.productInfo}>
        <div className={Styles.thumbnail}>
          <a href={`/${get(product, 'cmsData.id')}`} > <img src={productImageUrl} /> </a>
        </div>
        <div className={Styles.info}>
          <div className={Styles.brand}> {brandName} </div>
          <div className={Styles.productName}> {productName} </div>
          <div className={Styles.rateBox}>
            <MyRating
              start={0}
              stop={5}
              step={1}
              rating={rating}
              changeRatings={this.onChange}
              emptySymbol={<SVGImages name="emptyStar" customStyle={{ height: '22px', width: '40px' }} />}
              fullSymbol={<SVGImages name="filledStar" customStyle={{ height: '22px', width: '40px' }} />} />
              {
                false && reviewPendingStates.indexOf(reviewStatus) > -1 && <div className={`${Styles.reviewText}`}>Review under Moderation</div>
              }
              {
                  false && reviewEditStates.indexOf(reviewStatus) > -1 && <div
                    onClick={() => this.handleReviewClick(styleId, reviewId)}
                    className={`${Styles.reviewText} ${Styles.active}`}>
                  Edit Review</div>
              }
              {
                false && reviewWriteStates.indexOf(reviewStatus) > -1 &&
                  <div onClick={() => this.handleReviewClick(styleId, reviewId)} className={`${Styles.reviewText} ${Styles.active}`}>
                  Write Review</div>
              }
          </div>
        </div>
      </div>
      {this.state.showWriteReview &&
        <WriteReviewDesktop
          displaySuccess={this.props.displaySuccess}
          userRating={rating}
          pdtImageURL={productImageUrl}
          pdtTitle={get(product, 'cmsData.name')}
          styleId={styleId.toString()}
          showWriteReview={this.showWriteReview}
          reviewStatus={reviewStatus}
          reviewId={reviewId} />}
      <Notify ref="notify" />
    </div>);
  }
}

ProductRating.propTypes = {
  product: PropTypes.object,
  rating: PropTypes.number,
  displaySuccess: PropTypes.func
};

export default ProductRating;
