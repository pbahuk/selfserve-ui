
import React from 'react';
import at from 'v-at';
import Item from '../Reusables/Item';
import PageStyles from './../../resources/page.css';

// Functional Imports.
import Madalytics from '../../utils/trackMadalytics';
import { getUidx } from '../../utils/pageHelpers';
import { getInsiderPoints } from '../../utils/services/insider';

import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';

// Reused components everywhere.
import ErrorCard from '../Reusables/Error';

// Order List related components.
import CrossSell from '../CrossSell';

class ItemDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageError: false,
      loading: true,
      order: {},
      items: [],
      item: 0,
      removeCancelABValue: 'disabled',
      insiderItemsPoints: null
    };
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    if (!serviceResponse || at(serviceResponse, 'error')) {
      this.setState({ pageError: true, loading: false });
    } else {
      const item = Number(at(serviceResponse, 'item')) || 0;
      const items = at(serviceResponse, 'response.order.items') || [];
      this.setState({
        loading: false,
        order: at(serviceResponse, 'response.order') || {},
        item,
        items,
        removeCancelABValue: at(serviceResponse, 'removeCancelAB')
      });
    }

    this.savingsAB = at(serviceResponse, 'savingsAB') || 'disabled';
    this.itemsTagAB = at(serviceResponse, 'itemsTagAB') || 'disabled';
    this.type = 'itemDetails';
    this.crossSellAB = at(serviceResponse, 'crossSellFilterAB');
    this.collapseTrackerABValue = at(serviceResponse, 'collapseTrackerAB');
    this.myOrderInsiderABValue = at(serviceResponse, 'myOrderInsiderAB');
    Madalytics.sendEvent('ScreenLoad', {
      type: 'My_Orders_Item_details',
      data_set: {
        sucess: !this.state.pageError,
        userId: getUidx()
      }
    });

    if (this.myOrderInsiderABValue) {
      getInsiderPoints((err, res) => {
        if (!err) {
          const insiderResponse = res.body || {};
          const enrolmentStatus = insiderResponse.enrolmentStatus;
          if (enrolmentStatus === 'active') {
            const insiderOrdersPoints = insiderResponse.orders || [];
            const storeOrderId = at(this, 'state.order.storeOrderId');
            let insiderItem = null;
            insiderOrdersPoints.forEach(order => {
              if (order.orderId === storeOrderId) {
                const items = order.lineItems || [];
                items.forEach(item => {
                  const itemId = Number.parseInt(item.lineItemId, 10);
                  if (itemId === this.state.item) {
                    insiderItem = item;
                  }
                });
              }
            });
            if (insiderItem) {
              this.setState({
                insiderItemsPoints: {
                  lineItemId: insiderItem.lineItemId,
                  points: insiderItem.points,
                  status: insiderItem.status,
                  canClaim: insiderItem.canClaim
                }
              });
            }
          }
        }
      });
    }
  }

  render() {
    let page = null;
    const type = this.type;

    if (this.state.loading) {
      page = (<div className={PageStyles.page}>
        <div className={PageStyles.fullWidthComponent}>
          <div className={PageStyles.loader} />
        </div>
      </div>);
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <div className={PageStyles.fullWidthComponent}>
          <ErrorCard
            message="Something Went Wrong"
            secondaryMessage="Please try again after some time."
            actionName="Retry"
            action={() => location.reload()} />
        </div>
      </div>);
    } else {
      const { items = [] } = this.state;
      const mainItem = items.find(item => item.id === this.state.item) || null;
      if (mainItem) {
        const product = at(mainItem, 'product') || {};
        page = (<div className={PageStyles.page}>
          <Account />
          <SideBar active="my/orders" />
          <div className={PageStyles.fullWidthComponent}>
            <Item
              type={type}
              item={mainItem}
              order={this.state.order}
              showCancelReturnsButton
              itemsTagAB={this.itemsTagAB}
              savingsAB={this.savingsAB}
              collapseTrackerABValue={this.collapseTrackerABValue}
              insiderItemsPoints={this.state.insiderItemsPoints}
              removeCancelABValue={this.state.removeCancelABValue} />
            <CrossSell
              product={product}
              itemId={this.state.item}
              orderId={this.state.order.storeOrderId}
              showFilter={this.crossSellAB} />
          </div>
        </div>);
      } else {
        const product = at(items[0], 'product') || {};
        page = (<div className={PageStyles.page}>
          <Account />
          <SideBar active="my/orders" />
          <div className={PageStyles.fullWidthComponent}>
          {items.map((item, key) =>
            <Item
              key={key}
              type={type}
              item={item}
              order={this.state.order}
              savingsAB={this.savingsAB}
              removeCancelABValue={this.removeCancelABValue}
              insiderItemsPoints={this.state.insiderItemsPoints}
              collapseTrackerABValue={this.collapseTrackerABValue}
              itemsTagAB={this.itemsTagAB} />)}
            <CrossSell
              product={product}
              itemId={this.state.item}
              orderId={this.state.order.storeOrderId}
              showFilter={this.crossSellAB} />
          </div>
        </div>);
      }
    }
    return page;
  }
}

export default ItemDetails;
