import { getProgressiveImage } from '../../../../utils/imageUtils';
import Madalytics from '../../../utils/trackMadalytics';
import { securify } from '../../../utils/securify';
import get from 'lodash/get';

import { emptyStateEventData } from '../../../resources/data/constants';

const secure = securify();

export const getShopMorePillsData = () => get(window, '__myx_growthHackConfig__.shopMorePills', []);

export const triggerClickEvent = (eventParams = {}) => {
  Madalytics.sendEvent(
    emptyStateEventData.widgetClickEvent,
    {
      entity_name: `${eventParams.contentType}_click`,
      entity_type: 'card',
      entity_id: eventParams.entityId
    },
    {
      widget: { name: eventParams.contentType, type: 'card' },
      widget_items: {
        name: `${eventParams.contentType}_click`,
        type: 'carousel',
        v_position: eventParams.vPos,
        h_position: eventParams.hPos,
        data_set: {
          data: [
            {
              entity_type: 'product',
              entity_name: eventParams.entityName,
              entity_id: eventParams.entityId
            }
          ]
        }
      },
      custom: {
        v1: 'empty'
      }
    }
  );
};

export const triggerWidgetLoadEvent = contentType => {
  Madalytics.sendEvent(
    emptyStateEventData.widgetLoadEvent,
    {
      entity_name: contentType,
      entity_type: 'card'
    },
    {
      widget: { name: contentType, type: 'card' },
      custom: {
        v1: 'empty'
      }
    }
    );
};

export const getSecureUrl = (url, w, h, c) => {
  const secureUrl = secure(url, w, h);
  const attr = {};
  if (w) attr.w = w;
  if (h) attr.h = h;
  if (c) attr.c = c;
  return getProgressiveImage(secureUrl, attr);
};

/*
    tranforms the related/ similar api resposne into one which the component
    responible for rendering the horizontal carousel card can understand
*/
export const transformRelated = (related = [], articleType, emptyStateWidgets) => {
  if (!related.length || !get(emptyStateWidgets, 'view-similar.show')) {
    return [];
  }

  const newCard = {
    type: 'COMPONENT_CARD',
    children: [
      {
        type: 'TITLE_TEXT',
        props: {
          text: `LOOKING FOR ${articleType || 'SOMETHING SIMILAR'}`
        }
      },
      {
        type: 'PRODUCT_RACK',
        props: {
          products: []
        }
      }
    ],
    props: {
      meta: {
        contentType: 'view-similar'
      }
    }
  };
  const allProducts = [];

  related.forEach(({ products }) => products.forEach((product) => {
    allProducts.push({
      styleid: product.id,
      image: {
        src: product.defaultImage.src
      },
      brands_facet: product.brand.name,
      stylename: product.name,
      discounted_price: product.price.discounted,
      price: product.price.mrp,
      discount_label: product.price.discount.label,
      discount: !!product.price.discount.label
    });
  }));

  newCard.children[1].props.products = allProducts;
  return newCard;
};
