import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

import { getEmptyStateProducts } from '../../utils/services/emptyState';

// Local Components
import PlainCard from '../Reusables/Cards/PlainCard';
import TilesCard from '../Reusables/Cards/TilesCard';
import GridCard from '../Reusables/Cards/GridCard';
import ShopMore from './ShopMore';

// Utilities
import {
  getShopMorePillsData,
  triggerClickEvent,
  triggerWidgetLoadEvent,
  getSecureUrl,
  transformRelated
} from './helpers/index';

// Styles
import Styles from '../Reusables/Cards/cards.css';

const cardConfig = {
  GRID: GridCard,
  PRODUCT_RACK: PlainCard,
  LINK_TILES: TilesCard
};

const TITLE_TEXT = 'TITLE_TEXT';

const CardBuilder = ({ cards = [], widgets = {} }) => {
  const componentCards = [];
  cards.forEach((card, index) => {
    if (card.children) {
      const headerObj = card.children.find(child => child.type === TITLE_TEXT);
      const cardProductsObj = card.children.find(child =>
        Object.keys(cardConfig).indexOf(child.type) > -1);
      // case:- if the product list is empty, then don't render the header as well
      let products = [];
      if (cardProductsObj) {
        const tilesProduct = get(cardProductsObj, 'props.tiles');
        const gridProduct = get(cardProductsObj, 'props.grid');
        const rackProduct = get(cardProductsObj, 'props.products');
        products = tilesProduct || gridProduct || rackProduct || [];
      }
      if (products.length) {
        card.children.forEach(childData => {
          childData.contentType = get(card, 'props.meta.contentType', '');
          const CardComponent = cardConfig[childData.type];
          if (CardComponent) {
            triggerWidgetLoadEvent(get(card, 'props.meta.contentType', ''));
            componentCards.push(
              <div className={Styles.card} key={index}>
                <div className={Styles.cardHeader}>
                  {get(headerObj, 'props.text', '')}
                </div>
                <CardComponent
                  cardData={childData}
                  index={index}
                  getSecureUrl={getSecureUrl}
                  triggerClickEvent={triggerClickEvent} />
              </div>
            );
          }
        });
      }
    }
  });
  return (
    <div className={Styles.cardContainer}>
      <div className={Styles.title}>
        <div className={Styles.titleText}>
            Have Something Else in Mind?
        </div>
      </div>
      {componentCards.length > 0 && componentCards}
      <ShopMore
        widgets={widgets}
        shopMorePillsData={getShopMorePillsData()}
        componentCardsLength={componentCards.length} />
    </div>
  );
};

class EmptyStateBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: []
    };
  }

  componentWillMount() {
    const { product: { id, articleType } } = this.props || {};
    this.emptyStateWidgets = get(window, '__myx_growthHackConfig__.emptyState.widgets', {});

    getEmptyStateProducts(id, (err, res) => {
      if (err || get(res, 'body.status.statusType') === 'ERROR') {
        console.error('[ERROR] fetching Empty State products', err);
      } else {
        const resp = get(res, 'body', []);
        this.filterCards(get(resp[0], 'related', []), get(resp[1], 'cards', []), articleType);
      }
    });
  }

  filterCards = (related = [], cards = [], articleType) => {
    if (!cards.length && !related.length) {
      return;
    }

    const filteredCards = [];
    const relatedCard = transformRelated(related, articleType, this.emptyStateWidgets);
    filteredCards.push(relatedCard);

    const cardsMap = cards.reduce((acc, card = {}) => {
      const widgetType = get(card, 'props.meta.contentType', '');
      if (widgetType) {
        acc[widgetType] = card;
      }
      return acc;
    }, {});

    Object.keys(this.emptyStateWidgets).forEach((widgetName) => {
      if (this.emptyStateWidgets[widgetName].show && cardsMap[widgetName]) {
        filteredCards.push(cardsMap[widgetName]);
      }
    });

    this.setState({
      cards: filteredCards
    });
  };

  render() {
    return (<CardBuilder cards={this.state.cards} widgets={this.emptyStateWidgets} />);
  }
}

CardBuilder.propTypes = {
  cards: PropTypes.array,
  widgets: PropTypes.object
};

EmptyStateBlock.propTypes = { product: PropTypes.object };

export default EmptyStateBlock;

