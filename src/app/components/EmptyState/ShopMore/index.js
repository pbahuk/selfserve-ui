import React, { PureComponent } from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';

import CardCarousel from '../../Reusables/Cards/PlainCard/CardCarousel';

import { triggerWidgetLoadEvent, triggerClickEvent } from '../helpers/index';
import { emptyStateEventData } from '../../../resources/data/constants';

import Styles from './shopMore.css';

const ShopMorePills = ({ shopMorePillsData, componentCardsLength }) =>
(
  <div className={`${Styles.continueContainer} ${Styles.shopMore}`}>
    <CardCarousel>
      <div className={Styles.contents}>
      {
          shopMorePillsData.map((pillsData, index) => {
            const clickEventData = {
              vPos: componentCardsLength,
              hPos: index,
              contentType: emptyStateEventData.contentTypeShopMore,
              cardType: emptyStateEventData.cardTypeComponentCard,
              entityName: get(pillsData, 'link'),
              entityId: get(pillsData, 'id')
            };
            return (
              <a
                key={get(pillsData, 'id')}
                onClick={() => triggerClickEvent(clickEventData)}
                href={`/${get(pillsData, 'link')}`} >
                <div className={Styles.continueShopPills}>
                  <div className={Styles.pillText}>
                    {get(pillsData, 'tag')}
                  </div>
                </div>
              </a>
            );
          })
        }
      </div>
    </CardCarousel>
  </div>
);

class ShopMore extends PureComponent {

  constructor(props) {
    super(props);
    const { componentCardsLength } = props;
    this.clickEventData = {
      vPos: componentCardsLength,
      hPos: 0,
      contentType: emptyStateEventData.contentTypeContinueShopping,
      cardType: emptyStateEventData.cardTypeComponentCard,
      entityName: '/',
      entityId: ''
    };
  }

  componentDidMount() {
    const { widgets, shopMorePillsData } = this.props;
    const shopMore = get(widgets, 'shop-more.show', false);

    if (shopMore && shopMorePillsData.length) {
      triggerWidgetLoadEvent(emptyStateEventData.contentTypeShopMore);
    } else {
      triggerWidgetLoadEvent(emptyStateEventData.contentTypeContinueShopping);
    }
  }

  render() {
    const { widgets, shopMorePillsData, componentCardsLength } = this.props;
    const shopMore = get(widgets, 'shop-more.show', false);

    if (shopMore && shopMorePillsData.length) {
      return (<ShopMorePills shopMorePillsData={shopMorePillsData} componentCardsLength={componentCardsLength} />);
    }

    return (<div className={`${Styles.continueContainer} ${Styles.continue}`} >
      <div className={Styles.continueButton}>
        <a
          onClick={() => triggerClickEvent(this.clickEventData)}
          href={'/'} >
          Continue Shopping
        </a>
      </div>
    </div>);
  }
}

ShopMore.propTypes = {
  widgets: PropTypes.object,
  shopMorePillsData: PropTypes.array,
  componentCardsLength: PropTypes.number
};

ShopMorePills.propTypes = {
  shopMorePillsData: PropTypes.array,
  componentCardsLength: PropTypes.number
};

export default ShopMore;
