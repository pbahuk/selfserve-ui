import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './exchangeReasons.css';

// React Component Imports.
import MyTextArea from './../../Reusables/Forms/MyTextArea';
import LineDropdown from './../../Reusables/Forms/LineDropdown';

class ExchangeReasons extends React.Component {
  formatReasons(reasons) {
    return reasons.map((reason) => ({
      id: reason.id,
      code: reason.reasonCode,
      displayName: reason.reasonDisplayName
    }));
  }

  render() {
    const form = this.props.form;
    const exchangeReasons = this.formatReasons(this.props.reasons);
    return (<div className={Styles.card}>
      <div className={Styles.cardHeading}> Please tell the reason for exchange* </div>
      <div className={Styles.reasonMessage}>
        Please tell us correct reason for exchange. This information is only used to improve our service
      </div>
      <LineDropdown
        name="exchangeReason"
        title="Select Exchange Reason"
        value={form.exchangeReason.value}
        expanded={form.exchangeReason.expanded}
        data={exchangeReasons}
        selectRadioOption={this.props.selectRadioOption} />
      <MyTextArea
        name="comment"
        value={form.comment.value}
        placeHolder={form.comment.placeHolder}
        handleChange={this.props.handleChange}
        handleBlur={this.props.handleBlur}
        error={form.comment.error} />
    </div>);
  }
}

ExchangeReasons.propTypes = {
  reasons: PropTypes.array,
  form: PropTypes.object,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  selectRadioOption: PropTypes.func
};

export default ExchangeReasons;
