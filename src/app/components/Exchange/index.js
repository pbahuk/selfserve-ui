import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import PageStyles from './../../resources/page.css';

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';

// Specific to this page.
import ExchangeForm from './ExchangeForm';
import Confirmation from './Confirmation';

class Exchange extends React.Component {
  constructor(props) {
    super(props);
    this.renderConfirmationComponent = this.renderConfirmationComponent.bind(this);

    this.state = {
      loading: true,
      itemExchanged: null,
      pageError: false,
      exchangeNum: null
    };
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    let pageError = false;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }
    const order = at(serviceResponse, 'response.order');
    const reasons = at(serviceResponse, 'response.reasons');

    this.setState({
      loading: false,
      pageError,
      order,
      reasons
    });
  }

  renderConfirmationComponent(itemExchanged, exchangeNum) {
    this.setState({
      itemExchanged,
      exchangeNum
    }, () => window.scrollTo(0, 0));
  }

  render() {
    let page = null;
    const locationQuery = this.props.location.query;
    const itemId = locationQuery.itemId || locationQuery.itemid;

    if (this.state.loading) {
      page = <div className={PageStyles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.component}>
          <ErrorCard
            message="Something Went Wrong"
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else if (this.state.itemExchanged) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <Confirmation
            order={this.state.order}
            item={this.state.itemExchanged}
            exchangeNum={this.state.exchangeNum}
            returnMode="exchange" />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <ExchangeForm
            order={this.state.order}
            reasons={this.state.reasons}
            itemId={itemId}
            renderConfirmationComponent={this.renderConfirmationComponent} />
        </div>
      </div>);
    }

    return page;
  }
}

Exchange.propTypes = {
  location: PropTypes.object
};

export default Exchange;
