import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Style Related Imports.
import Styles from './exchangeableItem.css';

// Component Related Imports.
import ItemViews from './../../Reusables/ItemViews';

import { getDate } from '../../../utils/pageHelpers';

class ExchangeableItem extends React.PureComponent {
  render() {
    const { item } = this.props;
    const exchangeDate = getDate(new Date(+get(item, 'status.updatedOn', '') +
      get(item, 'product.returnPeriod', 30) * 24 * 60 * 60 * 1000));
    return (<div className={Styles.exchangeableItem}>
      <ItemViews item={item} view="actionable" />
      <div className={Styles.exchangeInfo}>
        <div className={Styles.info}>
          <span className={Styles.label}> Valid for return before: </span>
          <span>{exchangeDate}</span>
        </div>
        <div> For more information, please view our
          <a className={Styles.link} href="/faqs#exchange"> exchange policy </a>
        </div>
      </div>
    </div>);
  }
}

ExchangeableItem.propTypes = {
  item: PropTypes.object
};

export default ExchangeableItem;
