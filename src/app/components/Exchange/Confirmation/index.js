import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './exchangeConfirmation.css';

// React Component Imports.
import ItemViews from './../../Reusables/ItemViews';
import ConfirmedItem from './ConfirmedItem';
import NextSteps from './../../Reusables/NextSteps';

// Functional Imports.
import { getProfileForProduct, getProfilesForUser } from '../../../utils/services/userProfileService';
import features from '../../../utils/features';
import { getDeviceData, getUidx } from '../../../utils/pageHelpers';
const uspFG = features('userSizeProfilesEnabled') === 'true';

class Confirmation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showProfileSuggestor: false,
      itemTagged: false
    };

    this.tagItem = this.tagItem.bind(this);
  }

  componentWillMount() {
    const { item, order = {} } = this.props;
    const isProfileSuggestionEnabled = this.isProfileSuggestionEnabled(item);

    if (isProfileSuggestionEnabled) {
      const payload = {
        uidx: getUidx(),
        skuId: at(item, 'product.skuId') || '',
        styleId: at(item, 'product.id') || '',
        orderId: at(order, 'storeOrderId') || at(order, 'id')
      };
      getProfileForProduct(payload, (err, res) => {
        if (err) {
          console.error('Error [User Profiles]: getProfileForProduct', err);
        } else {
          try {
            const profileResponse = (JSON.parse(at(res, 'body.text') || {}).data || [])[0] || {};
            if (at(profileResponse, 'pidx')) {
              this.setState({
                showProfileSuggestor: false
              });
            } else {
              getProfilesForUser({ userId: getUidx() }, (err1, res1) => {
                console.log('I will be fetching the profile for the user:::', err1, res1.body);
                const profileData = (JSON.parse(at(res1, 'body.text') || {}).data || [])[0];
                this.profiles = at(profileData, 'profileList');
                this.setState({
                  showProfileSuggestor: true
                });
              });
            }
          } catch (e) {
            console.error('Error in parsing response: ', e);
          }
        }
      });
    }
  }

  isProfileSuggestionEnabled(item) {
    const profileEnabledAgeGroup = ['Adults-Men', 'Adults-Women', 'Adults-Unisex'];
    const profileEnabledMasterCategories = ['Apparel', 'Footwear'];
    const product = at(item, 'product') || {};
    const isMobile = getDeviceData().isMobile;

    const ageGroup = at(product, 'ageGroup');
    const masterCategory = at(product, 'masterCategory');

    const profileSuggestionsEnabled = isMobile && uspFG &&
      profileEnabledAgeGroup.indexOf(ageGroup) > -1 &&
      profileEnabledMasterCategories.indexOf(masterCategory) > -1;

    return profileSuggestionsEnabled;
  }

  tagItem() {
    this.setState({
      itemTagged: true
    });
  }

  renderConfirmedItem(item) {
    console.log('uspFG:::::::', uspFG);
    return (<div>
      {uspFG ?
        <ConfirmedItem
          item={item}
          orderId={at(this, 'props.order.storeOrderId')}
          exchangeNum={this.props.exchangeNum}
          showProfileSuggestor={this.state.showProfileSuggestor}
          itemTagged={this.state.itemTagged}
          tagItem={this.tagItem}
          allProfiles={this.profiles} /> :
        <ItemViews view="confirmed" item={item} />}
    </div>);
  }


  render() {
    const { item, returnMode } = this.props;

    return (<div className={Styles.returnConfirmation}>
      <div className={Styles.itemsCard}>
        <div className={Styles.heading}> Exchange Process Completed </div>
        <div className={Styles.itemHolder}>
          {this.renderConfirmedItem(item)}
        </div>
        <div className={Styles.information}>
          <span className={Styles.bold}> Please note: </span>
          <span> This does not affect other items in the order </span>
        </div>
      </div>
      <NextSteps type={returnMode} />
      <div className={Styles.button} onClick={() => { window.location = '/my/orders'; }}> Done </div>
    </div>);
  }
}

Confirmation.propTypes = {
  order: PropTypes.object,
  item: PropTypes.object,
  exchangeNum: PropTypes.string,
  returnMode: PropTypes.string
};

export default Confirmation;
