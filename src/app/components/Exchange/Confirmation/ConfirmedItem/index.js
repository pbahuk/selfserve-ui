import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Style Related Imports.
import Styles from './confirmedItem.css';
import classnames from 'classnames/bind';

// React Component Imports.
import ProfileSuggestor from '../../../ProfileSuggestor';
import SVGImages from '../../../Reusables/SVGImages';
import Icon from '../../../Reusables/Icon';

import { getDeviceData } from '../../../../utils/pageHelpers';

const bindClass = classnames.bind(Styles);

const DetailsView = ({ exchangeNum }) => (
  <div>
    <div className={Styles.icon}>
      <Icon name="success_tick" className={Styles.tick} />
    </div>
    <div className={Styles.heading}> Exchange Request submitted </div>
    <div className={Styles.subHeading}>{`Exchange number: ${exchangeNum}`}</div>
    <div className={Styles.subBlock}>
      <div className={Styles.desc}>You will receive a exchange request email shortly with the expected pick up date.</div>
    </div>
  </div>
);

const ProductView = ({ isMobile, productImageUrl, productName, item, allProfiles, size, orderId, showProfileSuggestor, itemTagged, tagItem }) => (
  <div>
    <div className={Styles.thumbnail}>
      {itemTagged &&
        <div className={Styles.taggedTick}><SVGImages name="taggedTick" customStyle={{ height: '29px', width: '32px' }} /></div>}
      <img src={productImageUrl} />
    </div>
    <div className={Styles.info}>
      <div> {productName} </div>
      <div>
        Size: <span className={Styles.blackText}> {size.label} </span>
        / Quantity: <span className={Styles.blackText}> {at(item, 'quantity')} </span>
      </div>
    </div>
    {isMobile &&
      <ProfileSuggestor
        allProfiles={allProfiles}
        item={item}
        tagItem={tagItem}
        orderId={orderId}
        showProfileSuggestor={showProfileSuggestor} />}
  </div>
);

const ConfirmedItem = (props) => {
  const { item = {}, orderId, exchangeNum, allProfiles, showProfileSuggestor, itemTagged, tagItem } = props;
  const productImages = at(item, 'product.images.0') || {};
  let productImageUrl = productImages.secureSrc || '';
  const { isMobile, isDesktop } = getDeviceData();

  // Product Info.
  const brandName = at(item, 'product.brand.name') || '';
  const productDisplayName = at(item, 'product.name') || '';
  let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
  productName = productName.length > 30 ? `${productName.substr(0, 30)}...` : productName;
  const sizes = at(item, 'product.sizes') || [];
  const size = sizes.find((option) => option.skuId === item.product.skuId);

  productImageUrl = productImageUrl.replace('h_($height)', 'h_125').replace('q_($qualityPercentage)', 'q_100').replace('w_($width)', 'w_161');

  return (<div className={bindClass('confirmedItem', { desktop: isDesktop, mobile: !isDesktop })}>
    {isDesktop ? <div>
      <div className={Styles.leftSection}>
        <ProductView
          isMobile={isMobile}
          productImageUrl={productImageUrl}
          productName={productName}
          item={item}
          allProfiles={allProfiles}
          itemTagged={itemTagged}
          tagItem={tagItem}
          size={size}
          orderId={orderId}
          showProfileSuggestor={showProfileSuggestor} />
      </div>
      <div className={Styles.verticalSeparator}></div>
      <div className={Styles.rightSection}>
        <DetailsView exchangeNum={exchangeNum} />
      </div></div> : <div>
        <DetailsView exchangeNum={exchangeNum} />
        <ProductView
          isMobile={isMobile}
          productImageUrl={productImageUrl}
          productName={productName}
          item={item}
          allProfiles={allProfiles}
          itemTagged={itemTagged}
          tagItem={tagItem}
          size={size}
          orderId={orderId}
          showProfileSuggestor={showProfileSuggestor} />
      </div>}
  </div>);
};

ConfirmedItem.propTypes = {
  item: PropTypes.object,
  allProfiles: PropTypes.array,
  orderId: PropTypes.string,
  exchangeNum: PropTypes.string,
  showProfileSuggestor: PropTypes.bool,
  itemTagged: PropTypes.bool,
  tagItem: PropTypes.func
};

DetailsView.propTypes = {
  exchangeNum: PropTypes.string
};

ProductView.propTypes = {
  isMobile: PropTypes.bool,
  productImageUrl: PropTypes.string,
  productName: PropTypes.string,
  item: PropTypes.object,
  allProfiles: PropTypes.array,
  size: PropTypes.object,
  orderId: PropTypes.string,
  showProfileSuggestor: PropTypes.bool,
  itemTagged: PropTypes.bool,
  tagItem: PropTypes.func
};

export default ConfirmedItem;
