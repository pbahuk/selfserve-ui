import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import some from 'lodash/some';

// Style Related Imports.
import Styles from './exchangeForm.css';

// Service calls.
import { checkServiceability } from '../../../utils/services/serviceability';
import { createExchange } from '../../../utils/services/exchange';

// Component Related Imports.
import OrderInfo from './../../Reusables/OrderInfo';
import ExchangeableItem from './../ExchangeableItem';
import SizeSelector from './../SizeSelector';
import ExchangeReasons from './../ExchangeReasons';
import UserConfirmation from '../../Reusables/UserConfirmation';
import Address from '../Address';
import { gaEmitter, getDeviceData } from '../../../utils/pageHelpers';

class ExchangeForm extends React.Component {
  constructor(props) {
    super(props);
    this.checkExchangeServiceability = this.checkExchangeServiceability.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.selectRadioOption = this.selectRadioOption.bind(this);
    this.handleUserConfirm = this.handleUserConfirm.bind(this);
    this.selectSize = this.selectSize.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.updateAddress = this.updateAddress.bind(this);
    this.showSizeNotSelectedError = this.showSizeNotSelectedError.bind(this);
    this.resetServiceable = this.resetServiceable.bind(this);
    this.state = {
      formInactive: false,
      serviceable: 'inProgress',
      confirmEnabled: false,
      actionError: '',
      sizeOOS: false,
      sizeNotSelectedError: false,
      address: at(props, 'order.address'),
      form: {
        size: {
          value: '',
          required: true,
          error: ''
        },
        exchangeReason: {
          value: '',
          id: '',
          required: true,
          expanded: true,
          error: ''
        },
        comment: {
          value: '',
          required: false,
          placeHolder: 'Additional Comments',
          validation: /^[a-zA-Z0-9!@#$%\^&* )(+=.,_-]*$/,
          error: ''
        },
        userConfirmation: {
          value: false,
          error: '',
          required: true
        }
      }
    };
  }

  getItem() {
    const { props } = this;
    const { order: { items = [] } = {} } = props;
    return items.find((item) =>
      (item.status.code === 'C' || item.status.code === 'D') && at(item, 'flags.exchangeable.isExchangeable') && at(item, 'id').toString() === props.itemId
    );
  }

  checkExchangeServiceability(pincode, callback) {
    const { form = {} } = this.state;
    const { order } = this.props;
    let items = this.getItem() || {};
    const sizes = at(items, 'product.sizes') || [];
    const skuId = form.size.value;
    const selectedSize = sizes.find((size) => size.skuId.toString() === skuId);
    const warehouseIds = at(selectedSize, 'warehouses') || [];
    items = Array.isArray(items) ? items : [items];
    if (form.size.value) {
      const data = {
        serviceType: 'EXCHANGE',
        pincode,
        items,
        skuId,
        order,
        warehouseIds
      };
      checkServiceability(data, 'EXCHANGE', (err, res) => {
        if (err) {
          console.error('LMS CheckServiceability Failure', err);
        } else {
          const serviceable = at(res, 'body.serviceable');
          console.log('******* Serviceability Data:', at(serviceable, 'order.summary'), serviceable);
          this.setState({
            serviceable: serviceable ? 'serviceable' : 'notServiceable'
          }, () => {
            if (callback) {
              callback(err, res);
            }
          });
        }
      });
    } else {
      if (callback) {
        callback();
      }
    }
  }

  handleChange(target) {
    const form = this.state.form;
    const name = target.name;
    form[name] = { ...form[name], value: target.value, error: '' };

    this.setState({
      form
    }, this.validateForm);
  }

  handleBlur(target) {
    const form = this.state.form;
    const name = target.name;
    form[name].value = target.value;

    if (!target.value.match(form[name].validation)) {
      form[name].error = 'Invalid Input';
    }

    this.setState({
      form
    }, this.validateForm);
  }

  selectRadioOption(target) {
    const form = this.state.form;
    const name = target.name;
    form[name].value = target.value;
    form[name].id = target.id;

    this.setState({
      form
    }, this.validateForm);
  }

  handleUserConfirm(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], value: target.checked, error: '' };
    this.setState({
      form
    }, this.validateForm);
  }

  selectSize(target) {
    const { state: { form, address } } = this;
    const { size } = form;
    const updatedSize = { ...size, value: target.currentTarget.id };
    const available = target.currentTarget.dataset.available === 'true';
    const emptySize = { value: '', required: true, error: '' };
    this.setState({
      form: { ...form, size: available ? updatedSize : emptySize },
      sizeOOS: !available,
      sizeNotSelectedError: false,
      serviceable: 'inProgress',
      confirmEnabled: false
    }, () => {
      this.checkExchangeServiceability(address.pincode, () => {
        this.validateForm();
      });
    });

    gaEmitter('profile-exchange', 'exchange-size', '', {
      'new-size': updatedSize.value
    });
  }

  createPayload() {
    const { props: { order }, state: { form, address } } = this;
    const item = this.getItem();
    const txRefId = ['EXCHANGE', order.id, 'SKU', at(item, 'product.skuId'), Date.now()].join('');
    const size = (at(item, 'product.sizes') || []).find(s => s.skuId.toString() === form.size.value) || {};
    const payload = {
      orderReleaseId: at(item, 'orderReleaseId'),
      originalOrderID: order.id,
      originalPpsID: at(order, 'payments.ppsId'),
      txRefId,
      items: [{
        original: {
          type: 'SKU',
          itemIdentifier: `${at(item, 'product.skuId')}`,
          quantity: item.quantity,
          styleId: `${at(item, 'product.id')}`,
          sellerId: `${at(item, 'sellerId')}` || '21',
          supplyType: size.supplyType
        },
        exchange: {
          type: 'SKU',
          itemIdentifier: form.size.value,
          quantity: item.quantity,
          styleId: `${at(item, 'product.id')}`,
          sellerId: `${at(item, 'sellerId')}` || '21',
          supplyType: size.supplyType
        },
        originalOrderLineId: `${item.id}`,
        reasonCode: form.exchangeReason.id
      }],
      shippingAddress: {
        addressId: `${at(address, 'id')}`,
        receiverName: at(address, 'user.name'),
        shippingAddress: at(address, 'streetAddress'),
        shippingLocality: at(address, 'locality'),
        shippingCity: at(address, 'city'),
        shippingState: at(address, 'state.code'),
        shippingCountry: at(address, 'country.name'),
        shippingZipcode: at(address, 'pincode'),
        receiverMobile: at(address, 'user.mobile'),
        receiverEmail: at(address, 'user.email')
      },
      userComments: form.comment.value,
      reasonCode: form.exchangeReason.id
    };

    return payload;
  }

  validateForm() {
    const { state: { form, serviceable } } = this;
    this.setState({
      confirmEnabled: !some(form, (val) => (val.required && !val.value)) && serviceable === 'serviceable'
    });
  }

  handleSubmit() {
    if (this.state.confirmEnabled) {
      this.setState({
        formInactive: true,
        confirmEnabled: false
      });
      const payload = this.createPayload();
      createExchange(payload, (err, res) => {
        if (!err) {
          const item = this.getItem();
          this.props.renderConfirmationComponent(item, at(res, 'body.exchangeOrderId'));

          const { form } = this.state;
          gaEmitter('profile-exchange', 'exchange-request-success', '', {
            'new-size': form.size.value,
            'exchange-reason': form.exchangeReason.value
          });
          gaEmitter('profile-exchange', 'exchange-submitted', '', {
            'exchange-reason': form.exchangeReason.value,
            'new-size': form.size.value
          });
          gaEmitter(`profile-exchange/options/orderid=
            ${payload.originalOrderID}/styleid=${at(item, 'product.id')}`, 'screen-load');
        } else {
          this.setState({
            actionError: err.message || 'Something went wrong',
            formInactive: false,
            confirmEnabled: true
          });
        }
      });
    }
  }

  showSizeNotSelectedError() {
    window.scroll({ top: 0, behavior: 'smooth' });
    this.setState({
      sizeNotSelectedError: true
    });
  }

  resetServiceable() {
    this.setState({
      serviceable: 'inProgress',
      confirmEnabled: false
    });
    this.checkExchangeServiceability(at(this, 'state.address.pincode'), () => {
      this.validateForm();
    });
  }

  updateAddress(address) {
    this.setState({
      address
    }, this.validateForm);
  }

  render() {
    const { state: { form, confirmEnabled, actionError, sizeOOS, address, serviceable, sizeNotSelectedError }, props } = this;
    const order = props.order || {};
    const { userAgent } = getDeviceData();
    console.log('Serviceable in render:', serviceable, 'confirm button: ', confirmEnabled);
    const exchangeableItem = this.getItem();
    return (<div>
      <OrderInfo order={order} />
      <ExchangeableItem item={exchangeableItem} />
      <SizeSelector
        item={exchangeableItem}
        onSelect={this.selectSize}
        selectedSize={form.size.value}
        sizeNotSelectedError={sizeNotSelectedError}
        serviceable={serviceable}
        sizeOOS={sizeOOS} />
      <div className={Styles.card} >
        <ExchangeReasons
          form={form}
          handleChange={this.handleChange}
          handleBlur={this.handleBlur}
          selectRadioOption={this.selectRadioOption}
          reasons={props.reasons} />
        <Address
          item={exchangeableItem}
          updateAddress={this.updateAddress}
          sizeSelected={form.size.value}
          showSizeNotSelectedError={this.showSizeNotSelectedError}
          checkServiceability={this.checkExchangeServiceability}
          resetServiceable={this.resetServiceable}
          address={address} />
      </div>
      <div className={Styles.iphoneBottomMargin}>
      </div>

      <UserConfirmation onSelect={this.handleUserConfirm} error={!!actionError} />
      {actionError && <div className={Styles.actionError}> {actionError} </div>}
      <div className={`${Styles.buttons} ${userAgent.indexOf('MyntraRetailAndroid') !== -1 ? Styles.android : ''}`}>
        <div className={Styles.button} onClick={() => { window.location = '/my/orders'; }}> Cancel </div>
        <div className={`${Styles.button} ${confirmEnabled ? '' : Styles.disabled}`} onClick={this.handleSubmit}>
          Confirm
          <div className={this.state.formInactive ? Styles.buttonLoader : ''} />
        </div>
      </div>
    </div>);
  }
}

ExchangeForm.propTypes = {
  order: PropTypes.object,
  reasons: PropTypes.array,
  itemId: PropTypes.string,
  renderConfirmationComponent: PropTypes.func
};

export default ExchangeForm;
