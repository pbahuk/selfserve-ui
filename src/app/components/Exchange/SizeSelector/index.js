import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './sizeSelector.css';

const OOSReturn = () => {
  const path = window.location.search;
  return (<div className={Styles.oosReturnBlock}>
    <div className={Styles.oosReturnHeader}>Sorry! You are out of luck</div>
    <div>This size is out of stock. You can opt to return your purchase instead.</div>
    <div className={Styles.returnBtn}>
      <a href={`/my/return${path}`} className={Styles.returnLink}>RETURN ITEM</a>
    </div>
  </div>);
};

class SizeSelector extends React.Component {
  constructor(props) {
    super(props);
    this.renderSizes = this.renderSizes.bind(this);
  }

  renderSizes(sizes, { selectedSize, onSelect }) {
    return sizes.map((size) => (
      <div
        className={
          `${Styles.sizeRadioButton} 
          ${`${size.skuId}` === selectedSize ? Styles.selected : ''} 
          ${!size.available ? Styles.disabled : ''}`
        }
        onClick={onSelect}
        id={size.skuId}
        data-available={size.available}
        key={size.skuId}>
          {size.label}
      </div>));
  }

  render() {
    const item = this.props.item;
    const orderedSkuId = at(item, 'product.skuId');
    const sizes = at(item, 'product.sizes') || [];
    const serviceable = this.props.serviceable;

    const orderedSize = sizes.find((size) => orderedSkuId === size.skuId);
    return (<div className={Styles.card}>
      <div className={Styles.cardHeading}> Choose Another Size: </div>
      <span className={Styles.label}> You Ordered: </span>
      <span> {orderedSize.label} </span>
      <div className={Styles.label}> Exchange it for </div>
      {this.renderSizes(sizes, this.props)}
      {this.props.sizeOOS && <OOSReturn />}
      {serviceable === 'notServiceable' &&
        <div className={Styles.notServiceable}>This size is not serviceable for the address. Please select a different address.</div>}
      {this.props.sizeNotSelectedError &&
        <div className={Styles.notServiceable}>Sorry! You need to select your size before updating your address.</div>}
      <div className={Styles.unavailableMessage}> In case the required size is unavailable, select that size to return the item. </div>
    </div>);
  }
}

SizeSelector.propTypes = {
  item: PropTypes.object,
  selectedSize: PropTypes.string,
  onSelect: PropTypes.func,
  sizeOOS: PropTypes.bool,
  serviceable: PropTypes.string,
  sizeNotSelectedError: PropTypes.bool
};

export default SizeSelector;
