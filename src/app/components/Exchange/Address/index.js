import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './address.css';

// Functional Imports.
import { unescapeHTML } from '../../../utils/pageHelpers';

// React Component Imports
import ChangeAddress from '../../Reusables/ChangeAddress';

class Address extends React.Component {
  constructor(props) {
    super(props);

    // Functional Binds.
    this.openChangeAddressModal = this.openChangeAddressModal.bind(this);
    this.closeChangeAddressModal = this.closeChangeAddressModal.bind(this);
    this.updateAddress = this.updateAddress.bind(this);

    this.state = {
      loading: true,
      addresses: [],
      changeAddresModal: false
    };
  }

  componentWillMount() {
    this.setState({
      loading: true
    }, () => {
      this.props.checkServiceability(this.props.address.pincode, () => { this.setState({ loading: false }); });
    });
  }

  openChangeAddressModal() {
    if (!this.props.sizeSelected) {
      this.props.showSizeNotSelectedError();
    } else {
      document.body.style.overflow = 'hidden';
      this.setState({
        changeAddresModal: true
      });
    }
  }

  closeChangeAddressModal(updated) {
    document.body.style.overflow = 'auto';
    if (updated !== 'updated') {
      this.props.resetServiceable();
    }
    this.setState({
      changeAddresModal: false
    });
  }

  updateAddress(address) {
    this.closeChangeAddressModal('updated');
    const newAddress = {
      city: address.city,
      country: {
        code: address.countryCode,
        name: address.countryName
      },
      id: address.id,
      locality: address.locality,
      pincode: address.pincode,
      state: {
        code: address.stateCode,
        name: address.stateName
      },
      streetAddress: address.address,
      user: {
        email: address.email,
        mobile: address.mobile,
        name: address.name
      }
    };
    this.props.updateAddress(newAddress);
  }

  render() {
    const { address = {} } = this.props;
    let addressComponent = null;

    if (this.state.loading) {
      addressComponent = (<div className={Styles.exchangeAddress}>
        <div className={Styles.heading}>Address for pickup</div>
        <div className={Styles.loader} />
      </div>);
    } else {
      addressComponent = (
        <div className={Styles.card} >
          <div className={Styles.exchangeAddress}>
            <div className={Styles.heading}>Address for pickup</div>
            <div>{unescapeHTML(at(address, 'user.name'))}</div>
            <div>{unescapeHTML(at(address, 'streetAddress'))}</div>
            <div>{unescapeHTML(at(address, 'locality'))}, {unescapeHTML(at(address, 'city'))} - {address.pincode}</div>
            <div
              className={Styles.button}
              onClick={this.openChangeAddressModal}> Change Address </div>
            {this.state.changeAddresModal &&
              <ChangeAddress
                origin="exchangeChangeAddress"
                preferredAddress={address}
                updateAddress={this.updateAddress}
                checkServiceability={this.props.checkServiceability}
                closeChangeAddressModal={this.closeChangeAddressModal} />}
          </div>
        </div>
      );
    }

    return addressComponent;
  }
}

Address.propTypes = {
  address: PropTypes.object,
  item: PropTypes.object,
  order: PropTypes.object,
  checkServiceability: PropTypes.func,
  updateAddress: PropTypes.func,
  sizeSelected: PropTypes.string,
  showSizeNotSelectedError: PropTypes.func,
  resetServiceable: PropTypes.func
};

export default Address;
