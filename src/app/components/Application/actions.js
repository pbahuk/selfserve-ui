import { isBrowser } from '../../utils';

export function onMount() {
  if (!isBrowser()) {
    return;
  }
  document.getElementById('mountRoot').style.minHeight = '';
}

export function reset() {
  if (!isBrowser()) {
    return;
  }
  document.getElementById('mountRoot').style.minHeight = '1000px';
}
