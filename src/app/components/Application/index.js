import React, { PropTypes } from 'react';
import styles from './application.css';
import { onMount } from './actions';

class Application extends React.Component {
  componentDidMount() {
    onMount();
  }

  render() {
    return (
      <div className={`${styles.base} full-height`}>
        {this.props.children}
      </div>
    );
  }
}

Application.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ])
};

export default Application;
