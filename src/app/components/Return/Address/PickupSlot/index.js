import React from 'react';
import PropTypes from 'prop-types';

import Styles from './pickupSlot.css';

import { getPickupSlotString } from '../../../../utils/pageHelpers';
import Madalytics from '../../../../utils/trackMadalytics';


import PickupSlotModal from './../../PickupSlotModal';

class PickupSlot extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showPickupSlotModal: false
    };
  }

  onSlotSelected = (slot, isAnySlot) => {
    this.props.updateSelectedSlot(slot, isAnySlot);
    this.togglePickupSlotModal();
  }

  togglePickupSlotModal = () => {
    this.setState({
      showPickupSlotModal: !this.state.showPickupSlotModal
    });
    if (!this.state.showPickupSlotModal) {
      Madalytics.sendEvent('pickupSlotWidgetLoad');
    }
  }

  render() {
    const { anySlot, selectedSlot, slots } = this.props;
    const { showPickupSlotModal } = this.state;
    return (
      <div className={Styles.pickupSlot}>
        <div className={Styles.heading}>Pickup Date</div>
        <div className={Styles.pickupSlotWrapper} onClick={() => this.togglePickupSlotModal()} >
          <div className={Styles.pickupSlot}>
            {getPickupSlotString(anySlot, selectedSlot)}
            <span className={Styles.changeSlot}>
              Change Slot
            </span>
          </div>
        </div>
        {
          showPickupSlotModal && <PickupSlotModal
            close={() => this.togglePickupSlotModal()}
            slots={slots}
            selectedSlot={selectedSlot}
            selectSlot={(slot, isAnySlot) => this.onSlotSelected(slot, isAnySlot)}
            anySlot={anySlot} />
        }
        <div className={Styles.desc}>
          Our delivery executive will pick up the return during selected slot.
        </div>
      </div>);
  }
}

PickupSlot.propTypes = {
  anySlot: PropTypes.bool,
  selectedSlot: PropTypes.object,
  slots: PropTypes.array,
  updateSelectedSlot: PropTypes.func
};

export default PickupSlot;
