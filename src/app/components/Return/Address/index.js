import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import each from 'lodash/each';
import sortBy from 'lodash/sortBy';
import filter from 'lodash/filter';
import moment from 'moment';

// Style Related Imports.
import Styles from './address.css';

// Functional Imports.
import { unescapeHTML } from '../../../utils/pageHelpers';
import { checkServiceability } from '../../../utils/services/serviceability';
import Madalytics from './../../../utils/trackMadalytics';


// React Component Imports
import SelfShipInstructions from '../../Reusables/SelfShipInstructions';
import ChangeAddress from '../../Reusables/ChangeAddress';
import PickupSlot from './PickupSlot';
const pickupServiceableTypes = ['OPEN_BOX_PICKUP', 'CLOSED_BOX_PICKUP'];

class Address extends React.Component {
  constructor(props) {
    super(props);

    // Functional Binds.
    this.openChangeAddressModal = this.openChangeAddressModal.bind(this);
    this.closeChangeAddressModal = this.closeChangeAddressModal.bind(this);
    this.checkServiceability = this.checkServiceability.bind(this);
    this.updateAddress = this.updateAddress.bind(this);

    this.state = {
      loading: true,
      pickupServiceable: false,
      addresses: [],
      changeAddresModal: false,
      slots: [],
      anySlot: false
    };
  }

  componentWillMount() {
    this.checkItemServiceability(this.props.address);
  }

  checkItemServiceability = (address) => {
    const { item = {}, order = {}, renderErrorComponent } = this.props;
    this.setState({
      loading: true
    }, () => {
      const data = {
        order,
        items: [item],
        pincode: address.pincode,
        scheduleEnabled: true,
        warehouseIds: [`${item.dispatchWarehouseId}`]
      };
      checkServiceability(data, 'RETURN', (err, res) => {
        if (err) {
          console.error('CSS Return CheckServiceability Failure', err);
          this.setState({
            loading: false
          });
          renderErrorComponent();
        } else {
          const itemServiceabilityEntries = at(res, 'body.itemServiceabilityEntries') || [];
          let serviceabilityEntries = itemServiceabilityEntries.length ? itemServiceabilityEntries[0] : {};
          serviceabilityEntries = at(serviceabilityEntries, 'serviceabilityEntries') || [];
          serviceabilityEntries = serviceabilityEntries[0];

          const serviceType = at(serviceabilityEntries, 'serviceType');
          const pickupServiceable = pickupServiceableTypes.indexOf(serviceType) > -1;

          /*
             A function that calculates everything regarding the slots and gives back the value.
             Slot Related Calculations.
          */
          const schedules = at(serviceabilityEntries, 'schedules') || [];
          const processedSchedules = this.processSchedules(schedules, null);
          const slots = at(processedSchedules, 'slots');
          const selectedSlot = at(processedSchedules, 'selectedSlot');

          this.setState({
            loading: false,
            pickupServiceable,
            slots,
            selectedSlot,
            anySlot: false
          }, () => {
            this.props.updateReturnData(
              { serviceType,
                pickupSlot: { pickupSlotSelected: selectedSlot, anySlot: false }
              }
            );
          });
        }
      });
    });
  }

  /*
    processSchedules
    @param  schedules   schedules from the serviceability api response.
    @param  selected    Any pre-selected slot(optional)
    @return   { slots, selectedSlot }
            slots         : An array of slots meant for PickupSlotModal.
            selectedSlot  : default slot to be shown in PickupSlotModal.

  */
  processSchedules = (schedules, selected) => {
    // sorting the slots as per the scheduleStart
    const sortedSlots = sortBy(schedules, (schedule) => schedule.scheduleStart);
    const processedSchedules = [];
    const slotsByDate = {};
    let previousDate = null;
    // default slot selected initially
    let defaultSlot = null;
    const slotsLen = sortedSlots.length;

    const processSlot = (date) => {
      const daySlots = slotsByDate[date];
      // Identifying the active slots in a days
      const activeSlots = filter(daySlots,
        (s) => s.isActive ||
        (selected && (selected.scheduleStart === s.scheduleStart && selected.scheduleEnd === s.scheduleEnd)));
      let isActive = false;
      let dayDefaultSlot = {};
      if (activeSlots && activeSlots.length > 0) {
        isActive = true;
        dayDefaultSlot = activeSlots[0];
        if (!defaultSlot) {
          // Assigning the first available active slot as deafault slot
          defaultSlot = dayDefaultSlot;
        }
      }
      return {
        date, // Day of the slots
        slots: daySlots, // slots in a day
        isActive, // true if there is atleast one active slot in a day
        defaultSlot: dayDefaultSlot // defaultSlot in a day is used as 'anySlot'
      };
    };

    // Iterating through the sorted slots to re-arrange them in a UI friendly manner
    each(sortedSlots, (slot, id) => {
      const date = moment(slot.scheduleStart).format('YYYYMMDD');
      if (!slotsByDate[date]) {
        slotsByDate[date] = [];
      }
      // grouping the slots as per the date
      slotsByDate[date].push(slot);
      // identifying the last slot
      const lastSlot = slotsLen - 1 === id;
      // identifying the date change
      const dateChange = previousDate && previousDate !== date;
      if (dateChange || lastSlot) {
        // on date change, we process the slots of previous date
        if (dateChange) {
          processedSchedules.push(processSlot(previousDate, slotsByDate));
        }
        // for last time, we process the slots of present date
        if (lastSlot) {
          processedSchedules.push(processSlot(date, slotsByDate));
        }
      }
      previousDate = date;
    });
    return { slots: processedSchedules, selectedSlot: selected || defaultSlot };
  }

  checkServiceability(pincode, callback) {
    const { item = {}, order = {} } = this.props;
    const data = {
      order,
      items: [item],
      pincode,
      scheduleEnabled: true,
      warehouseIds: [`${item.dispatchWarehouseId}`]
    };
    checkServiceability(data, 'RETURN', (err, res) => {
      callback(err, res);
    });
  }

  openChangeAddressModal() {
    document.body.style.overflow = 'hidden';
    this.setState({
      changeAddresModal: true
    });
  }

  closeChangeAddressModal() {
    document.body.style.overflow = 'auto';
    this.setState({
      changeAddresModal: false
    });
  }

  updateAddress(address) {
    this.closeChangeAddressModal();
    const newAddress = {
      city: address.city,
      country: {
        code: address.countryCode,
        name: address.countryName
      },
      id: address.id,
      locality: address.locality,
      pincode: address.pincode,
      state: {
        code: address.stateCode,
        name: address.stateName
      },
      streetAddress: address.address,
      user: {
        email: address.email,
        mobile: address.mobile,
        name: address.name
      }
    };
    this.props.updateAddress(newAddress);
    this.checkItemServiceability(newAddress);
  }

  updateSelectedSlot = (slot, isAnySlot) => {
    this.setState({
      selectedSlot: slot,
      anySlot: isAnySlot
    });
    Madalytics.sendEvent('pickupSlotSelected');
    this.props.updateReturnData(
      {
        pickupSlot: { pickupSlotSelected: slot, anySlot: isAnySlot }
      }
    );
  }

  render() {
    const { address = {}, item = {}, order = {} } = this.props;
    const { slots, selectedSlot, anySlot } = this.state;
    console.log('item passed:::', item.id, order.id);
    let addressComponent = null;

    if (this.state.loading) {
      addressComponent = (
        <div className={Styles.section}>
          <div className={Styles.returnAddress}>
            <div className={Styles.heading}>Address for pickup</div>
            <div className={Styles.loader} />
          </div>
        </div>
      );
    } else {
      addressComponent = (
        <div className={Styles.section}>
          <div className={Styles.returnAddress}>
            <div className={Styles.heading}>Address for pickup</div>
            <div className={`${!this.state.pickupServiceable && Styles.fadeAddress}`}>
              <div>{unescapeHTML(at(address, 'user.name'))}</div>
              <div>{unescapeHTML(at(address, 'streetAddress'))}</div>
              <div>{unescapeHTML(at(address, 'locality'))}, {unescapeHTML(at(address, 'city'))} - {address.pincode}</div>
              {!this.state.pickupServiceable && <div className={Styles.notEligible} >Not Eligible for pickup</div>}
            </div>
            <div
              className={Styles.button}
              onClick={this.openChangeAddressModal}> Change Address </div>
            {!this.state.pickupServiceable &&
              <SelfShipInstructions />}
            {this.state.changeAddresModal &&
              <ChangeAddress
                origin="returnsChangeAddress"
                preferredAddress={address}
                checkServiceability={this.checkServiceability}
                updateAddress={this.updateAddress}
                closeChangeAddressModal={this.closeChangeAddressModal} />}
          </div>
          {selectedSlot && <PickupSlot
            anySlot={anySlot}
            selectedSlot={selectedSlot}
            slots={slots}
            updateSelectedSlot={this.updateSelectedSlot} />}
        </div>
      );
    }

    return addressComponent;
  }
}

Address.propTypes = {
  address: PropTypes.object,
  item: PropTypes.object,
  order: PropTypes.object,
  updateReturnData: PropTypes.func,
  updateAddress: PropTypes.func,
  renderErrorComponent: PropTypes.func
};

export default Address;
