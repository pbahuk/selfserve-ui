import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Style Related Imports.
import Styles from './returnableItems.css';

// React Component Imports.
import ItemViews from '../../Reusables/ItemViews';
import Icon from '../../Reusables/Icon';

import { getDate, getReturnRefundAmount, currencyValue } from '../../../utils/pageHelpers';

class ReturnableItems extends React.Component {
  constructor(props) {
    super(props);
    this.renderItems = this.renderItems.bind(this);
  }

  renderItems(items) {
    return (items.map((returnableItem) => {
      const returnDate = getDate(new Date(+get(returnableItem, 'status.updatedOn', '') +
        get(returnableItem, 'product.returnPeriod', 30) * 24 * 60 * 60 * 1000));

      return (<div key={returnableItem.id} className={Styles.returnableItem}>
        <ItemViews
          view="actionable"
          item={returnableItem} />
        <div className={Styles.returnableInfo}>
          <div>
            <span className={Styles.label}> Valid for return before: </span>
            <span> {returnDate} </span>
          </div>
          <div>
            <span className={Styles.label}> Refund Amount </span>
            <span>
              <Icon name="rupee500" customStyle={{ fontSize: '14px', fontWeight: '500' }} />
              {currencyValue(getReturnRefundAmount(returnableItem) / 100, false)}
            </span>
          </div>
        </div>
      </div>);
    }));
  }

  render() {
    const { items = [] } = this.props;

    return (<div className={Styles.returnableItems}>
      {this.renderItems(items)}
      <div className={Styles.helpMessage}>
        <div> For more information, please view our
          <a className={Styles.link} href="/faqs#returns"> return policy </a>
        </div>
      </div>
    </div>);
  }
}

ReturnableItems.propTypes = {
  items: PropTypes.array
};

export default ReturnableItems;
