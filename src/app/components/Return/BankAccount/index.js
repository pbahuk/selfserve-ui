import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './bankAccount.css';

// React Component Imports.
import Icon from '../../Reusables/Icon';

const ICON_STYLE = { fontSize: '18px', color: '#535766' };

class BankAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { account = {}, type = 'account' } = this.props;
    const icon = this.props.active ?
      { name: 'radio_active', style: { ...ICON_STYLE, color: '#14cda8' } } :
      { name: 'radio_inactive', style: ICON_STYLE };
    const accountNumber = at(account, 'accountNumber') ? at(account, 'accountNumber').toString() : '';
    const maskedAccountNumber = new Array(accountNumber.length - 3).join('x') + accountNumber.slice(-4);
    let bankName = at(account, 'bankName') || '';
    bankName = bankName.toLowerCase().replace('bank', '').replace(/ /g, '');

    return (<div
      className={`${Styles.eachBankAccount} ${Styles[type]}`}
      onClick={() => { if (type === 'accountList') { this.props.selectBankAccount(this.props.account.neftAccountId); } }}>
      {type === 'accountList' && <div className={Styles.selector}>
        <Icon customStyle={icon.style} name={icon.name} />
      </div>}
      <div className={Styles.accountDetails}>
        <div className={Styles.row}>
          <span className={Styles.label}> Name: </span>
          <span className={Styles.value}> {account.accountName} </span>
        </div>
        <div className={Styles.row}>
          <span className={Styles.label}> Bank: </span>
          <span className={Styles.value}> {account.bankName} </span>
        </div>
        <div className={Styles.row}>
          <span className={Styles.label}> Ifsc Code: </span>
          <span className={Styles.value}> {account.ifscCode} </span>
        </div>
        <div className={Styles.row}>
          <span className={Styles.label}> Account Number: </span>
          <span className={Styles.value}> {maskedAccountNumber} </span>
        </div>
        <div className={Styles.row}>
          <span className={Styles.label}> Account Type: </span>
          <span className={Styles.value}> {account.accountType} </span>
        </div>
        <div className={`${Styles[account.bankName.toLowerCase()]} ${Styles.bankLogo}`} />
      </div>
    </div>);
  }
}

BankAccount.propTypes = {
  type: PropTypes.string,
  account: PropTypes.object,
  active: PropTypes.bool,
  selectBankAccount: PropTypes.func
};

export default BankAccount;
