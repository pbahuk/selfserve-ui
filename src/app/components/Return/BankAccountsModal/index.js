import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

// Style Related Imports.
import Styles from './bankAccountsModal.css';

// React Component Imports
import BankAccount from '../BankAccount';
import BankAccountForm from '../../Reusables/BankAccountForm';

class BankAccountsModal extends React.Component {
  constructor(props) {
    super(props);
    this.renderAccounts = this.renderAccounts.bind(this);
    this.selectBankAccount = this.selectBankAccount.bind(this);
    this.openBankAccountForm = this.openBankAccountForm.bind(this);
    this.closeBankAccountForm = this.closeBankAccountForm.bind(this);
    this.addBankAccountProcessComplete = this.addBankAccountProcessComplete.bind(this);
    this.confirm = this.confirm.bind(this);

    const { selectedAccount, accounts } = this.props;

    this.state = {
      activeAccountId: get(selectedAccount, 'neftAccountId') || get(accounts, '0.neftAccountId'),
      bankAccountFormActive: false
    };
  }

  selectBankAccount(neftAccountId) {
    this.setState({
      activeAccountId: neftAccountId
    });
  }

  openBankAccountForm() {
    this.setState({
      bankAccountFormActive: true
    });
  }

  closeBankAccountForm() {
    this.setState({
      bankAccountFormActive: false
    });
  }

  addBankAccountProcessComplete() {
    this.props.getAccounts();
    this.setState({
      bankAccountFormActive: false
    });
  }

  confirm() {
    this.props.confirmAction(this.state.activeAccountId);
  }

  renderAccounts(accounts) {
    return (
      <div className={Styles.bankAccountList}>
        {accounts.map((account) => <BankAccount
          key={account.neftAccountId}
          type="accountList"
          account={account}
          active={this.state.activeAccountId === account.neftAccountId}
          selectBankAccount={this.selectBankAccount} />)}
      </div>
    );
  }

  render() {
    return (<div className={Styles.bankAccountsModal}>
      <div className={Styles.shimmer} />
      <div className={Styles.modal}>
        <div className={Styles.modalHeading}> Bank Accounts </div>
        <div className={Styles.modalActions}>
          <div className={Styles.heading}> Choose Account </div>
          <div className={Styles.addAccountButton} onClick={this.openBankAccountForm}> Add Account </div>
        </div>
        {this.renderAccounts(this.props.accounts)}
        <div className={Styles.buttons}>
          <div className={Styles.button} onClick={this.props.cancelAction}> Cancel </div>
          <div className={Styles.button} onClick={this.confirm}> Confirm </div>
        </div>
      </div>
      {this.state.bankAccountFormActive &&
        <BankAccountForm
          cancelAction={this.closeBankAccountForm}
          confirmAction={this.closeBankAccountForm}
          addBankAccountProcessComplete={this.addBankAccountProcessComplete} />}
    </div>);
  }
}

BankAccountsModal.propTypes = {
  accounts: PropTypes.array,
  cancelAction: PropTypes.func,
  confirmAction: PropTypes.func,
  getAccounts: PropTypes.func,
  selectedAccount: PropTypes.object
};

export default BankAccountsModal;
