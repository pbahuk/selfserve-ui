import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import forEach from 'lodash/forEach';
import Madalytics from './../../../utils/trackMadalytics';

// Style Related Imports.
import Styles from './refundOptions.css';

// React Component imports.
import AccountDetails from '../AccountDetails';
import MyRadio from '../../Reusables/Forms/MyRadio';

// Functional Imports.
import { currencyValue, getUidx } from '../../../utils/pageHelpers';
import kvpairs from '../../../utils/kvpairs';
import features from '../../../utils/features';

const mcRefundExtraText = kvpairs('myntracredit.refundoffer.text') || '';
const mcRefundExtraConfig = kvpairs('myntracredit.refundoffer.config') || {};
const mcRefundExpConfig = kvpairs('mymyntra.creditRefundExperiment.config') || {};
const myntraCreditEnabled = features('myntracredit.enable') === 'true' || features('myntracredit.enable') === true;
const myntraCreditRefundCodEnabled = features('myntracredit.refund.codenable') === 'true' || features('myntracredit.refund.codenable') === true;
const myntraCreditRefundPPEnabled = features('myntracredit.refund.ppenable') === 'true' || features('myntracredit.refund.ppenable') === true;

const MCOffer = ({ show, amount }) => (
  <div className={`${Styles.mcOffer} ${show ? '' : Styles.hide}`}>
    <div className={Styles.offerHeading}>Introductory Offer</div>
    <div
      className={Styles.offerDesc}>
      Get extra {amount}
      {' '} on your first refund into Myntra Credit, for new Myntra Credit users only.
    </div>
  </div>
);

MCOffer.propTypes = {
  show: PropTypes.bool,
  amount: PropTypes.string
};

const getOptionsComponentsMapper = (options) => {
  const { item: { payments, quantity } = {}, ordersMyntraCreditAB = false } = options;
  const paymentMethods = [];
  forEach(at(payments, 'instruments') || {}, (val, key) => {
    if (val !== 0) {
      paymentMethods.push(key);
    }
  });
  const onMethods = ['neft', 'creditCard', 'debitCard', 'netBanking', 'phonepe'];
  const isCOD = paymentMethods.indexOf('cod') !== -1;
  const isPayLater = paymentMethods.indexOf('paylater') !== -1;
  const isON = onMethods.find(method => paymentMethods.indexOf(method) !== -1);
  const externalInstrumentRefundAmount = (at(payments, 'amount') / quantity) / 100;
  let refundAmountString = null;
  let refundOptions = {
    BACK_TO_SOURCE: {
      label: 'Back to Source',
      key: 'OR',
      description: 'After receiving this item, Rs. {refund_amount} will be credited to the original payment method(s).'
    }
  };
  if (!isPayLater) {
    refundOptions = {
      ...refundOptions,
      WALLET: {
        label: 'PhonePe Wallet',
        key: 'WALLET',
        description: `After recieving this item, we will credit Rs. {refund_amount} to the original payment methods.
          Refund to source bank account or credit/debit cards will be credited within 7-10 business days`
      },
      BANK_ACCOUNT: {
        label: 'Bank Account',
        key: 'NEFT',
        description: `After receiving this item, Rs. {refund_amount} will be credited to the original payment method(s).
          Refund to your specified bank account will be credited within 1-3 days.`,
        component: <AccountDetails updateReturnData={options.updateReturnData} />
      }
    };
  }
  if (
    !isPayLater && myntraCreditEnabled &&
    (isCOD && myntraCreditRefundCodEnabled) ||
    (isON && myntraCreditRefundPPEnabled && externalInstrumentRefundAmount < 10000)
  ) {
    if (isCOD && at(mcRefundExtraConfig, 'cod.enabled')) {
      refundAmountString = `Rs. ${at(mcRefundExtraConfig, 'cod.amount')}`;
    } else if (isON && at(mcRefundExtraConfig, 'prepaid.enabled')) {
      refundAmountString = `Rs. ${at(mcRefundExtraConfig, 'prepaid.amount')}`;
    }

    const refundText = `${refundAmountString} ${mcRefundExtraText}`;
    const mcDetailsHideKnowMore = ordersMyntraCreditAB && at(mcRefundExpConfig, 'hideKnowMore');
    const myntraCreditExpLabel = ordersMyntraCreditAB
      ? (<span className={Styles.myntraCreditLabel}>
        <span>Myntra Credit</span>
        {(at(mcRefundExpConfig, 'benefits') || []).map(benefit =>
          <span className={Styles.myntCreditTag}>{benefit}</span>
        )}
      </span>)
      : 'Myntra Credit';

    refundOptions = {
      ...refundOptions,
      MYNTRA_CREDIT: {
        label: myntraCreditExpLabel,
        key: 'MYNTCREDIT',
        description: `After receiving this item, Rs. {refund_amount} will be instantly refunded to your Myntra Credit.
          ${options.mcOfferShow && options.mcRefundOffer && refundAmountString ? refundText : ''}`,
        defaultDisplay: options.mcRefundOffer && refundAmountString && <MCOffer show={options.mcOfferShow} amount={refundAmountString} />,
        component: (<div className={Styles.mcNote}>
          <span className={Styles.mcNoteHead}>NOTE: </span>
          Myntra Credit can’t be withdrawn in the form of cash or transferred to any bank account.
          It can be used for purchases on Myntra only. The refund will be valid for 1 year.
          {mcDetailsHideKnowMore ? null : <a href="/faqs#myntracredit" className={Styles.mcKnowMore}>Know More</a>}
        </div>)
      }
    };
  }

  return { optionsComponentsMapper: refundOptions, paymentMode: isCOD ? 'cod' : 'on', refundAmountString };
};

const sortRefundOptions = (refundOptions, paymentMode, mcDefaultExp) => {
  const myntraCreditOption = refundOptions.find(option => option.name === 'MYNTRA_CREDIT');
  const otherOptions = refundOptions.filter(option => option.name !== 'MYNTRA_CREDIT');
  return myntraCreditOption && (paymentMode === 'cod' || mcDefaultExp) ? [
    myntraCreditOption,
    ...otherOptions
  ] : refundOptions;
};

class RefundOptions extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);

    this.state = {
      selectedOption: '',
      options: [],
      mcOfferShow: false
    };
  }

  componentWillMount() {
    const { state: { mcOfferShow } } = this;
    const options = this.getOptions({ mcOfferShow });
    const selectedOption = options[0].key || '';
    const updatedMcOfferShow = selectedOption !== 'MYNTCREDIT';
    const updatedOptions = this.getOptions({ mcOfferShow: updatedMcOfferShow });
    this.myntcreditBannerShown = false;
    this.setState({
      selectedOption,
      options: updatedOptions,
      mcOfferShow: updatedMcOfferShow
    });
    this.props.onSelect({ name: 'refundMode', value: selectedOption });
  }

  componentDidMount() {
    this.myntcreditBannerEvent();
  }

  componentWillUpdate() {
    this.myntcreditBannerEvent();
  }

  onChange(target) {
    const { state: { mcOfferShow, options } } = this;
    this.setState({
      selectedOption: target.value,
      options: !mcOfferShow ? this.getOptions({ mcOfferShow: !mcOfferShow }) : options,
      mcOfferShow: true
    });
    this.props.onSelect(target);
  }

  getOptions({ mcOfferShow }) {
    const refundDetails = this.props.refundDetails || {};
    const amount = at(refundDetails, 'amount') || 0;
    const refundOptions = at(refundDetails, 'refundOptions') || [];
    const { optionsComponentsMapper, paymentMode, refundAmountString } =
      getOptionsComponentsMapper({ ...this.props, mcOfferShow, ordersMyntraCreditAB: this.props.ordersMyntraCreditAB });
    this.paymentMode = paymentMode;
    // Required for event 'myntracreditBannerShown'
    this.refundAmountString = refundAmountString;
    const sanitizedRefundOptions = [];
    const mcDefaultExp = this.props.ordersMyntraCreditAB && at(mcRefundExpConfig, 'defaultMyntraCredit');

    sortRefundOptions(refundOptions, paymentMode, mcDefaultExp).forEach((option) => {
      let optionComponent = optionsComponentsMapper[option.name];
      if (optionComponent) {
        optionComponent = {
          ...optionComponent,
          description: (at(optionComponent, 'description') || '').replace('{refund_amount}', currencyValue(amount / 100, false))
        };
        sanitizedRefundOptions.push(optionComponent);
      }
    });

    return sanitizedRefundOptions;
  }

  myntcreditBannerEvent = () => {
    if (this.state.mcOfferShow && !this.myntcreditBannerShown && this.refundAmountString && this.props.mcRefundOffer) {
      Madalytics.sendEvent(
        'myntracreditBannerShown',
        {
          entity_type: 'refundDetails',
          'entity_id': getUidx()
        },
        { custom:
          { v1: this.paymentMode, v2: Date.now() }
        });

      this.myntcreditBannerShown = true;
    }
  }

  render() {
    const refundOptions = at(this, 'state.options');
    const onlyORoption = refundOptions.length === 1 && refundOptions[0].key === 'OR';

    return (<div className={Styles.refundMode}>
      <div className={Styles.heading}>{onlyORoption ? 'Mode of refund' : 'Choose mode of refund'}</div>
      <MyRadio
        group="refundMode"
        defaultSelected={this.state.selectedOption}
        options={this.state.options}
        onChange={this.onChange}
        selectable={!onlyORoption} />
    </div>);
  }
}

RefundOptions.propTypes = {
  refundDetails: PropTypes.object,
  mcRefundOffer: PropTypes.bool,
  item: PropTypes.object,
  onSelect: PropTypes.func,
  updateReturnData: PropTypes.func,
  ordersMyntraCreditAB: PropTypes.bool
};

export default RefundOptions;
