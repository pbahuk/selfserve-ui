import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './returnReasons.css';

// React Component Imports.
import MyTextArea from './../../Reusables/Forms/MyTextArea';
import LineDropdown from './../../Reusables/Forms/LineDropdown';
import ExchangeOption from '../ExchangeOption';

class ReturnReasons extends React.Component {
  sanitizeDropDownData(data) {
    return data.map(d => ({ id: d.id, mode: d.mode, code: d.reasonCode, displayName: d.reasonDisplayName }));
  }

  render() {
    const { returnReasons = [], form = {}, returnReasonCategories = [] } = this.props;
    const categoryIdSelected = form.reasonCategory.id;
    const categories = [];
    let reasons = [];

    returnReasonCategories.forEach((category) => categories.push(category.categoryid));
    const reasonCategories = returnReasons.filter((reason) => (categories.indexOf(reason.reasonCode) > -1));

    if (form.reasonCategory.value) {
      const selectedReasonCategory = returnReasonCategories.find((category) => category.categoryid === categoryIdSelected);
      const reasonKeys = selectedReasonCategory.reasonids || [];
      reasons = returnReasons.filter((reason) => reasonKeys.indexOf(reason.reasonCode) > -1);
    }

    return (<div className={Styles.returnReasons}>
      <div className={Styles.inputWrapper}>
        <div className={Styles.heading}>
          Please tell the reason for return*
        </div>
        <div className={Styles.reasonMessage}>
          Please tell us correct reason for return. This information is only used to improve our service
        </div>
        <LineDropdown
          name="reasonCategory"
          label="Select issue*"
          data={this.sanitizeDropDownData(reasonCategories)}
          expanded={!form.reasonCategory.value}
          value={form.reasonCategory.value}
          error={form.reasonCategory.error}
          selectRadioOption={this.props.selectRadioOption} />
        {(form.reasonCategory.value && reasons.length > 0) &&
          <LineDropdown
            name="reason"
            label="Select issue detail*"
            data={this.sanitizeDropDownData(reasons)}
            expanded={!form.reason.value}
            value={form.reason.value}
            error={form.reason.error}
            selectRadioOption={this.props.selectRadioOption} />}
        {this.props.showExchangeOption &&
          <ExchangeOption
            closeExchangeOptionCard={this.props.closeExchangeOptionCard} />}
        <MyTextArea
          name="comment"
          value={form.comment.value}
          placeHolder={form.comment.placeHolder}
          handleChange={this.props.handleChange}
          handleBlur={this.props.handleBlur}
          error={form.comment.error} />
      </div>
    </div>);
  }
}

ReturnReasons.propTypes = {
  form: PropTypes.object,
  returnReasons: PropTypes.array,
  returnReasonCategories: PropTypes.array,
  showExchangeOption: PropTypes.bool,
  selectRadioOption: PropTypes.func,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  closeExchangeOptionCard: PropTypes.func,
  updateReturnData: PropTypes.func,
  updateAddress: PropTypes.func,

  // Order Related.
  order: PropTypes.object,
  item: PropTypes.object,
  address: PropTypes.object
};

export default ReturnReasons;
