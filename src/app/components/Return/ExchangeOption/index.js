import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './exchangeOption.css';

// React Component Imports.
import Icon from '../../Reusables/Icon';

import features from '../../../utils/features';

const oldRoute = features('returns.tab.enable') === 'true';

const ExchangeOption = (props) => (<div className={Styles.exchangeOption}>
  <div className={Styles.icon}>
    <Icon name="avatar" customStyle={{ fontSize: '45px', fontWeight: '500' }} />
  </div>
  <div className={Styles.info}>
    <div className={Styles.heading}> Facing Sizing Issues ? </div>
    <div className={Styles.subHeading}> Exchage Item and get the size you want. </div>
    <a
      href={
        oldRoute ? `/my/orders/exchange${window.location.search}` : `/my/exchange${window.location.search}`
      }
      className={Styles.button}> Exchange Now </a>
  </div>
  <div className={Styles.closeCard} onClick={() => { props.closeExchangeOptionCard(); }}>
    <Icon name="clear" customStyle={{ fontSize: '14px', cursor: 'pointer' }} />
  </div>
</div>);

ExchangeOption.propTypes = {
  closeExchangeOptionCard: PropTypes.func
};

export default ExchangeOption;
