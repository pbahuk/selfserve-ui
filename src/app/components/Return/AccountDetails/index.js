import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import isEmpty from 'lodash/isEmpty';

// Style Related Imports.
import Styles from './accountDetails.css';

// Functional Imports.
import { getBankDetails } from '../../../utils/services/crm.js';
import BankAccountForm from '../../Reusables/BankAccountForm';
import BankAccountsModal from '../BankAccountsModal';
import BankAccount from '../BankAccount';


class AccountDetails extends React.Component {
  constructor(props) {
    super(props);
    this.openBankForm = this.openBankForm.bind(this);
    this.closeBankForm = this.closeBankForm.bind(this);
    this.openBankAccountsModal = this.openBankAccountsModal.bind(this);
    this.closeBankAccountsModal = this.closeBankAccountsModal.bind(this);
    this.updateBankAccount = this.updateBankAccount.bind(this);
    this.getAccounts = this.getAccounts.bind(this);
    this.addBankAccountProcessComplete = this.addBankAccountProcessComplete.bind(this);

    this.state = {
      loading: true,
      bankAccounts: [],
      selectedAccount: {},
      bankAccountFormActive: false,
      bankAccountsModal: false
    };
  }

  componentWillMount() {
    this.getAccounts();
  }

  getAccounts() {
    let bankAccounts = [];

    getBankDetails((err, res) => {
      if (err) {
        console.error('Error, Fetching Bank Details', err, res.body);
      } else {
        const neftAccountResponse = at(res, 'body.neftAccountResponse');
        if (at(neftAccountResponse, 'status.statusType') === 'ERROR') {
          console.log('Error again');
        } else if (at(neftAccountResponse, 'status.totalCount') === 0 || at(neftAccountResponse, 'data') === '') {
          console.log('No bank details present');
        } else {
          bankAccounts = at(neftAccountResponse, 'data.neftAccountEntry') || [];
          bankAccounts = Array.isArray(bankAccounts) ? bankAccounts : [bankAccounts];
        }
      }
      let { selectedAccount } = this.state;
      selectedAccount = !isEmpty(selectedAccount) ? selectedAccount : bankAccounts[0];
      this.setState({
        loading: false,
        bankAccounts,
        selectedAccount
      });
      this.props.updateReturnData({ accountId: at(selectedAccount, 'neftAccountId') });
    });
  }

  addBankAccountProcessComplete() {
    this.getAccounts();
    this.closeBankForm();
  }

  openBankForm() {
    document.body.style.overflow = 'hidden';
    this.setState({
      bankAccountFormActive: true
    });
  }

  closeBankForm() {
    document.body.style.overflow = 'auto';
    this.setState({
      bankAccountFormActive: false
    });
  }

  openBankAccountsModal() {
    document.body.style.overflow = 'hidden';
    this.setState({
      bankAccountsModal: true
    });
  }

  closeBankAccountsModal() {
    document.body.style.overflow = 'auto';
    this.setState({
      bankAccountsModal: false
    });
  }

  updateBankAccount(accountId) {
    const account = this.state.bankAccounts.find((acc) => acc.neftAccountId === accountId);
    this.setState({
      selectedAccount: account
    });
    this.props.updateReturnData({ accountId });
    this.closeBankAccountsModal();
  }

  render() {
    let bankDetailsComponent = null;

    if (this.state.loading) {
      bankDetailsComponent = (<div className={Styles.bankDetails}>
        <div className={Styles.loader} />
      </div>);
    } else if (this.state.bankAccounts.length === 0) {
      bankDetailsComponent = (<div className={Styles.bankDetails}>
        <div className={Styles.noAccountButton} onClick={this.openBankForm}> Please enter Bank Account details </div>
        {this.state.bankAccountFormActive &&
          <BankAccountForm
            cancelAction={this.closeBankForm}
            confirmAction={this.closeBankForm}
            addBankAccountProcessComplete={this.addBankAccountProcessComplete} />}
      </div>);
    } else {
      const { selectedAccount = {} } = this.state;
      bankDetailsComponent = (<div className={Styles.bankDetails}>
        <div className={Styles.headingComponent}>
          <div className={Styles.heading}> Your Account Details: </div>
          <div className={Styles.changeAccountButton} onClick={this.openBankAccountsModal}> Change Account </div>
        </div>
        <BankAccount
          type="account"
          account={selectedAccount} />
        {this.state.bankAccountsModal &&
          <BankAccountsModal
            accounts={this.state.bankAccounts}
            selectedAccount={selectedAccount}
            getAccounts={this.getAccounts}
            cancelAction={this.closeBankAccountsModal}
            confirmAction={this.updateBankAccount} />}
      </div>);
    }
    return bankDetailsComponent;
  }
}

AccountDetails.propTypes = {
  updateReturnData: PropTypes.func
};

export default AccountDetails;
