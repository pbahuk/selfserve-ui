import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';

// Style Related Imports.
import PageStyles from './../../resources/page.css';

// Reused components everywhere.
import Account from '../Reusables/Account';
import SideBar from '../Reusables/SideBar';
import ErrorCard from '../Reusables/Error';
import CCintervention from '../Reusables/CCintervention';
import Madalytics from '../../utils/trackMadalytics';

// Specific to this page.
import ReturnForm from './ReturnForm';
import Confirmation from './Confirmation';

// functional imports
import { getDeviceData } from './../../utils/pageHelpers';

class Return extends React.Component {
  constructor(props) {
    super(props);
    this.renderConfirmationComponent = this.renderConfirmationComponent.bind(this);
    this.state = {
      loading: true,
      pageError: props.location.query.itemId === '',
      order: {},
      returnedItems: [],
      nonReturnedItems: [],
      returnReasons: [],
      returnReasonCategories: [],
      refundDetails: {},
      returnId: '',
      isApp: false,
      isAndroid: false
    };
    this.isApp = false;
    this.isAndroid = false;
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.omsServiceResponse');
    let pageError = this.state.pageError;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    }
    const order = at(serviceResponse, 'response.order');
    const returnReasons = at(serviceResponse, 'response.reasons');
    const returnReasonCategories = at(serviceResponse, 'response.returnReasonCategories');
    const refundDetails = at(serviceResponse, 'response.refundDetails');
    this.mcRefundOffer = at(serviceResponse, 'response.mcRefundOffer');
    this.ordersMyntraCreditAB = at(serviceResponse, 'ordersMyntraCreditAB');
    this.emptyStateEnabled = at(serviceResponse, 'emptyStateEnabled') === 'enabled';
    const { isApp, userAgent } = getDeviceData();
    this.isApp = isApp;
    this.isAndroid = userAgent.indexOf('MyntraRetailAndroid') !== -1;
    this.setState({
      loading: false,
      pageError,
      order,
      returnReasons,
      returnReasonCategories,
      refundDetails
    });
  }
  componentDidMount() {
    if (!this.state.returnedItems.length) {
      const order = this.state.order || {};
      const itemId = Number.parseInt(this.props.location.query.itemId, 10);
      Madalytics.sendEvent('ScreenLoad', {
        'event_type': 'My Orders - Return-page',
        'entity_type': 'order',
        'entity_id': itemId,
        'entity_order_id': at(order, 'storeOrderId')
      }, null, 'react');
    }
  }
  sendMadalyticsEvent() {
    const order = this.state.order || {};
    Madalytics.sendEvent('widgetClick', {
      'event_type': 'My Orders - No-Return-call',
      'entity_type': 'order',
      'entity_id': Number.parseInt(this.props.location.query.itemId, 10),
      'entity_order_id': at(order, 'storeOrderId')
    }, null, 'react');
  }
  renderConfirmationComponent(returnedItems, nonReturnedItems, returnId, slotInfo) {
    this.setState({
      returnedItems,
      nonReturnedItems,
      returnId,
      slotInfo
    }, () => window.scrollTo(0, 0));
  }

  renderErrorComponent = () => {
    this.setState({
      pageError: true
    });
  }

  render() {
    let page = null;
    const order = this.state.order || {};
    const items = at(order, 'items') || [];
    const { isApp, isAndroid } = this;

    // Categorising the items, array to handle the free Gift Case.
    const returnableItems = [];
    const nonReturnableItems = [];
    const locationQuery = this.props.location.query;
    const itemId = Number.parseInt(locationQuery.itemId || locationQuery.itemid, 10);
    const item = items.find((eachItem) => eachItem.id === itemId);
    const freeItem = items.find((eachItem) => (at(eachItem, 'flags.free.isFree') && at(eachItem, 'flags.free.associatedItem') === itemId));
    if (at(item, 'flags.returnable.isReturnable')) {
      returnableItems.push(item);
      /** Special case of free Items, pushing the free item associated with the selected item
        * into the returnable array.
      */

      if (freeItem) {
        const freeItemStatus = at(freeItem, 'status.code');
        if ((freeItemStatus === 'D' || freeItemStatus === 'C') &&
          at(freeItem, 'flags.returnable.isReturnable')) {
          returnableItems.push(freeItem);
        } else if ((freeItemStatus !== 'D' && freeItemStatus !== 'C') &&
          at(freeItem, 'flags.cancellable.isCancellable')) {
          returnableItems.push(freeItem);
        }
      }
    } else {
      nonReturnableItems.push(item);
    }

    if (this.state.loading) {
      page = <div className={PageStyles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.component}>
          <ErrorCard
            message="Something Went Wrong"
            actionName="Retry"
            action={() => { location.reload(); }} />
        </div>
      </div>);
    } else if (this.state.returnedItems.length) {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <Confirmation
            order={this.state.order}
            returnedItems={this.state.returnedItems}
            nonReturnedItems={this.state.nonReturnedItems}
            returnId={this.state.returnId}
            slotInfo={this.state.slotInfo}
            returnMode="pickup"
            emptyStateEnabled={this.emptyStateEnabled}
            isApp={isApp}
            isAndroid={isAndroid} />
        </div>
      </div>);
    } else {
      page = (<div className={PageStyles.page}>
        <Account />
        <SideBar active="my/orders" />
        <div className={PageStyles.fullWidthComponent}>
          <div onClick={this.sendMadalyticsEvent}><CCintervention type={'returning'} /></div>
          <ReturnForm
            order={this.state.order}
            returnableItems={returnableItems}
            nonReturnableItems={nonReturnableItems}
            returnReasons={this.state.returnReasons}
            returnReasonCategories={this.state.returnReasonCategories}
            refundDetails={this.state.refundDetails}
            mcRefundOffer={this.mcRefundOffer}
            renderErrorComponent={this.renderErrorComponent}
            isAndroid={isAndroid}
            ordersMyntraCreditAB={this.ordersMyntraCreditAB}
            renderConfirmationComponent={this.renderConfirmationComponent} />
        </div>
      </div>);
    }

    return page;
  }
}

Return.propTypes = {
  location: PropTypes.object
};

export default Return;
