import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import capitalize from 'lodash/capitalize';

// Style Related Imports.
import Styles from './returnConfirmedItem.css';
import classnames from 'classnames/bind';

// React Component Imports.
import ProfileSuggestor from '../../../ProfileSuggestor';
import SVGImages from '../../../Reusables/SVGImages';
import Icon from '../../../Reusables/Icon';

import { getDeviceData, getPickupSlotString } from '../../../../utils/pageHelpers';

const bindClass = classnames.bind(Styles);

const DetailsView = ({ returnId, slotInfo }) => {
  const anySlot = at(slotInfo, 'anySlot');
  const pickupSlot = at(slotInfo, 'pickupSlotSelected');
  return (
    <div>
      <div className={Styles.icon}>
        <Icon name="success_tick" className={Styles.tick} />
      </div>
      <div className={Styles.heading}> Return Request submitted </div>
      <div className={Styles.subBlock}>
        {pickupSlot && <div className={Styles.subHeading}>
          Pickup Date:
          <span> {getPickupSlotString(anySlot, pickupSlot)} </span>
        </div>}
        <div className={Styles.subHeading}> Reference number: <span>{returnId}</span></div>
        {!pickupSlot && <div className={Styles.desc}>You will receive a return request email shortly with the expected pick up date.</div>}
      </div>
    </div>);
};

const ProductView = ({ isMobile, productImageUrl, productName, item, allProfiles, size, orderId, showProfileSuggestor, itemTagged, tagItem, showUSP }) => (
  <div>
    <div className={Styles.thumbnail}>
      {itemTagged &&
        <div className={Styles.taggedTick}><SVGImages name="taggedTick" customStyle={{ height: '29px', width: '32px' }} /></div>}
      <img src={productImageUrl} />
    </div>
    <div className={Styles.info}>
      <div> {productName} </div>
      <div>
        Size: <span className={Styles.blackText}> {size.label} </span>
        / Quantity: <span className={Styles.blackText}> {at(item, 'quantity')} </span>
      </div>
    </div>
    {showUSP && isMobile &&
      <ProfileSuggestor
        allProfiles={allProfiles}
        item={item}
        orderId={orderId}
        showProfileSuggestor={showProfileSuggestor}
        tagItem={tagItem} />}
  </div>
);

class ConfirmedItem extends React.PureComponent {
  render() {
    const { item = {}, mainItem = {}, orderId, allProfiles, showProfileSuggestor, returnMode, returnId, itemTagged, tagItem, slotInfo, showUSP } = this.props;
    const productImages = at(item, 'product.images.0') || {};
    let productImageUrl = productImages.secureSrc || '';
    const { isMobile, isDesktop } = getDeviceData();

    // Product Info.
    const brandName = at(item, 'product.brand.name') || '';
    const productDisplayName = at(item, 'product.name') || '';
    let productName = productDisplayName.replace(new RegExp(`${brandName}`, 'i'), '');
    productName = productName.length > 30 ? `${productName.substr(0, 30)}...` : productName;
    const sizes = at(item, 'product.sizes') || [];
    const size = sizes.find((option) => option.skuId === item.product.skuId);
    productImageUrl = productImageUrl.replace('h_($height)', 'h_125').replace('q_($qualityPercentage)', 'q_100').replace('w_($width)', 'w_161');
    return (<div className={bindClass('confirmedItem', { desktop: isDesktop, mobile: !isDesktop })} >
      {isDesktop ? <div>
        <div className={Styles.leftSection}>
          <ProductView
            isMobile={isMobile}
            productImageUrl={productImageUrl}
            productName={productName}
            item={item}
            allProfiles={allProfiles}
            itemTagged={itemTagged}
            tagItem={tagItem}
            size={size}
            orderId={orderId}
            showUSP={showUSP}
            showProfileSuggestor={showProfileSuggestor} />
        </div>
        <div className={Styles.verticalSeparator}></div>
        <div className={Styles.rightSection}>
          {mainItem.id === item.id &&
            <DetailsView returnId={returnId} slotInfo={slotInfo} />}
          <div className={Styles.subBlock}>
            <div className={Styles.subHeading}>Mode of return : <span>{capitalize(returnMode)}</span></div>
          </div>
        </div></div> : <div>
          {mainItem.id === item.id && <DetailsView
            returnId={returnId}
            slotInfo={slotInfo} />}
          <ProductView
            isMobile={isMobile}
            productImageUrl={productImageUrl}
            productName={productName}
            item={item}
            itemTagged={itemTagged}
            tagItem={tagItem}
            allProfiles={allProfiles}
            size={size}
            orderId={orderId}
            showUSP={showUSP}
            showProfileSuggestor={showProfileSuggestor} />
        </div>}
    </div>);
  }
}

ConfirmedItem.propTypes = {
  item: PropTypes.object,
  mainItem: PropTypes.object,
  allProfiles: PropTypes.array,
  orderId: PropTypes.string,
  showProfileSuggestor: PropTypes.bool,
  returnMode: PropTypes.string,
  returnId: PropTypes.string,
  itemTagged: PropTypes.bool,
  tagItem: PropTypes.func,
  slotInfo: PropTypes.object
};

DetailsView.propTypes = {
  returnId: PropTypes.string,
  slotInfo: PropTypes.object
};

ProductView.propTypes = {
  isMobile: PropTypes.bool,
  productImageUrl: PropTypes.string,
  productName: PropTypes.string,
  item: PropTypes.object,
  allProfiles: PropTypes.array,
  size: PropTypes.object,
  orderId: PropTypes.string,
  showProfileSuggestor: PropTypes.bool,
  itemTagged: PropTypes.bool,
  tagItem: PropTypes.func,
  showUSP: PropTypes.bool
};

export default ConfirmedItem;
