import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './returnConfirmation.css';

// React Component Imports.
import ItemViews from './../../Reusables/ItemViews';
import ConfirmedItem from './ConfirmedItem';
import NextSteps from './../../Reusables/NextSteps';
import SuccessAnimation from '../../Reusables/SuccessAnimation';
import EmptyState from '../../EmptyState';

// Functional Imports.
import { getProfileForProduct, getProfilesForUser } from '../../../utils/services/userProfileService';
import features from '../../../utils/features';
import { getDeviceData, getUidx } from '../../../utils/pageHelpers';
const uspFG = features('userSizeProfilesEnabled') === 'true';

class Confirmation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showProfileSuggestor: false,
      itemTagged: false
    };

    this.tagItem = this.tagItem.bind(this);
  }

  componentWillMount() {
    const { returnedItems, order = {} } = this.props;
    const item = returnedItems.length ? returnedItems[0] : returnedItems;
    const isProfileSuggestionEnabled = this.isProfileSuggestionEnabled(item);
    if (isProfileSuggestionEnabled) {
      const payload = {
        uidx: getUidx(),
        skuId: at(item, 'product.skuId') || '',
        styleId: at(item, 'product.id') || '',
        orderId: at(order, 'storeOrderId') || at(order, 'id')
      };
      getProfileForProduct(payload, (err, res) => {
        if (err) {
          console.error('Error [User Profiles]: getProfileForProduct', err);
        } else {
          try {
            const profileResponse = (JSON.parse(at(res, 'body.text') || {}).data || [])[0] || {};
            if (at(profileResponse, 'pidx')) {
              this.setState({
                showProfileSuggestor: false
              });
            } else {
              getProfilesForUser({ userId: getUidx() }, (err1, res1) => {
                const profileData = (JSON.parse(at(res1, 'body.text') || {}).data || [])[0];
                this.profiles = at(profileData, 'profileList');
                this.setState({
                  showProfileSuggestor: true
                });
              });
            }
          } catch (e) {
            console.error('Error in parsing response: ', e);
          }
        }
      });
    }
  }

  isProfileSuggestionEnabled(item) {
    const profileEnabledAgeGroup = ['Adults-Men', 'Adults-Women', 'Adults-Unisex'];
    const profileEnabledMasterCategories = ['Apparel', 'Footwear'];
    const product = at(item, 'product') || {};
    const isMobile = getDeviceData().isMobile;

    const ageGroup = at(product, 'ageGroup');
    const masterCategory = at(product, 'masterCategory');

    const profileSuggestionsEnabled = isMobile && uspFG &&
      profileEnabledAgeGroup.indexOf(ageGroup) > -1 &&
      profileEnabledMasterCategories.indexOf(masterCategory) > -1;

    return profileSuggestionsEnabled;
  }

  tagItem() {
    this.setState({
      itemTagged: true
    });
  }

  renderConfirmedItems(items, returnMode) {
    return (items.map((item, key) => (
      <ConfirmedItem
        key={key}
        mainItem={items[0]}
        returnMode={returnMode}
        returnId={this.props.returnId}
        slotInfo={this.props.slotInfo}
        item={item}
        itemTagged={this.state.itemTagged}
        tagItem={this.tagItem}
        orderId={at(this, 'props.order.storeOrderId')}
        showProfileSuggestor={this.state.showProfileSuggestor}
        allProfiles={this.profiles}
        showUSP={uspFG} />)
    ));
  }

  render() {
    const { returnedItems, nonReturnedItems, returnMode, emptyStateEnabled, isApp, isAndroid } = this.props;

    return (<div className={Styles.returnConfirmation}>
      <SuccessAnimation isApp={isApp} isAndroid={isAndroid} />
      {returnedItems.length > 0 &&
        <div className={`${Styles.itemsCard} ${Styles.noMargin}`}>
          <div className={Styles.itemHolder} >
            {this.renderConfirmedItems(returnedItems, returnMode)}
          </div>
          <div className={Styles.information}>
            <span className={Styles.bold}> Please note: </span>
            <span> This does not affect other items in the order </span>
          </div>
          <NextSteps type={returnMode} />
        </div>}
      {nonReturnedItems.length > 0 &&
        <div className={Styles.itemsCard}>
          <div className={Styles.heading}> Following items could not be returned: </div>
          {nonReturnedItems.map((item, key) => <ItemViews key={key} view="confirmed" item={item} />)}
        </div>}
      {(!returnedItems.length || !emptyStateEnabled) && <div className={Styles.button} onClick={() => { window.location = '/my/orders'; }}> Done </div>}
      {returnedItems.length > 0 && emptyStateEnabled && <EmptyState product={returnedItems[0].product} />}
    </div>);
  }
}

Confirmation.propTypes = {
  order: PropTypes.object,
  returnedItems: PropTypes.array,
  nonReturnedItems: PropTypes.array,
  returnMode: PropTypes.string,
  returnId: PropTypes.string,
  slotInfo: PropTypes.object,
  isAndroid: PropTypes.bool,
  emptyStateEnabled: PropTypes.bool,
  isApp: PropTypes.bool
};

export default Confirmation;
