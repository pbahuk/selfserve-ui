import React from 'react';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './pickupSlotModal.css';
import Icon from '../../Reusables/Icon';
import Slot from './Slot';


class PickupSlotModal extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentSlot: {}
    };
  }

  componentWillMount() {
    document.body.style.overflow = 'hidden';
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto';
  }

  onSlotSelection = (slot, isAnySlot, ev) => {
    ev.preventDefault();
    this.props.selectSlot(slot, isAnySlot);
  }

  closeModal = () => {
    this.props.close();
  }

  render() {
    const { slots, selectedSlot, anySlot } = this.props;
    return (
      <div className={Styles.pickupSlotModal}>
        <div className={Styles.shimmer} onClick={this.closeModal} />
        <div className={Styles.modal}>
          <div className={Styles.modalHeading}>
            Select a Slot for Pickup
            <Icon name="clear" className={Styles.modalClose} onClick={this.closeModal} />
          </div>
          <div className={Styles.modalContent}>
          {
            slots.map((slot, index) =>
            (<Slot
              key={index}
              daySlot={slot}
              selectedSlot={selectedSlot}
              anySlot={anySlot}
              onSlotSelection={(sSlot, isAnySlot, ev) => this.onSlotSelection(sSlot, isAnySlot, ev)} />))
          }
          </div>
        </div>
      </div>
    );
  }
}

PickupSlotModal.propTypes = {
  close: PropTypes.func,
  slots: PropTypes.array,
  selectedSlot: PropTypes.object,
  selectSlot: PropTypes.func,
  anySlot: PropTypes.bool
};

export default PickupSlotModal;
