import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import some from 'lodash/some';

// Style Related Imports.
import Styles from './../pickupSlotModal.css';
import Icon from './../../../Reusables/Icon';


class Slot extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentSlot: {}
    };
  }

  setScrollRef(ref) {
    if (ref) {
      ref.scrollIntoView();
    }
  }

  render() {
    const { daySlot, selectedSlot, onSlotSelection, anySlot } = this.props;
    const selected = [];
    let anySlotSelected = false;
    let slotsLen = 0;
    if (daySlot.slots && Array.isArray(daySlot.slots)) {
      slotsLen = daySlot.slots.lenght;
    }
    return (
      <div className={Styles.slotWrapper} >
        <div
          ref={(ref) => (some(selected, Boolean) || anySlotSelected) && this.setScrollRef(ref)}
          className={`${Styles.slotDay} ${!daySlot.isActive && Styles.noSlot}`}>
          {
            moment(daySlot.date).calendar(null, {
              lastWeek: 'ddd, D MMM',
              lastDay: 'ddd, D MMM',
              sameDay: 'ddd, D MMM',
              nextDay: '[Tomorrow]',
              nextWeek: 'ddd, D MMM',
              sameElse: 'ddd, D MMM'
            })
          }
          {
            !daySlot.isActive && <div className={Styles.noSlotInfo}>
            No slots available
            </div>
          }
        </div>
        {
          daySlot.isActive && slotsLen > 1 && ((slot, aSlot) => {
            anySlotSelected = aSlot && selectedSlot.scheduleStart === slot.scheduleStart && selectedSlot.scheduleEnd === slot.scheduleEnd;
            return (
              <div
                className={`${Styles.slotEntry} ${anySlotSelected && Styles.selected} ${!slot.isActive && !anySlotSelected && Styles.inActive} `}
                onClick={(ev) => !anySlotSelected && slot.isActive && onSlotSelection(slot, true, ev)}>
                <span className={Styles.dot} />Any Slot
                {
                anySlotSelected && <Icon name="tick" className={Styles.tick} />
                }
              </div>
              );
          })(daySlot.defaultSlot, anySlot)
        }

        {
          daySlot.isActive && daySlot.slots.map((slot, key) => {
            selected[key] = !anySlot && selectedSlot.scheduleStart === slot.scheduleStart && selectedSlot.scheduleEnd === slot.scheduleEnd;
            return (
              <div
                className={`${Styles.slotEntry} ${selected[key] && Styles.selected} ${!slot.isActive && !selected[key] && Styles.inActive} `}
                key={key}
                onClick={(ev) => !selected[key] && slot.isActive && onSlotSelection(slot, false, ev)}>
                <span className={Styles.dot} />{`${moment(slot.scheduleStart).format('LT')}–${moment(slot.scheduleEnd).format('LT')}`}
                {
                selected[key] && <Icon name="tick" className={Styles.tick} />
                }
              </div>
              );
          })
        }
      </div>
      );
  }
}

Slot.propTypes = {
  daySlot: PropTypes.object,
  selectedSlot: PropTypes.object,
  onSlotSelection: PropTypes.func,
  anySlot: PropTypes.bool
};

export default Slot;
