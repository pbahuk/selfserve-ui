import React from 'react';
import at from 'v-at';
import some from 'lodash/some';
import get from 'lodash/get';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './returnForm.css';

// Specific to this page.
import OrderInfo from '../../Reusables/OrderInfo';
import ReturnableItems from '../ReturnableItems';
import NonReturnableItems from '../NonReturnableItems';
import ReturnReasons from '../ReturnReasons';
import RefundOptions from '../RefundOptions';
import Address from '../Address';
import UserConfirmation from '../../Reusables/UserConfirmation';
import MobileToast from '../../Reusables/MobileToast';
import { createReturn } from '../../../utils/services/returns';
import { cancelItems } from '../../../utils/services/oms';
import { gaEmitter, getUidx, getDeviceData, getUserEmail } from '../../../utils/pageHelpers';

class ReturnForm extends React.Component {
  constructor(props) {
    super(props);

    // Functional Binds.
    this.selectRadioOption = this.selectRadioOption.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleUserConfirm = this.handleUserConfirm.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.closeExchangeOptionCard = this.closeExchangeOptionCard.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.updateReturnData = this.updateReturnData.bind(this);
    this.updateAddress = this.updateAddress.bind(this);
    this.validateForm = this.validateForm.bind(this);

    this.state = {
      showExchangeOption: false,
      formInactive: false,
      actionError: '',
      confirmEnabled: false,
      serviceType: '',
      accountId: '',
      address: at(props, 'order.address'),
      step: 'REASONS', // REASONS, ADDRESS
      proceedEnabled: false,
      form: {
        reasonCategory: {
          value: '',
          id: '',
          displayName: '',
          error: '',
          required: true
        },
        reason: {
          value: '',
          id: '',
          displayName: '',
          error: '',
          required: true
        },
        refundMode: {
          value: '',
          id: '',
          displayName: '',
          error: '',
          required: true
        },
        comment: {
          value: '',
          placeHolder: 'Additional Comments',
          validation: /^[a-zA-Z0-9!@#$%\^&* )(+=.,_-]*$/,
          error: '',
          required: false
        },
        userConfirmation: {
          value: false,
          error: '',
          required: true
        }
      }
    };
  }

  getExchangeOptionForSize() {
    // First item will be the original item.
    const item = this.props.returnableItems[0] || {};
    const isExchangeableProduct = at(item, 'flags.exchangeable.isExchangeable') || false;
    const sizes = at(item, 'product.sizes') || [];
    const isAnySizeAvailable = sizes.find((size) => size.available);
    const pickUpEnabled = at(item, 'product.attributes.pickupEnabled') || false;

    return isExchangeableProduct && isAnySizeAvailable && pickUpEnabled;
  }

  closeExchangeOptionCard() {
    this.setState({
      showExchangeOption: false
    });
  }

  selectRadioOption(target) {
    const { form } = this.state;
    let { showExchangeOption } = this.state;

    if (target.name === 'reasonCategory') {
      form.reason = { ...form.reason, value: '', id: '', error: '' };
      if (showExchangeOption) {
        showExchangeOption = false;
      }
    } else {
      if (target.id === 'STS' || target.id === 'STL') {
        showExchangeOption = this.getExchangeOptionForSize();
      } else {
        showExchangeOption = false;
      }
    }

    form[target.name] = { ...form[target.name], value: target.value, id: target.id, error: '' };

    if ((at(form, 'reasonCategory') || {}).id === 'RNL') {
      form.reason.id = 'RNL';
      form.reason.value = form.reasonCategory.value;
    }

    this.setState({
      showExchangeOption,
      form
    }, this.validateForm);
  }

  handleChange(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], value: target.value, error: '' };

    this.setState({
      form
    }, this.validateForm);
  }

  handleBlur(target) {
    const form = this.state.form;

    if (!target.value.match(form[target.name].validation)) {
      form[target.name] = { ...form[target.name], value: target.value, error: 'Invalid Input' };
    } else {
      form[target.name] = { ...form[target.name], value: target.value, error: '' };
    }

    this.setState({
      form
    }, this.validateForm);
  }

  updateReturnData(data) {
    this.setState(data);
  }

  validateForm() {
    const { state: { form } } = this;
    const check = ['reasonCategory', 'reason'];
    this.setState({
      confirmEnabled: !some(form, (val) => (val.required && !val.value)),
      proceedEnabled: !some(form, (val, name) => (check.indexOf(name) > -1 && val.required && !val.value))
    });
  }

  constructPayload(item) {
    const { state, props: { order = {} } } = this;
    const { form, address, pickupSlot } = state;
    const lineEntries = [{
      comment: form.comment.value || 'No comments',
      orderId: order.id,
      orderLineId: `${at(item, 'id')}`,
      orderReleaseId: `${at(item, 'orderReleaseId')}`,
      quantity: parseInt(at(item, 'quantity'), 10),
      supplyType: at(item, 'supplyType'),
      returnReasonId: parseInt(form.reason.value || form.reasonCategory.value, 10),
      skuId: at(item, 'product.skuId'),
      styleId: at(item, 'product.id')
    }];

    const payload = {
      comment: 'Return created from MyMyntra',
      orderId: order.id,
      storeOrderId: order.storeOrderId,
      returnType: 'NORMAL',
      returnMode: state.serviceType,
      customerName: address.user.name,
      email: getUserEmail() || address.user.email,
      mobile: address.user.mobile,
      returnAddressDetailsEntry: {
        addressId: address.id,
        address: address.streetAddress,
        city: address.city,
        state: address.state.code,
        country: address.country.code,
        zipcode: address.pincode
      },
      returnLineEntries: lineEntries,
      returnRefundDetailsEntry: {
        refundMode: form.refundMode.value,
        refundAccountId: form.refundMode.value === 'NEFT' ? `${state.accountId}` : ''
      },
      returnAdditionalDetailsEntry: {}
    };

    if (pickupSlot && pickupSlot.pickupSlotSelected) {
      payload.returnAdditionalDetailsEntry = Object.assign(pickupSlot, payload.returnAdditionalDetailsEntry);
    }
    return payload;
  }

  handleProceed = () => {
    this.setState({
      step: 'ADDRESS'
    }, () => window.scrollTo(0, 0));
  }

  handleFormSubmit() {
    const { state, props: { returnableItems, order = {} } } = this;

    const renderConfirmation = (res, item) => {
      const { form } = this.state;
      const refundMode = form.refundMode.value;

      const returnResponse = at(res, 'body') || {};
      const slotInfo = get(returnResponse, 'returnAdditionalDetailsEntry') || {};
      if (slotInfo.pickupSlotSelected && !get(slotInfo, 'pickupSlotSelected.isActive', false)) {
        slotInfo.pickupSlotSelected = null;
      }
      this.props.renderConfirmationComponent(returnableItems, [], at(returnResponse, 'id'), slotInfo);

      // GA Event.
      gaEmitter('profile-returns', 'return-submitted', '', {
        'return-reason': form.reason.value,
        'return-mode': refundMode,
        'amount-refund-mode': refundMode === 'NEFT' ? 'bankaccount' : refundMode,
        'refund-amount': at(res, 'body.returnRefundDetailsEntry.refundAmount')
      });

      gaEmitter(`/my/returns/success/orderid=${order.id}/
        StyleId=${at(item, 'product.id')}`, 'Screen Load');
    };

    const renderError = (err) => {
      const that = this;
      this.setState({
        formInactive: false,
        confirmEnabled: true,
        actionError: get(err, 'response.body.error', 'Something went wrong'),
        formError: get(err, 'response.body.error', 'Something went wrong')
      }, () => {
        setTimeout(() => {
          that.setState({ actionError: '' });
        }, 5000);
      });

      gaEmitter('Profile -my/returns', 'Return-submitted', '', {
        failureReason: get(err, 'response.body.error', 'Something went wrong')
      });
    };

    const createCancelRequest = (freeItem, mainItem, callback) => {
      const payload = {
        _storeOrderId: order.storeOrderId,
        releaseUpdateEntry: {
          orderId: order.id,
          releaseId: freeItem.orderReleaseId,
          cancellationReasonId: '1',
          userId: getUidx(),
          comment: 'Automated Cancellation for free item while creating return of main product',
          cancellationType: 'CCR',
          linesToBeCancelled: [{
            id: freeItem.id,
            quantity: mainItem.quantity
          }]
        }
      };

      cancelItems(payload, callback);
    };

    if (at(state, 'form.refundMode.value') === 'NEFT' && !state.accountId) {
      const that = this;
      this.setState({
        actionError: 'No refund account id found',
        formError: 'No refund account id found'
      }, () => {
        if (!that.toastTimer) {
          that.toastTimer = setTimeout(() => {
            that.setState({ actionError: '' });
            that.toastTimer = null;
          }, 5000);
        }
      });
    } else if (state.confirmEnabled) {
      this.setState({
        formInactive: true,
        confirmEnabled: false
      });
      const mainItem = returnableItems[0];
      createReturn(this.constructPayload(mainItem), (err1, res1) => {
        if (!err1) {
          const freeItem = returnableItems.find(item => at(item, 'flags.free.isFree'));
          if (freeItem) {
            const freeItemStatus = at(freeItem, 'status.code');
            if (freeItemStatus !== 'D' && freeItemStatus !== 'C') {
              createCancelRequest(freeItem, mainItem, (err2) => {
                if (err2) {
                  console.log('cancel error free: ', err2);
                  this.props.returnableItems = [this.props.returnableItems[0]];
                }
                renderConfirmation(res1, mainItem);
              });
            } else {
              createReturn(this.constructPayload(freeItem), (err3) => {
                if (err3) {
                  console.log('return error free: ', err3);
                  this.props.returnableItems = [this.props.returnableItems[0]];
                }
                gaEmitter('Profile -my/returns', `Return-submitted-freebie-${err3 ? 'failure' : 'success'}`,
                  at(freeItem, 'id'));
                renderConfirmation(res1, mainItem);
              });
            }
          } else {
            renderConfirmation(res1, mainItem);
          }
        } else {
          renderError(err1);
        }
      });
    }
  }

  handleUserConfirm(target) {
    const form = this.state.form;
    form[target.name] = { ...form[target.name], value: target.checked, error: '' };
    this.setState({
      form
    }, this.validateForm);
  }

  updateAddress(address) {
    this.setState({
      address
    });
  }

  render() {
    const {
      order = {},
      returnableItems = [],
      nonReturnableItems = [],
      returnReasons = [],
      returnReasonCategories = {},
      isAndroid,
      ordersMyntraCreditAB
    } = this.props;
    const { form = {}, confirmEnabled, proceedEnabled, actionError, address, step, formError } = this.state;
    const item = returnableItems[0] || {};
    const { userAgent } = getDeviceData();

    return (<div className={Styles.returnForm}>
          {
            step === 'REASONS' && <div>
              <OrderInfo order={order} />
              {returnableItems.length > 0 && <ReturnableItems items={returnableItems} />}
              {nonReturnableItems.length > 0 && <NonReturnableItems items={nonReturnableItems} />}
              {returnableItems.length === 0 &&
                <a href="/my/orders" className={Styles.orderRedirectButton}> Go to My Orders </a>}

              {returnableItems.length > 0 && <div>
                <ReturnReasons
                  form={form}
                  returnReasons={returnReasons}
                  returnReasonCategories={returnReasonCategories}
                  showExchangeOption={this.state.showExchangeOption}
                  closeExchangeOptionCard={this.closeExchangeOptionCard}
                  order={this.props.order}
                  address={address}
                  item={item}
                  updateReturnData={this.updateReturnData}
                  updateAddress={this.updateAddress}
                  selectRadioOption={this.selectRadioOption}
                  handleChange={this.handleChange}
                  handleBlur={this.handleBlur} />
                <div className={Styles.actionError}> {actionError} </div>
                {actionError && <MobileToast text={actionError} error />}
                <div className={`${Styles.buttons} ${userAgent.indexOf('MyntraRetailAndroid') !== -1 ? Styles.android : ''}`}>
                  <div className={Styles.button}>
                    <a href="/my/orders" > Cancel </a>
                  </div>
                  <div
                    className={`${Styles.button} ${!proceedEnabled ? Styles.disabled : ''}`}
                    onClick={proceedEnabled && this.handleProceed}> Proceed
                    <div className={this.state.formInactive ? Styles.buttonLoader : ''} />
                  </div>
                </div>
              </div>
              }
            </div>
          }

          {
            step === 'ADDRESS' && <div>
              <Address
                order={this.props.order}
                address={address}
                item={item}
                updateAddress={this.updateAddress}
                updateReturnData={this.updateReturnData}
                renderErrorComponent={() => this.props.renderErrorComponent()} />

              <RefundOptions
                refundDetails={this.props.refundDetails}
                mcRefundOffer={this.props.mcRefundOffer}
                updateReturnData={this.updateReturnData}
                ordersMyntraCreditAB={ordersMyntraCreditAB}
                item={item}
                onSelect={this.handleChange} />
              <div
                className={Styles.iphoneBottomMargin}>
              </div>
              <UserConfirmation onSelect={this.handleUserConfirm} error={!!actionError} />
              <div className={Styles.actionError}> {formError} </div>
              {actionError && <MobileToast text={actionError} error />}
              <div className={`${Styles.buttons} ${Styles.confirm} ${isAndroid ? Styles.android : ''}`}>
                <div className={Styles.button}>
                  <a href="/my/orders" > Cancel </a>
                </div>
                <div
                  className={`${Styles.button} ${!confirmEnabled ? Styles.disabled : ''}`}
                  onClick={confirmEnabled && this.handleFormSubmit}> Confirm
                  <div className={this.state.formInactive ? Styles.buttonLoader : ''} />
                </div>
              </div>
            </div>
          }
    </div>);
  }
}

ReturnForm.propTypes = {
  order: PropTypes.object,
  returnableItems: PropTypes.array,
  nonReturnableItems: PropTypes.array,
  returnReasons: PropTypes.array,
  returnReasonCategories: PropTypes.array,
  refundDetails: PropTypes.object,
  mcRefundOffer: PropTypes.bool,
  renderConfirmationComponent: PropTypes.func,
  renderErrorComponent: PropTypes.func,
  isAndroid: PropTypes.bool,
  ordersMyntraCreditAB: PropTypes.bool
};

export default ReturnForm;
