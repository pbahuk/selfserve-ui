import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';

// Style Related Imports.
import Styles from './nonReturnableItems.css';

// React Component Imports.
import ItemViews from '../../Reusables/ItemViews';

const NonReturnableItems = (props) =>
  (<div className={Styles.nonReturnableItems}>
    {props.items.map((item) =>
      <ItemViews
        key={item.id}
        view="nonActionable"
        item={item}
        reason={at(item, 'flags.returnable.remark')} />)}
  </div>);

NonReturnableItems.propTypes = {
  items: PropTypes.array
};

export default NonReturnableItems;
