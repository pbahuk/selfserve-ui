import React from 'react';
import at from 'v-at';
import PropTypes from 'prop-types';
import Madalytics from '../../utils/trackMadalytics';
import { getUidx, getImageUrl } from '../../utils/pageHelpers';
import { getCrossSellDetails } from '../../utils/services/crossSell';
import MiniPDP from '../Reusables/MiniPDP';
import debounce from 'lodash/debounce';
import Styles from './crossSell.css';
import ScrollToTop from '../Reusables/ScrollToTop';
import { getWishtListForUser, addToWishListOfUser, removeFromWishListOfUser } from '../../utils/services/wishList';
import Toast from '../Reusables/Toast';
import uniq from 'lodash/uniq';
import articleMapping from '../../resources/data/articleMapping';
import Icon from '../Reusables/Icon';
import { crossSellTxtList } from '../../resources/data/constants';

const CrossSellFilter = ({ filterTags, onFilterClick, activeFilter }) => (
  <div className={Styles.crossSellFilter}>
    <div className={Styles.title}>You'll want these too!</div>
    <div className={Styles.filterTags}>
      {
        filterTags.map((tag, i) => <div
          className={tag === activeFilter ? Styles.activeFilter : Styles.inactiveFilter}
          key={`filter-${i + 1}`}
          onClick={() => onFilterClick(tag, i)}>
          {articleMapping[tag] || tag}
        </div>)
      }
    </div>
  </div>
);

CrossSellFilter.propTypes = {
  filterTags: PropTypes.array,
  onFilterClick: PropTypes.func,
  activeFilter: PropTypes.string
};

const triggerMoreCrossSellEvent = (title, styleId, article, orderId, itemId) => {
  Madalytics.sendEvent(
    'widgetClick',
    {
      type: 'More_cross-sell_click',
      data_set: { entity_type: 'product', entity_name: title, entity_id: styleId }
    },
    {
      payload: {
        widget: { name: 'Cross-sell', type: 'list' },
        widget_items: { name: `More_${article}`, type: 'button' }
      },
      custom: {
        v1: article,
        v2: orderId,
        v3: itemId
      }
    }
  );
  return true; // Added so that anchor tags goes to the href set
};

const MoreItems = ({ searchLink, category, categoryImg, styleId, title, orderId, itemId, randNo }) => (<div className={Styles.moreItems}>
  <a href={searchLink} className={Styles.categoryLink} onClick={() => triggerMoreCrossSellEvent(title, styleId, category, orderId, itemId)}>
    <div className={Styles.category} style={{ backgroundImage: `url(${categoryImg})` }}>
    </div>
    <div className={Styles.categoryDetails}>
      <div className={Styles.categoryName}>{`More ${category}`}</div>
      <div className={Styles.categoryDesc}>{crossSellTxtList[randNo]}</div>
    </div>
    <div className={Styles.rightArrow}>
      <Icon name="chevron_right" customStyle={{ fontSize: '26px', color: '#94989f', fontWeight: 'bold' }} />
    </div>
  </a>
</div>);

MoreItems.propTypes = {
  searchLink: PropTypes.string,
  category: PropTypes.string,
  categoryImg: PropTypes.string,
  styleId: PropTypes.number,
  title: PropTypes.string,
  orderId: PropTypes.string,
  itemId: PropTypes.number,
  randNo: PropTypes.number
};

const ALL = 'All';

const getFilterTags = (products) => uniq(products.map(product => product.category));

class CrossSell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingProducts: true,
      loadingWishList: true,
      showScrollToTop: false,
      wishListStyleIDs: [],
      products: [],
      showToast: false,
      width: 0,
      height: 0,
      activeFilter: ALL,
      filterTags: [],
      filteredProducts: []
    };
    [
      'fetchAllProducts',
      'updateWindowDimensions',
      'onFilterClick',
      'triggerFilterLoad',
      'updateWishList',
      'sendMadalyticsEvent'
    ].forEach(method => (this[method] = this[method].bind(this)));
    this.timer = null;
    this.updateWindowDimensions = debounce(this.updateWindowDimensions, 500);
  }

  componentWillMount() {
    this.getWishListStyleIds();
  }

  componentDidMount() {
    this.updateWindowDimensions();
    this.fetchAllProducts();
    Madalytics.sendEvent(
      'widgetLoad',
      {
        type: 'Cross-sell Widget Loaded',
        data_set: {
          userId: getUidx(),
          payload: { screen: { name: 'My_Orders_Item_details', data_set: { entity_id: this.props.product.id } } }
        }
      },
      { payload: {
        widget: { name: 'Cross-sell', type: 'list' } }
      }
    );
  }

  onFilterClick(category, index) {
    const { products, height, width } = this.state;
    const filteredProducts = products.filter(product => product.category === category);
    const { product, orderId, itemId } = this.props;
    let categoryImg = at(filteredProducts[0], 'defaultImage.secureSrc') || '';
    categoryImg = getImageUrl(categoryImg, parseInt(height / 5, 10), parseInt(width / 5, 10));
    Madalytics.sendEvent(
      'widgetClick',
      {
        type: 'Style filter click',
        data_set: {
          payload: { screen: { name: 'My_Orders_Item_details', data_set: { entity_id: product.id } } }
        }
      },
      {
        payload: {
          widget: { name: 'style_filter', type: 'carousel' },
          widget_items: { name: 'style_filter_capsule', type: 'button', h_position: index }
        },
        custom: {
          v1: orderId,
          v2: category,
          v3: itemId
        }
      }
    );
    this.setState({
      activeFilter: category,
      filteredProducts: category === ALL ? this.state.products : filteredProducts,
      categoryImg
    });
  }

  getWishListStyleIds() {
    getWishtListForUser((err, res) => {
      if (err) {
        console.log(err);
      } else {
        this.setState({
          wishListStyleIDs: at(res, 'body.data.styleIds') || [],
          loadingWishList: false
        });
      }
    });
  }

  updateWindowDimensions() {
    let width;
    if (window.innerWidth < 500) {
      width = Math.floor((window.innerWidth - 2) / 2);
    } else if (window.innerWidth < 780) {
      width = Math.floor((window.innerWidth - 2) / 3);
    } else {
      width = Math.floor(((Math.min(window.innerWidth, 980) * 0.71) - 2) / 3);
    }
    this.setState({
      width,
      height: Math.floor(width * (4 / 3))
    });
  }

  triggerFilterLoad() {
    const tagsCount = this.state.filterTags.length;
    const { orderId, itemId, product } = this.props;
    Madalytics.sendEvent(
      'widgetLoad',
      {
        type: 'Style filter load',
        data_set: {
          userId: getUidx(),
          payload: { screen: { name: 'My_Orders_Item_details', data_set: { entity_id: product.id } } }
        }
      },
      {
        payload: {
          widget: { name: 'style_filter', type: 'carousel' }
        },
        custom: {
          v1: orderId,
          v2: tagsCount,
          v3: itemId
        }
      }
    );
  }


  fetchAllProducts() {
    getCrossSellDetails(this.props.product.id, (err, res) => {
      if (err || at(res, 'body.status.statusType') === 'ERROR') {
        console.error('[ERROR] Cross Sell Fetching all cross sell products', err);
      } else {
        const products = at(res, 'body.related.0.products');
        const { showFilter } = this.props;
        const filterTags = showFilter ? [ALL, ...getFilterTags(products)] : [];
        this.setState({
          loadingProducts: false,
          products,
          filteredProducts: products,
          filterTags
        }, () => showFilter && this.triggerFilterLoad());
      }
    });
  }

  handleToast = ({ success, text, boldText }) => {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.setState({
      toastText: text,
      toastSuccess: success,
      toastBoldText: boldText,
      showToast: true
    });
    this.timer = setTimeout(() => this.setState({ showToast: false }), 3000);
  }

  sendMadalyticsEvent(eventType, styleId) {
    Madalytics.sendEvent(
      'widgetClick',
      {
        type: eventType,
        data_set: { entity_type: 'order', entity_id: styleId }
      },
      { widget: { name: 'Cross-sell', type: 'list' } }
    );
  }

  updateWishList(styleId) {
    const { wishListStyleIDs } = this.state;
    const wishlistIndex = wishListStyleIDs.indexOf(styleId);
    if (wishlistIndex > -1) {
      this.sendMadalyticsEvent('RemoveFromCollection', styleId);
      removeFromWishListOfUser(styleId, (err) => {
        if (err) {
          console.log(err);
        } else {
          wishListStyleIDs.splice(wishlistIndex, 1);
          this.setState({
            wishListStyleIDs
          });
          this.handleToast({ success: true, text: 'Removed from ', boldText: 'Wishlist!' });
        }
      });
    } else {
      this.sendMadalyticsEvent('AddToCollection', styleId);
      addToWishListOfUser(styleId, (err, res) => {
        if (err) {
          console.log(err);
          const errorDetail = at(res, 'body.response.meta.errorDetail') || 'Please try again later!';
          if (errorDetail) {
            this.handleToast({ success: false, text: errorDetail, boldText: '.' });
          }
        } else {
          wishListStyleIDs.push(styleId);
          this.setState({
            wishListStyleIDs
          });
          this.handleToast({ success: true, text: 'Added to ', boldText: 'Wishlist!' });
        }
      });
    }
  }

  renderItems() {
    const { filteredProducts, products } = this.state;
    const { showFilter } = this.props;
    const list = showFilter ? filteredProducts : products;
    return list.map((item, index) => (<li className={Styles.halfList} key={`${item.id}`}>
      <div className={Styles.itemContainer}>
        <MiniPDP
          data={item}
          isWishListed={this.state.wishListStyleIDs.includes(item.id)}
          handleToast={this.handleToast}
          width={this.state.width}
          index={index}
          updateWishList={this.updateWishList}
          height={this.state.height}
          itemId={this.props.itemId}
          orderId={this.props.orderId} />
      </div>
    </li>));
  }

  render() {
    let crossSellComponent = null;
    const { filteredProducts, categoryImg, activeFilter } = this.state;
    const { showFilter, orderId, itemId, product: { gender, name, id } = {} } = this.props;
    const showArticleCard = showFilter && activeFilter !== ALL;
    const showGender = gender === 'Men' || gender === 'Women';
    const minLen = showFilter ? 1 : 4;
    const wrapperClass = filteredProducts.length % 2 === 0 ? Styles.evenCrosssellList : Styles.oddCrosssellList;
    const listLink = showArticleCard && `/${activeFilter}${showGender ? `?f=gender:${gender.toLowerCase()}` : ''}`;
    const randNo = Math.floor(crossSellTxtList.length * Math.random());
    if (!this.state.loadingProducts && !this.state.loadingWishList && this.state.products && this.state.products.length >= minLen && this.state.height) {
      crossSellComponent = (
        <div>
          <div>
            {showFilter ?
              <CrossSellFilter
                filterTags={this.state.filterTags}
                onFilterClick={this.onFilterClick}
                activeFilter={this.state.activeFilter} /> :
              <div className={Styles.crossSaleTitle}>You'll want these too!</div>
            }
          </div>
          <div className={showFilter ? wrapperClass : ''}><ul className={Styles.uList}>{this.renderItems()}</ul></div>
          {listLink && <MoreItems
            orderId={orderId}
            itemId={itemId}
            searchLink={listLink}
            category={filteredProducts[0].category}
            categoryImg={categoryImg}
            title={name}
            randNo={randNo}
            styleId={id} />}
          <ScrollToTop />
          {this.state.showToast ? <div><Toast text={this.state.toastText} boldText={this.state.toastBoldText} success={this.state.toastSuccess} /></div> : null}
        </div>
        );
    } else {
      crossSellComponent = (<div></div>);
    }
    return crossSellComponent;
  }
}

CrossSell.propTypes = {
  product: PropTypes.object,
  orderId: PropTypes.string,
  showFilter: PropTypes.bool,
  itemId: PropTypes.number
};

export default CrossSell;
