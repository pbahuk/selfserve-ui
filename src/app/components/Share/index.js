import React from 'react';
import Styles from './share.css';
import Error from '../Reusables/Error';
import at from 'v-at';
import { getDeviceData, gaEmitter, getGCconfigs } from './../../utils/pageHelpers';

class Share extends React.Component {
  constructor(props) {
    super(props);
    this.action = this.action.bind(this);
    gaEmitter('Say Thanks', 'Home Page');
    const queryParams = at(this, 'props.location.query') || {};
    const gcConfig = getGCconfigs('gcManagement') || {};
    let message = queryParams.share ? gcConfig.shareMessage : gcConfig.thanksMessage;
    message = message || 'Thanks for buying me a Gift Card';

    this.state = {
      loading: true,
      pageError: queryParams.gcid !== '',
      pageType: queryParams.share ? 'share' : 'thanks',
      gcid: queryParams.gcid,
      imageUrl: 'https://cdn.myntassets.com/giftcard/assets/png/GC_Banner_1.97435e7209c4c3f2b93aeb3a68566e8c.png',
      message
    };
  }

  componentWillMount() {
    const serviceResponse = at(window, '__myx.giftServiceResponse');
    let pageError = false;

    if (!serviceResponse || at(serviceResponse, 'error')) {
      pageError = true;
    } else if (at(serviceResponse, 'response') && at(serviceResponse, 'response.status.statusType') === 'ERROR') {
      pageError = true;
    }
    let imageUrl;
    if (at(serviceResponse, 'response.data') && at(serviceResponse, 'response.data.gcImage')) {
      const imageData = JSON.parse(at(serviceResponse, 'response.data.gcImage'));
      imageUrl = at(imageData, 'securedDomain') + at(imageData, 'resolutionFormula').replace('h_($height),q_($qualityPercentage),w_($width)/', '');
    }
    imageUrl = imageUrl || 'https://cdn.myntassets.com/giftcard/assets/png/GC_Banner_1.97435e7209c4c3f2b93aeb3a68566e8c.png';

    this.setState({
      pageError,
      loading: false,
      imageUrl,
      data: at(serviceResponse, 'response.data')
    });
  }

  generateSocialUrls(type) {
    const data = this.state.data || {};
    const message = this.state.message ? `${this.state.message}\n` : '';
    const emailAddress = this.state.pageType === 'share' ? data.recipientEmail : data.senderEmail;
    let url = '';

    switch (type) {
      case 'EMAIL':
        url = `mailto:${emailAddress}?subject=${'Gift Card'}&body=${encodeURIComponent(message)}${encodeURIComponent(this.state.imageUrl)}`;
        break;

      case 'FACEBOOK':
        url = `https://www.facebook.com/dialog/feed?
          app_id= ${encodeURIComponent('182424375109898')}
          &display=popup&caption=${encodeURIComponent(message)}
          &picture=${encodeURIComponent(this.state.imageUrl)}
          &redirect_uri=${encodeURIComponent('https://www.facebook.com/')}`;
        break;

      case 'TWITTER':
        url = `https://twitter.com/intent/tweet?text=${encodeURIComponent(message)}${encodeURIComponent(this.state.imageUrl)}`;
        break;

      case 'WHATSAPP':
        url = `whatsapp://send?text=${message}${this.state.imageUrl}`;
        break;

      case 'SMS':
        url = `sms://+91?body=${message}${this.state.imageUrl}`;
        break;

      default:
        url = 'www.myntra.com';
    }
    return url;
  }

  action(button) {
    window.location = this.generateSocialUrls(button.name);
  }

  generateButtons() {
    const deviceData = getDeviceData();
    let buttons;

    const deviceFeatureMapping = {
      'mSite': [
        {
          name: 'FACEBOOK',
          logo: '-302px -15px'
        },
        {
          name: 'EMAIL',
          logo: '-302px -90px'
        },
        {
          name: 'WHATSAPP',
          logo: '-301px -52px'
        },
        {
          name: 'SMS',
          logo: '-302px -126px'
        }
      ],
      'webSite': [
        {
          name: 'FACEBOOK',
          logo: '-302px -15px'
        },
        {
          name: 'EMAIL',
          logo: '-302px -90px'
        }
      ]
    };

    const features = deviceData.isDesktop ? at(deviceFeatureMapping, 'webSite') : at(deviceFeatureMapping, 'mSite');
    buttons = features.map((feature, key) => {
      const image = {
        backgroundPosition: feature.logo
      };
      return (<div className={Styles.socialButton} onClick={() => { this.action(feature); }} key={key}>
        <div style={image} className={Styles.socialImage} />
        <div className={Styles.buttonName}> {feature.name} </div>
      </div>);
    });

    return (<div className={Styles.buttonWrapper}>
      {buttons}
    </div>);
  }

  generateActions() {
    const deviceData = getDeviceData();
    const displayMessage = this.state.pageType === 'share' ? 'Share the Joy of sending the Gift Card via.' :
     'Return the favor by sending a thank you message via.';
    let actions = null;

    if (deviceData.isApp) {
      window.location = `share://dialogTitle=${'Say Thanks With'}&body=${this.state.message}${this.state.imageUrl}&subject=${this.state.message}`;
    } else {
      actions = (<div className={Styles.giftCardActions}>
        <div className={Styles.messageWrapper}>
          <div className={Styles.leftImage} />
          <div className={Styles.message}> {displayMessage} </div>
          <div className={Styles.rightImage} />
        </div>
        {this.generateButtons()}
      </div>);
    }
    return actions;
  }

  render() {
    let page = null;

    if (this.state.loading) {
      page = <div className={Styles.loader} />;
    } else if (this.state.pageError) {
      page = (<div className={Styles.page}>
        <Error
          message="Something Went Wrong"
          actionName="Retry"
          action={() => { location.reload(); }} />
      </div>);
    } else {
      page = (<div className={Styles.page}>
        <div className={Styles.component}>
          <img className={Styles.image} src={this.state.imageUrl} />
          {this.generateActions()}
        </div>
      </div>);
    }
    return page;
  }
}

export default Share;
