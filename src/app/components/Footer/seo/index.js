import React, { Component, PropTypes } from 'react';
import { isBrowser } from '../../../utils';
import at from 'v-at';
import styles from './index.css';

class Seo extends Component {

  constructor(props) {
    super(props);
    let mData = props.metaData;
    if (isBrowser()) {
      mData = at(window.__myx.searchData, 'seo.metaData');
    }
    this.state = {
      expand: true,
      metaData: mData
    };
  }

  getMarkUp(description = '') {
    if (!at(this.state, 'expand') && !this.props.desktop) {
      return { __html: `${description.substr(0, 250)}...` };
    } return { __html: description };
  }

  getReadMoreSection() {
    if (!this.props.desktop) {
      return (
        <span
          onClick={() => this.updateRead()}
          className={styles.toggleRead}> read {at(this.state, 'expand') ? 'less' : 'more'}
        </span>
      );
    } return null;
  }

  updateRead() {
    this.setState({
      expand: !this.state.expand
    });
  }

  render() {
    const meta = at(this.state, 'metaData') || {};
    if (meta.page_description) {
      return (
        <div className={styles.seoContainer}>
          <div className={styles.title}> {`buy ${meta.page_title}`} </div>
          <div
            className={styles.descContainer}
            dangerouslySetInnerHTML={this.getMarkUp(meta.page_description)} />
          {this.getReadMoreSection()}
        </div>
      );
    } return null;
  }

}

Seo.propTypes = {
  seoData: PropTypes.object,
  desktop: PropTypes.bool,
  metaData: PropTypes.object
};

export default Seo;
