import React from 'react';
import at from 'v-at';
import Desktop from './Desktop';
// import Mobile from './Mobile';
import staticData from './links.json'; // Contains static links
import { isBrowser, fetchTopNav } from '../../utils';

class Footer extends React.Component {

  constructor(props) {
    super(props);
    const browserNavigationData = typeof localStorage !== 'undefined' && typeof localStorage === 'object' && localStorage.getItem('v1navData') ?
      JSON.parse(localStorage.getItem('v1navData')) : '';
    const nData = isBrowser() ? browserNavigationData : at(this.props, 'groups');
    const footerDataCombined = [].concat.apply([], at(this.props, 'footerData'));
    this.state = {
      navData: nData,
      groups: at(nData, 'children') ?
        at(nData, 'children').map((val) => ({ linkUrl: at(val, 'props.url'), name: at(val, 'props.title') })) : staticData.navigation,
      footerData: at(this.props, 'footerData') ? footerDataCombined : at(staticData, 'links'),
      doRender: typeof nData === 'object'
    };
  }

  componentDidMount() {
    const _self = this;
    if (isBrowser() && at(window, '__myx_navigationData__')) {
      try {
        _self.setState({ navData: at(window, '__myx_navigationData__'), doRender: true });
        localStorage.setItem('v1navData', JSON.stringify(at(window, '__myx_navigationData__')));
      } catch (e) {
        console.log('Browser does not support localStorage');
      }
    } else {
      fetchTopNav('topnav', (err, navigationData) => {
        if (!err) {
          if (typeof localStorage !== 'undefined' && typeof localStorage === 'object') {
            try {
              localStorage.setItem('v1navData', JSON.stringify(navigationData));
            } catch (e) {
              console.log('Browser does not support localStorage');
            }
          }
          _self.setState({ navData: navigationData, doRender: true });
        } else {
          _self.setState({ navData: null, doRender: true });
        }
      });
    }
  }

  isApp() {
    if (isBrowser()) {
      const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
      return deviceChannel.toLowerCase() === 'mobile_app';
    } return false;
  }

  render() {
    const nData = isBrowser() ? at(this.state, 'groups') : at(this.props, 'groups');
    const footerStyle = { 'zIndex': 0, padding: '30px 0px 40px 0px', background: '#FAFBFC' };
    const isApp = isBrowser() ? this.isApp() : at(this.props, 'isApp');
    if (!isApp) {
      if (at(this.state, 'doRender')) {
        if (!at(this.props, 'isMobile')) {
          return (
            <div style={footerStyle}>
              <Desktop
                fData={at(this.state, 'footerData')}
                pageName={at(this.props, 'pageName')}
                seo={at(this.props, 'seo')}
                navData={nData}
                deviceData={at(this.props, 'deviceData')} />
            </div>
          );
        }
      } return null;
    } return null;
  }

}

export default Footer;
