import React from 'react';
import styles from './mobile.css';
import at from 'v-at';
import statics from '../links.json';
import Seo from '../seo/index.js';
import { isBrowser } from '../../../utils';
const userAgent = require('@myntra/myx/lib/userAgents');
import onlineShoppingGyan from '../onlineShoppingGyan';
import { legalCompliance } from '../Common';

const navigate = (url) => {
  if (isBrowser()) {
    window.location.href = url;
  }
};

const myntraExperience = (deviceInfo) => {
  if (isBrowser() && typeof at(deviceInfo, 'os') !== 'undefined') {
    const os = at(deviceInfo, 'os') ? at(deviceInfo, 'os') : null;
    const kvpairs = at(window, '__myx_kvpairs__');
    let link = null;
    if (os) {
      if (kvpairs) {
        link = kvpairs[statics.osLinkMap[`${os}`]];
        link = link ? kvpairs[statics.osLinkMap[`${os}`]] : statics.kvpairsAppLinkMap[statics.osLinkMap[`${os}`]];
      } else {
        link = statics.kvpairsAppLinkMap[statics.osLinkMap[`${os}`]];
      }
    } else {
      link = '/mobile/apps';
    }
    return (
      <div className={styles.appSection}>
        <div className={styles.myntExp}> EXPERIENCE MYNTRA APP</div>
        <div data-link={link} onClick={() => navigate(link)} className={styles.appLink}>
          <div className={`myntraweb-footer-sprite ${styles[`${os}`]}`}></div>
          <div className={styles.btnText}> DOWNLOAD {os ? os.toUpperCase() : 'MYNTRA'} APP</div>
        </div>
        <p className={styles.or}> (OR) </p>
        <p className={styles.missCall}>
          In case of any concerns contact us on <br />
          <strong> +91-80-61561999 </strong>
        </p>
      </div>
    );
  } return null;
};

const constructLink = (links = []) => {
  let linkArr = null;
  try {
    if (typeof links !== 'undefined') {
      linkArr = links.map((val, index) => {
        if (val && val.linkUrl && val.name) {
          if (val && val.linkUrl && (val.linkUrl.indexOf('http') >= 0 || val.linkUrl.match('^/'))) {
            return <a key={index} href={val.linkUrl}> {val.name} </a>;
          } return <a key={index} href={`/${val.linkUrl}`}> {val.name} </a>;
        } return null;
      });
    } return linkArr;
  } catch (ex) {
    console.log('Footer link failed with ', ex);
  } return null;
};

class Mobile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      server: true,
      deviceData: null
    };
  }

  componentDidMount() {
    if (isBrowser()) {
      const ua = userAgent(window.navigator.userAgent);
      const osType = ua.getOS();
      this.updateDeviceInfo({ os: osType });
    }
  }

  getOnlineShoppingText() {
    if (at(this, 'props.pageName') === 'Home') {
      return <div className={styles.gyanContainer} dangerouslySetInnerHTML={{ __html: onlineShoppingGyan }} />;
    } return null;
  }

  updateDeviceInfo(data) {
    this.setState({ deviceData: data, server: false });
  }

  render() {
    const footerData = this.props.fData;
    const nData = at(this.props, 'navData') ? at(this.props, 'navData') : statics.navigation;
    return (
      <div className={styles.base}>
        <Seo {...this.props.seo} desktop={false} />
        {!at(this.state, 'server') ? myntraExperience(at(this.state, 'deviceData')) : null}
        <div className={styles.usefulLinks}>
          <a href="/" className={styles.oShopping}> ONLINE SHOPPING </a>
          {constructLink(nData)}
        </div>
        <div className={styles.socialLinks}>
          <p> KEEP IN TOUCH </p>
          <a href="https://www.facebook.com/myntra" className={`myntraweb-footer-sprite ${styles.facebook}`}></a>
          <a href="https://twitter.com/myntra" className={`myntraweb-footer-sprite ${styles.twitter}`}></a>
          <a href="https://www.youtube.com/user/myntradotcom" className={`myntraweb-footer-sprite ${styles.youtube}`}></a>
          <a href="https://www.instagram.com/myntra" className={`myntraweb-footer-sprite ${styles.instagram}`}></a>
        </div>
        <div className={styles.usefulLinks}>
          <p> USEFUL LINKS </p>
          {constructLink(statics.usefulLinks)}
        </div>
        {footerData ? <div className={styles.popularLinks}>
          <p> POPULAR LINKS </p>
          {constructLink(footerData)}
        </div> : null}
        <p className={styles.contact}>
          <span> Have issues ? &nbsp;</span>
          <a href="mailto:support@myntra.com"> Contact Us </a>
        </p>
        <p className={styles.copyright}> &copy; {new Date().getFullYear()} www.myntra.com. All rights reserved. </p>
        <a className={styles.flipkartCompany} href="https://www.flipkart.com/"> A Flipkart company </a>
        {legalCompliance(at(this, 'props.pageName'))}
        {this.getOnlineShoppingText()}
      </div>
    );
  }
}

Mobile.propTypes = {
  fData: React.PropTypes.array,
  pageName: React.PropTypes.string,
  seo: React.PropTypes.object
};

export default Mobile;
