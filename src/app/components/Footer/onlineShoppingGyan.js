const onlineShoppingGyan = `<p>
<h1><strong>Online Shopping: Convenient shopping experience at an affordable price! </strong></h1>
</p>
<p> Fashion has taken today’s youth by surprise, and the availability of numerous options just leaves them spoilt of
choice. Online stores fuel fashion by making the latest trending dresses, accessories, and apparels available to you
within a few clicks. Shopping is no longer a day long affair with online shopping sites offering convenience of easy
shopping facility from your home, anywhere and anytime you wish. Online shopping presents a large variety of quality
products to choose from. With our fast changing lifestyle, online shopping offers fast, easy & a money saving solution
giving you a very interesting shopping experience. Moreover, at your service round the clock, online shopping sites
offers you feasibility to order any products anytime. </p>
<p>
<h3><strong>Myntra: The trailblazer of online fashion destination! </strong></h3>
</p>
<p>  The leader in fashion online shopping has stamped its mark throughout India. With an aim to redefine the fashion
arena in India, Myntra has everything to offer, ranging from all kinds of men’s and women’s apparels to
<a href="/accessories” class="seolink">accessories</a>. Featuring some of the best deals in the realm of fashion
accessories and trending gears, you may attractive discounts and shop till you drop at end of the season sales.
To make shopping convenient, Myntra provides easy payment facility through Cash on Delivery, Card on Delivery,
and similar payment options. And to get your purchased apparel on time, it boasts an efficient network of delivery.
You can also easily make a wishlist and shortlist items for future puchases.  Whether it is <a href="/clothing”
class="seolink">clothing</a>, footwear, jewelry, accessories and <a href="/women-beauty” class="seolink">cosmetics</a>,
we showcase the most elite brands in the world.  Now, if you did not like the purchased product, then shop something
else through our easy return or exchange policy. Explore the latest collections of top brands like United Colors of
Benetton, Arrow, Esprit, French Connection, <a href="/adidas" class="seolink">Adidas</a>, <a href="/reebok"
class="seolink">Reebok</a>, <a href="/nike" class="seolink">Nike</a>, Clarks, Burberry, Calvin Klein and many others.
</p>
<p>
<h3><strong>Avail added online shopping benefits</strong></h3>
</p>
<p> With a variety of products and their information available in the most user-friendly way on Myntra, you get a
chance to analyze the product you want to buy according to your terms. You can shop in complete privacy and get easy
replacement and returns as well. You can choose your desired product and order it on Myntra. Our team will leave no
stone unturned to deliver it right at your doorstep anywhere in India at the promised time. You just need to pay for
the product, while we ensure free shipping on almost everything. To offer you a safe and risk-free online shopping
experience, we have COD facility as well. Now, you can buy gifts for your loved ones and avail our special gift-wrap
facility at a nominal cost on Myntra. </p>
<p>
<h3><strong>Myntra.com: the 24 x7 one stop Fashion & Lifestyle store for everyone</strong></h3>
</p>
<p> We, at Myntra, have all that you need to spruce up your fashion quotient. From an extensive range of
<a href="/shirts" class="seolink">men’s shirts</a>, <a href="/women-dresses" class="seolink">western dresses
for women</a>, funky clothes for kids and matching footwear, <a href="/sports-shoes" class="seolink">sports
shoes</a> and accessories for everyone, we cater to a diversity of choices of online shoppers in India under
one umbrella. So, don’t lose out on your chance to buy your desired piece of apparel and avail our
attractive discounts! </p>`;

export default onlineShoppingGyan;
