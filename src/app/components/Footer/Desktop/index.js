import React from 'react';
import styles from './desktop.css';
import onlineShoppingGyan from '../onlineShoppingGyan';
import statics from '../links.json';
import Seo from '../seo/index.js';
import { legalCompliance } from '../Common';

// Functional Imports.
import kvpairs from '../../../utils/kvpairs';

const constructLink = (links = []) => {
  let linkArr = null;
  try {
    if (typeof links !== 'undefined' && links) {
      linkArr = links.map((val, index) => {
        if (val && val.linkUrl && val.name) {
          if ((val.linkUrl.indexOf('http') >= 0 || val.linkUrl.match('^/'))) {
            return <a key={index} href={val.linkUrl}> {val.name} </a>;
          } return <a key={index} href={`/${val.linkUrl}`}> {val.name} </a>;
        } return null;
      });
    } return linkArr;
  } catch (ex) {
    console.log('Footer link failed with ', ex);
  } return null;
};

const getOnlineShoppingText = (pageName) => {
  if (pageName === 'Home') {
    return <div className={styles.gyanContainer} dangerouslySetInnerHTML={{ __html: onlineShoppingGyan }} />;
  } return null;
};

const Desktop = (props) =>
  <footer className={styles.footerContainer}>
    <div className={styles.base}>
      <Seo {...props.seo} desktop />
      <div className={styles.genericInfo}>
        <div className={styles.shopLinks} >
          <p className={styles.gInfoTitle}>
            <a href="/?src=onlineShopping"> ONLINE SHOPPING </a>
          </p>
          {constructLink(props.navData)}
          <a key="giftCards#1" href="/giftcard">
            Gift Cards
            <span className={styles.superscript}> New </span>
          </a>
          <a key="myntrainsider#1" href="/myntrainsider?cache=false">
            Myntra Insider
            <span className={styles.superscript}> New </span>
          </a>
        </div>
        <div className={styles.usefulLinks}>
          <p className={styles.gInfoTitle}> USEFUL LINKS </p>
          {constructLink(statics.usefulLinks)}
        </div>
        <div className={styles.appExperience}>
          <p className={styles.gInfoTitle}> EXPERIENCE MYNTRA APP ON MOBILE </p>
          <div className={styles.downLinkContainer}>
            <a
              href={statics.googleAppDownloadLink}
              className={`myntraweb-footer-sprite ${styles.androidDownLink}`} />
            <a href={statics.iosAppDownLoadLink} className={`myntraweb-footer-sprite ${styles.iOSDownLink}`} />
          </div>
          <p className={styles.or}> (OR) </p>
          <p> In case of any concerns contact us on <br /> <strong> +91-80-61561999 </strong> </p>
          <div className={styles.keepInTouch}> KEEP IN TOUCH </div>
          <a href="https://www.facebook.com/myntra" className={`myntraweb-footer-sprite ${styles.facebook}`}></a>
          <a href="https://twitter.com/myntra" className={`myntraweb-footer-sprite ${styles.twitter}`}></a>
          <a href="https://www.youtube.com/user/myntradotcom" className={`myntraweb-footer-sprite ${styles.youtube}`}></a>
          <a href="https://www.instagram.com/myntra" className={`myntraweb-footer-sprite ${styles.instagram}`}></a>
        </div>
        <div className={styles.promises}>
          <div className={styles.section}>
            <div className={`myntraweb-footer-sprite ${styles.original}`}></div>
            <div><strong>100% ORIGINAL </strong> guarantee for all products at myntra.com </div>
          </div>
          <div className={styles.section}>
            <div className={`myntraweb-footer-sprite ${styles.return}`}></div>
            <div><strong>Return within 30days </strong>of receiving your order</div>
          </div>
          <div className={styles.section}>
            <div className={`myntraweb-footer-sprite ${styles.delivery}`}></div>
            <div><strong>Get free delivery </strong>for every order above Rs. {kvpairs('shipping.charges.cartlimit') || '1199'}</div>
          </div>
        </div>
      </div>
      <div className={styles.popularSearch}>
        {props.fData ? <div>
          <hr />
          <div className={styles.pSearchTitle}> POPULAR SEARCHES </div>
          <div className={styles.pSearchlinks}> {constructLink(props.fData)} </div> </div> : null}
      </div>
      <div className={styles.fInfoSection}>
        <div className={styles.contact}>
          Have issues ? <a href={'/contactus'}> Contact Us </a>
        </div>
        <div className={styles.copywrite}>
          &copy; {new Date().getFullYear()} www.myntra.com. All rights reserved.
        </div>
        <div className={styles.flipkartCompany}>
          <a href="https://www.flipkart.com/"> A Flipkart company </a>
        </div>
      </div>
      {legalCompliance(props.pageName)}
      {getOnlineShoppingText(props.pageName)}
    </div>
  </footer>;

Desktop.propTypes = {
  navData: React.PropTypes.array,
  fData: React.PropTypes.array,
  pageName: React.PropTypes.string,
  seo: React.PropTypes.object
};

export default Desktop;
