import React from 'react';
import styles from './index.css';

const legalCompliance = (page = '') => {
  if (page.toLowerCase() === 'home') {
    return (
      <address className={styles.lcContainer}>
        <p className={styles.lcHeader}> Registered Office Address </p>
        <div className={styles.lcInfoContainer}>
          <div className={styles.lcAddress}>
            3rd Floor, A Block, <br />
            AKR Tech Park, 7th Mile, <br />
            Krishna Reddy Industrial Area, <br />
            Kudlu Gate, <br />
            Bangalore – 560068 <br />
          </div>
          <div className={styles.lcLegalInfo}>
            <p> CIN: U72300KA2007PTC041799 </p>
            <p> Email: <a href="mailto:support@myntra.com" className={styles.lcEmail}> support@myntra.com </a> </p>
            <p> Telephone: <a href="tel:18004193500" className={styles.lcTelephone}> 18004193500 </a> </p>
          </div>
        </div>
      </address>
    );
  } return null;
};

export { legalCompliance };
