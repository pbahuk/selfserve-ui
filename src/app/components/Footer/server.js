import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Footer from './index';
import at from 'v-at';
import { isBrowser, isApp } from '../../utils';

export const footerRender = (req, group, callback) => {
  const dData = isBrowser() ? window.__myx_deviceData__ : at(req, 'myx.deviceData');
  const pageName = isBrowser() ? at(window, '__myx.pageName') : at(req, 'myx.pageName');
  const checkIsMobile = at(req, 'myx.deviceData.isMobile') || at(req, 'isMobile');
  const seo = at(req, 'seo') || null;
  const reactOutput = ReactDOMServer.renderToString(
    <Footer isApp={isApp(req)} isMobile={checkIsMobile} seo={seo} pageName={pageName} footerData={at(seo, 'footerData')} groups={group} deviceData={dData} />
  );
  callback(reactOutput);
};
