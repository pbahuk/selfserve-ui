import { isSlotEntryEnabled, inPriceRevealMode } from '../../utils/slotUtils';
import cookies from '../../utils/cookies';
import at from 'v-at';
import { getDateOffset } from '../../utils/serverDateUtil';

let timerEndDate = '';
let timerStartDate = '';

const isGreaterDate = (date) => {
  if (date) {
    const curDate = new Date().getTime();
    return date > curDate;
  }
  return false;
};

const isLesserDate = (date) => {
  if (date) {
    const curDate = new Date().getTime();
    return date < curDate;
  }
  return false;
};

const setStartAndEndDate = () => {
  if (timerEndDate && timerStartDate) {
    return true;
  }
  let passCookie = cookies.get('stp');
  let sdata = {};
  if (passCookie) {
    try {
      passCookie = decodeURIComponent(passCookie);
      passCookie = passCookie.replace(/'/g, '"');
      sdata = JSON.parse(passCookie);
    } catch (e) {
      sdata = {};
    }
    const startDate = at(sdata, 'sl.st');
    const endDate = at(sdata, 'sl.et');
    if (!startDate || !endDate) {
      return false;
    }
    timerStartDate = startDate - getDateOffset();
    timerEndDate = endDate - getDateOffset();
    const startDateLesserThanCurDate = isLesserDate(timerStartDate);
    const endDateGreaterThanCurDate = isGreaterDate(timerEndDate);
    if (startDateLesserThanCurDate && endDateGreaterThanCurDate) {
      return true;
    }
  }
  return false;
};

const isLoggedIn = () => at(window, '__myx_session__.isLoggedIn');

export const SlotTimer = {
  show: () => inPriceRevealMode() && isSlotEntryEnabled() && isLoggedIn(),

  getTimerData: () => {
    const validStartAndEndDate = setStartAndEndDate();
    if (validStartAndEndDate && timerEndDate) {
      const curDate = new Date().getTime();
      const endDate = timerEndDate;
      const diff = endDate - curDate;
      let msec = diff;
      if (diff <= 0) {
        timerStartDate = timerEndDate = '';
        return {
          hour: '-1',
          minute: '-1',
          second: '-1',
          showTimer: false
        };
      }
      let hours = Math.floor(msec / 1000 / 60 / 60);
      hours = hours < 10 ? `0${hours}` : hours;
      msec -= hours * 1000 * 60 * 60;
      let minutes = Math.floor(msec / 1000 / 60);
      minutes = minutes < 10 ? `0${minutes}` : minutes;
      msec -= minutes * 1000 * 60;
      let seconds = Math.floor(msec / 1000);
      seconds = seconds < 10 ? `0${seconds}` : seconds;
      msec -= seconds * 1000;
      return {
        hour: hours,
        minute: minutes,
        second: seconds,
        showTimer: true
      };
    }
    timerStartDate = timerEndDate = '';
    return {
      showTimer: false
    };
  }
};
