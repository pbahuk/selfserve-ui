import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Header from './index';
import at from 'v-at';
import { fetchTopNav, isApp } from '../../utils'; // isBrowser,

let navDataStore = null;
const navCacheTime = 20 * 60 * 1000;//cache for 10 min
let lastSaveTime = 0;
export const headerRender = (req, callback) => {
  const topNav = new Promise((resolve, reject) => {
    const duration = Date.now() - lastSaveTime;
    if (navDataStore && duration < navCacheTime) {
      resolve(navDataStore);
    } else {
      fetchTopNav('topnav', (err, navigationData) => {
        if (!err) {
          navDataStore = navigationData;
          lastSaveTime = Date.now();
          resolve(navigationData);
        } reject(err);
      });
    }
  });
  topNav.then((data) => {
    const myxSession = at(req, 'myx.session') ? at(req, 'myx.session') : {};
    const checkIsMobile = at(req, 'myx.deviceData.isMobile') || at(req, 'isMobile');
    const reactOutput = ReactDOMServer.renderToString(<Header isApp={isApp(req)} isMobile={checkIsMobile} navData={data} session={myxSession} />);
    const headerStr = `<div style="z-index: 3" class="myx-header-container" id="desktop-headerMount">${reactOutput}</div>`;
    callback({ navData: data, header: headerStr });
  }).catch((err) => {
    console.error('Error while fetching navigation data', err.stack);
    const myxSession = at(req, 'myx.session') ? at(req, 'myx.session') : {};
    const checkIsMobile = at(req, 'myx.deviceData.isMobile') || at(req, 'isMobile');
    const reactOutput = ReactDOMServer.renderToString(<Header isApp={isApp(req)} isMobile={checkIsMobile} navData={null} session={myxSession} />);
    const headerStr = `<div style="z-index: 3" class="myx-header-container" id="desktop-headerMount">${reactOutput}</div>`;
    callback({ navData: null, header: headerStr });
  });
};
