import React from 'react';
import at from 'v-at';
import styles from './mobile.css';
import { isBrowser } from '../../../utils';
import features from '../../../utils/features';

const myntraCreditEnable = (features('myntracredit.enable') || '').toLowerCase() === 'true' || false;

const SHOP_ME = 'SHOP FOR';

class Navigation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      menuSelected: false,
      menuText: SHOP_ME,
      menuType: null,
      showSubMenu: false,
      selectedSubMenu: null,
      navigationData: this.fetchGroups(at(this.props, 'data')),
      session: at(this.props, 'session') ? at(this.props, 'session') : {} /* TO BE CROSS CHECKED */
      // deviceData: at(this.props, 'deviceData') ? window.__myx_deviceData__ : deviceData /* TO BE CROSS CHECKED */
    };
  }

  getGroups(content) {
    if (at(content, 'children')) {
      return content.children.map((child, index) => {
        if (at(child, 'group')) {
          const groupType = at(child, 'group.type');
          const groupName = at(child, 'group.name');
          return (
            <div
              key={index}
              className={styles.navMenuContainer}
              onClick={() => this.setState({
                menuText: groupName,
                menuType: groupType,
                menuSelected: true })}>
              <div className={styles.navMenuItems} > {groupName} </div>
              <div className={`myntraweb-sprite ${styles.navFwd}`}></div>
            </div>);
        }
        return null;
      });
    }
    return null;
  }

  getFooterLinks = () => {
    const faqs = '/faqs';
    const contactUs = '/contactus';
    return (
      <div className={styles.footerLinks}>
        <a href={contactUs} className={styles.fLinks}>
          <span> Contact Us </span>
        </a>
        <a href={faqs} className={styles.fLinks}>
          <span> FAQ's </span>
        </a>
      </div>
    );
  }

  getDynamicUserSection(uAction) {
    const data = at(uAction, 'group');
    const isLoggedIn = at(this.state, 'session.isLoggedIn');
    return (
      <div className={styles.accountLinks}>
        <div className={styles.navAccTitle}> My Account </div>
        {
          data.map((section) =>
            section.map((value) => {
              let url = isLoggedIn ? `${(value.link)}` : `/login?referer=${value.link}`;
              if (url === '/my/giftcards' && myntraCreditEnable) {
                url = '/my/myntracredit';
                value.name = 'Myntra Credit';
              }
              return (
                <a href={url} className={at(value, 'mobile') ? `${styles.navAccItems}` : `${styles.navAccItemsHidden}`}>
                  <span> {at(value, 'name')} </span>
                </a>
              );
            })
          )
        }
      </div>
    );
  }

  setSubMenuState(scope) {
    scope.stopPropagation();
    this.setState({ showSubMenu: true });
  }

  getNavItems(navigationData) {
    let toBeSentBack = null;
    if (isBrowser()) {
      if (at(this.state, 'menuSelected')) {
        toBeSentBack = this.showSubCategories();
      } else {
        toBeSentBack = this.getGroups(this.state.navigationData);
      }
    } else {
      toBeSentBack = this.getGroups(navigationData);
    }
    return toBeSentBack;
  }

  getUrlLink = (link = '') => link.toLowerCase().replace(/ /g, '-');

  shoeSubMenu(scope, value) {
    if (at(this.state, 'selectedSubMenu') === at(value, 'type')) {
      this.setState({ selectedSubMenu: at(value, 'type'), showSubMenu: !this.state.showSubMenu });
    } else {
      this.setState({ selectedSubMenu: at(value, 'type'), showSubMenu: true });
    }
  }

  clickedLink(url) {
    window.location.href = url;
  }

  showSubCategories() {
    const navData = at(this.state, 'navigationData.children');
    let subNav = navData.find((val) => (at(val, 'group.type') === at(this.state, 'menuType')));
    subNav = at(subNav, 'group.children');
    if (subNav) {
      return subNav.map((val) => {
        const show = at(this.state, 'selectedSubMenu') === at(val, 'type') && at(this.state, 'showSubMenu');
        const hasChildren = at(val, 'children') && at(val, 'children').length > 0;
        return (<ul key={at(val, 'type')} data-type="container" onClick={(scope) => this.shoeSubMenu(scope, val)}>
          <li className={styles.catContainer}>
            {hasChildren ? <span className={`${styles.catValue} ${show ? `${styles.active}` : ''}`}> {at(val, 'name')} </span> :
              <div className={styles.catValue} onClick={() => { this.clickedLink(this.getUrlLink(at(val, 'name'))); }}> {at(val, 'name')} </div>}
            {hasChildren ? <div className={`${styles.catExpansion} ${show ? styles.minus : ''}`}> {show ? '-' : '+'} </div> : null}
          </li>
          {hasChildren ? <ul
            data-sel={at(val, 'type')}
            className={`${styles.catList} ${show ? `${styles.show}` : `${styles.hide}`}`}>
              {at(val, 'children').map((ele, index) => {
                if (at(ele, 'name') !== 'whitespace') {
                  return (
                    <li key={`${at(ele, 'name') + index}`} onClick={(scope) => this.setSubMenuState(scope)}>
                      <div
                        key={at(ele, 'type')}
                        className={styles.catSubValue}
                        onClick={() => { this.clickedLink(this.getUrlLink(at(ele, 'url'))); }}>
                          {at(ele, 'name')}
                      </div>
                    </li>);
                } return null;
              })}
          </ul> : null}
        </ul>);
      });
    }
    return null;
  }

  fetchGroups(navData) {
    if (navData) {
      navData.children.forEach((l0) => { // loop for men, women, kids, home decor
        const metaData = JSON.parse(at(l0, 'props.meta'));
        let l0Data = {
          name: at(l0, 'props.title'),
          type: at(l0, 'props.title').replace(/ /g, '-').toLowerCase(),
          url: at(l0, 'props.url'),
          color: at(metaData, 'template_config.color'),
          isDock: at(metaData, 'dock')
        };
        if (at(l0, 'children')) {
          l0Data.children = [];
          l0.children.forEach((l1) => { // loop for topwear, bottomwear ...
            const l1Data = {
              name: at(l1, 'props.title'),
              type: at(l1, 'props.title').replace(/ /g, '-').toLowerCase(),
              url: at(l1, 'props.url'),
              isExpandable: true
            };
            l1Data.children = [];
            if (at(l1, 'children')) {
              l1.children.forEach((l2) => { // loop for men-top-wear
                l1Data.children.push({ name: at(l2, 'props.title'), type: at(l2, 'props.title').replace(/ /g, '-').toLowerCase(), url: at(l2, 'props.url') });
              });
            }
            l0Data.children.push(l1Data);
          });
        }
        l0.group = l0Data;
        l0Data = {};
      });
    }
    return navData;
  }

  render() {
    return (
      <div>
        <div className={at(this.state, 'menuSelected') ? styles.navSubHeading : styles.navHeading}>
          {at(this.state, 'menuSelected') ?
            <span
              className={`myntraweb-sprite ${styles.backBtn}`}
              onClick={() => { this.setState({ menuSelected: false, menuText: SHOP_ME }); }}>
            </span> : null}
          <div className={at(this.state, 'menuSelected') ? `${styles.navSubHeadingText}` : `${styles.navHeadingText}`}> {at(this.state, 'menuText')} </div>
        </div>
        <div className={styles.subCatContainer}>
          {this.getNavItems(this.fetchGroups(at(this.props, 'data')))}
        </div>
        {at(this.state, 'menuSelected') ? null : this.getDynamicUserSection(at(this.props, 'userActions'))}
        {this.getFooterLinks()}
      </div>
    );
  }
}

Navigation.propTypes = {
  data: React.PropTypes.object
};

export default Navigation;
