import React from 'react';
import at from 'v-at';
import Navigation from './navigation.js';
import config from '../../../../config';
import Client from '../../../../utils/Client';
import styles from './mobile.css';
import { isBrowser } from '../../../utils';
import * as helper from './../helpers';
import Track from '../../../utils/track';
import { SlotTimer } from './../slotTimer';

const TERM_LIMIT = 5;

let salesBanner = {};

if (isBrowser()) {
  salesBanner = window.__myx_kvpairs__['hrdr.salebanner.data'];
  if (typeof salesBanner === 'string') {
    try {
      salesBanner = JSON.parse(salesBanner);
    } catch (e) {
      salesBanner = window.__myx_kvpairs__['hrdr.salebanner.data'];
    }
  }
}

class Mobile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      autoSuggest: null,
      navVisible: false,
      activateSearch: false,
      searchTerm: '',
      saleEndDate: null,
      pUserActions: helper.parseUserActionData(at(this.props, 'userActions'))
    };
  }

  getUrlLink = (link = '') => link.toLowerCase().replace(/ /g, '-');

  getSalesBanners() {
    let banner = null;
    if (SlotTimer.show()) {
      const timerData = SlotTimer.getTimerData();
      banner = this.showSlotBanner(timerData);
    }
    return banner === null ? this.showSalesBanner() : banner;
  }

  getSBTimers() {
    const completeDate = helper.getBannerTimers(at(salesBanner, 'enddate'));
    setTimeout(() => {
      this.setState({
        saleEndDate: completeDate
      });
    }, 1000);
    const daysLabel = at(this.state, 'saleEndDate.day') > 1 ? 'Days' : 'Day';
    return (
      <div className={styles.timeContainer}>
        {at(this.state, 'saleEndDate.day') > 0 ?
          (<span>
            <span className={styles.timer}> {at(this.state, 'saleEndDate.day') < 10 ?
            `0${at(this.state, 'saleEndDate.day')}` : at(this.state, 'saleEndDate.day')} </span>
            <small className={styles.days}> {daysLabel} </small>
          </span>) :
          (<span></span>)
        }
        <span className={styles.timer}> {at(this.state, 'saleEndDate.hour') < 10 ?
        `0${at(this.state, 'saleEndDate.hour')}` : at(this.state, 'saleEndDate.hour')} </span> <small>Hrs </small><span> : </span>
        <span className={styles.timer}> {at(this.state, 'saleEndDate.minute') < 10 ?
        `0${at(this.state, 'saleEndDate.minute')}` : at(this.state, 'saleEndDate.minute')} </span> <small>Min </small><span> : </span>
        <span className={styles.timer}> {at(this.state, 'saleEndDate.second') < 10 ?
        `0${at(this.state, 'saleEndDate.second')}` : at(this.state, 'saleEndDate.second')} </span> <small>Sec </small>
      </div>
    );
  }

  getSlotTimers(timerData) {
    setTimeout(() => {
      this.setState({
        saleEndDate: timerData
      });
    }, 1000);
    return (
      <div className={styles.timeContainer}>
        <span className={styles.timer}> {at(this.state, 'saleEndDate.hour')} </span> <small> Hrs </small> :
        <span className={styles.timer}> {at(this.state, 'saleEndDate.minute')} </span> <small> Min </small> :
        <span className={styles.timer}> {at(this.state, 'saleEndDate.second')} </span> <small> Sec </small>
      </div>
    );
  }

  showSalesBanner() {
    const endDate = at(salesBanner, 'enddate') ? new Date(at(salesBanner, 'enddate')) : null;
    const dateCheck = helper.isGreaterDate(endDate);
    if (at(salesBanner, 'enable') === 'true' && dateCheck) {
      return (
        <div className={styles.sbContainer}>
          <div className={styles.sbInfo}>
            <p className={styles.sbContent}> {at(salesBanner, 'when')} </p>
            <p className={styles.sbContent}> {at(salesBanner, 'name')} </p>
          </div>
          <div className={styles.sbTimer}>
            <p className={styles.sbContent}> {at(salesBanner, 'timerlabel') || ''} </p>
            {this.getSBTimers()}
          </div>
        </div>
      );
    } return null;
  }

  showSlotBanner(timerData) {
    timerData = timerData || {};
    let message = 'BUY WITHIN';
    if (timerData.hour === '-1' && timerData.minute === '-1' && timerData.second === '-1') {
      message = 'SLOT ENDED';
      return null;
    }
    if (timerData.showTimer) {
      return (
        <div className={styles.sbContainer}>
          <div className={`${styles.sbTimer} ${styles.slotTimer}`}>
            <p className={styles.sbContent}> {message} </p>
            {this.getSlotTimers(timerData)}
          </div>
        </div>
      );
    } return null;
  }

  constructAutoSug() {
    const sugRes = at(this.state, 'autoSuggest.data');
    if (sugRes) {
      return sugRes.map((val) => <a href={`/${val.value ? val.value.replace(/ /g, '-') : val.value}`} key={val.label} className={styles.sugContainer}>
        <div className={`myntraweb-sprite ${styles.sugIcon}`}></div>
        <div className={styles.sugLabel}> {val.value} </div>
        <div className={styles.sugCount}> {val.count} </div>
      </a>);
    }
    return null;
  }

  fetchAutosugResult(event) {
    const targetVal = event.target.value;
    this.setState({ searchTerm: targetVal });
    if (event.keyCode === 13) {
      location.href = `/${this.getUrlLink(this.state.searchTerm)}?userQuery=true`;
    }
    if (targetVal.length >= 3) {
      const sTerm = this.getUrlLink(targetVal);
      Client.get(`${config('search')}autosuggest/${sTerm}?limit=${TERM_LIMIT}`).then((res) => {
        const status = at(res, 'body.status.statusType');
        if (status && status.toLowerCase() === 'success') {
          this.setState({ autoSuggest: res.body });
        }
      });
    }
  }

  render() {
    const itemCount = at(this.props, 'session.CART:totalQuantity');
    const itemCountColor = typeof itemCount !== 'undefined' && itemCount > 0 ? styles.melon : null;
    const searchCharacters = at(this.state, 'searchTerm');
    const searchURL = searchCharacters && searchCharacters.length > 0 ? `/${searchCharacters}` : '';
    return (
      <div>
        <div className={styles.container}>
          <div className={styles.header}>
            <div className={styles.left}>
              <a className={`myntraweb-sprite ${styles.leftNavBar}`} onClick={() => this.setState({ navVisible: !this.state.navVisible })}></a>
              <a href="/" className={`myntraweb-sprite ${styles.mLogo}`}></a>
            </div>
            <div className={styles.right}>
              <a
                href="/checkout/cart"
                className={`myntraweb-sprite ${styles.mBag}`}
                onClick={() => { Track.event('navigation', 'TopNavClick', 'Cart'); }}>
                <span className={`${styles.itemCount} ${itemCountColor}`}>{itemCount}</span>
              </a>
              <a href="/login?referer=/my/dashboard" className={`myntraweb-sprite ${styles.mUser}`}></a>
              <a className={`myntraweb-sprite ${styles.mSearch}`} onClick={() => this.setState({ activateSearch: !at(this.state, 'activateSearch') })}></a>
            </div>
          </div>
          {/* <div className={styles.subHeader}>
            {this.getSubHeader()}
          </div> */}
          {at(this.state, 'activateSearch') ? <div className={styles.searchContainer}>
            <div className={styles.searchBarContainer}>
              <input
                autoFocus
                type="text"
                placeholder="Search for brands & products"
                value={at(this.state, 'searchTerm')}
                id={styles.searchInput}
                className={styles.searchInput}
                onKeyUp={(scope) => this.fetchAutosugResult(scope)}
                onChange={(scope) => this.fetchAutosugResult(scope)} autoFocus="autoFocus" />
              <a className={styles.searchBtn} href={searchURL}> Search </a>
            </div>
            {at(this.state, 'searchTerm').length >= 3 ?
              <div className={styles.searchListContainer}>
                {this.constructAutoSug()}
              </div> : null}
          </div> : null}
          <div
            className={at(this.state, 'navVisible') ? `${styles.sideNavClicked}` : `${styles.sideNav}`}>
            <Navigation
              userActions={at(this.state, 'pUserActions')}
              session={at(this.props, 'session')}
              data={at(this.props, 'navData')} />
          </div>
          <div
            className={`${styles.overlay} ${at(this.state, 'navVisible') ? `${styles.show}` : `${styles.hide}`}`}
            onClick={() => this.setState({ navVisible: !this.state.navVisible })}></div>
        </div>
        {this.getSalesBanners()}
      </div>
    );
  }

}

export default Mobile;
