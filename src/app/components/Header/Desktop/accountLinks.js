import styles from './desktop.css';
import at from 'v-at';
import React from 'react';
import config from '../../../../config';
import Client from '../../../../utils/Client';
import features from '../../../utils/features';
import { clearCache } from '../../../utils/beacon';
import Track from '../../../utils/track';
import { setItem } from '../../../utils/localStorageUtil';

const myntraCreditEnable = (features('myntracredit.enable') || '').toLowerCase() === 'true' || false;

const logout = (event, session, scope) => {
  const date = new Date();
  const time = date.getTime();
  setItem('lastLogout', time);
  event.preventDefault();
  const parameters = {
    action: 'signout',
    xsrf: at(session, 'USER_TOKEN'),
    email: at(session, 'email')
  };
  Client.post(config('signout'), parameters).then((res) => {
    const respData = at(res, 'body.status');
    if (at(respData, 'statusType') === 'ERROR') {
      const errMessage = respData.statusMessage ? respData.statusMessage : 'Oops! Something went wrong. Please try again in some time!';
      scope.refs.notify.error(errMessage);
    } else if (at(respData, 'statusType') === 'SUCCESS') {
      const errMessage = respData.statusMessage ? respData.statusMessage : 'Successfully logged out.';
      scope.refs.notify.info({
        message: errMessage
      });
      clearCache();
      window.location.reload();
    }
  });
};

const getReferer = (url = '/') => {
  if (typeof window !== 'undefined') {
    return `/${url}?referer=${window.location.href}`;
  } return url;
};

const getGateLinks = (uAction) => {
  const data = at(uAction, 'group');
  if (data) {
    return (
      <div>
        <div className={styles.getUserInLinks}>
          <a
            href={getReferer('register')}
            data-track="signup"
            onClick={() => { Track.event('header', 'profile', 'Sign up'); }}
            className={styles.linkButton}>Sign up</a>
          <a
            href={getReferer('login')}
            data-track="login"
            onClick={() => { Track.event('header', 'profile', 'log in'); }}
            className={styles.linkButton}>log in</a>
        </div>
          {data.map((section, dataIndex) =>
            <div key={dataIndex} className={styles.getInLinks}>
              {
                section.map((value, sectionIndex) => {
                  if (value.link === '/my/myntracredit' && !myntraCreditEnable) {
                    value.link = '/my/giftcards';
                    value.name = 'Manage Gift Cards';
                  }
                  return (
                    <a
                      href={at(value, 'link')}
                      data-track="coupons"
                      className={styles.info}
                      key={sectionIndex}
                      onClick={() => { Track.event('header', 'profile', at(value, 'name')); }}>
                      <div className={styles.infoSection}>
                        {at(value, 'name')}
                        {value.isNew ? <span className={styles.superscriptTag}> New </span> : null}
                      </div>
                    </a>
                  );
                })
              }
            </div>
        )}
      </div>
    );
  } return null;
};

const getDynamicAccLinks = (uAction, session, scope) => {
  const data = at(uAction, 'group');
  if (data) {
    return (
      <div>
        {data.map((section, dataIndex) =>
          <div key={dataIndex} className={styles.getInLinks}>
            {
              section.map((value, sectionIndex) => {
                if (value.link === '/my/myntracredit' && !myntraCreditEnable) {
                  value.link = '/my/giftcards';
                  value.name = 'Manage Gift Cards';
                }
                return (
                  <a
                    href={at(value, 'link')}
                    data-track="coupons"
                    className={styles.info}
                    key={sectionIndex}
                    onClick={() => { Track.event('header', 'profile', at(value, 'name')); }}>
                    <div className={styles.infoSection}>
                      {at(value, 'name')}
                      {value.isNew ? <span className={styles.superscriptTag}> New </span> : null}
                    </div>
                  </a>
                );
              })
            }
          </div>
        )}
        <div className={styles.accActions}>
          <a
            href="/my/profile/edit"
            data-track="edit_profile"
            onClick={() => { Track.event('header', 'profile', 'Edit Profile'); }}
            className={styles.info}>
            <div className={styles.accInfoSection}> Edit Profile </div>
          </a>
          <div
            data-track="logout"
            className={styles.info}
            onClick={(event) => { logout(event, session, scope); Track.event('header', 'profile', 'Logout'); }}>
            <div className={styles.accInfoSection}> Logout </div>
          </div>
        </div>
      </div>
    );
  } return null;
};

export const getUserAccContent = (session, uAction, scope) => {
  const isLoggedIn = at(session, 'isLoggedIn') ? '/my/profile' : '';
  return at(session, 'isLoggedIn') ? (<div className={styles.userActionsContent}>
    <div className={styles.contentInfo}>
      <a
        href={isLoggedIn}
        data-track="edit_profile"
        onClick={() => { Track.event('header', 'profile', 'My Profile'); }}
        className={styles.uaProfileLink}>
        <div className={styles.infoTitle}>
          Hello {at(session, 'userfirstname')}
        </div>
        <div className={styles.infoEmail}>
          {at(session, 'email')}
        </div>
      </a>
    </div>
    {getDynamicAccLinks(uAction, session, scope)}
  </div>) : (<div className={styles.userActionsContent}>
    <div className={styles.contentInfo}>
      <div className={styles.infoTitle}>
        {'Welcome'}
      </div>
      <div className={styles.infoEmail}>
        {'To access account and manage orders'}
      </div>
    </div>
    {getGateLinks(uAction)}
  </div>);
};
