import React from 'react';
import at from 'v-at';
import styles from './desktop.css';
import config from '../../../../config';
import Client from '../../../../utils/Client';
import { isBrowser } from '../../../utils';
import * as helper from './../helpers';
import { SlotTimer } from './../slotTimer';
import features from '../../../utils/features';
import { getUserAccContent } from './accountLinks.js';
import Notify from '../../Notify';
import Track from '../../../utils/track';

let preHeader = {};
let salesBanner = {};
let requestInProgress = null;

if (isBrowser()) {
  const promoheader = window.__myx_kvpairs__['myntraweb.promoheader.data'];
  preHeader = promoheader ? JSON.parse(promoheader) : {};
  salesBanner = window.__myx_kvpairs__['hrdr.salebanner.data'];
  if (typeof salesBanner === 'string') {
    try {
      salesBanner = JSON.parse(salesBanner);
    } catch (e) {
      salesBanner = window.__myx_kvpairs__['hrdr.salebanner.data'];
    }
  }
}

const DOWN = 40;
const UP = 38;
const ENTER = 13;
const LEFT = 37;
const RIGHT = 39;
const ESC = 27;

let curScrollPosition = 0;

class Desktop extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      searchTermResult: [],
      autoSuggestList: { count: -1, presentIndices: [], focused: null }, /* These are interdependant, state update for them happens with change in every key. */
      autoSuggestFlayoutVisibility: false,
      autoSuggestFlayoutStyle: null,
      session: at(this.props, 'session'),
      server: !isBrowser(),
      pNavData: helper.parseNavgationData(at(this.props, 'navData')),
      pUserActions: helper.parseUserActionData(at(this.props, 'userActions')),
      prePromoHeader: helper.parsePreNavigationData(preHeader),
      showPromoHeader: true,
      saleEndDate: null
    };
  }

  componentDidMount() {
    const _self = this;
    if (isBrowser()) {
      let bottomPadding = 0;
      const mountPoint = document.getElementById('desktop-headerMount');
      // Give bottom padding for new additions in header
      if (mountPoint) {
        const preHeaderLinks = [];
        const preHeaderData = at(preHeader, 'data.children');

        for (const value of preHeaderData) {
          const enabledLinks = value.group && value.group.filter((link) => (
            link.disable === false
          ));
          if (enabledLinks.length > 0) {
            preHeaderLinks.push(enabledLinks);
          }
        }

        if (features('myntraweb.promoheader.enable') && preHeaderLinks.length > 0) {
          bottomPadding += 25;
        }
        if (mountPoint && bottomPadding) {
          mountPoint.style.paddingBottom = `${bottomPadding}px`;
        }
      }
      window.addEventListener('scroll', () => {
        if (window.pageYOffset > curScrollPosition) {
          curScrollPosition = window.pageXOffset;
          if (_self.state.showPromoHeader) {
            _self.setState({
              showPromoHeader: false
            });
          }
        } else {
          curScrollPosition = window.pageXOffset;
          if (!_self.state.showPromoHeader) {
            _self.setState({
              showPromoHeader: true
            });
          }
        }
      }, true);

      const endDate = at(salesBanner, 'enddate') ? new Date(at(salesBanner, 'enddate')) : null;
      const dateCheck = helper.isGreaterDate(endDate);

      if (SlotTimer.show()) {
        const slotTimerId = setInterval(() => {
          const timerData = SlotTimer.getTimerData();
          this.setState({
            saleEndDate: timerData
          });
          if (!at(timerData, 'showTimer')) {
            window.clearInterval(slotTimerId);
          }
        }, 1000);
      } else if (at(salesBanner, 'enable') === 'true' && dateCheck) {
        const salesBannerTimerId = setInterval(() => {
          const completeData = helper.getBannerTimers(endDate);
          this.setState({
            saleEndDate: completeData
          });
          if (!helper.isGreaterDate(endDate)) {
            window.clearInterval(salesBannerTimerId);
          }
        }, 1000);
      }
    }
  }

  getNavigationEntities(categories, navTitle) {
    const maxItemTodisplay = 18;
    return categories.map((entities, index) => {
      if (index < maxItemTodisplay) {
        if (entities.name !== 'whitespace') {
          return entities.link !== '' ? (<li key={index}>
            <a
              href={entities.link}
              style={{ 'color': at(entities, 'color') ? entities.color : '' }}
              onClick={() => { Track.event('navigation_menu', navTitle, entities.name); }}
              className={at(entities, 'istitle') ? styles.categoryName : styles.categoryLink}>
              {at(entities, 'name') && entities.name.toLowerCase() !== 'whitespace' ? entities.name : ''}
            </a>
          </li>) : (<li key={index}>
            <div
              style={{ 'color': at(entities, 'color') ? entities.color : '' }}
              className={at(entities, 'istitle') ? styles.categoryName : styles.categoryLink}>
              {at(entities, 'name') && entities.name.toLowerCase() !== 'whitespace' ? entities.name : ''}
            </div>
          </li>);
        } else if (index + 1 !== categories.length && index - 1 >= 0 && !at(categories[index - 1], 'istitle')) {
          return (
            <div className={styles.hrLine}></div>
          );
        }
      }
      return null;
    });
  }

  getUrlLink = (link = '') => link.toLowerCase().replace(/ /g, '-');

  getNavigationCategories(groups) {
    const navTitle = groups.props.title;
    return groups.group.map((categories, index) => {
      if (index < 5) {
        return (<li className={index % 2 === 1 ? styles.evenColumnContent : styles.oddColumnContent}>
          <ul className={styles.navBlock} key={index}>
            {(typeof categories !== 'undefined') ? this.getNavigationEntities(categories, navTitle) : ''}
          </ul>
        </li>);
      }
      return null;
    });
  }

  getNavigationContent(entities) {
    let result = null;
    if (at(entities, 'children')) {
      result = entities.children.map((group, index) => {
        const meta = JSON.parse(group.props.meta);
        const title = at(group, 'props.title').toLowerCase().replace(/ /g, '-');
        const props = group.props;
        const metaData = JSON.parse(props.meta);
        return (
          <div
            key={index}
            className={styles.navContent}>
            <div className={styles.navLink}>
              <a
                href={props.url}
                data-index={index}
                data-group={at(props, 'title') ? title : 'null'}
                data-color={at(metaData, 'template_config.color') ? metaData.template_config.color : ''}
                data-type="navElements"
                style={{ 'borderBottomColor': `${at(metaData, 'template_config.color')}` }}
                onClick={() => { Track.event('navigation_menu', props.title); }}
                className={styles.main}>
                {at(props, 'title')}
              </a>
              <div className={styles.backdropStyle}>
                <div className={styles.paneContent}>
                  <div
                    className={styles.categoryContainer}
                    data-index={index}
                    data-group={at(group, 'props.title') ? title : 'null'}
                    data-color={at(meta, 'template_config.color')} >
                    {at(group, 'group') ? this.getNavigationCategories(group) : ''}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    }
    return result;
  }

  getPromoSection(preData) {
    if (isBrowser() && features('myntraweb.promoheader.enable') === 'true' && preData) {
      return (
        <div className={`${styles.preHeaderContent} ${at(this.state, 'showPromoHeader') ? `${styles.showBanner}` : `${styles.hideBanner}`}`}>
          {
            preData.map((preNavObject) => {
              if (at(preNavObject, 'group')) {
                return at(preNavObject, 'group').map((data, key) => {
                  if (!data.disable) {
                    return (
                      <a key={key} className={styles.preHeaderLinks} href={data.linkUrl}>
                        {data.name}
                        {data.isNew ? <span className={styles.superscriptTag}> New </span> : null}
                      </a>
                    );
                  } return null;
                });
              } return null;
            })
          }
        </div>
      );
    }
    return null;
  }

  getSearchSugList(object) {
    const autoSugDomEles = helper.parseAutosuggestData(object);
    const listEntities = this.state.autoSuggestList;
    listEntities.presentIndices = [];
    if (autoSugDomEles) {
      return (<ul className={styles.group}>
        {
          autoSugDomEles.map((categories, index) => (at(categories, 'data.isTitle') ?
            <li key={index} className={styles.suggestionTitle}> {at(categories, 'data.title')} </li> : <li
              key={index}
              className={`${styles.suggestion} ${at(this.state, 'autoSuggestList.focused') === index ? styles.active : null}`}
              data-index={index}
              onClick={() => {
                Track.event('search', 'autosuggest_result_click', at(categories, 'data.name'), index);
                window.location.href = helper.stripSpecialChars(at(categories, 'data.action'));
              }}
              data-activeIndex={at(listEntities, 'presentIndices') ?
                listEntities.presentIndices.push({ key: index, value: at(categories, 'data.name') }) : ''}
              data-count={at(categories, 'data.count')}
              data-value={helper.stripSpecialChars(at(categories, 'data.action'))} >
              {at(categories, 'data.name')}
            </li>
          ))
        }
      </ul>);
    }
    return null;
  }

  getSalesBanner() {
    let banner = null;
    if (SlotTimer.show()) {
      let timerData = {};
      if (this.state.saleEndDate) {
        timerData = this.state.saleEndDate;
      } else {
        timerData = SlotTimer.getTimerData();
      }
      banner = this.showSlotBanner(timerData);
    }
    const url = at(salesBanner, 'url');
    const salebanner = url ? (
      <a href={url} onClick={() => { Track.event('timer', 'buttonclick', 'landing page url'); }}>
      {this.showSalesBanner()}
      </a>
     ) : this.showSalesBanner();
    return banner === null ? salebanner : banner;
  }

  getSearchTermLink = () => (
    `/${this.getUrlLink(this.state.searchTerm)}?userQuery=true`
  );

  fetchBannerImage = (url) => {
    if (url) {
      return (
        <div className={styles.sbImageContainer}>
          <img className={styles.sbImage} src={url} />
        </div>
      );
    } return null;
  }

  showSalesBanner() {
    const endDate = at(salesBanner, 'enddate') ? new Date(at(salesBanner, 'enddate')) : null;
    const dateCheck = helper.isGreaterDate(endDate);
    const imageURL = at(salesBanner, 'image') || null;
    if (at(salesBanner, 'enable') === 'true' && dateCheck) {
      const daysLabel = at(this.state, 'saleEndDate.day') > 1 ? 'Days' : 'Day';
      return (
        <div className={styles.sbContainer} style={{ 'top': '82px' }}>
          {this.fetchBannerImage(imageURL)}
          <div className={styles.saleStartsContainer}>
            <div className={styles.saleMessage}> {at(salesBanner, 'timerlabel') || ''} </div>
            <div className={styles.timeContainer}>
              {at(this.state, 'saleEndDate.day') > 0 ?
                (<span>
                  <span className={styles.timer}> {at(this.state, 'saleEndDate.day') < 10 ?
                  `0${at(this.state, 'saleEndDate.day')}` : at(this.state, 'saleEndDate.day')} </span>
                  <span className={styles.days}> {daysLabel} </span>
                </span>) :
                (<span></span>)
              }
              <span className={styles.timer}> {at(this.state, 'saleEndDate.hour') < 10 ?
              `0${at(this.state, 'saleEndDate.hour')}` : at(this.state, 'saleEndDate.hour')} </span> <span className={styles.days}> H </span> :
              <span className={styles.timer}> {at(this.state, 'saleEndDate.minute') < 10 ?
              `0${at(this.state, 'saleEndDate.minute')}` : at(this.state, 'saleEndDate.minute')} </span> <span className={styles.days}> M </span> :
              <span className={styles.timer}> {at(this.state, 'saleEndDate.second') < 10 ?
              `0${at(this.state, 'saleEndDate.second')}` : at(this.state, 'saleEndDate.second')} </span> <span className={styles.days}> S </span>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }

  showSlotBanner(timerData) {
    timerData = timerData || {};
    let message = 'BUY WITHIN';
    const preheaderPresent = features('myntraweb.promoheader.enable') === 'true' && at(this.state, 'prePromoHeader');
    const fromTop = preheaderPresent ? '108px' : '82px';
    if (timerData.hour === '-1' && timerData.minute === '-1' && timerData.second === '-1') {
      message = 'SLOT ENDED';
      return null;
    }
    if (timerData.showTimer) {
      const imageURL = at(salesBanner, 'image') || null;
      return (
        <div className={styles.sbContainer} style={{ 'top': fromTop }}>
          {this.fetchBannerImage(imageURL)}
          <div className={styles.saleStartsContainer}>
            <div className={styles.saleMessage}> {message} </div>
            <div className={styles.timeContainer}>
              <span className={styles.timer}> {at(this.state, 'saleEndDate.hour')} </span> <span className={styles.days}> H </span> :
              <span className={styles.timer}> {at(this.state, 'saleEndDate.minute')} </span> <span className={styles.days}> M </span> :
              <span className={styles.timer}> {at(this.state, 'saleEndDate.second')} </span> <span className={styles.days}> S </span>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }

  fetchAutosugResult(search) {
    if (requestInProgress) {
      requestInProgress.abort();
    }

    requestInProgress = Client.get(`${config('search')}/${search}`)
      .set({ ...{ Accept: 'application/json', 'Content-Type': 'application/json' } })
      .timeout(15000)
      .send()
      .end((err, res) => {
        const status = at(res, 'body.status.statusType');
        requestInProgress = null;
        if (status !== 'ERROR') {
          const entries = at(res, 'body.groups.0.entries') || [];
          this.setState({
            searchTermResult: entries,
            autoSuggestFlayoutVisibility: entries.length > 0
          });
        }
      });
  }

  fetchSearch(event, which) {
    const code = event.keyCode;
    const value = event.target.value;
    if (event.keyCode === ENTER || which === 'btn') {
      if (value) {
        Track.event('search', 'performed_manual_search', at(this.state, 'searchTerm'));
        location.href = `/${this.getUrlLink(this.state.searchTerm)}?userQuery=true`;
      }
    } else if ((code === DOWN || code === UP || code === LEFT || code === RIGHT) && value.length > 0) {
      const listEntities = at(this.state, 'autoSuggestList');
      const presentIndices = at(listEntities, 'presentIndices') ? at(listEntities, 'presentIndices') : [];
      let count = listEntities.count;
      if (presentIndices.length > 0) {
        if (count < presentIndices.length - 1) {
          switch (code) {
            case DOWN : count = count + 1; break;
            case UP : if (count) { count = count - 1; } else { count = presentIndices.length - 1; } break;
            default: break;
          }
        } else {
          switch (code) {
            case DOWN : count = 0; break;
            case UP : count = count - 1; break;
            default: break;
          }
        }
        listEntities.focused = presentIndices[count].key;
      }
      listEntities.count = count;
      this.setState({
        autoSuggestList: listEntities,
        searchTerm: presentIndices[count].value
      });
      return;
    } else if (code === ESC) {
      let suggestionState = this.state.autoSuggestFlayoutVisibility;
      suggestionState = false;
      this.setState({ autoSuggestFlayoutVisibility: suggestionState });
    } else {
      const target = event.target;
      let asfVisibility = this.state.autoSuggestFlayoutVisibility;
      let asfStyle = this.state.autoSuggestFlayoutStyle;
      if (target.value.length >= 3) {
        this.fetchAutosugResult(this.getUrlLink(target.value));
        asfVisibility = true;
        asfStyle = {
          left: `${target.offsetLeft - 40}px`,
          top: `${target.offsetTop + 40}px`
        };
      } else {
        asfVisibility = false;
      }
      this.setState({
        searchTerm: target.value,
        autoSuggestFlayoutVisibility: asfVisibility,
        autoSuggestFlayoutStyle: asfStyle
      });
      return;
    }
  }

  updateGA = (GAEventCategory, GAEventAction, GAEventLabel, GAEventValue) => {
    Track.event(GAEventCategory, GAEventAction, `${GAEventLabel} | ${at(window, GAEventValue)}`);
  }

  render() {
    if (!this.props) {
      return null;
    }
    // Uncomment to not render navigation from server
    // const navigationContent = at(this.props, 'navData') && at(this.state, 'server') ? null : this.getNavigationContent(at(this.state, 'pNavData'));
    const navigationContent = at(this.props, 'navData') ? this.getNavigationContent(at(this.state, 'pNavData')) : null;
    const userActionContent = getUserAccContent(at(this.props, 'session'), at(this.state, 'pUserActions'), this);
    const searchSugList = this.getSearchSugList(at(this.state, 'searchTermResult'));
    const promoSection = this.getPromoSection(at(this.state, 'prePromoHeader'));
    return (
      <div>
        <header className={styles.container} id="desktop-header-cnt">
          {promoSection}
          <div className={styles.bound}>
            <div className={styles.logoContainer}>
              <a
                href="/"
                className={`myntraweb-sprite ${styles.logo}`}
                onClick={() => this.updateGA('header', 'logo_click', 'source_page_of_click', 'location.href')}>
              </a>
            </div>
            <nav className={styles.navbar} >
              <div className={styles.navLinks}>
                {navigationContent}
              </div>
            </nav>
            <div className={styles.actions}>
              <div className={styles.user}>
                <div className={styles.userIconsContainer}>
                  <span className={`myntraweb-header-sprite ${styles.iconUser}`} ></span>
                  <span className={styles.userTitle}>Profile</span>
                </div>
                <div className={styles.userActions}>
                  <div className={styles.userActionsArrow}></div>
                  {userActionContent}
                </div>
              </div>
              <a
                href="/wishlist"
                className={styles.wishlist}
                onClick={() => this.updateGA('header', 'wishlist_icon_click', 'source_page_of_click', 'location.href')}>
                <span className={`myntraweb-header-sprite ${styles.iconWishlist}`}></span>
                <span
                  className={styles.userTitle}>
                  Wishlist
                </span>
              </a>
              <a
                href="/checkout/cart"
                className={styles.cart}
                onClick={() => this.updateGA('header', 'cart_icon_click', 'source_page_of_click', 'location.href')}>
                <span className={`myntraweb-header-sprite ${styles.iconBag}`}></span>
                <span
                  className={`${styles.badge} ${at(this.props, 'animateCount') ? `${styles.animated} ${styles.pulse}` : ''}
                    ${at(this.props, 'session.CART:totalQuantity') === undefined || at(this.props, 'session.CART:totalQuantity') <= 0 ?
                  `${styles.grey}` : `${styles.melon}`}`}>
                  {at(this.props, 'session.CART:totalQuantity')}
                </span>
                <span className={styles.userTitle}>Bag</span>
              </a>
            </div>
            <div className={styles.query}>
              <input
                placeholder="Search for products, brands and more"
                className={styles.searchBar}
                value={this.state.searchTerm}
                onClick={() => this.updateGA('header', 'search_icon_click', 'source_page_of_click', 'location.href')}
                onKeyUp={(scope) => this.fetchSearch(scope, 'input')}
                onChange={(scope) => this.fetchSearch(scope, 'input')} />
              <a
                href={typeof at(this.state, 'searchTerm') !== 'undefined' && at(this.state, 'searchTerm').length > 0 ? this.getSearchTermLink() : null}
                className={styles.submit}
                onClick={() => { Track.event('search', 'performed_manual_search', at(this.state, 'searchTerm')); }}>
                <span className={`myntraweb-sprite ${styles.iconSearch}`}></span>
              </a>
              {at(this.state, 'searchTerm').length >= 3 && this.state.searchTermResult.length > 0 ? <div
                className={` ${styles.autoSuggest} ${at(this.state, 'autoSuggestFlayoutVisibility') ? `${styles.showContent}` : ''}`}
                style={at(this.state, 'autoSuggestFlayoutStyle')}>
                {searchSugList}
              </div> : null}
            </div>
          </div>
        </header>
        {this.getSalesBanner()}
        <Notify ref="notify" />
      </div>
    );
  }
}

Desktop.propTypes = {
  session: React.PropTypes.object,
  navData: React.PropTypes.object,
  animateCount: React.PropTypes.bool
};

export default Desktop;
