import at from 'v-at';
import uniq from 'lodash/uniq';


export const parseAutosuggestData = (list) => {
  const autoSuggestData = [];
  if (list) {
    const uArray = {};
    const categories = uniq(list.map((val) => val.category));
    list.forEach((val) => {
      const str = val.category.toLowerCase().replace(/\s/g, '-');
      if (uArray[str]) {
        uArray[str].push(val);
      } else {
        uArray[str] = [val];
      }
    });
    categories.forEach((cats) => {
      autoSuggestData.push({ data: { title: cats, isTitle: true } });
      uArray[cats.toLowerCase().replace(/\s/g, '-')].forEach((stuff) => {
        autoSuggestData.push({ data: stuff });
      });
    });
  }
  return autoSuggestData;
};

export const getBannerTimers = (enddate) => {
  if (enddate) {
    const endDate = new Date(enddate).getTime();

    const t = parseInt(endDate, 10) - new Date().getTime();
    const seconds = Math.floor((t / 1000) % 60);
    const minutes = Math.floor((t / 1000 / 60) % 60);
    const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    const days = Math.floor(t / (1000 * 60 * 60 * 24));

    return {
      day: days,
      hour: hours,
      minute: minutes,
      second: seconds
    };
  } return null;
};

export const isGreaterDate = (endDate) => {
  if (endDate) {
    const curDate = new Date();
    return endDate > curDate;
  } return false;
};

export const parseUserActionData = (data) => {
  const userData = data;
  if (at(userData, 'children')) {
    const group = userData.group = [];
    userData.children.forEach((l0) => {
      const categories = [];
      const meta = l0.props.meta;
      l0.children.forEach((l1) => {
        categories.push({ 'link': at(l1, 'props.url'), 'name': at(l1, 'props.title'), mobile: at(meta, 'mobile'), isNew: at(l1, 'props.meta.new') });
      });
      group.push(categories);
    });
  }
  return userData;
};

export const parsePreNavigationData = (preHead) => {
  const preHeaderData = at(preHead, 'data.children');
  if (preHeaderData) {
    preHeaderData.forEach((l0) => {
      const group = l0.group = [];
      if (l0.children && l0.children.length > 0) {
        l0.children.forEach((l1) => {
          if (at(l1, 'children').length > 0) {
            // loop for children inside level 1
          } else {
            group.push({
              linkUrl: at(l1, 'props.url'),
              name: at(l1, 'props.title'),
              isNew: at(l1, 'props.meta.new'),
              disable: at(l1, 'props.meta.disable')
            });
          }
        });
      }
    });
  }
  return preHeaderData;
};

export const parseNavgationData = (data) => {
  if (data) {
    const completeData = data;
    const itemsInColLimit = 16;
    if (at(data, 'children')) {
      data.children.forEach((l0) => {
        const group = l0.group = [];
        let categories = [];
        const meta = JSON.parse(l0.props.meta);
        group.push(categories);
        if (at(l0, 'children')) {
          l0.children.forEach((l1, index) => {
            if (l1.children && (categories.length + l1.children.length) > itemsInColLimit && index > 0) {
              categories = [];
              group.push(categories);
              categories.push({ 'name': at(l1, 'props.title'), 'link': at(l1, 'props.url'), 'istitle': true, 'color': at(meta, 'template_config.color') });
              if (at(l1, 'children')) {
                l1.children.forEach((l2) => {
                  categories.push({ 'name': at(l2, 'props.title'), 'link': at(l2, 'props.url') });
                });
              }
              categories.push({ 'name': 'whitespace', 'link': 'whitespace', 'style': 'whitespace' });
            } else {
              categories.push({ 'name': at(l1, 'props.title'), 'link': at(l1, 'props.url'), 'istitle': true, 'color': at(meta, 'template_config.color') });
              if (at(l1, 'children')) {
                l1.children.forEach((l2) => {
                  categories.push({ 'name': at(l2, 'props.title'), 'link': at(l2, 'props.url') });
                });
              }
              categories.push({ 'name': 'whitespace', 'link': 'whitespace', 'style': 'whitespace' });
            }
          });
        }
      });
    }
    return completeData;
  }
  return null;
};

export const stripSpecialChars = (link) => {
  const mapObj = {
    '@': '-at-',
    '&': '-and-'
  };
  link = link.replace(/@|&/gi, (matched) => {
    const char = mapObj[matched];
    return char;
  });
  link = link.replace(/\s/gi, '-').replace(/-+/g, '-').toLowerCase();
  return link;
};
