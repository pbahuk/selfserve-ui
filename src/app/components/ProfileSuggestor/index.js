import React, { Component, PropTypes } from 'react';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import Suggestor from './profileSuggestor';
import Madalytics from '../../utils/trackMadalytics';
import { addProfile, tagProfile } from '../../utils/services/userProfileService.js';

const UNISEX = 'Unisex';
const genderMap = {
  Men: 'male',
  Women: 'female'
};

class SuggestorWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allProfiles: [],
      selectedProfile: '',
      infoCardOpen: false,
      saveInProgess: false,
      fetchInProgress: true,
      profileTagged: false,
      gender: '',
      tagged: false,
      form: {
        name: {
          value: '',
          error: '',
          label: 'Name',
          required: true
        }
      }
    };

    ['onProfileClick', 'onGenderClick', 'saveProfile', 'openSizeInfoPage', 'handleChange', 'hideInfoCard']
      .forEach(method => (this[method] = this[method].bind(this)));
  }

  componentWillMount() {
    const product = get(this, 'props.item.product');
    const gender = get(product, 'gender', 'MEN');
    this.unisexProduct = gender === UNISEX;
    this.setState({
      gender: this.unisexProduct ? 'male' : genderMap[gender]    // Defaulting to male for gender toggle if unisex
    });
  }

  componentWillReceiveProps(props) {
    if (isEmpty(this.props.allProfiles)) {
      this.setState({
        allProfiles: props.allProfiles
      });
    }
  }

  onProfileClick(e) {
    const id = e.currentTarget.id;

    // If clicked on 'Myself' or 'Others' button, open half card for profile creation
    if (id === 'myself' || id === 'others') {
      this.openInfoCard(id === 'myself' ? get(window, '__myx_session__.userfirstname') : '');
    } else if (get(e, 'currentTarget.dataset.disabled') === 'false') {
      this.tagProfileToProduct(id);
      this.setState({
        selectedProfile: id
      });
    }
  }

  onGenderClick(e) {
    const id = e.currentTarget.id;
    this.setState({
      gender: id
    });
  }

  tagProfileToProduct(id) {
    const { orderId, item } = this.props;
    const product = get(item, 'product', {});
    const payload = {
      uidx: get(window, '__myx_session__.login'),
      pidx: id,
      skuId: product.skuId,
      styleId: product.id,
      orderId,
      gender: product.gender,
      articleType: product.articleType
    };

    tagProfile(payload, (err) => {
      if (err) {
        console.log('error in tagging profile: ', err);
        return;
      }
      this.setState({
        tagged: true
      });
      if (this.props.tagItem) {
        this.props.tagItem();
      }
    });

    Madalytics.sendEvent('tag_size_profile', { entity_type: 'product', entity_id: product.id }, { custom: { v1: id } });
  }

  handleChange(target) {
    const { form } = this.state;
    form[target.name] = { ...form[target.name], value: target.value, error: '' };
    this.setState({
      form
    });
  }

  saveProfile() {
    const { form, allProfiles, gender } = this.state;
    if (this.validateDetails()) {
      const payload = {
        name: form.name.value,
        gender,
        userId: get(window, '__myx_session__.login')
      };
      this.setState({
        saveInProgess: true
      });

      addProfile(payload, (err, resp) => {
        if (err) {
          console.log('error in saving profile: ', err);
          this.setState({
            saveInProgess: false
          });
        } else {
          const profileData = ((JSON.parse(resp.body.text) || {}).data || [])[0];
          const newProfile = get(profileData, 'profileList.0');
          this.tagProfileToProduct(newProfile.pidx);
          this.setState({
            allProfiles: [...allProfiles, newProfile],
            selectedProfile: newProfile.pidx,
            saveInProgess: false,
            hideInfoCard: true,
            infoCardOpen: false
          });
          Madalytics.sendEvent('create_size_profile');
        }
      });
    }
  }

  validateDetails() {
    const { form } = this.state;

    if (isEmpty(form.name.value)) {
      form.name = { ...form.name, error: 'Name is required' };
      this.setState({
        form
      });
      return false;
    }

    return true;
  }

  openInfoCard() {
    this.setState({
      infoCardOpen: true
    });
  }

  hideInfoCard() {
    this.setState({
      infoCardOpen: false
    });
  }

  openSizeInfoPage() {
    window.location = `/my/sizingInfo?pidx=${this.state.selectedProfile}&mode=create&referrer=orders`;
  }

  render() {
    const moreProps = {};
    ['unisexProduct', 'onGenderClick', 'onProfileClick', 'saveProfile', 'openSizeInfoPage', 'hideInfoCard', 'handleChange']
      .forEach(prop => (moreProps[prop] = this[prop]));
    return (
      <div>
        {this.props.showProfileSuggestor && <Suggestor
          profiles={this.state.allProfiles}
          selectedProfile={this.state.selectedProfile}
          gender={this.state.gender}
          infoCardOpen={this.state.infoCardOpen}
          nameField={this.state.form.name}
          infoCardSaveDisplay={this.state.saveInProgess ? 'Saving...' : 'Save'}
          tagged={this.state.tagged}
          {...moreProps} />}
      </div>
    );
  }
}

SuggestorWrapper.propTypes = {
  allProfiles: PropTypes.array,
  item: PropTypes.object,
  orderId: PropTypes.string,
  showProfileSuggestor: PropTypes.bool,
  tagItem: PropTypes.func
};

export default SuggestorWrapper;
