import React from 'react';
import get from 'lodash/get';

import InfoCard from '../Reusables/Dialog';
import SVGImages from '../Reusables/SVGImages';
import ProfileCreationDialog from '../Reusables/ProfileCreationDialog';

import Styles from './profileSuggestor.css';
import classnames from 'classnames/bind';
import { sortUserProfiles } from './../../utils/pageHelpers';

const bindClass = classnames.bind(Styles);

class ProfileSuggestor extends React.PureComponent {
  render() {
    const { profiles, selectedProfile, onProfileClick, gender, unisexProduct, hideInfoCard, saveProfile, tagged } = this.props;
    const sortedProfiles = sortUserProfiles(gender, profiles);
    const selPr = sortedProfiles.find(profile => (profile.pidx === selectedProfile));
    return (
      <div className={Styles.block}>
        <div className={Styles.container}>
          <div className={Styles.arrowLine}>
            <div className={Styles.sideLine}></div>
            <div className={Styles.triangle}></div>
            <div className={Styles.sideLine}></div>
          </div>
          <div className={bindClass('recommendText', { hide: tagged })}>
            Myntra can recommend sizes for you at the time of purchase. Help Myntra learn more about you
          </div>
          <div className={bindClass('taggedAllMessage', { hide: !tagged })}>
            <div className={Styles.mfaLogo}><SVGImages name="mfaLogo" customStyle={{ height: '29px', width: '135px' }} /></div>
            <span className={Styles.tamHeading}>Thank you!</span>
            <span className={Styles.tamDesc}>We will use this information to recommend you the perfect size.</span>
            <div className={Styles.tamSeparator}></div>
          </div>
          <div className={Styles.suggHeading}>Who did you buy this for?</div>
          <div className={Styles.allProfiles}>{profiles.map(profile => (
            <div
              id={profile.pidx}
              className={bindClass('profile', {
                selected: profile.pidx === selectedProfile, disabled: !(profile.gender === gender || unisexProduct)
              })}
              data-disabled={!(profile.gender === gender || unisexProduct)}
              onClick={onProfileClick}>
              {profile.name}
            </div>)
          )}
            {profiles.length === 0 ? <div id="myself" className={Styles.profile} onClick={onProfileClick}>
              {get(window, '__myx_session__.userfirstname') || 'Myself'}
            </div> : null}
            <div id="others" className={Styles.profile} onClick={onProfileClick}>
              Others
            </div>
          </div>
          {(selPr && !selPr.complete) && <div className={Styles.moreDetails}>
            <div className={Styles.completeInfoText}>We don't have complete size information for <span style={{ color: '#282c3f' }}>{selPr.name}</span></div>
            <div className={Styles.addDetails} onClick={this.props.openSizeInfoPage}>Add details</div>
          </div>}
          {this.props.infoCardOpen &&
            <InfoCard
              title="Add Details"
              content={<ProfileCreationDialog {...this.props} Styles={Styles} />}
              customStyles={{ height: '252px' }}
              onOverlayClick={hideInfoCard}
              buttons={[{ displayName: 'Cancel', action: 'cancel' }, { displayName: this.props.infoCardSaveDisplay, action: 'save' }]}
              actions={{ cancel: hideInfoCard, save: saveProfile }} />}
        </div>
      </div>
    );
  }
}

export default ProfileSuggestor;
