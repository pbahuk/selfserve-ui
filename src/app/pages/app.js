import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from '../routes';
import header from '../components/Header/client';
import footer from '../components/Footer/client';

header();
footer();

ReactDOM.render(
  <Router history={browserHistory}>{routes}</Router>,
  document.getElementById('mountRoot')
);
