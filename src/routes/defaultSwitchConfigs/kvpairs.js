'use strict';
module.exports = {
	'product.emi.info': '{"minInterestRate":"12","maxMonths":"24"}',
	'msite.shutdown.message':
		'For a faster &amp; smoother browsing experience <br>Loved by 3+ million shoppers' + ' <br>',
	'halloffame.topbanner.right': '',
	'shipping.charges.sameDayDelivery': '1',
	'checkout.otpvalidation.count': '3',
	'checkout.otpvalidation.timerLimit': '{"minutes":"0","seconds":"30"}',
	'hrdr.pricereveal.data':
		'{"enable":"false","buttonlabel":"Sales starts at midnight","info":"Se' +
		'lect size to check if we have very few left"}',
	signupPoints:
		'{"imageFlag": "image1", "content":[{"heading":"UNLOCK FASHION SECRETS"' +
		',"subtext":"Get latest trends, fashion tips, personalised offers and much mor' +
		'e...", "imageClass":"fashionWoman"}]}',
	'globalstore.additionalmessage': 'Price inclusive of all import duties and shipping charges.',
	'mobile.shop.olapic.data':
		'{\r\n\t"src" : "//photorankstatics-a.akamaihd.net/81b03e40475846d5883661ff57b' +
		'34ece/static/frontend/latest/build.min.js",\r\n\t"data-instance" : "af363c43' +
		'67c24798167d53b8f45550fd",\r\n\t"data-apikey" : "545fdec92213c3948f549e59736' +
		'689bb33007cbb0ecd15184f6aefeab7800231",\r\n\t"enabled" : false\r\n}',
	'hof.tambola.hourlymessage': 'Submit all your patterns before 6:00 PM',
	'halloffame.stamptestimony.heading': 'A little thank you to the web',
	'mymyntra.freebie.return':
		'Your purchase of this item included a free gift; if you proceed both items will ' +
		'have to be returned together.',
	'shipping.trynbuy.max_items': '3',
	'mymyntra.freebie.cancelLine':
		'Your purchase of this item included a free gift; if you proceed both items will ' + 'be cancelled.',
	'mymyntra.tracking.statusMap': {
		ORDER_CREATION: 'Placed',
		PACK_IN_SOURCE_WAREHOUSE: 'Processing',
		PACK_IN_DISPATCH_WAREHOUSE: 'Packed',
		SHIP_TO_CUSTOMER: 'Shipped',
		DELIVERED_TO_CUSTOMER: 'Delivered',
		RECEIVED_IN_DISPATCH_WAREHOUSE: 'Packing',
		OUT_FOR_DELIVERY: 'Out for delivery',
		DISPATCH_FROM_SOURCE_WAREHOUSE: 'Processing',
		CANCELLED: 'Cancelled'
	},
	'disabledCouponServiceOnPDP.message': 'All Items are already at the best price.',
	quickBuyButtonText: '1-STEP BUY',
	'myntracredit.refundoffer.text': '',
	'myntracredit.refundoffer.config': { cod: { amount: '30', enabled: true }, prepaid: { amount: '30', enabled: true }},
	'mymyntra.creditRefundExperiment.config': {
		benefits: ['Immediate Refund', 'Faster Checkout'],
		defaultMyntraCredit: true,
		hideKnowMore: true
	},
	loginTimeout: '30',
	'checkout.qtydropdown.limit': 10,
	'mymyntra.item.statusMap': {
		RFR: 'Ready For Release',
		C: 'Delivered',
		PV: 'Ordered',
		D: 'Delivered',
		OH: 'On Hold',
		RTO: 'Returned To Origin',
		PP: 'Ordered',
		L: 'Lost',
		Q: 'Ordered',
		S: 'Shipped',
		WP: 'Work In Progress',
		PK: 'Packed',
		IC: 'Cancelled',
		DEC: 'Declined'
	},
	'myntraweb.tax.message': '{"enabled":"true","message":"Additional tax may apply; charged at checkou' + 't"}',
	'confirmation.addressType.enable': 'true',
	'mobile.pdp.olapic.data': '',
	'cart.config.message':
		'{"cart":{"enabled":"true","messages":[{"message":"10X Reward points o' +
		'n all spends with Kotak Visa Signature Credit Cards. Use coupon code KTK10X duri' +
		'ng checkout. TCA"},{"message":"Go Cashless & get Rs.100 OFF on online orders' +
		'. Use coupon ONLINE100 to avail. TCA."}],"color":"true"}}',
	'coupons.eors.message': '',
	telesalesNumber: '',
	customerSupportTime: '24x7',
	'mymyntra.orderwithlooks.number': '-1',
	'checkout.salebanner.data': '{"enable":"false","page":{"cart":"1","address":"1","payment":"1' + '"}}',
	'alteration.slot_booking_grace_time_mins': '10',
	'usp.sizedetailsrange': {
		male: {
			chest: [ 16, 70 ],
			height: [ 36, 96 ],
			waist: [ 15, 100 ],
			weight: [ 10, 300 ],
			footLength: [ 7.5, 13 ]
		},
		female: {
			footLength: [ 7.5, 13 ],
			waist: [ 13, 80 ],
			weight: [ 10, 300 ],
			bust: [ 13, 80 ],
			height: [ 36, 96 ],
			hips: [ 12, 100 ]
		}
	},
	'checkout.quantityLimit.check': 5,
	'hrdr.mymyntra.delayMessage': '',
	'usp.helpcontent': {
		bust_female: {
			video: 'https://youtube.com/embed/WArwa-cZ0o8?enablejsapi=1',
			detailText: 'Measure at the fullest part of your bust. Wrap it around to get the measurement.',
			title: 'How to measure your bust'
		},
		waist_female: {
			video: 'https://youtube.com/embed/543XmTR4WR0?enablejsapi=1',
			detailText:
				'Use the tape to circle your waist at your natural waistline, which is located ab' +
				'ove your belly button & below your rib cage.',
			title: 'How to measure your waist'
		},
		footLength_female: {
			video: 'https://youtube.com/embed/Eb7qPf_zV4Q?enablejsapi=1',
			detailText:
				'Stand on a plain surface with the surface edge & your foot against the wall. Mar' +
				'k the longest toe on the surface. Note the measurement from the surface edge to ' +
				'the mark.',
			title: 'How to measure your foot length'
		},
		footLength_male: {
			video: 'https://youtube.com/embed/DurvN2iLIHs?enablejsapi=1',
			detailText:
				'Stand on a hard surface with the surface edge & your foot against the wall. Mark' +
				' the longest toe on the surface. Note the measurement from the surface edge to t' +
				'he mark.',
			title: 'How to measure your foot length'
		},
		hips_female: {
			video: 'https://youtube.com/embed/PXOilP5Ix90?enablejsapi=1',
			detailText:
				'Put your feet together, and wrap the measuring tape straight and snug around the' +
				' widest part of your hips.',
			title: 'How to measure your hips'
		},
		chest_male: {
			video: 'https://youtube.com/embed/LQoTc2-hyJc?enablejsapi=1',
			detailText:
				'Place one end of the tape measure at the fullest part of your chest. Wrap it aro' +
				'und to get the measurement.',
			title: 'How to measure your chest'
		},
		waist_male: {
			video: 'https://youtube.com/embed/fgxk1vIiyoA?enablejsapi=1',
			detailText:
				'Use the tape to circle your waist at your natural waistline, which is located ab' +
				'ove your belly button & below your rib cage.',
			title: 'How to measure your waist'
		}
	},
	'globalstore.moreinfo': [
		'Price inclusive of all import duties and shipping charges.',
		'Products might not have MRP'
	],
	'topup.amount.options': [ '500', '1,500', '2,000', '2,500' ],
	'no.of.styles': '1,60,000',
	'topup.coupon.discount': {
		amounts: [ '500', '1,000', '1,500', '2,000', '2,500', '3,000' ],
		validCouponCodes: [ 'UPGRADE10' ],
		percent: 10,
		label: 'GET EXTRA'
	},
	'checkout.urgencyMetric.message': 'Is in {0}+ bags right now',
	'usp.layout': {
		male: [
			{
				displayName: 'Height',
				name: 'height',
				unit: 'inch'
			},
			{
				displayName: 'Weight',
				name: 'weight',
				unit: 'kg',
				extra: 'true'
			},
			{
				displayName: 'Chest',
				name: 'chest',
				unit: 'inch'
			},
			{
				displayName: 'Waist',
				name: 'waist',
				unit: 'inch'
			},
			{
				displayName: 'Foot Length',
				name: 'footLength',
				unit: 'cm'
			}
		],
		female: [
			{
				displayName: 'Height',
				name: 'height',
				unit: 'inch'
			},
			{
				displayName: 'Weight',
				name: 'weight',
				unit: 'kg',
				extra: 'true'
			},
			{
				displayName: 'Bust',
				name: 'bust',
				unit: 'inch'
			},
			{
				displayName: 'Waist',
				name: 'waist',
				unit: 'inch'
			},
			{
				displayName: 'Hips',
				name: 'hips',
				unit: 'inch'
			},
			{
				displayName: 'Foot Length',
				name: 'footLength',
				unit: 'cm'
			}
		]
	},
	'shipping.charges.cartlimit': '1199',
	smsPromoText: '',
	'disabledcouponserviceOnCart.message': 'All Items are already at the best price.',
	'halloffame.mail.subject': 'You are showcased on www.myntra.com',
	'payments.sellername': 'TITAN, BlueStone, Aditya Birla, Fossil India and Puma India.',
	'mobile.header.promotion.data':
		'{\r\n    "home": {\r\n        "offers": []\r\n    },\r\n    "list": {\r\n ' +
		'       "default": {\r\n            "title": "Get 20% Off* on orders above R' +
		's.1599 only on App | Download Now",\r\n            "label": "Shop Now",\r\n' +
		'            "url": "/shopping-offer16"\r\n        },\r\n        "url-specif' +
		'ic": {\r\n            "minimum-rs-1000-off": [\r\n                {\r\n      ' +
		'              "title": "Get Minimum Rs 1000 Off* - Shop Now"\r\n            ' +
		'    }\r\n            ],\r\n            "bogo-on-sport-and-casual-wear": [\r\n ' +
		'               {\r\n                    "title": "Buy One Get One - Exclusive' +
		' Offers On Sports-Wear And More"\r\n                }\r\n            ],\r\n    ' +
		'        "men-formal-shirts": [\r\n                {\r\n                    "t' +
		'itle": "Shirts made with World\'s best Cotton",\r\n                    "label' +
		'": "Shop Now",\r\n                    "url": "/supima"\r\n               ' +
		' }\r\n            ],\r\n            "men-formal-trousers": [\r\n              ' +
		'  {\r\n                    "title": "Shirts made with World\'s best Cotton",' +
		'\r\n                    "label": "Shop Now",\r\n                    "url":' +
		' "/supima"\r\n                }\r\n            ],\r\n            "sports-foot' +
		'wear-01": [\r\n                {\r\n                    "title": "Click for ' +
		'Offer",\r\n                    "label": "Shop Now",\r\n                    ' +
		'"url": "/u/PUMA"\r\n                }\r\n            ],\r\n            "sup' +
		'ima": [\r\n                {\r\n                    "title": "Shirts made wi' +
		'th World\'s best cotton"\r\n                }\r\n            ],\r\n            ' +
		'"may-new-arrivals": [\r\n                {\r\n                    "title": ' +
		'"The Jun Catalogue : 1500+ New Styles"\r\n                }\r\n            ],' +
		'\r\n            "puma-brand-day-sale-items": [\r\n                {\r\n       ' +
		'             "title": "#3000likes3000likes, go like the products & get the pr' +
		'ice down!"\r\n                }\r\n            ],\r\n            "aqua-trend"' +
		': [\r\n                {\r\n                    "title": "Aqua : Dive in Summ' +
		'er Colours"\r\n                }\r\n            ],\r\n            "wedding-sto' +
		're": [\r\n                {\r\n                    "title": "Explore The Wed' +
		'ding Store"\r\n                }\r\n            ],\r\n            "shopping-of' +
		'fer15": [\r\n                {\r\n                    "title": "Get 30% Off*' +
		' on orders above Rs.999"\r\n                }\r\n            ],\r\n            ' +
		'"premium-brands-collection": [\r\n                {\r\n                    "t' +
		'itle": "Buy MERCHANDISE worth at least Rs. 4,999 and get 1 Mad Love Women Cora' +
		'l Orange Lydia Flat Shoes",\r\n                    "desc": ""\r\n          ' +
		'      }\r\n            ],\r\n            "roadster-vintage-offer": [\r\n      ' +
		'          {\r\n                    "title": "Code for the Road - Flat 55% Off' +
		'*, Shop Now | Only Today"\r\n                }\r\n            ],\r\n           ' +
		' "flat-30-sale": [\r\n                {\r\n                    "title": "Fl' +
		'at 30% Off* | Shop Now"\r\n                }\r\n            ],\r\n            ' +
		'"flat-40-sale": [\r\n                {\r\n                    "title": "Fla' +
		't 40% Off* | Shop Now"\r\n                }\r\n            ],\r\n            "' +
		'flat-50-sale": [\r\n                {\r\n                    "title": "Flat ' +
		'50% Off* | Shop Now"\r\n                }\r\n            ],\r\n            "fl' +
		'at-60-sale": [\r\n                {\r\n                    "title": "Flat 60' +
		'% Off* | Shop Now"\r\n                }\r\n            ],\r\n            "flat' +
		'-70-sale": [\r\n                {\r\n                    "title": "Flat 70% ' +
		'Off* | Shop Now"\r\n                }\r\n            ],\r\n            "nike-n' +
		'tk-cricket-jersey": [\r\n                {\r\n                    "title": "' +
		'Wear your pride, with merchandise from The Stadium"\r\n                }\r\n   ' +
		'         ],\r\n            "stadium-dressing-room": [\r\n                {\r\n' +
		'                    "title": "Wear your pride, with merchandise from The Stad' +
		'ium"\r\n                }\r\n            ],\r\n            "cricket-performanc' +
		'e-wear": [\r\n                {\r\n                    "title": "Wear your p' +
		'ride, with merchandise from The Stadium"\r\n                }\r\n            ],' +
		'\r\n            "nike-jersey-look-women": [\r\n                {\r\n          ' +
		'          "title": "Wear your pride, with merchandise from The Stadium"\r\n ' +
		'               }\r\n            ],\r\n            "nike-main-jersey": [\r\n   ' +
		'             {\r\n                    "title": "Wear your pride, with merchan' +
		'dise from The Stadium"\r\n                }\r\n            ],\r\n            "' +
		'hrx": [\r\n                {\r\n                    "title": "HRX Signature ' +
		'line by Hrithik Roshan. Classy. Evolved. Sleek. Just like the man himself",\r\n' +
		'                    "url": "/hrx-signature"\r\n                }\r\n        ' +
		'    ],\r\n            "hrx-signature ": [\r\n                {\r\n            ' +
		'        "title": "HRX Signature line by Hrithik Roshan. Classy. Evolved. Slee' +
		'k. Just like the man himself"\r\n                }\r\n            ],\r\n       ' +
		'     "sportsSocks-Caps-Etc": [\r\n                {\r\n                    "t' +
		'itle": "Run when you can, walk if you have to, crawl if you must, just never g' +
		'ive up – Dean Karnazes"\r\n                }\r\n            ],\r\n            ' +
		'"running-gear-for-men": [\r\n                {\r\n                    "title' +
		'": "Run Fast, Run Slow. Run Far, Run Close. Just Run."\r\n                }\r' +
		'\n            ],\r\n            "running-gear-for-women": [\r\n               ' +
		' {\r\n                    "title": "Run Fast, Run Slow. Run Far, Run Close. J' +
		'ust Run."\r\n                }\r\n            ],\r\n            "walking-gear-' +
		'for-men": [\r\n                {\r\n                    "title": "Run when y' +
		'ou can, walk if you have to, crawl if you must, just never give up – Dean Karnaz' +
		'es"\r\n                }\r\n            ],\r\n            "walking-gear-for-wo' +
		'men": [\r\n                {\r\n                    "title": "Run when you c' +
		'an, walk if you have to, crawl if you must, just never give up – Dean Karnazes"' +
		'\r\n                }\r\n            ],\r\n            "jogging-gear-for-men":' +
		' [\r\n                {\r\n                    "title": "Run Fast, Run Slow. ' +
		'Run Far, Run Close. Just Run."\r\n                }\r\n            ],\r\n      ' +
		'      "jogging-gear-for-women": [\r\n                {\r\n                    ' +
		'"title": "Run Fast, Run Slow. Run Far, Run Close. Just Run."\r\n            ' +
		'    }\r\n            ],\r\n            "running-store-running-gear": [\r\n    ' +
		'            {\r\n                    "title": "Run Fast, Run Slow. Run Far, R' +
		'un Close. Just Run."\r\n                }\r\n            ],\r\n            "ru' +
		'nning-store-walking-gear": [\r\n                {\r\n                    "titl' +
		'e": "Run Fast, Run Slow. Run Far, Run Close. Just Run."\r\n                }' +
		'\r\n            ],\r\n            "running-store-jogging-gear": [\r\n         ' +
		'       {\r\n                    "title": "Run Fast, Run Slow. Run Far, Run Cl' +
		'ose. Just Run."\r\n                }\r\n            ],\r\n            "adidas-' +
		'boost": [\r\n                {\r\n                    "title": "Run Fast, Ru' +
		'n Slow. Run Far, Run Close. Just Run."\r\n                }\r\n            ],\r' +
		'\n            "sports-watches": [\r\n                {\r\n                    ' +
		'"title": "Believe you can, and you are halfway there"\r\n                }\r' +
		'\n            ],\r\n            "air-max": [\r\n                {\r\n         ' +
		'           "title": "Run Fast, Run Slow. Run Far, Run Close. Just Run."\r\n ' +
		'               }\r\n            ],\r\n            "reebok-z": [\r\n           ' +
		'     {\r\n                    "title": "Run Fast, Run Slow. Run Far, Run Clos' +
		'e. Just Run."\r\n                }\r\n            ],\r\n            "denim-men' +
		'-college": [\r\n                {\r\n                    "title": "Stay coll' +
		'ege cool in slim-fit washed denims"\r\n                }\r\n            ],\r\n ' +
		'           "denim-men-work": [\r\n                {\r\n                    "t' +
		'itle": "Office calls for dark slim-fit denims"\r\n                }\r\n      ' +
		'      ],\r\n            "denim-men-party": [\r\n                {\r\n         ' +
		'           "title": "Dress it up for parties in textured skin fit denims"\r' +
		'\n                }\r\n            ],\r\n            "denim-men-date": [\r\n  ' +
		'              {\r\n                    "title": "Date nights get hotter with ' +
		'high on fade skin fit denims"\r\n                }\r\n            ],\r\n       ' +
		'     "denim-men-weekend": [\r\n                {\r\n                    "titl' +
		'e": "Kick-start your weekends with rugged / ripped straight cut denims"\r\n  ' +
		'              }\r\n            ],\r\n            "denim-men-travel": [\r\n    ' +
		'            {\r\n                    "title": "Get travel ready in comfortabl' +
		'e solid straight cut denims"\r\n                }\r\n            ],\r\n        ' +
		'    "denim-women-jeggings": [\r\n                {\r\n                    "ti' +
		'tle": "Jeggings are the all time college favourites"\r\n                }\r\n' +
		'            ],\r\n            "denim-work-women": [\r\n                {\r\n  ' +
		'                  "title": "Light-fade slim fit denims are always work-ready' +
		'"\r\n                }\r\n            ],\r\n            "denim-women-party": ' +
		'[\r\n                {\r\n                    "title": "Turn up the heat at p' +
		'arties with dark skinny denims"\r\n                }\r\n            ],\r\n     ' +
		'       "denim-date-women": [\r\n                {\r\n                    "tit' +
		'le": "Heavy fade skinny denims make date nights hotter"\r\n                }' +
		'\r\n            ],\r\n            "denim-weekend-women": [\r\n                ' +
		'{\r\n                    "title": "Ripped and high fade denims are the weeken' +
		'd staples"\r\n                }\r\n            ],\r\n            "denim-travel' +
		'-women": [\r\n                {\r\n                    "title": "Travel in s' +
		'tyle with straight cut high-fade denims"\r\n                }\r\n            ],' +
		'\r\n            "fw15-sneak-peek": [\r\n                {\r\n                 ' +
		'   "title": "Be trendy with our latest collections"\r\n                }\r\n' +
		'            ],\r\n            "under-299-store": [\r\n                {\r\n   ' +
		'                 "title": "Explore our Budget Store | Under Rs 299 styles - S' +
		'hop Now"\r\n                }\r\n            ],\r\n            "under-399-stor' +
		'e": [\r\n                {\r\n                    "title": "Explore our Budg' +
		'et Store | Under Rs 399 styles - Shop Now"\r\n                }\r\n            ' +
		'],\r\n            "under-499-store": [\r\n                {\r\n               ' +
		'     "title": "Explore our Budget Store | Under Rs 499 styles - Shop Now"\r' +
		'\n                }\r\n            ],\r\n            "under-599-store": [\r\n ' +
		'               {\r\n                    "title": "Explore our Budget Store | ' +
		'Under Rs 599 styles - Shop Now"\r\n                }\r\n            ],\r\n     ' +
		'       "under-699-store": [\r\n                {\r\n                    "titl' +
		'e": "Explore our Budget Store | Under Rs 699 styles - Shop Now"\r\n          ' +
		'      }\r\n            ],\r\n            "under-799-store": [\r\n             ' +
		'   {\r\n                    "title": "Explore our Budget Store | Under Rs 799' +
		' styles - Shop Now"\r\n                }\r\n            ],\r\n            "und' +
		'er-899-store": [\r\n                {\r\n                    "title": "Explo' +
		're our Budget Store | Under Rs 899 styles - Shop Now"\r\n                }\r\n ' +
		'           ],\r\n            "under-999-store": [\r\n                {\r\n    ' +
		'                "title": "Explore our Budget Store | Under Rs 999 styles - Sh' +
		'op Now"\r\n                }\r\n            ],\r\n            "under-1299-stor' +
		'e": [\r\n                {\r\n                    "title": "Explore our Budg' +
		'et Store | Under Rs 1299 styles - Shop Now"\r\n                }\r\n           ' +
		' ],\r\n            "under-1499-store": [\r\n                {\r\n             ' +
		'       "title": "Explore our Budget Store | Under Rs 1499 styles - Shop Now"' +
		'\r\n                }\r\n            ],\r\n            "shopping-offer19": [\r' +
		'\n                {\r\n                    "title": "Extra 32% Off; 9 AM - 12' +
		'PM"\r\n                }\r\n            ],\r\n            "shopping-offer20":' +
		' [\r\n                {\r\n                    "title": "Extra 32% Off; 12 PM' +
		' - 3 PM"\r\n                }\r\n            ],\r\n            "shopping-offer' +
		'21": [\r\n                {\r\n                    "title": "Extra 32% Off; ' +
		'3 PM - 6 PM"\r\n                }\r\n            ],\r\n            "shopping-o' +
		'ffer22": [\r\n                {\r\n                    "title": "Extra 32% O' +
		'ff; 6 PM - 10 PM"\r\n                }\r\n            ],\r\n            "shopp' +
		'ing-offer23": [\r\n                {\r\n                    "title": "Extra ' +
		'32% Off. Use Coupon:HH22R"\r\n                }\r\n            ],\r\n          ' +
		'  "shopping-offer24": [\r\n                {\r\n                    "title":' +
		' "Extra 32% Off. Use Coupon:HH22A"\r\n                }\r\n            ],\r\n ' +
		'           "shopping-offer17": [\r\n                {\r\n                    ' +
		'"title": "Get 30% Off on orders above Rs.999"\r\n                }\r\n      ' +
		'      ],\r\n            "all-about-you": [\r\n                {\r\n           ' +
		'         "title": "Everyday find something new and make it ALL ABOUT YOU"\r' +
		'\n                }\r\n            ],\r\n            "all-about-you-launch": [' +
		'\r\n                {\r\n                    "title": "Everyday find somethin' +
		'g new and make it ALL ABOUT YOU"\r\n                }\r\n            ],\r\n    ' +
		'        "partly-cloudy-women": [\r\n                {\r\n                    ' +
		'"title": "Turn gloomy days bright in style"\r\n                }\r\n        ' +
		'    ]\r\n        }\r\n    }\r\n}',
	signupPoints2:
		'{"imageFlag": "image0", "content":[{"heading":"Track your orders","su' +
		'btext":"Get upto date information about your order\'s location", "imageClass' +
		'":"indiaMap"}]}',
	'pickup.disabled.articletypes': '138,141,151,153,257,353,361,459,460,461,462,463',
	'app.ios.dlink':
		'http://ad.apsalar.com/api/v1/ad?re=0&st=687711906604&h=1f2b9e36f2083b55df9843105' + '94e479adb75adb5',
	customerSupportNewNumber: '+91-80-61561999',
	'checkout.earlybird.message': '{0} left at this discount',
	SuppliedBy_Partner_Tooltip:
		'This product is supplied by our partner after an order is placed, hence delivery' +
		' time for this product is higher. We deliver it to you only after performing ext' +
		'ensive quality checks on the product to ensure consistent experience.',
	listPageCouponsViews: '{}',
	'pdp.offers.visible': 'EMI',
	'hrdr.shipping.message': 'Delivery might be delayed due to high order volume.',
	'mymyntra.alpha.sellers': [ 1, 19, 21, 25, 29, 30, 32 ],
	'nps.singlePageSurveys': 'SHIPMENT_DELIVERY, SHIPMENT_DELAYED_DELIVERY',
	'mymyntra.return.statusMap': {
		DLC: {
			message:
				'Your return request has been declined as it did not meet our return policy. The ' +
				'item will be shipped back to you.',
			name: 'Reshipped to You'
		},
		RFR: {
			message: 'Your return request placed. Please check your email for details.',
			name: 'Return Request Placed'
		},
		SHS: {
			message:
				'Your refund for the return has been processed successfully. Please check your em' +
				'ail for refund details.',
			name: 'Refund Processed'
		},
		D: {
			message: 'Your return request has been declined.',
			name: 'Return Request Declined'
		},
		RRC: {
			message: 'Your return has been processed successfully. Please check your email for refund ' + 'details.',
			name: 'Refund Processed'
		},
		L: {
			message:
				'Your refund for the return has been processed successfully. Please check your em' +
				'ail for refund details.',
			name: 'Refund Processed'
		},
		Q: {
			message: 'Your return request placed. Please check your email for details.',
			name: 'Return Request Placed'
		},
		RRSH: {
			message:
				'Your return request has been declined as it did not meet our return policy. The ' +
				'item will be shipped back to you.',
			name: 'Reshipped to You'
		},
		RPC: {
			message: 'Your return has been processed successfully. Please check your email for refund ' + 'details.',
			name: 'Refund Processed'
		},
		SHC: {
			message:
				'Your return request has been declined as it did not meet our return policy. The ' +
				'item will be shipped back to you.',
			name: 'Reshipped to You'
		},
		LPI: {
			message: 'Your return request placed. Please check your email for details.',
			name: 'Return Request Placed'
		},
		RL: {
			message:
				'Your return has been picked up/received successfully. Please check your email fo' +
				'r refund details.',
			name: 'Return Picked Up/Received'
		},
		DEC: {
			message: 'Your return request has been declined.',
			name: 'Return Request Declined'
		},
		DLS: {
			message:
				'Your refund for the return has been processed successfully. Please check your em' +
				'ail for refund details.',
			name: 'Refund Processed'
		}
	},
	'shipping.cutOffTime.sameDayDelivery': '11',
	'shipping.cutOffTime.nextDayDelivery': '19',
	'automated.captcha.verification.user':
		'portal-automation@myntra.com,mobile-automation@myntra.com,mobile-automation3@myn' +
		'tra.com,pdp-automation@myntra.com,perf@myntra.com,androidapp-automation@myntra.c' +
		'om,portal-ui-automation@myntra.com,iosapp-automation@myntra.com,paralleluser1@my' +
		'ntra.com,paralleluser2@myntra.com,paralleluser3@myntra.com,paralleluser4@myntra.' +
		'com,paralleluser5@myntra.com,paralleluser6@myntra.com,paralleluser7@myntra.com,p' +
		'aralleluser8@myntra.com,paralleluser9@myntra.com,paralleluser10@myntra.com,paral' +
		'leluser11@myntra.com,paralleluser12@myntra.com,paralleluser13@myntra.com,paralle' +
		'luser14@myntra.com,paralleluser15@myntra.com,paralleluser16@myntra.com,parallelu' +
		'ser17@myntra.com,paralleluser18@myntra.com,paralleluser19@myntra.com,paralleluse' +
		'r20@myntra.com,windowsapp-automation@myntra.com,sanityandroid@myntra.com,sanitys' +
		'uite@myntra.com,sanityandroidsuite@myntra.com,androidapp-sanity@myntra.com,myntr' +
		'a-androidsanity@myntra.com,myntra-androidsanity1@myntra.com,myntra-androidsanity' +
		'2@myntra.com,myntra-androidsanity3@myntra.com,paralleluser21@myntra.com,parallel' +
		'user22@myntra.com,paralleluser23@myntra.com,myntra-androidsanity4@myntra.com,and' +
		'roid-ui-automation@myntra.com,myntraandroidsanity5@myntra.com,mobile_single_serv' +
		'er@myntra.com',
	'mymyntra.freebie.cancelRelease':
		'Some items in this shipment included free gifts; if you proceed all such gifts w' + 'ill also be cancelled.',
	'shipping.trynbuy.min_amount': '1199',
	Signup_Offers_Text_Desktop:
		'You have got ..<strong> Flat 30% discount coupon </strong> in your account. Appl' +
		'y it in your shopping bag',
	'topup.maximum.amount': '10000',
	'halloffame.header.links':
		'[{"link":"faq","url":" https://www.myntra.com/faqs"}, {"link":"Get AP' +
		'K","url":"https://cdn.myntassets.com/installapp/instructions.html"}]',
	extraDevices:
		'{"MOTOE2":"XT1021","NOTE":"1W","honor6":"H60-L04","LenevoZ2":"X2-' +
		'AP","Yota":"C9660","HonorHolly":"U19","Alcatel":"KOT49H","HTC":"' +
		'M9PLUS","ZENFON1":"ASUS_Z00AD","ZENFON2":"ASUS_Z008D","SAMSUNG-NOTE":' +
		'"SM-T561","MOTOG3":"MOTOG3","Android":"MyntraRetailAndroid","iPhone"' +
		':"MyntraRetailiPhone"}',
	'topup.minimun.amount': '100',
	'halloffame.image.url': 'https://cdn.myntassets.com/assets/banners/home/2015/jul/11/desktop-bb-weekend.jp' + 'g',
	'halloffame.mail.html': 'Congratulations! Your message has been showcased on www.myntra.com',
	'topup.notification': '',
	'desktop.pdp.olapic.data': '',
	'mymyntra.recos.messages':
		'{\r\n   "recos_heading": "Handpicked for you",\r\n   "recos_subheadig": "' +
		'Styles we know you like, and some we want you to try",\r\n   "recos_subheading' +
		'_text": "",\r\n   "recos_recent_title": "Find the perfect match for",\r\n' +
		'   "recos_recent_bought": "YOU RECENTLY BOUGHT",\r\n   "recos_recent_subtit' +
		'le": "Expand your wardrobe with these matching items for your recent purchase' +
		'",\r\n   "blank_state_title": "We are still trying to figure out your style ' +
		'profile. In the meantime, here are some ideas for men & women",\r\n   "blank_s' +
		'tate_subheading": "Styles we know you like, and some we want you to try",\r\n' +
		'   "blank_state_subtitle": "",\r\n   "recos_footer_quote": "\'You can have' +
		' anything you want in life if you dress for it\'",\r\n   "recos_footer_author"' +
		': "Edith Head",\r\n   "not_logged_in": "You need to log in to view recommen' +
		'dations for you.\'",\r\n   "disabled": "This feature has been disabled",\r\n' +
		'   "not_an_app": "Please download app to view recommendation.",\r\n   "erro' +
		'r_title": "Oops! something went wrong.",\r\n   "error_subtitle": "please t' +
		'ry again after sometime"\r\n}',
	'alteration.holiday.slot_booking_lead_time_hrs': '48',
	'payments.cardoffer.message': 'To use PhonePe wallet, select PhonePe payment option below.',
	'mymyntra.features.unavailable.message': '',
	vatFilterConfiguration:
		'{\r\n\t"inclusion": {\r\n\t\t"article_types": {\r\n\t\t\t"92": {\r\n\t\t\t' +
		'\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"93": {\r\n' +
		'\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"94":' +
		' {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t' +
		'"95": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n' +
		'\t\t\t"127": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t' +
		'\t},\r\n\t\t\t"279": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19' +
		'\r\n\t\t\t},\r\n\t\t\t"280": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshol' +
		'd": 19\r\n\t\t\t},\r\n\t\t\t"283": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"t' +
		'hreshold": 19\r\n\t\t\t},\r\n\t\t\t"434": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t' +
		'\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"43": {\r\n\t\t\t\t"vat": 14.5,\r' +
		'\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"44": {\r\n\t\t\t\t"vat": 1' +
		'4.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"45": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"46": {\r\n\t\t\t' +
		'\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"49": {\r\n' +
		'\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"50":' +
		' {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t' +
		'"51": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n' +
		'\t\t\t"52": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t' +
		'},\r\n\t\t\t"54": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n' +
		'\t\t\t},\r\n\t\t\t"56": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": ' +
		'19\r\n\t\t\t},\r\n\t\t\t"59": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"thresho' +
		'ld": 19\r\n\t\t\t},\r\n\t\t\t"61": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"t' +
		'hreshold": 19\r\n\t\t\t},\r\n\t\t\t"62": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t' +
		'\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"63": {\r\n\t\t\t\t"vat": 14.5,\r' +
		'\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"64": {\r\n\t\t\t\t"vat": 1' +
		'4.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"65": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"66": {\r\n\t\t\t' +
		'\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"105": {\r' +
		'\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"107' +
		'": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t' +
		'\t"111": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},' +
		'\r\n\t\t\t"12.50": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r' +
		'\n\t\t\t},\r\n\t\t\t"12.51": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshol' +
		'd": 19\r\n\t\t\t},\r\n\t\t\t"12.59": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t' +
		'"threshold": 19\r\n\t\t\t},\r\n\t\t\t"130": {\r\n\t\t\t\t"vat": 14.5,\r\n' +
		'\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"132": {\r\n\t\t\t\t"vat": 14' +
		'.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"148": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"149": {\r\n\t\t' +
		'\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"155": {' +
		'\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"1' +
		'56": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t' +
		'\t\t"158": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t}' +
		',\r\n\t\t\t"164": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n' +
		'\t\t\t},\r\n\t\t\t"167": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold":' +
		' 19\r\n\t\t\t},\r\n\t\t\t"168": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"thres' +
		'hold": 19\r\n\t\t\t},\r\n\t\t\t"170": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t' +
		'"threshold": 19\r\n\t\t\t},\r\n\t\t\t"171": {\r\n\t\t\t\t"vat": 14.5,\r\n' +
		'\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"172": {\r\n\t\t\t\t"vat": 14' +
		'.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"177": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"178": {\r\n\t\t' +
		'\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"185": {' +
		'\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"1' +
		'86": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t' +
		'\t\t"187": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t}' +
		',\r\n\t\t\t"205": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n' +
		'\t\t\t},\r\n\t\t\t"206": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold":' +
		' 19\r\n\t\t\t},\r\n\t\t\t"209": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"thres' +
		'hold": 19\r\n\t\t\t},\r\n\t\t\t"229": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t' +
		'"threshold": 19\r\n\t\t\t},\r\n\t\t\t"230": {\r\n\t\t\t\t"vat": 14.5,\r\n' +
		'\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"231": {\r\n\t\t\t\t"vat": 14' +
		'.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"232": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"233": {\r\n\t\t' +
		'\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"252": {' +
		'\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"2' +
		'84": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t' +
		'\t\t"308": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t}' +
		',\r\n\t\t\t"314": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n' +
		'\t\t\t},\r\n\t\t\t"346": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold":' +
		' 19\r\n\t\t\t},\r\n\t\t\t"350": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"thres' +
		'hold": 19\r\n\t\t\t},\r\n\t\t\t"369": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t' +
		'"threshold": 19\r\n\t\t\t},\r\n\t\t\t"375": {\r\n\t\t\t\t"vat": 14.5,\r\n' +
		'\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"376": {\r\n\t\t\t\t"vat": 14' +
		'.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"380": {\r\n\t\t\t\t"va' +
		't": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"382": {\r\n\t\t' +
		'\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"395": {' +
		'\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"4' +
		'16": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t' +
		'\t\t"421": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t}' +
		',\r\n\t\t\t"425": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n' +
		'\t\t\t},\r\n\t\t\t"426": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold":' +
		' 19\r\n\t\t\t},\r\n\t\t\t"427": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"thres' +
		'hold": 19\r\n\t\t\t},\r\n\t\t\t"428": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t' +
		'"threshold": 19\r\n\t\t\t},\r\n\t\t\t"429": {\r\n\t\t\t\t"vat": 14.5,\r\n' +
		'\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"436": {\r\n\t\t\t\t"vat": 14' +
		'.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"47": {\r\n\t\t\t\t"vat' +
		'": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"48": {\r\n\t\t\t' +
		'\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"60": {\r\n' +
		'\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t"169"' +
		': {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r\n\t\t\t' +
		'"211": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t\t\t},\r' +
		'\n\t\t\t"202": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 19\r\n\t' +
		'\t\t},\r\n\t\t\t"441": {\r\n\t\t\t\t"vat": 14.5,\r\n\t\t\t\t"threshold": 1' +
		'9\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}',
	'mymyntra.gst.transitionDate': '2016-06-19T12:00:00+05:30',
	'sale.futurediscount.label': 'PRICE AFTER 24th Oct',
	'checkout.mfu.info': '1 point = 1 Rupee',
	msg: {
		introUrl: 'https://www.myntra.com/shop/msg-faq',
		rewards: {
			postCampaignDescription: 'Rewards unlocked by you',
			description: 'Check out your rewards and enjoy them for the next 6 months!',
			postCampaignLogoUrl:
				'https://assets.myntassets.com/assets/images/lookbook/2017/6/3/11496467576365-MSG' + '-unit.png',
			noRewardDataText: 'No rewards yet :( Please try again later!',
			noRewardCondolenceText: 'No rewards have been unlocked by you as of now :('
		},
		join: {
			seoDescription:
				"Hey! Join my Myntra shopping group & let's shop together to win awesome offers &" +
				' special discounts',
			seoImage: 'https://assets.myntassets.com/assets/images/lookbook/2017/6/3/11496467576365-MSG' + '-unit.png',
			shareMessage:
				"Hey! Join my Myntra shopping group & let's shop together to win awesome offers &" +
				' special discounts',
			seoTitle: 'Invite People'
		},
		'checkout.mfu.info': '',
		dashboard: {
			enableLeaderboardLink: false,
			enableMemberStats: false,
			viewRewardsUrl: '/my/rewards',
			inactiveMessage: 'Minimum 2 members needed to activate group'
		},
		logoUrl: 'https://assets.myntassets.com/assets/images/lookbook/2017/6/3/11496467576365-MSG' + '-unit.png',
		leaderboard: {
			disableCreateGroupAction: true,
			description:
				'Shop with your friends and family to claim bigger rewards! Create your shopping ' + 'group now!',
			disabledTitle: 'OOPS! \n\n Myntra Shopping Group invites have expired now',
			seoImage: 'https://assets.myntassets.com/assets/images/lookbook/2017/6/3/11496467576365-MSG' + '-unit.png',
			seoTitle: 'Share my group',
			enabled: false,
			multiplier: 1,
			disabledText:
				'You can however enjoy your favourite styles \n\n on the best of discounts \n\n a' +
				'nd definitely join a group the next time it’s live! \n',
			seoDescription:
				'My shopping group is all set to shop & win rewards. Have you created a Myntra Sh' + 'opping Group yet!'
		}
	},
	'automated.captcha.verification.captchacode': 'testcheckout',
	'header.mymyntra.menuitem':
		'{\r\n"title": "Refer a Friend",\r\n"desc": "and earn upto Rs. 1500*",\r' +
		'\n"link": "/mymyntra.php?view=myreferrals"\r\n}',
	confMessages:
		'[{"message":"New Arrivals","color":"#FF8787","href":"/new?regV2=1"},' +
		'{"message":"Apparel","color":"#FFADD7","href":"/apparel?regV2=2"},{' +
		'"message":"Footwear","color":"#6DD9f3","href":"/footwear?regV2=3"},{' +
		'"message":"Sale - 50% Off","color":"#B8D45d","href":"/hello-sale?regV' +
		'2=4"}]',
	'header.promotion.data':
		'{\r\n    "code": "App only",\r\n    "text": "Get 20% Off* on orders above' +
		' Rs.1599 only on App | Download Now",\r\n    "link": "/mobile/apps",\r\n   ' +
		' "background-color": "#857D1E"\r\n}',
	'myntraweb.promoheader.data':
		'{"data":{"children":[{"children":[{"children":[],"props":{"title":"' +
		'Gift Cards","url":"/giftcard","meta": {"new":true,"disable":false}}},' +
		'{"children":[],"props":{"title":"Track Orders","url":"/my/orders","' +
		'meta": {"new":false,"disable":false}}},{"children":[],"props":{"title' +
		'":"Contact Us","url":"/contactus","meta": {"new":false,"disable":fa' +
		'lse}}}],"props":{"title":"block","meta": {"mobile":false}}}],"props"' +
		':{"title":"giftCard.links"}}}',
	'app.windows.dlink':
		'http://www.windowsphone.com/en-us/store/app/myntra-india-fashion-store/a92e3afb-' +
		'c791-4624-86da-8624f91cfbc1',
	filter_article_widget_atsa:
		'{"sizes_facet":[["Sweaters","Sweatshirts","Jackets"],["Jodhpuri Pants"' +
		',"Kurta Pyjama","Kurta Sets","Kurtas"],["Trunk","Briefs"],["Night sui' +
		'ts","Bath Robe","Lounge Pants","Lounge Set","Lounge Shorts","Pyjamas"' +
		',"Sleep Shorts"],["Sandals","Sports Sandals"],["Smart Watches","Fitness' +
		' Bands","Watches"],["Sunglasses","Frames"],["Backpacks","Bag","Card ' +
		'Holder","Laptop Bag","Messenger Bag"],["Duffel Bag","Luggage Bags","Ru' +
		'cksacks","Trolley Bag"],["Cufflinks","Pocket Squares","Suspenders","Ti' +
		'e Cufflink & Pocket Square Set","Ties"],["Caps","Hat"],["Mobile Pouch",' +
		'"Mobile Accessories"],["Churidar Kurta","Kurta Sets","Kurta and Dupatta"' +
		',"Kurtas","Patiala Kurta Set","Salwar Suit"],["Kurtis","Tops","Tunics' +
		'"],["Churidar","Churidar and Dupatta","Leggings","Patiala","Patiala an' +
		'd Dupatta","Salwar","Salwar and Dupatta"],["Skirts","Trousers"],["Trad' +
		'itional Sari","Embellished Saree","Fashion Sari","Partywear Sari","Print' +
		'ed Sari","Saree Blouse"],["Dupatta","Shawl"],["Jackets","Waistcoat"],' +
		'["Dresses","Jumpsuit","Dungarees"],["Jeans","Jeggings"],["Shorts","' +
		'Skirts"],["Blazers","Coats"],["Sports Bra","Bra","Lingerie Set","Lin' +
		'gerie accessories"],["Thermal Tops","Bodysuit","Camisoles","Thermal Bott' +
		'oms"],["Bath Robe","Swimwear","Swimwear Accessories"],["Flip Flops","C' +
		'asual Shoes","Flats"],["Sports Shoes","Sports Sandals"],["Shapewear","' +
		'Shaper Brief"],["Wallets","Backpacks","Bag","Card Holder","Clutches",' +
		'"Handbags","Laptop Bag","Messenger Bag","Tablet Sleeve","Tote Bags"],[' +
		' "Track Pants","Tracksuits" ],["Sports Shoes","Casual Shoes","Formal Sh' +
		'oes"]]}',
	'nps.npsSinglePageSurveys': 'SHIPMENT_DELIVERY, SHIPMENT_DELAYED_DELIVERY, RETURN_PROCESS_COMPLETION',
	sessionExpirySecs: '604800',
	paymentsFailureAttempts: '2',
	'pwa.routes.external': [ 'wishlist', 'shoppinggroups', 'checkout' ],
	'login.secureSessionExpireMessage': 'Your session expired. Please login again.',
	'shipping.charges.nextDayDelivery': '1',
	'faqs.returnAddresses':
		' ["You can send the return to any one of the following addresses depending on y' +
		'our location:","1. **South**\\n2. **Vector E commerce Pvt Ltd**\\n3. Khata No ' +
		'- 20/18/15/113-1,\\n4. Survey No - 113,\\n5. Krishna Reddy Industrial Area,\\n6.' +
		' 7th Mile, Kudlu Gate, Off Hosur Road,\\n7. Bangalore- 560068","1. **North**"' +
		' +\n2. **Vector E-Commerce Pvt Ltd.**\\n3. Dev Niwas, K-2 block,\\n4. Khasra no 81' +
		'9, Mahipalpur,\\n5. Behind Apra Maruti Showroom.\\n6. New Delhi - 110037","1. ' +
		'**EAST**\\n2. **Vector E-Commerce Pvt Ltd.**\\n3. Plot no: 22, 1st floor,\\n4. B' +
		'anerjee Para Road,\\n5. West Putiary,\\n6. Kolkata- 700041","1. **WEST**\\n2. ' +
		'**Vector E-Commerce Pvt Ltd.**\\n3. Acropolis,\\n4. Lower Ground floor. CTS NO 1' +
		'386\\n5. Church Road, Marol Andheri ( East )\\n6. Mumbai 400 059"]',
	'globalstore.cardtext': {
		points: [ 'Not eligible for return or exchange', 'ID proof needed for delivery' ],
		subText:
			'100% Genuine Product, imported from specially for you from various fashion desti' +
			'nations from all over the globe'
	},
	'pdp.emi.terms': {
		intro:
			'Welcome to Myntra. This page details the Equated Monthly Installments (EMI)  mad' +
			'e available on Myntra marketplace platform (Myntra) on behalf of participating s' +
			'eller/s at their sole discretion and may be revoked or suspended by them any tim' +
			'e. These terms and conditions are in addition to the Myntra Terms of Use to whic' +
			'h you agree and are bound by using Myntra.',
		terms: [
			'The EMI payment option is available only on credit cards issued by the following' +
				' banks: HDFC, ICICI, SBI, CitiBank, Axis, Amex, HSBC, IndusInd, Kotak, RBL ,Stan' +
				'dard Chartered.',
			'The EMI facility is offered by the card issuing bank through PayUBiz. The EMI el' +
				'igibility, payment plan and refund rules are solely on the internal discretion a' +
				'nd approvals of the bank. Myntra shall not be held liable for any dispute betwee' +
				'n the customer and bank arising out of or in connection with the EMI payment mad' +
				'e on Myntra.',
			'The EMI facility is available for all products on Myntra except some jewellery i' +
				'tems especially gold items. Myntra reserves the right to stop the EMI facility a' +
				'gainst any product sold on the platform anytime without any priority notice.',
			'The facility is available only for order value above Rs. 2500. The minimum eligi' +
				'ble order value varies from bank to bank.',
			'Myntra reserves the right to temporarily or permanently stop the EMI facility wi' +
				'thout any prior notice.',
			'Myntra does not charge the customer any processing or convenience fee for this s' + 'ervice',
			'Myntra is offering the EMI facility through PayU Biz. The EMI plans, tenure and ' +
				'interest are as per the information shared with PayU Biz. For any concern or dis' +
				'pute a customer should contact the respective bank.',
			'In the case of cancellations or returns of items purchased on EMI, Myntra will f' +
				'acilitate the refunds for and on behalf of the sellers as per the relevant refun' +
				'd policy. The customer is advised to check with the respective bank/issuer offer' +
				'ing the EMI how the cancellations or refund will affect the EMI terms and of any' +
				' pre-closure or interest charges levied on the customer.'
		]
	},
	'alteration.order_delivery_lead_time_days': '60',
	'no.of.brands': '1,000',
	'myntra.orders.datelimit': '2016-03-01',
	'hof.tambola.gameId': '10',
	'app.android.dlink':
		'https://335741.measurementapi.com/serve?action=click&publisher_id=335741&site_id' +
		'=128503&invoke_id=282383&my_campaign=Organic&my_publisher=Other%20Organic&my_adg' +
		'roup=Myntra%20Brand&my_ad=0&site_id_android=128503&site_id_ios=128517',
	'myntra.prelaunch.data':
		'{\r\n        "enable": "true",\r\n        "message": "please reload the p' +
		'age to start shopping",\r\n        "enddate": "2016-10-23T13:30:00Z",\r\n  ' +
		'      "users": "28000"\r\n}',
	'giftcard.data':
		'{"amount":{"minimum":100,"maximum":10000},"quantity":{"maximum":1},"e' +
		'xpiryDateDuration":{"years":1},"staticAmount":{"option1":"500","option' +
		'2":"1,000","option3":"2,000"},"defaultMessage":"Best wishes"}',
	'shipping.charges.tryNbuy': '1',
	'myntraweb.pdp.hideSizeTooltip': [
		'Men_Jeans',
		'Men_Trousers',
		'Men_Shorts',
		'Women_Jeans',
		'Women_Trousers',
		'Women_Shorts'
	],
	'myntra.valueShipping.charges': '25',
	'cart.paidTryNBuy': '{"firstUser":"free for first order","codNotAvailable":"Available on Prepa' + 'id"}',
	'hrdr.salebanner.data':
		'{"enable":"true","image":"","timerlabel":"Sale ends in ","message"' +
		':"","enddate":"2018-02-11T18:29:59.999Z","when":"","name":""}',
	'myntraweb.vat.message': '{"enabled":"true","message":"Additional tax may apply; charged at checkou' + 't"}',
	'myntra.valueShipping.text': 'Value Shipping',
	'alteration.slot_booking_lead_time_hrs': '24',
	'halloffame.stamptestimony.description':
		'As they say, all good things come to end...for great things to follow! Join us a' + 's we bid adieu to web.',
	'halloffame.topbanner.left': '',
	'msite.banner.url': 'https://cdn.myntassets.com/getapp/img/new/home_2x.png',
	'search.countdown':
		'{\r\n"happy-mothers-day": "10,05,2015,23,59",\r\n"bags-and-backpacks": "1' +
		'4,05,2015,23,59",\r\n"men-tees-polos": "14,05,2015,23,59",\r\n"women-kurta' +
		's-kurtis-suits": "14,05,2015,23,59",\r\n"men-casual-shirts": "14,05,2015,2' +
		'3,59",\r\n"men-casual-shoes": "14,05,2015,23,59",\r\n"men-jeans": "14,05' +
		',2015,23,59",\r\n"women-dresses": "14,05,2015,23,59",\r\n"ethnic-wear": ' +
		'"14,05,2015,23,59",\r\n"men-apparel-new-arrivals": "14,05,2015,23,59",\r\n' +
		'"men-footwear-new-arrivals": "14,05,2015,23,59",\r\n"men-sports-shoes": "' +
		'14,05,2015,23,59",\r\n"men-sweaters-sweatshirts": "14,05,2015,23,59",\r\n"' +
		'men-formal-shirts": "14,05,2015,23,59",\r\n"men-formal-trousers": "14,05,2' +
		'015,23,59",\r\n"women-apparel-new-arrivals": "14,05,2015,23,59",\r\n"women' +
		'-footwear-new-arrivals": "14,05,2015,23,59",\r\n"women-casual-shoes": "14,' +
		'05,2015,23,59",\r\n"women-tops-tees": "14,05,2015,23,59",\r\n"women-salwar' +
		's-churidars": "14,05,2015,23,59",\r\n"women-sweaters-sweatshirts": "14,05,' +
		'2015,23,59",\r\n"winter-wear": "14,05,2015,23,59",\r\n"men-bags-backpacks-' +
		'wallets": "14,05,2015,23,59",\r\n"men-flip-flop-sandals": "14,05,2015,23,5' +
		'9",\r\n"men-jackets": "14,05,2015,23,59",\r\n"men-jeans-trouser": "14,05' +
		',2015,23,59",\r\n"men-sports-casual-shoes": "14,05,2015,23,59",\r\n"men-sh' +
		'irts-tees-polos": "14,05,2015,23,59",\r\n"men-sweaters-sweatshirts": "14,0' +
		'5,2015,23,59",\r\n"men-tracksuits": "14,05,2015,23,59",\r\n"men-track-pant' +
		's": "14,05,2015,23,59",\r\n"men-watches": "14,05,2015,23,59",\r\n"women-' +
		'kurtis-churidar-kurtas": "14,05,2015,23,59",\r\n"women-jackets-shrugs": "1' +
		'4,05,2015,23,59",\r\n"women-jeans-trouser": "14,05,2015,23,59",\r\n"women-' +
		'shirts-tees-polos": "14,05,2015,23,59",\r\n"women-sports-casual-shoes": "1' +
		'4,05,2015,23,59",\r\n"women-sweaters-sweatshirts": "14,05,2015,23,59",\r\n' +
		'"women-flats-heels": "14,05,2015,23,59",\r\n"women-watches": "14,05,2015,' +
		'23,59",\r\n"flat-20-sale": "14,05,2015,23,59",\r\n"flat-30-sale": "14,05' +
		',2015,23,59",\r\n"flat-40-sale": "14,05,2015,23,59",\r\n"flat-50-sale": ' +
		'"14,05,2015,23,59",\r\n"flat-60-sale": "14,05,2015,23,59",\r\n"flat-70-sa' +
		'le": "14,05,2015,23,59",\r\n"flat-80-sale": "14,05,2015,23,59",\r\n"new-' +
		'offer-aff": "14,05,2015,23,59"\r\n}',
	'payments.wallet.walletName': 'PhonePe wallet',
	'checkout.confirmation.partnerMessage': '',
	'payments.notifications.bankmessages':
		'{"heading": "Bank Offers","messages": [{"message":"10X Reward points on' +
		' all spends with Kotak Visa Signature Credit Cards. Use code KTK10X during check' +
		'out and Kotak Visa Signature Credit Card as a mode of payment. TCA"}]}',
	'mobile.home.olapic.data':
		'{\r\n\t"src" : "//photorankstatics-a.akamaihd.net/81b03e40475846d5883661ff57b' +
		'34ece/static/frontend/latest/build.min.js",\r\n\t"data-instance" : "00fa6318' +
		'e406e9134c3fb8b704d84823",\r\n\t"data-apikey" : "545fdec92213c3948f549e59736' +
		'689bb33007cbb0ecd15184f6aefeab7800231",\r\n\t"enabled" : false\r\n}',
	customerSupportCall: '91-80-61561999',
	loginPoints: '["Apply coupons & cashback","Place orders easily","Track past orders","Ma' + 'nage WishList"]',
	smsPromoDesc: '',
	'mfu.exchange.url': 'https://www.myntra.com/growth/donate-items',
	'halloffame.stamptestimony.imageUrl':
		'https://cdn.myntassets.com/myx/images/mobile_adieu2web_graphic_2x.5f32468ad89724' +
		'd09e36fdc62e13cb87bae62d6c.jpg',
	'pdp.emi.plans': [
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 14,
					f: 0.11769
				},
				{
					months: 12,
					interest: 14,
					f: 0.08979
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'HDFC'
		},
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 13,
					f: 0.11722
				},
				{
					months: 12,
					interest: 13,
					f: 0.08932
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'ICICI'
		},
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 15,
					f: 0.11817
				},
				{
					months: 12,
					interest: 15,
					f: 0.09026
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'CITI'
		},
		{
			plans: [
				{
					months: 3,
					interest: 14,
					f: 0.34114
				},
				{
					months: 6,
					interest: 14,
					f: 0.17354
				},
				{
					months: 9,
					interest: 14,
					f: 0.11769
				},
				{
					months: 12,
					interest: 14,
					f: 0.08979
				}
			],
			bank: 'SBI'
		},
		{
			plans: [
				{
					months: 3,
					interest: 12,
					f: 0.34002
				},
				{
					months: 6,
					interest: 12,
					f: 0.17255
				},
				{
					months: 9,
					interest: 13,
					f: 0.11722
				},
				{
					months: 12,
					interest: 13,
					f: 0.08932
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'AXIS'
		},
		{
			plans: [
				{
					months: 3,
					interest: 12,
					f: 0.34002
				},
				{
					months: 6,
					interest: 12,
					f: 0.17255
				},
				{
					months: 9,
					interest: 14,
					f: 0.11769
				},
				{
					months: 12,
					interest: 14,
					f: 0.08979
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'KOTAK'
		},
		{
			plans: [
				{
					months: 3,
					interest: 15,
					f: 0.3417
				},
				{
					months: 6,
					interest: 15,
					f: 0.17403
				},
				{
					months: 9,
					interest: 15,
					f: 0.11817
				},
				{
					months: 12,
					interest: 15,
					f: 0.09026
				}
			],
			bank: 'AMEX'
		},
		{
			plans: [
				{
					months: 3,
					interest: 12.5,
					f: 0.3403
				},
				{
					months: 6,
					interest: 12.5,
					f: 0.1728
				},
				{
					months: 9,
					interest: 13.5,
					f: 0.11745
				},
				{
					months: 12,
					interest: 13.5,
					f: 0.08955
				},
				{
					months: 18,
					interest: 13.5,
					f: 0.06168
				}
			],
			bank: 'HSBC'
		},
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 13,
					f: 0.11722
				},
				{
					months: 12,
					interest: 13,
					f: 0.08932
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'INDUSIND'
		},
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 13,
					f: 0.11722
				},
				{
					months: 12,
					interest: 13,
					f: 0.08932
				},
				{
					months: 18,
					interest: 13,
					f: 0.06145
				},
				{
					months: 24,
					interest: 13,
					f: 0.04754
				}
			],
			bank: 'RBL'
		},
		{
			plans: [
				{
					months: 3,
					interest: 13,
					f: 0.34058
				},
				{
					months: 6,
					interest: 13,
					f: 0.17304
				},
				{
					months: 9,
					interest: 14,
					f: 0.11769
				},
				{
					months: 12,
					interest: 14,
					f: 0.08979
				},
				{
					months: 18,
					interest: 15,
					f: 0.06238
				},
				{
					months: 24,
					interest: 15,
					f: 0.04849
				}
			],
			bank: 'Standard Chartered'
		}
	],
	'mymyntra.valueReturnDiscount': 100
};
