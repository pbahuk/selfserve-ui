'use strict';

module.exports = {
	gcManagement: {
		available: true,
		addToAccountUrl: 'https://www.myntra.com/my/giftcards',
		shareMessage: 'Sharing with joy. ',
		thanksMessage: 'Thank you for the Myntra gift card. ',
		cardsDisplayCount: 5,
		manageGCUrl: 'https://www.myntra.com/my/myntracredit'
	}
};
