import express from 'express';
const router = express();
import at from 'v-at';
import middlewares from './middlewares';
import async from 'async';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway files.
import { omsServiceGateway } from '../apigateway/modules/oms';
import { switchServiceGateway } from '../apigateway/modules/switch';

router.get('/my/cancel', middlewares, (req, res) => {
  const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
  const cacheEnabled = req.myx.features['mymyntra.staticCache.enable'] === 'true';
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
    storeOrderId: req.query.storeOrderId,
    packetId: req.query.packetId,
    payload: {
      getStyle: 'true',
      getPayments: !ppsDisabled ? 'true' : 'false'
    }
  };
  let type = '';

  function getPacketCancellationWrapper(uidx, packetId, callback) {
    omsServiceGateway.isPacketCancellationAllowed(uidx, packetId, callback);
  }

  function getPacketWrapper(callback) {
    omsServiceGateway.getPacket(data, callback);
  }

  function getCancellationReasons(reasonType, callback) {
    switchServiceGateway.getStaticData(reasonType, cacheEnabled, callback);
  }

  omsServiceGateway.getOrder(data, {}, (error, response) => {
    if (error) {
      render(req, res, { omsServiceResponse: { error, response } }, 'Cancel Order', outline(req));
    } else {
      if (req.query.itemId && req.query.packetId) {
        /* **
          * Packet level cancellation.
          * Check for the specific conditions.
        */

        type = 'packetCancellation';
        const reasonType = 'packetCancellationReasons';
        async.parallel([
          getPacketWrapper,
          getCancellationReasons.bind(null, reasonType),
          getPacketCancellationWrapper.bind(null, data.user, req.query.packetId)], (error1, response1) => {
          if (error1) {
            render(req, res, { omsServiceResponse: { error: error1, response1 } }, 'Cancel Packet', outline(req));
          } else {
            console.log('Reponse1[2]', response1[2], at(response1[2], 'body'));
            const isPacketCancellationAllowed = at(response1[2], 'body.statusType') === 'SUCCESS';
            const packet = at(response1[0], 'packet');
            if (!isPacketCancellationAllowed) {
              packet.flags.cancellable = {
                isCancellable: isPacketCancellationAllowed,
                remark: 'Cancellation of this packet is not possible as it is ready to be shipped'
              };
              const items = at(packet, 'items') || [];
              items.forEach(item => {
                item.flags = item.flags || {};
                item.flags.cancellable = {
                  isCancellable: isPacketCancellationAllowed,
                  remark: 'Cancellation of this item is not possible as it is ready to be shipped'
                };
              });
            }

            const cumulativeResponse = { type, ...at(response, 'body'), ...response1[0], ...at(response1[1], 'body.data') };
            render(req, res, { omsServiceResponse: {
              error,
              response: cumulativeResponse,
              emptyStateEnabled: req.myx._mxab_['cancel.emptystate'] || 'disabled'
            } }, 'Cancel Order', outline(req));
          }
        });
      } else if (req.query.itemId) {
        /* **
          * Item level cancellation.
          * Check for the specific conditions.
        */
        type = 'itemCancellation';
        const reasonType = 'itemCancellationReasons';
        switchServiceGateway.getStaticData(reasonType, cacheEnabled, (error1, response1) => {
          if (error1) {
            render(req, res, { omsServiceResponse: { error: error1, response1 } }, 'Cancel Item', outline(req));
          } else {
            const cumulativeResponse = { type, ...at(response, 'body'), ...at(response1, 'body.data') };
            render(req, res, { omsServiceResponse: {
              error,
              response: cumulativeResponse,
              emptyStateEnabled: req.myx._mxab_['cancel.emptystate'] || 'disabled'
            } }, 'Cancel Order', outline(req));
          }
        });
      } else {
        /* **
          * Order level cancellation.
          * Check for the specific conditions.
        */
        type = 'orderCancellation';
        const reasonType = 'orderCancellationReasons';
        switchServiceGateway.getStaticData(reasonType, cacheEnabled, (error1, response1) => {
          if (error1) {
            render(req, res, { omsServiceResponse: { error: error1, response1 } }, 'Cancel Order', outline(req));
          } else {
            const cumulativeResponse = { type, ...at(response, 'body'), ...at(response1, 'body.data') };
            render(req, res, { omsServiceResponse: {
              error,
              response: cumulativeResponse,
              emptyStateEnabled: req.myx._mxab_['cancel.emptystate'] || 'disabled'
            } }, 'Cancel Order', outline(req));
          }
        });
      }
    }
  });
});

export default router;
