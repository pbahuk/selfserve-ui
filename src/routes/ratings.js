import express from 'express';
const router = express();
import at from 'v-at';
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway Files.
import { maximusServiceGateway } from '../apigateway/modules/maximus';
import { ratingsApiGateway } from '../apigateway/modules/ratings';

router.get('/my/ratings', middlewares, (req, res) => {
  const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');

  ratingsApiGateway.getEligibleStyles(user, (error, response) => {
    if (error) {
      render(req, res, { ratingsResponse: { error, response: null } }, 'Ratings', outline(req));
    } else {
      const styleIds = at(response, 'body.styleIds') || [];
      if (styleIds.length === 0) {
        render(req, res, { ratingsResponse: { error, response: { data: [] } } }, 'Ratings', outline(req));
      } else {
        maximusServiceGateway.getPdpDataBulk(styleIds, (error1, response1) => {
          if (error) {
            render(req, res, { ratingsResponse: { error1, response: null } }, 'Ratings', outline(req));
          } else {
            render(req, res, { ratingsResponse: { error: null, response: { data: response1 } } }, 'Ratings', outline(req));
          }
        });
      }
    }
  });
});

export default router;
