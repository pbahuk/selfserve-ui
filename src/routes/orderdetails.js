import express from 'express';
const router = express();
import at from 'v-at';
import { pageMiddlewareConfig } from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import ua from '@myntra/myx/lib/userAgents';

// Apigateway files.
import { omsServiceGateway } from '../apigateway/modules/oms';

router.get('/my/order/details', pageMiddlewareConfig.orderDetails, (req, res) => {
  const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
  const userAgent = ua(req.headers['user-agent']);
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
    storeOrderId: req.query.storeOrderId || req.query.orderno,
    payload: {
      getStyle: 'true',
      getPayments: !ppsDisabled ? 'true' : 'false',
      getTracking: 'true',
      getReturn: 'true',
      getGiftCard: 'true'
    }
  };
  const dependencies = {
    getRatings: req.myx.features['mymyntra.ratings.enable'] || false
  };


  omsServiceGateway
    .getOrder(data, dependencies, (error, response) => {
      if (error) {
        render(req, res, { omsServiceResponse: { error, response } }, 'Order Details', outline(req));
      } else {
        render(req, res,
          { omsServiceResponse: {
            error: null,
            response: response.body,
            item: req.query.itemId,
            looksAb: req.looksABTest,
            removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
            itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled',
            savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled'
          } },
          'Order Details',
          outline(req));
      }
    });
});

export default router;
