import express from 'express';
const router = express();
import at from 'v-at';
import async from 'async';
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway routes.
import { omsServiceGateway } from '../apigateway/modules/oms';
import { switchServiceGateway } from '../apigateway/modules/switch';
import { returnServiceGateway } from '../apigateway/modules/returns';

router.get('/my/return', middlewares, (req, res) => {
  const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
    storeOrderId: req.query.storeOrderId,
    payload: {
      getStyle: 'true',
      getPayments: !ppsDisabled ? 'true' : 'false'
    }
  };

  function getOrderWrapper(callback) {
    omsServiceGateway.getOrder(data, {}, callback);
  }

  function getReturnReasons(callback) {
    returnServiceGateway.getReturnReasons(data.user, 'return', callback);
  }

  function getReturnReasonCategories(callback) {
    const cacheEnabled = req.myx.features['mymyntra.staticCache.enable'] === 'true';
    switchServiceGateway.getStaticData('returnReasonCategories', cacheEnabled, callback);
  }

  function renderPage() {
    async.parallel([getOrderWrapper, getReturnReasons, getReturnReasonCategories], (error, response) => {
      if (error) {
        render(req, res, { omsServiceResponse: { error, response } }, 'Return Item', outline(req));
      } else {
        const order = at(response[0], 'body.order') || {};
        const itemId = req.query.itemId || req.query.itemid;
        const item = at(order, 'items').find((eachItem) => eachItem.id.toString() === itemId);
        const payload = {
          ppsID: at(order, 'payments.ppsId'),
          item: {
            sellerPartnerId: at(item, 'sellerPartnerId').toString(),
            skuID: at(item, 'product.skuId').toString()
          }
        };
        console.log('payload preperation::::', payload);

        returnServiceGateway.getRefundDetails(data.user, payload, (err1, res1) => {
          if (err1) {
            console.error('[Error: Refund Details], router:', err1);
            render(req, res, { omsServiceResponse: { error: err1, response: null } }, 'Return Item', outline(req));
          } else {
            console.log(' ********** Refund details for order start ********** ');
            console.log(res1.body);
            console.log(' ********** Refund details for order end ********** ');

            const cumulativeResponse = {
              ...at(response[0], 'body'),
              ...at(response[1], 'body'),
              returnReasonCategories: (at(response[2], 'body.data') || {}).returnReasonCategories || [],
              refundDetails: res1.body,
              mcRefundOffer: req.mCRefundOffer === 'enabled'
            };
            render(req, res, { omsServiceResponse: {
              error,
              response: cumulativeResponse,
              ordersMyntraCreditAB: req.myx._mxab_['orders.mcredit'] === 'enabled',
              emptyStateEnabled: req.myx._mxab_['cancel.emptystate'] || 'disabled'
            } }, 'Return Item', outline(req));
          }
        });
      }
    });
  }

  if (!data.storeOrderId) {
    console.log('storeOrderId not present in return: ', req.query);
    const orderID = req.query.orderid;
    if (orderID) {
      omsServiceGateway.getOrderFromOrderId(orderID, (err, resp) => {
        if (err) {
          res.redirect(302, '/my/orders');
        } else {
          data.storeOrderId = at(resp, 'body.orderResponse.data.order.storeOrderId');
          console.log('storeOrderId fetched: ', data.storeOrderId);
          renderPage();
        }
      });
    } else {
      res.redirect(302, '/my/orders');
    }
  } else {
    renderPage();
  }
});

export default router;
