import switchMiddleware from './switch';
import mymyntra from './mymyntra';
import defaultGcData from '../defaultSwitchConfigs/gcData';
import { secureRoot } from '../../config';
const excludedUrlList = [
  '/my/share'
];
import getGrowthHackConfig from './utils/growthHack';

// 'common' middleware
const chain = [
  function (req, res, next) {
    req.myx = req.myx || {};
    req.startTime = new Date().getTime();

    // Adding the request for the switch configuration.
    if (!req.switch) {
      req.switch = {};
    }
    req.switch.giftcardsConfig = {
      namespace: 'giftcard',
      variants: ['gcstandard'],
      list: ['gcManagement'],
      tenantId: 'myntra',
      options: {
        username: 'checkout',
        password: 'checkout@123'
      },
      defaultConfig: defaultGcData
    };
    req.switch.growthHackConfig = getGrowthHackConfig();
    next();
  }, function (req, res, next) {
    // Server Side redirection for user not logged in.
    let referer = req.originalUrl;
    referer = referer.charAt(0) === '/' ? referer.substr(1) : referer;
    const completeUrl = `${secureRoot()}${referer}`;
    const loginRedirectionUrl = `/login?referer=${encodeURIComponent(completeUrl)}`;
    const isExcludedUrl = excludedUrlList.find((url) => url === req.baseUrl);
    if (res.statusCode === 401 && !isExcludedUrl) {
      res.redirect(302, loginRedirectionUrl);
    } else {
      next();
    }
  }, 'device', 'apps', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session', 'env', mymyntra, 'cookies/mcRefundOffer'
];

const middlewares = chain.map((w) => ((typeof w === 'function') ? w : require(`@myntra/myx/lib/server/ware/${w}`)));

const pageMiddlewareConfig = {
  orders: [...middlewares,
    require('@myntra/myx/lib/server/ware/cookies/looks'),
    require('@myntra/myx/lib/server/ware/cookies/mcRefundOffer')
  ],
  orderDetails: [...middlewares,
    require('@myntra/myx/lib/server/ware/cookies/looks'),
    require('@myntra/myx/lib/server/ware/cookies/mcRefundOffer')
  ],
  archivedOrders: [...middlewares,
    require('@myntra/myx/lib/server/ware/cookies/looks'),
    require('@myntra/myx/lib/server/ware/cookies/mcRefundOffer'),
    require('@myntra/myx/lib/server/ware/authToken'),
    require('./appLoginCookieHandler')
  ],
  default: middlewares
};

export default pageMiddlewareConfig.default;
export {
  pageMiddlewareConfig
};
