const mcookies = require('@myntra/myx/lib/server/cookies');

const LOGIN_COOKIE = 'ilgim';

const clearLoginCookies = cookies => {
  cookies.set(LOGIN_COOKIE, 'false', {
    secure: false,
    httpOnly: false
  });
  cookies.set('sxid', '', {
    secure: true,
    encode: String,
    expires: new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
  });
};

module.exports = (req, res, next) => {
  const cookies = mcookies(req, res);
  const rt = (req.myx.tokens || {}).rt;
  const updatedTokens = req.updatedTokens;
  const rtPresent = rt && rt !== '0';
  const rtNotExpired = updatedTokens ? !updatedTokens.rtExpired : true;

  // Takes care of login cookie
  if (rtPresent && rtNotExpired) {
    cookies.set(LOGIN_COOKIE, 'true', {
      secure: false,
      httpOnly: false
    });
  } else {
    clearLoginCookies(cookies);
  }

  const xid = cookies.get('xid');
  const sxid = cookies.get('sxid');
  // Takes care of xid or sxid being 0
  if (xid === '0' || (xid && sxid === '0')) {
    clearLoginCookies(cookies);
  }
  next();
};
