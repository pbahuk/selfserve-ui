import SwitchClient from 'switch-node';
import outline from '@myntra/myx/lib/outline';


function promisedProperties(object) {
  const promisedProperties = [];
  const objectKeys = Object.keys(object);

  objectKeys.forEach((key) => promisedProperties.push(object[key]));

  return Promise.all(promisedProperties)
    .then((resolvedValues) => {
      return resolvedValues.reduce((resolvedObject, property, index) => {
        resolvedObject[objectKeys[index]] = property;
        return resolvedObject;
      }, object);
    });
}

export default function (req, res, next) {
  const { switch: switchData } = req;
  if (!switchData) {
    next();
  }

  if (!req.myx) {
    req.myx = {};
  }

  const configs = Object.keys(switchData);
  const promises = {};

  configs.forEach(config => {
    const d = switchData[config];
    const client = new SwitchClient(d.tenantId, d.namespace, d.defaultConfig, d.options);
    promises[config] = client.getManyWithKeys(d.list, d.variants);
  });

  promisedProperties(promises).then(data => {
    req.myx = { ...req.myx, ...data };
    outline(req).pump('giftcardsConfig', req.myx.giftcardsConfig);
    outline(req).pump('kvpairs', req.myx.kvpairs);
    outline(req).pump('features', req.myx.features);
    outline(req).pump('growthHackConfig', req.myx.growthHackConfig);
    next();
  });
}
