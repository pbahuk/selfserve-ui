/*
  Write all your repo related middlewares here.
  Function will be executed at the last of all the middlewares.
*/

import when from '@myntra/myx/lib/when';
import async from '@myntra/myx/lib/async';
import { push } from '../../utils/pump';
import { secureRoot } from '../../config';

const excludedUrlList = [
  '/my/share',
  '/my/api/reviewApi'
];

module.exports = (req, res, next) => {
  const w = when(req);
  async.parallel([w.all], () => {
    const deviceData = req.myx.deviceData || {};
    const platform = deviceData.isMobile ? 'mobile' : 'desktop';
    push(req, `<script>window.__myx_deviceType__ = '${platform}';</script>`);

    // Server Side redirection for user not logged in.
    let referer = req.originalUrl;
    referer = referer.charAt(0) === '/' ? referer.substr(1) : referer;
    const completeUrl = `${secureRoot()}${referer}`;
    const loginRedirectionUrl = `/login?referer=${encodeURIComponent(completeUrl)}`;
    const sessionData = req.myx.session ? req.myx.session : {};
    const isExcludedUrl = excludedUrlList.find((url) => url === req.baseUrl);

    if (!sessionData.isLoggedIn && !isExcludedUrl) {
      res.redirect(302, loginRedirectionUrl);
    } else {
      res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate'); // HTTP 1.1.
      res.setHeader('Pragma', 'no-cache'); // HTTP 1.0.
      res.setHeader('Expires', '-1'); // Proxies.
      next();
    }
  });
};
