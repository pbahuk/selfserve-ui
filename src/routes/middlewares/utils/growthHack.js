import getSwitchConfig from '../../../utils/getSwitchConfig'

const getGrowthHackConfigObject = (key, defaultValue) => ({
    key,
    defaultValue
  });

const emptyStateDefaultValue = {
    "widgets": {
        "view-similar" : { "show": true },
        "recently-viewed" : { "show": true },
        "reco-brands" : { "show": true },
        "st-reco-bag" : { "show": true },
        "continue-shopping" : { "show": true },
        "reco-new-arrivals" : { "show": false },
        "reco-most-popular" : { "show": false },
        "reco-bag" : { "show": false },
        "wishlist-items" : { "show": false }
    }
}

const shopMoreDefaultValue = [
    {
      "id": 1,
      "tag": "Men",
      "link": "/shop/men"
    },
    {
      "id": 2,
      "tag": "Women",
      "link": "/shop/women"
    },
    {
      "id": 3,
      "tag": "Kids",
      "link": "/shop/kids"
    },
    {
      "id": 4,
      "tag": "Personal Care",
      "link": "/shop/beauty-page"
    },
    {
      "id": 5,
      "tag": "Home",
      "link": "/shop/home-living"
    }
  ];

const growthHackConfigMap = {
    EMPTY_STATE_WIDGETS: getGrowthHackConfigObject(
        'emptyState',
        emptyStateDefaultValue
    ),
    SHOP_MORE_CAROUSEL: getGrowthHackConfigObject(
        'shopMorePills',
        shopMoreDefaultValue
    ),
};

const getGrowthHackConfig = () => ({
    namespace: 'growth-hack',
    variants: ['default'],
    ...getSwitchConfig(growthHackConfigMap),
})

export default getGrowthHackConfig