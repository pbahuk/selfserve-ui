// Import the apigateway files here: router.
import { router as omsRouter } from '../apigateway/modules/oms';
import { router as patronRouter } from '../apigateway/modules/patron';
import { uspRouter } from '../apigateway/modules/userProfiles';
import { router as crmRouter } from '../apigateway/modules/crm';
import { router as crossSellRouter } from '../apigateway/modules/crossSell';
import { router as emptyStateRouter } from '../apigateway/modules/lgpRelated';
import { router as wishListRouter } from '../apigateway/modules/wishList';
import { router as sellerRouter } from '../apigateway/modules/seller';
import { router as returnsRouter } from '../apigateway/modules/returns';
import { router as exchangeRouter } from '../apigateway/modules/exchange';
import { router as paymentsRouter } from '../apigateway/modules/payments';
import { router as insiderRouter } from '../apigateway/modules/insider';

import { router as writeReview } from '../apigateway/modules/writeReview';
import { ratingsRouter } from '../apigateway/modules/ratings';
import { router as serviceability } from '../apigateway/modules/serviceability';

export function run(app) {
  // Health Check for the app.
  app.use('/selfServeHealthCheck', (req, res) => {
    res.status(200).send('OK');
  });

  app.use(require('./share').default);

  app.use(require('./orders').default);

  app.use(require('./archivedOrders').default);

  app.use(require('./orderdetails').default);

  app.use(require('./itemdetails').default);

  app.use(require('./cancellation').default);

  app.use(require('./return').default);

  app.use(require('./exchange').default);

  app.use(require('./ratings').default);

  app.use(require('./writereview').default);

  app.use(require('./bankAccountEdit').default);

  app.use('/my/api/ordersApi', omsRouter);

  app.use('/my/api/patronApi', patronRouter);

  app.use('/my/api/userprofileapi', uspRouter);

  app.use('/my/api/crmApi', crmRouter);

  app.use('/my/api/crossSellApi', crossSellRouter);

  app.use('/my/api/insiderApi', insiderRouter);

  app.use('/my/api/emptyStateApi', emptyStateRouter);

  app.use('/my/api/wishListApi', wishListRouter);

  app.use('/my/api/sellerApi', sellerRouter);

  app.use('/my/api/payments', paymentsRouter);

  app.use('/my/api/reviewApi', writeReview);

  app.use('/my/api/returnsApi', returnsRouter);

  app.use('/my/api/exchangeApi', exchangeRouter);

  app.use('/my/api/ratingsApi', ratingsRouter);

  app.use('/my/api/serviceability', serviceability);
}
