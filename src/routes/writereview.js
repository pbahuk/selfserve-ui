import express from 'express';
const router = express();
import at from 'v-at';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import middlewares from './middlewares';
import Client from '../utils/Client';
import config from '../config';
import ua from '@myntra/myx/lib/userAgents';

router.get(['/my/writereview/:styleId', '/my/writereview/:styleId/:reviewId'], middlewares, (req, res) => {
    const styleId = req.params.styleId;
    const reviewId = req.params.reviewId;
    const promises = [];
    const userAgent = ua(req.headers['user-agent']);
    const isIOSApp = userAgent.isIOSApp();
    promises.push(new Promise(function(resolve, reject) {
      Client.get(`${config('pdp')}${styleId}`)
        .then(data => resolve([null, data]))
        .catch(err => resolve([err, null]))
    }));
    if (reviewId) {
      const header = {
        'x-mynt-ctx': 'storeid=2297',
        'Content-Type': 'application/json'
      };
      promises.push(new Promise(function(resolve, reject) {
        Client.get(`${config('reviews')}/review/${reviewId}`, header)
          .then(data => resolve([null, data]))
          .catch(err => resolve([err, null]))
      }));
    } else {
      if (at(req, 'myx.session.isLoggedIn')) {
        const uidx = req.myx.session.uidx;
        const header = {
          'x-mynt-ctx': `storeid=2297;uidx=${uidx}`,
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json'
        };
        promises.push(new Promise(function(resolve, reject) {
          Client.get(`${config('ratings')}/user?styleIds=${styleId}&uidx=${uidx}`, header)
            .then(data => resolve([null, data]))
            .catch(err => resolve([err, null]))
        }));
      }
    }

    return Promise.all(promises)
      .then(responses => {
        if (reviewId) {
          render(req, res, { pdpData: at(responses[0][1], 'body.style'), reviewData: at(responses[1][1], 'body'), isIOSApp },
          'WRITE REVIEW', outline(req));
        } else {
          render(req, res, { pdpData: at(responses[0][1], 'body.style'), userRatingData: at(responses[1][1], 'body'), isIOSApp },
          'WRITE REVIEW', outline(req));
        }
      })
      .catch(err => {
        console.info(err);
      });
  });

export default router;
