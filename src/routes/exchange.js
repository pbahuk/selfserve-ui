import express from 'express';
const router = express();
import at from 'v-at';
import async from 'async';
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway routes.
import { omsServiceGateway } from '../apigateway/modules/oms';
import { returnServiceGateway } from '../apigateway/modules/returns';

router.get('/my/exchange', middlewares, (req, res) => {
  const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
    storeOrderId: req.query.storeOrderId,
    payload: {
      getStyle: 'true',
      getPayments: !ppsDisabled ? 'true' : 'false'
    }
  };

  function getOrderWrapper(callback) {
    omsServiceGateway.getOrder(data, {}, callback);
  }

  function getReturnReasons(callback) {
    returnServiceGateway.getReturnReasons(data.user, 'exchange', callback);
  }

  function renderPage() {
    async.parallel([getOrderWrapper, getReturnReasons], (error, response) => {
      if (error) {
        render(req, res, { omsServiceResponse: { error, response } }, 'Exchange Item', outline(req));
      } else {
        const cumulativeResponse = { ...at(response[0], 'body'), ...at(response[1], 'body') };
        render(req, res, { omsServiceResponse: { error, response: cumulativeResponse } }, 'Exchange Item', outline(req));
      }
    });
  }

  if (!data.storeOrderId) {
    console.log('storeOrderId not present in exchange: ', req.query);
    const orderID = req.query.orderid;
    if (orderID) {
      omsServiceGateway.getOrderFromOrderId(orderID, (err, resp) => {
        if (err) {
          res.redirect(302, '/my/orders');
        } else {
          data.storeOrderId = at(resp, 'body.orderResponse.data.order.storeOrderId');
          console.log('storeOrderId fetched: ', data.storeOrderId);
          renderPage();
        }
      });
    } else {
      res.redirect(302, '/my/orders');
    }
  } else {
    renderPage();
  }
});

export default router;
