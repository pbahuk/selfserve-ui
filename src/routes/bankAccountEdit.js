import express from 'express';
const router = express();
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import get from 'lodash/get';
import { crmApigateway } from './../apigateway/modules/crm';


router.get('/my/bankaccount/edit', middlewares, (req, res) => {
  const tokenId = get(req, 'query.tokenId');
  if (tokenId) {
    const login = get(req, 'myx.session.identifiers.C.login') || get(req, 'myx.session.login');
    const query = {
      tenantId: 2297,
      eventName: 'updateBankAccountDetails',
      entityId: login,
      isEntityRequired: true,
      tokenId
    };

    crmApigateway.validateToken(query, (err, response) => {
      if (!err) {
        render(req, res, { tokenResponse: { error: null, response: get(response, 'body', null) } }, 'Bank Accounts', outline(req));
      } else {
        render(req, res, { tokenResponse: { error: err, response: null } }, 'Bank Accounts', outline(req));
      }
    });
  } else {
    render(req, res, { }, 'Bank Accounts', outline(req));
  }
});

export default router;
