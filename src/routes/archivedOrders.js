import express from 'express';
const router = express();
import at from 'v-at';
import { pageMiddlewareConfig } from './middlewares';
import async from 'async';
import semver from 'semver';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import ua from '@myntra/myx/lib/userAgents';
import when from '@myntra/myx/lib/when';
import handleAuthTokenUpdate from '../apigateway/modules/handleAuthTokenUpdate';
import { handleUnauthorized } from './../utils';

// Apigateway files.
import { omsServiceGateway } from '../apigateway/modules/oms';
import { uspServiceGateway } from '../apigateway/modules/userProfiles';

router.get('/my/archived/orders', pageMiddlewareConfig.archivedOrders, (req, res) => {
  const page = req.query && req.query.p ? req.query.p : 1;
  const userAgent = ua(req.headers['user-agent']);
  const androidVersion = userAgent.getMyntraAndroidVersionWithReleaseTags();
  const iosVersion = userAgent.getMyntraIosVersionWithReleaseTags();
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    at: at(req, 'myx.tokens.at'),
    rt: at(req, 'myx.tokens.rt'),
    payload: {
      page,
      getStyle: 'true',
      getPayments: 'false',
      getTracking: 'false',
      getReturn: 'true',
      getGiftCard: 'false'
    }
  };
  const dependencies = {
    getRatings: req.myx.features['mymyntra.ratings.enable'] || false
  };

  const looksEnabled = Boolean(req.looksABTest) && (
    (userAgent.isAndroidApp() && semver.valid(androidVersion) && semver.gt(androidVersion, '3.11.4')) ||
    (userAgent.isIOSApp() && semver.valid(iosVersion) && semver.gt(iosVersion, '3.10.0')));


  function getArchivedOrders(callback) {
    omsServiceGateway.getArchivedOrders(data, dependencies, callback);
  }

  function getProfiles(callback) {
    if (at(req, 'myx.features.userSizeProfilesEnabled') === 'true') {
      uspServiceGateway.getProfiles({ user: data.user }).end((err, resp) => {
        if (err) {
          console.error('Error in fetching profiles: ', err);
          callback(null, { error: true });
        } else {
          callback(null, resp);
        }
      });
    } else {
      callback(null);
    }
  }

  if (req.myx.features['mymyntra.pastorders.enable'] === 'false') {
    render(req, res, { omsServiceResponse: { error: null, orderResponse: null, profileResponse: null } }, 'Orders', outline(req));
  } else {
    async.parallel([getArchivedOrders, getProfiles], (error, response) => {
      if (error) {
        if (!handleUnauthorized(error, res)) {
          render(req, res, { omsServiceResponse: { error, orderResponse: response[0], profileResponse: response[1] } }, 'Orders', outline(req));
        }
      } else {
        handleAuthTokenUpdate(req, res, response, false);
        render(req, res, { omsServiceResponse: {
          error: null,
          orderResponse: at(response, '0.body'),
          profileResponse: at(response, '1.body'),
          archived: true,
          looksAb: looksEnabled,
          removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
          savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled',
          itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled'
        } }, 'Archived Orders', outline(req));
      }
    });
  }
});

router.get('/my/archived/order/details', pageMiddlewareConfig.archivedOrders, (req, res) => {
  const userAgent = ua(req.headers['user-agent']);
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    at: at(req, 'myx.tokens.at'),
    rt: at(req, 'myx.tokens.rt'),
    storeOrderId: req.query.storeOrderId || req.query.orderno,
    payload: {
      getStyle: 'true',
      getPayments: 'false',
      getTracking: 'false',
      getReturn: 'true',
      getGiftCard: 'true'
    }
  };
  const dependencies = {
    getRatings: req.myx.features['mymyntra.ratings.enable'] || false
  };
  omsServiceGateway
    .getArchivedOrder(data, dependencies, (error, response) => {
      if (error) {
        if (!handleUnauthorized(error, res)) {
          render(req, res, { omsServiceResponse: { error, response } }, 'Order Details', outline(req));
        }
      } else {
        handleAuthTokenUpdate(req, res, response, false);
        render(req, res,
          { omsServiceResponse: {
            error: null,
            archived: true,
            response: response.body,
            item: req.query.itemId,
            looksAb: req.looksABTest,
            removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
            itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled',
            savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled'
          } },
          'Order Details',
          outline(req));
      }
    });
});

router.get('/my/archived/item/details', pageMiddlewareConfig.archivedOrders, (req, res) => {
  when(req).all(() => {
    const userAgent = ua(req.headers['user-agent']);
    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      at: at(req, 'myx.tokens.at'),
      rt: at(req, 'myx.tokens.rt'),
      storeOrderId: req.query.storeOrderId || req.query.orderno,
      payload: {
        getStyle: 'true',
        getPayments: 'false',
        getTracking: 'false',
        getReturn: 'true',
        getGiftCard: 'true'
      }
    };
    const dependencies = {
      getRatings: req.myx.features['mymyntra.ratings.enable'] || false
    };

    omsServiceGateway
      .getArchivedOrder(data, dependencies, (error, response) => {
        if (error) {
          if (!handleUnauthorized(error, res)) {
            render(req, res, { omsServiceResponse: { error, response } }, 'Item Details', outline(req));
          }
        } else {
          handleAuthTokenUpdate(req, res, response, false);
          render(req, res, {
            omsServiceResponse: {
              error: null,
              archived: true,
              response: response.body,
              item: req.query.itemId,
              removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
              itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled',
              savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled'
            } },
            'Item Details',
            outline(req));
        }
      });
  });
});

export default router;
