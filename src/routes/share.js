import express from 'express';
const router = express();
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway file.
import { serviceGateway } from '../apigateway/modules/giftCard';

router.get('/my/share', middlewares, (req, res) => {
  const pageTitle = req.query && req.query.share ? 'Share Gift Card' : 'Say Thanks';

  serviceGateway.getDataFromGcid(req.query.gcid)
    .end((error, response) => {
      if (error) {
        console.error('Error [GiftCard Server]:', error);
        render(req, res, { giftServiceResponse: { error, response: null } }, pageTitle, outline(req));
      } else {
        render(req, res, { giftServiceResponse: { error: null, response: response.body } }, pageTitle, outline(req));
      }
    });
});

export default router;
