import express from 'express';
const router = express();
import at from 'v-at';
import { pageMiddlewareConfig } from './middlewares';
import async from 'async';
import semver from 'semver';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import ua from '@myntra/myx/lib/userAgents';

// Apigateway files.
import { omsServiceGateway } from '../apigateway/modules/oms';
import { uspServiceGateway } from '../apigateway/modules/userProfiles';

router.get('/my/orders', pageMiddlewareConfig.orders, (req, res) => {
  const page = req.query && req.query.p ? req.query.p : 1;
  const pastOrdersEnabled = req.myx.features['mymyntra.pastorders.enable'] === 'true';
  const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
  const userAgent = ua(req.headers['user-agent']);
  const androidVersion = userAgent.getMyntraAndroidVersionWithReleaseTags();
  const iosVersion = userAgent.getMyntraIosVersionWithReleaseTags();

  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
    pastOrdersEnabled,
    payload: {
      page,
      getStyle: 'true',
      getPayments: !ppsDisabled ? 'true' : 'false',
      getTracking: 'true',
      getReturn: 'true',
      getGiftCard: 'false'
    }
  };
  const dependencies = {
    getRatings: req.myx.features['mymyntra.ratings.enable'] || false
  };

  const looksEnabled = Boolean(req.looksABTest) && (
    (userAgent.isAndroidApp() && semver.valid(androidVersion) && semver.gt(androidVersion, '3.11.4')) ||
    (userAgent.isIOSApp() && semver.valid(iosVersion) && semver.gt(iosVersion, '3.10.0')));


  function getOrders(callback) {
    omsServiceGateway.getOrders(data, dependencies, callback);
  }

  function getProfiles(callback) {
    if (at(req, 'myx.features.userSizeProfilesEnabled') === 'true') {
      uspServiceGateway.getProfiles({ user: data.user }).end((err, resp) => {
        if (err) {
          console.error('Error in fetching profiles: ', err);
          callback(null, { error: true });
        } else {
          callback(null, resp);
        }
      });
    } else {
      callback(null);
    }
  }

  if (req.myx.features['hrdr.mymyntra.orders.disable'] === 'true') {
    render(req, res, { omsServiceResponse: { error: null, orderResponse: null, profileResponse: null } }, 'Orders', outline(req));
  } else {
    async.parallel([getOrders, getProfiles], (error, response) => {
      if (error) {
        render(req, res, { omsServiceResponse: { error, orderResponse: response[0], profileResponse: response[1] } }, 'Orders', outline(req));
      } else {
        render(req, res, { omsServiceResponse: {
          error: null,
          orderResponse: at(response, '0.body'),
          profileResponse: at(response, '1.body'),
          looksAb: looksEnabled,
          removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
          myOrderInsiderAB: req.myx._mxab_['Myorder.insider'] === 'enabled',
          savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled',
          itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled'
        } }, 'Orders', outline(req));
      }
    });
  }
});

export default router;
