import express from 'express';
const router = express();
import at from 'v-at';
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
import when from '@myntra/myx/lib/when';
import ua from '@myntra/myx/lib/userAgents';

// Apigateway files.
import { omsServiceGateway } from '../apigateway/modules/oms';

router.get('/my/item/details', middlewares, (req, res) => {
  when(req).all(() => {
    const ppsDisabled = req.myx.features['mymyntra.pps.disable'] === 'true';
    const userAgent = ua(req.headers['user-agent']);
    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      storeOrderId: req.query.storeOrderId || req.query.orderno,
      payload: {
        getStyle: 'true',
        getPayments: !ppsDisabled ? 'true' : 'false',
        getTracking: 'true',
        getReturn: 'true',
        getGiftCard: 'true'
      }
    };
    const dependencies = {
      getRatings: req.myx.features['mymyntra.ratings.enable'] || false
    };

    omsServiceGateway
      .getOrder(data, dependencies, (error, response) => {
        if (error) {
          render(req, res, { omsServiceResponse: { error, response } }, 'Item Details', outline(req));
        } else {
          render(req, res, {
            omsServiceResponse: {
              error: null,
              response: response.body,
              item: req.query.itemId,
              removeCancelAB: req.myx._mxab_['Order.RemoveCancel'] || 'disabled',
              myOrderInsiderAB: req.myx._mxab_['Myorder.insider'] === 'enabled',
              itemsTagAB: userAgent.isApp() ? req.myx._mxab_['orders.itemSpeciality'] : 'disabled',
              savingsAB: req.myx._mxab_['orderPage.savings'] || 'disabled',
              collapseTrackerAB: req.myx._mxab_['itemDetails.collapseTracker'] === 'enabled',
              crossSellFilterAB: req.myx._mxab_['itemDetails.crossSell'] === 'enabled'
            } },
            'Item Details',
            outline(req));
        }
      });
  });
});

export default router;
