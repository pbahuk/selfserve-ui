import express from 'express';
const router = express();
import at from 'v-at';
import middlewares from './middlewares';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';

// Apigateway file.
import { serviceGateway } from '../apigateway/modules/giftCard';

router.get('/my/giftcards', middlewares, (req, res) => {
  const data = {
    user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
    type: 'active',
    offset: 0,
    limit: 5
  };

  serviceGateway
    .getData(data)
    .end((error, response) => {
      if (error) {
        console.error('Error [GiftCard Server]:', error);
        render(req, res, { giftServiceResponse: { error, response: null } }, 'Manage Gift Cards', outline(req));
      } else {
        render(req, res, { giftServiceResponse: { error: null, response: response.body } }, 'Manage Gift Cards', outline(req));
      }
    });
});

export default router;
