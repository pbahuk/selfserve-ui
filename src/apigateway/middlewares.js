/**
 * Add only the middleware needed for your api layer.
 * No need to add all the middlewares present.
 */

// TO DO: Make the FG and KVPairs even on Client and server side. Hot fix for now.
const switchMiddleware = require('switch-express').default;

const config = {
  giftcard: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  mfg: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  savedcards: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  address: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  wishList: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  oms: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session', 'authToken'],
  userprofile: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  crm: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  css: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  ratings: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  payments: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  serviceability: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  lgp: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session'],
  emptyState: ['device', 'cookies/abtests', 'cookies/switch', switchMiddleware, 'session']
};

export default function (service) {
  const chain = config[service];

  const middlewareList = chain.map((middleware) => ((typeof middleware === 'function') ? middleware : require(`@myntra/myx/lib/server/ware/${middleware}`)));
  return middlewareList;
}
