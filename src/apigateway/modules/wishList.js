import Client from '../../utils/Client';
import config from '../../config';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import at from 'v-at';
import middlewares from '../middlewares';

router.post('/add/:productId', [middlewares('wishList'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const url = `${config('wishList')}add`;
    const postBody = {
      listID: 'wishlist',
      styleID: req.params.productId,
      listType: 'wishlist'
    };
    if (user && clientUserToken === serverUserToken) {
      Client.post(url, postBody, { xid })
      .end((error, response) => {
        if (error) {
          console.error('Error [wishlist post add Server]:', at(error, 'text'));
          res.status(error.status).send({
            error: error.error,
            response: response.body
          });
        } else {
          res.status(200).send(response.body);
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/remove/:productId', [middlewares('wishList'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const url = `${config('wishList')}delete`;
    delete req.body.USER_TOKEN;
    const postBody = {
      listID: 'wishlist',
      styleID: req.params.productId,
      listType: 'wishlist'
    };
    if (user && clientUserToken === serverUserToken) {
      Client.post(url, postBody, { xid })
        .end((error, response) => {
          if (error) {
            console.error('Error [wishlist post remove Server]:', at(error, 'error'));
            res.status(error.status).send({
              error: error.error,
              response: response.body
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);


router.get('/summary/', [middlewares('wishList'), (req, res) => {
  when(req).all(() => {
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const url = `${config('wishList')}summary?xid=${xid}`;
    Client.get(url, { xid })
      .end((error, response) => {
        if (error) {
          console.error('Error [Cross Sell summary Server]:', at(error, 'error'));
          res.status(error.status).send({
            error: error.error,
            response: response.body
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
}]);

export { router };
