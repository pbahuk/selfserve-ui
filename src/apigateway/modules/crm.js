import express from 'express';
const router = express();
import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.get('/getBankDetails', [middlewares('crm'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('crm')}crm/task/bank_account/detail?login=${user}`;
    console.log('get bank account details: ', url);
    if (user) {
      Client
        .get(url)
        .end((error, response) => {
          if (error) {
            console.error('Error [CRM Server, Bank Details]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);


router.get('/getIfscDetails', [middlewares('crm')], (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const code = at(req, 'query.ifsccode');

    console.log('##### user', user, xid, JSON.stringify(req.myx));

    if (user && code) {
      const url = `${config('gateway')}payments/ifsc/${code}`;
      const headers = { 'x-mynt-ctx': `storeid=2297;uidx=${user};nidx=${xid};` };

      Client.get(url, headers)
        .end((error, response) => {
          if (error) {
            console.error('Error [IFSC Server]:', error.error || error.stack);
            res.status(error.status || error.statusCode).send({
              error: error.error || error.stack
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else if (!code) {
      res.status(403).send({
        error: 'invalid ifsc'
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
});

router.post('/postBankAccountDetail', [middlewares('crm'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('crm')}crm/task/bank_account/save`;
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;
    console.log('post bank account details: ', url, req.body);

    if (user && (user === at(req, 'body.neftAccountEntry.login')) && (serverUserToken === clientUserToken)) {
      Client
        .post(url, JSON.stringify(req.body))
        .end((error, response) => {
          if (error) {
            console.error('Error [CRM Server, Add Bank Account]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);


const crmApigateway = {
  validateToken: (query, callback) => {
    const url = `${config('crm')}token/validate`;
    Client
          .get(url)
          .query(query)
          .end((error, response) => {
            if (error) {
              console.error('Error [CRM Server, Validate Token]:', error);
            }
            callback(error, response);
          });
  }
};


export { router, crmApigateway };
