import Client from '../../utils/Client';
import config from '../../config';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import at from 'v-at';

router.get('/fetchOrders', (req, res) => {
  when(req).all(() => {
    const url = `${config('myntrainsider')}orders`;
    const headers = {
      'm-uidx': req.headers['m-uidx']
    };
    Client.get(url, headers)
      .end((error, response) => {
        if (error) {
          console.error('Error [Styleplus Api Server]:', at(error, 'error') || 'Failed to fetch styleplus reponse');
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
});

router.post('/claimInsiderPoints/:itemId', (req, res) => {
  when(req).all(() => {
    const url = `${config('myntrainsider')}claimPoints/${req.params.itemId}`;
    const headers = {
      'm-uidx': req.headers['m-uidx']
    };
    Client.post(url, {}, headers)
      .end((error, response) => {
        if (error) {
          console.error('Error [Styleplus Api Server]:', at(error, 'error') || 'Failed to post styleplus reponse');
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
});

export { router };
