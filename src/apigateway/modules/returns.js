import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import forEach from 'lodash/forEach';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';
import { omsServiceGateway } from './oms';

const returnServiceGateway = {
  getRefundDetails(uidx, payload, callback) {
    const url = `${config('user')}returns/item/refunddetails`;
    const xMyntCtx = `storeid=2297;uidx=${uidx}`;

    return Client.post(url, payload, {
      'x-mynt-ctx': xMyntCtx
    }).end(callback);
  },

  getReturnReasons(uidx, context, callback) {
    const xMyntCtx = `storeid=2297;uidx=${uidx}`;
    const url = `${config('user')}order/getReturnReasons?context=${context}`;
    Client.get(url, {
      'x-mynt-ctx': xMyntCtx
    })
    .end((error, response) => {
      if (error) {
        console.error('Error fetching return reasons: ', error);
      } else {
        callback(null, response);
      }
    });
  }
};

router.post('/createReturn', [middlewares('css'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const url = `${config('user')}return/create`;
    const xMyntCtx = `storeid=2297;uidx=${user}`;
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;
    const payload = req.body;

    const createReturn = () => {
      Client
      .post(url, payload, { 'x-mynt-ctx': xMyntCtx })
      .end((error, response) => {
        if (error) {
          console.error('Error [Gateway Returns, create return]:', error);
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
    };

    const isON = (instruments = {}) => {
      const paymentMethods = [];
      forEach(instruments, (val, key) => {
        if (val !== 0) {
          paymentMethods.push(key);
        }
      });
      return ['neft', 'creditCard', 'debitCard', 'netBanking', 'phonepe']
        .find(method => paymentMethods.indexOf(method) !== -1);
    };

    const validateEmail = (email) => {
      const re = /\S+@\S+\.\S+/;
      return re.test(email);
    };

    if (user && (serverUserToken === clientUserToken)) {
      omsServiceGateway.getOrder({
        storeOrderId: payload.storeOrderId,
        user,
        xid
      }, {}, (err, resp) => {
        if (err) {
          console.error('error getting order in create return handler: ', err);
          res.status(err.status || 500).send({
            error: err.error || 'Something went wrong'
          });
        } else {
          const order = at(resp, 'body.order') || {};
          const ppsId = at(order, 'payments.ppsId');
          const refundMode = at(payload, 'returnRefundDetailsEntry.refundMode');
          const skuId = at(payload, 'returnLineEntries.0.skuId');
          const email = at(payload, 'email');

          if (isON(at(order, 'payments.instruments')) &&
            refundMode && refundMode.toLowerCase() === 'myntcredit') {
              console.log('Online order and refund mode myntcredit');
              if (!email || !validateEmail(email)) {
                res.status(500).send({
                  error: 'Myntra Credit refund mode is not available for this product, Please select any other refund mode.'
                });
                console.error(`Error [Email not present for uidx]:  ${user}`);
                return;
              }
              const __data = {
                uidx: user,
                email,
                skuId
              };

            __data.refundPayload = {
              paymentPlanItemList: [{
                ppsId,
                items: [{
                  type: 'SKU',
                  itemIdentifier: skuId,
                  quantity: '1'
                }]
              }]
            };

            omsServiceGateway.createPPEGiftCart(req, __data, (eGCErr, eGCResp) => {
              if (eGCErr || at(eGCResp, 'isAmountNotEligible')) {
                console.error('************************ Create CART error *****************************', eGCErr);
                res.status(500).send({
                  error: eGCErr || 'Refund amount is above the maxmimum limit'
                });
              } else if (at(eGCResp, 'isExchangeOrder')) {
                createReturn();
              } else {
                const cartId = at(eGCResp, 'cartId');
                if (cartId) {
                  payload.returnAdditionalDetailsEntry = payload.returnAdditionalDetailsEntry || {};
                  payload.returnAdditionalDetailsEntry.cartId = cartId;
                  createReturn();
                } else {
                  console.error('************************ Cart ID Not present *****************************');
                  res.status(500).send({
                    error: 'Refund to MC failed :: absolut error'
                  });
                }
              }
            });
          } else {
            createReturn();
          }
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export { returnServiceGateway, router };
