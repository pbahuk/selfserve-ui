import config from '../../config';
import at from 'v-at';
import Client from '../../utils/Client';
import cache from 'memory-cache';

const switchServiceGateway = {
  getStaticData: (type, cacheEnabled, callback) => {
    const ttl = 60 * 60 * 1000;
    const cacheKey = `cache-${type}`;
    let cachedStaticData = cache.get(cacheKey);

    if (!cacheEnabled && cachedStaticData) {
      console.log('Clearing all data in cache:::', cacheKey);
      cache.clear();
      cachedStaticData = null;
    } else if (cacheEnabled && cachedStaticData) {
      console.log('Sending data from cache:::', cacheKey);
      callback(false, {
        body: cachedStaticData
      });
    }

    if (!cachedStaticData) {
      const url = `${config('switch')}mymyntra/${type}`;

      Client
        .get(url)
        .end((error, response) => {
          if (error || !at(response, 'body.data')) {
            console.error(`Error [Switch Server ${type}]:`, error || `Response for staticData ${type} is empty`);
          } else {
            if (cacheEnabled) {
              console.log('Putting data in cache:::', cacheKey);
              cache.put(cacheKey, at(response, 'body'), ttl);
            }
          }
          callback(error, response);
        });
    }
  }
};

export { switchServiceGateway };
