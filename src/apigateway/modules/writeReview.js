import at from 'v-at';
import config from '../../config';
import express from 'express';
import Client from '../../utils/Client';
import middlewares from '../../routes/middlewares';
import when from '@myntra/myx/lib/when';
let proxy = require('http-proxy-middleware');
const router = express();
const ENV = process.env.NODE_ENV;

router.use(
    '/uploadImg',
    [middlewares, (req, res, next) => {
      if (at(req, 'myx.session.isLoggedIn')) {
        delete req.headers.cookie;
        next();
      } else {
        res.send(401, {
          error: {
            message:
              "User not logged in"
          }
        });
      }
    }],
    proxy({
      target: `${ENV === 'development' || ENV === 'release' ? 'http://dam.stage.myntra.com' : 'http://asset-service.myntra.com'}`,
      pathRewrite: { '^/my/api/reviewApi/uploadImg': '/myntra-asset-service/images?client_id=31' },
      logLevel: 'info',
      changeOrigin: true,
      logProvider: () => global.console,
      onProxyReq: (proxyReq, req, res) => {
        proxyReq.setHeader('cache-control', 'no-cache');
        if (ENV === 'development' || ENV === 'release') {
          proxyReq.setHeader('Authorization', 'Basic WVhCcFlXUnRhVzQ2YlRGOnVkSEpoVWpCamEyVjBNVE1oSXc9PQ==');
        } else {
          proxyReq.setHeader('Authorization', 'Basic cmF0aW5nc1Jldmlld3N+cmF0aW5nc19yZXZpZXdzOiIi');
        }
        proxyReq.setHeader('Accept', 'application/json');
      }
    })
);

router.post('/setReview', [middlewares, (req, res) => {
  when(req).all(() => {
      if (at(req, 'myx.session.isLoggedIn')) {
          const payload = req.body;
          const uidx = req.myx.session.uidx;
          const header = {
              'x-mynt-ctx': 'storeid=2297',
              'Content-Type': 'application/json',
              'm-uidx': `${uidx}`
            };
          Client.post(`${config('reviews')}/review`, payload, header).end((error, response) => {
              if (error) {
              res.status(error.status).send({
                  error: error.response.body
                });
            } else {
              res.status(200).send(response.body);
            }
            });
        } else {
          res.status(403).send({});
        }
    });
}]);

router.post('/fetchReview', [middlewares, (req, res) => {
    if (at(req, 'myx.session.isLoggedIn')) {
      const reviewId = req.body.id;

      const header = {
        'x-mynt-ctx': 'storeid=2297',
        'Content-Type': 'application/json'
      };
      Client.get(`${config('reviews')}/review/${reviewId}`, header).end((error, response) => {
        if (error) {
          res.status(error.status).send({
            error: error.response.body
          });
        } else {
          res.status(200).send(response.body);
        }
      });
    } else {
      res.status(403).send({});
    }
}]);

export { router };
