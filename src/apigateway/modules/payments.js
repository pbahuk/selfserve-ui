import Client from '../../utils/Client';
import generateAuthorizationToken from '../../utils/generateAuthorizationToken';
import config, { apiConfig } from '../../config';
import at from 'v-at';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.post('/transaction', [middlewares('payments'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const ppsId = at(req, 'body.ppsId');

    if (user && ppsId) {
      const url = `${config('payments')}transaction/${ppsId}`;
      const headers = {
        client: apiConfig('payments').client,
        version: apiConfig('payments').version,
        'x-mynt-ctx': `storeid=2297;uidx=${user};nidx=${xid};`
      };
      Client
        .get(url, headers)
        .end((error, response) => {
          if (error) {
            console.error('Error [Payments Server, transaction info]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            console.log(response.body);
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}
]);

router.post('/getAccount', [middlewares('payments'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const neftId = at(req, 'body.neftId');

    if (user && neftId) {
      const url = `${config('payments')}bankAccount?id=${neftId}`;
      const headers = {
        client: apiConfig('payments').client,
        version: apiConfig('payments').version,
        'x-mynt-ctx': `storeid=2297;uidx=${user};nidx=${xid};`
      };
      Client
        .get(url, headers)
        .end((error, response) => {
          if (error) {
            console.error('Error [Payments Server, bank account info]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            console.log(response.body);
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}
]);

router.post('/updateBankAccountDetail', [middlewares('crm'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    if (user) {
      const neftAccountID = at(req, 'body.id') || '';
      const Authorization = generateAuthorizationToken(req.body, apiConfig('payments'));
      const headers = { 'x-mynt-ctx': `storeid=2297;uidx=${user};`, client: apiConfig('payments').client, version: apiConfig('payments').version, Authorization };
      const url = `${config('payments')}bankAccount/${neftAccountID}`;
      console.log('update bank account details: ', url, req.body);
      Client
        .put(url, JSON.stringify(req.body), headers)
        .end((error, response) => {
          if (error) {
            console.error('Error [CRM Server, Update Bank Account]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export { router };
