import Client from '../../utils/Client';
import config from '../../config';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.post('/sellerConfiguration/search/bySellerId/', [middlewares('css'), (req, res) => {
  when(req).all(() => {
    const url = `${config('seller')}${req.url}?sellerId=${req.body.sellerId}&configurationCategory=CART`;
    console.log('Hitting the url:::', url);

    Client
      .get(url)
      .end((error, response) => {
        if (error) {
          console.error('Error [Seller Server, fetching Seller Configuration]:', error);
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
}]);

export { router };
