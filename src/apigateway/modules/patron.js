import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.post('/checkServiceability', [middlewares('css'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('patron')}serviceabilityv2/checkServiceabilitySummary`;

    console.log('Req Body:', at(req, 'body.CourierServiceabilityRequest.data.order.serviceType'));

    if (user) {
      Client
        .put(url, req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error [CSS Server, Check Serviceability]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            if (at(req, 'body.CourierServiceabilityRequest.data.order.serviceType') === 'EXCHANGE') {
              console.log('******************************* Exchange Serviceability  *************************');
              console.log('Request :=> ', JSON.stringify(req.body));
              console.log('####');
              console.log('Response :=> ', JSON.stringify(response.body));
            }
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}
]);

export { router };
