import express from 'express';
const router = express();
import at from 'v-at';
import maximus from 'maximus-node';
import findIndex from 'lodash/findIndex';
import config from '../../../config';
import when from '@myntra/myx/lib/when';
import Client from '../../../utils/Client';
import middlewares from '../../middlewares';
import { handleUnauthorized } from './../../../utils';
import magent from '@myntra/m-agent';
const agent = magent();


import helpers from './helpers';

const omsServiceGateway = {
  /** Get the list of orders for a user.
   *    1. The function returns 5 orders at a time.
   *    2. The orders are sorted by order id in descending order.
   *    3. MyMyntra -> APIfy -> { OMS, RMS, PPS, LMS, GiftCard}
   *    4. Some dependecies are resolved by the clients.
   */
  getOrders(data, dependencies, callback) {
    const context = this;
    const { user, xid, pastOrdersEnabled } = data;
    const url = `${config('apify')}orders`;

    if (user) {
      Client
        .post(url, at(data, 'payload'), { xid })
        .end((error, response) => {
          if (error) {
            console.error('Error [APIFY Server getOrders]:', error);
            console.error('-----order Payload-----', JSON.stringify(at(data, 'payload')));
            /**
             * Error Cases:
             * 1. Archived Orders Flag
             *    1.1 Enabled: Check for archived Orders.
             *    1.2 Disabled: Send the empty reponse back.
             */
            response = {};
            if (pastOrdersEnabled) {
              context.checkArchivedOrders(user, (archivedOrdersPresent) => {
                response.body = { ...{}, ...{ archivedOrdersLink: archivedOrdersPresent } };
                callback(error, response);
              });
            } else {
              callback(error, null);
            }
            return;
          } else {
            // Case when active orders are not present.
            /**
             * Orders Success Cases:
             * 1. No Orders present:
             *     1.1 Orders.lenth will be 0.
             *     1.2 Archived Flag: Enabled, check for archived orders else send back the response.
             * 2. Last Page for user Orders:
             *     2.1 Check : at(response, 'body.nextPage') === null
             *     2.2 Do the dependency attachment for the orders.
             *     2.3 Archived Flag: Enabled, check for archived orders else send back the response.
             * 3. Normal Flow:
             *    3.1 Do the dependency attachment for the orders. Send back the response.
             */
            const orders = at(response, 'body.orders') || [];

            if (!orders) {
              if (pastOrdersEnabled) {
                context.checkArchivedOrders(user, (archivedOrdersPresent) => {
                  response.body = { ...response.body, ...{ archivedOrdersLink: archivedOrdersPresent } };
                  callback(null, response);
                });
              } else {
                callback(null, response);
              }
              return;
            } else if (at(response, 'body.nextPage') === null) {
              if (pastOrdersEnabled) {
                helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
                  if (err) {
                    // This case will never happen for now.
                    // Keeping for further use.
                    callback(err, response);
                  } else {
                    context.checkArchivedOrders(user, (archivedOrdersPresent) => {
                      response.body = { ...response.body, ...{ archivedOrdersLink: archivedOrdersPresent, orders: updatedOrders } };
                      callback(null, response);
                    });
                  }
                });
              } else {
                helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
                  if (err) {
                    callback(err, response);
                  } else {
                    response.body = { ...response.body, orders: updatedOrders };
                    callback(err, response);
                  }
                });
              }
              return;
            } else {
              helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
                if (err) {
                  callback(err, response);
                } else {
                  response.body = { ...response.body, orders: updatedOrders };
                  callback(err, response);
                }
              });
            }
          }
        });
    } else {
      callback({ error: 'Not Valid User' }, null);
    }
  },
  getArchivedOrders(data, dependencies, callback) {
    const { user } = data;
    const url = `${config('apifyV2')}archivedOrders`;
    if (user) {
      agent
        .post(url)
        .send(at(data, 'payload'))
        .set({ at: data.at, rt: data.rt }) // add  'm-uidx': data.user in stage env
        .end((error, response) => {
          if (error) {
            console.error('Error [APIFY Server getArchivedOrders]:', error);
            console.error('-----order Payload-----', JSON.stringify(at(data, 'payload')));
            callback(error, null);
          } else {
            const orders = at(response, 'body.orders') || [];
            const httpCode = at(response, 'body.httpCode');
            if (httpCode === 401) {
              callback({ status: 401, error: 'unauthorized' }, null);
            } else if (!orders) {
              callback(null, response);
            } else {
              helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
                if (err) {
                  callback(err, response);
                } else {
                  response.body = { ...response.body, orders: updatedOrders };
                  callback(err, response);
                }
              });
            }
          }
        });
    } else {
      callback({ error: 'Not Valid User' }, null);
    }
  },

  getOrderFromOrderId: (orderID, callback) => {
    const url = `${config('oms')}oms/order/${orderID}`;
    Client.get(url)
      .end((error, response) => {
        if (error) {
          console.error('error getting order from orderid: ', error);
        }
        callback(error, response);
      });
  },

  getOrder: (data, dependencies, callback) => {
    const { user, xid, storeOrderId } = data;
    const url = `${config('apify')}order/${storeOrderId}`;
    console.log('URL :=> ', url, 'XID: ', xid);

    if (user) {
      Client.post(url, at(data, 'payload'), { xid })
        .end((error, response) => {
          if (error) {
            console.error('Error [OMS Server getOrder]:', at(error, 'error') || error);
            callback(error, response);
          } else {
            let orders = at(response, 'body.order') || [];
            orders = Array.isArray(orders) ? orders : [orders];

            helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
              if (err) {
                callback(err, response);
              } else {
                const order = updatedOrders.length ? updatedOrders[0] : orders[0];
                response.body = { ...response.body, order };
                callback(err, response);
              }
            });
          }
        });
    } else {
      callback({ error: 'User not found' }, null);
    }
  },

  getArchivedOrder: (data, dependencies, callback) => {
    const { user, xid, storeOrderId } = data;
    const url = `${config('apifyV2')}archivedOrder/${storeOrderId}`;
    console.log('URL :=> ', url, 'XID: ', xid);

    if (user) {
      agent.post(url)
        .send(at(data, 'payload'))
        .set({ at: data.at, rt: data.rt }) // add  'm-uidx': data.user in stage env
        .end((error, response) => {
          if (error) {
            console.error('Error [APIFY Server Archived Order]:', at(error, 'error') || error);
            callback(error, response);
          } else {
            let orders = at(response, 'body.order') || [];
            orders = Array.isArray(orders) ? orders : [orders];

            helpers.resolveOrderDependencies(orders, user, dependencies, (err, updatedOrders) => {
              if (err) {
                callback(err, response);
              } else {
                const order = updatedOrders.length ? updatedOrders[0] : orders[0];
                response.body = { ...response.body, order };
                callback(err, response);
              }
            });
          }
        });
    } else {
      callback({ error: 'User not found' }, null);
    }
  },


  /** Check if old orders are available
   *  Fetching a single order, if present then showing mentioning that Archived Orders are present.
  **/
  checkArchivedOrders: (user, callback) => {
    const url = `${config('vinyl')}oms/order/getOrders?q=login.eq:${user}`;

    Client.get(url)
      .query({
        start: 0,
        fetchSize: 1 })
      .end((err, res) => {
        if (err) {
          console.error('[OMS Apigateway Error]', err);
          callback(false);
        } else {
          const archivedOrders = at(res, 'body.orderResponse.data.order');
          // Send error message if no orders were retrieved.
          if (!archivedOrders) {
            callback(false);
          } else {
            callback(true);
          }
        }
      });
  },

  getPacket: (data, callback) => {
    console.log('************************************************************************************************');
    const { user } = data;
    const url = `${config('apifyV2')}packet/${data.packetId}`;

    if (user) {
      Client.post(url, data.payload, { 'm-uidx': user })
        .end((error, response) => {
          if (error) {
            console.error('[Error OMS getPacket]: ', error);
          }
          console.log('getPacket : Returning with response:::::::::::::::::::::::::::::::', response.body);
          callback(error, response.body);
        });
    } else {
      callback({ error: 'uidx not present' }, null);
    }
  },

  isPacketCancellationAllowed: (uidx, packetId, callback) => {
    const url = `${config('gateway')}user/packet/storePacket/${packetId}/isCancellationAllowed`;

    Client.get(url, { 'x-mynt-ctx': `storeid=2297;uidx=${uidx}` })
      .end((err, res) => {
        if (err) {
          console.error('[Error: Get isPacketCancellationAllowed]: ', err);
        }
        callback(err, res);
      });
  },

  getRefundAmountFromPPS(payload, callback) {
    const url = `${config('pps')}getPartialPaymentPlan`;
    Client.post(url, payload)
      .end((err, resp) => {
        if (err) {
          console.error('[Error OMS getRefundAmount]:', err);
        }
        callback(err, resp);
      });
  },

  getSkuIdForGCAmount(amount, callback) {
    if (!amount) {
      return;
    }

    const url = `${config('searchdevapi')}GiftCard/${amount}`;
    Client.get(url)
      .end((err, resp) => {
        if (err) {
          console.error(' An error occured in api call from getSkuIdForGCAmount =>', ':::: error=>', err);
          callback(err);
        } else {
          const searchRes = at(resp, 'body');
          const gcStyleId = at(searchRes, 'giftCards.0.styleid');
          if (gcStyleId) {
            const maximusClient = new maximus({ timeout: 1500 });
            maximusClient.fetchData('getPdpData', gcStyleId, 0)
              .then(data => {
                const skuId = at(data, 'data.0.styleOptionsData.styleOptions.0.skuId');
                callback(null, { skuId });
              })
              .catch(maximusErr => {
                console.error('MAXIMUS ISSUE', maximusErr);
                callback(maximusErr);
              });
          } else {
            callback('StyleID not found');
          }
        }
      });
  },

  createPPEGiftCart(req, data, callback) {
    let _data = data;
    this.getRefundAmountFromPPS(_data.refundPayload, (ppsErr, ppsResp) => {
      if (ppsErr) {
        callback(ppsErr);
      } else {
        let _ppsRes = at(ppsResp, 'body');
        const onlineModes = ['CREDITCARD', 'DEBITCARD', 'NETBANKING'];
        const pPlanDetails = at(_ppsRes, 'paymentPlanEntry.0.paymentPlanInstrumentDetails') || [];
        const onPPlan = pPlanDetails.find(item => onlineModes.indexOf(item.paymentInstrumentType) !== -1);

        if (onPPlan) {
          const refundAmount = onPPlan.totalPrice && Math.ceil(onPPlan.totalPrice / 100);
          if (refundAmount < 10000) {
            this.getSkuIdForGCAmount(refundAmount, (styleErr, styleResp) => {
              const skuId = at(styleResp, 'skuId');
              if (skuId) {
                const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
                const payload = {
                  cartItemEntries: [{
                    skuId,
                    customizedMessage: JSON.stringify({
                      cardProgramGroupName: 'Myntra Prepaid Return Refund eGift Cards',
                      login: _data.uidx
                    })
                  }],
                  giftMessage: {
                    message: 'Myntra Prepaid Refund',
                    recipientEmailId: _data.email
                  }
                };
                const url = `${config('absolut')}cart/prepaidEGiftCard/create`;
                Client.post(url, payload, { xid })
                  .end((absErr, absResp) => {
                    if (absErr) {
                      console.error('An error occured in api call inside createPPEGiftCart ', absErr);
                      callback(absErr);
                    } else {
                      const _absResp = at(absResp, 'body');
                      const cartId = `${at(_absResp, 'data.0.cartID')}`;
                      if (cartId) {
                        callback(null, { cartId });
                      } else {
                        callback('Absolut error');
                      }
                    }
                  });
              } else {
                callback(styleErr || 'Something went wrong');
                return;
              }
            });
          } else {
            callback(null, { isAmountNotEligible: true });
          }
        } else {
          callback(null, { isExchangeOrder: true });
        }
      }
    });
  }
};

router.post('/getOrders', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const pastOrdersEnabled = req.myx.features['mymyntra.pastorders.enable'] === 'true';
    const ratingsFG = req.myx.features['mymyntra.ratings.enable'];

    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      pastOrdersEnabled,
      payload: req.body
    };
    const dependencies = {
      getRatings: ratingsFG
    };

    if (user) {
      omsServiceGateway
        .getOrders(data, dependencies, (error, response) => {
          if (error) {
            console.error('Error [APIFY Server]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/getArchivedOrders', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const ratingsFG = req.myx.features['mymyntra.ratings.enable'];

    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      at: at(req, 'myx.tokens.at'),
      rt: at(req, 'myx.tokens.rt'),
      payload: req.body
    };
    const dependencies = {
      getRatings: ratingsFG
    };

    if (user) {
      omsServiceGateway
        .getArchivedOrders(data, dependencies, (error, response) => {
          if (error) {
            console.error('Error [APIFY Server Archived Orders]:', error);
            if (!handleUnauthorized(error, res)) {
              res.status(error.status).send({
                error: error.error
              });
            }
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/cancelOrder', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = req.myx.features['mymyntra.armor.enable'] === 'true' ?
      `${config('armor')}armor/order/v2/cancelOrder ` :
      `${config('oms')}oms/order/v2/cancelOrder`;
    const data = {
      user,
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      storeOrderId: req.body._storeOrderId,
      payload: {}
    };
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    ['USER_TOKEN', '_storeOrderId'].forEach(e => delete req.body[e]);

    if (user && clientUserToken === serverUserToken) {
      omsServiceGateway.getOrder(data, {}, (err, resp) => {
        if (err) {
          res.status(err.status).send({
            error: err.error
          });
        } else {
          const order = at(resp, 'body.order') || {};
          if (user !== at(order, 'createdBy.uidx')) {
            console.error(`[ Invalid Order Id ]: Order does not belong to user: ${user}`);
            res.status(401).send({
              error: `[ Invalid Order Id ]: Order does not belong to user: ${user}`
            });
          } else {
            Client.put(url, req.body)
              .end((error, response) => {
                if (error) {
                  console.error('Error [OMS Server CancelOrder]:', error);
                  res.status(error.status).send({
                    error: error.error
                  });
                } else {
                  res.status(200).send(response.body);
                }
              });
          }
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/cancelItems', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = req.myx.features['mymyntra.armor.enable'] === 'true' ?
      `${config('armor')}armor/orderrelease/v2/cancelLines` :
      `${config('oms')}oms/orderrelease/v2/cancelLines`;
    const data = {
      user,
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      storeOrderId: req.body._storeOrderId,
      payload: {}
    };
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    ['USER_TOKEN', '_storeOrderId'].forEach(e => delete req.body[e]);

    if (user && clientUserToken === serverUserToken) {
      omsServiceGateway.getOrder(data, {}, (err, resp) => {
        if (err) {
          res.status(err.status).send({
            error: err.error
          });
        } else {
          const order = at(resp, 'body.order') || {};
          if (user !== at(order, 'createdBy.uidx')) {
            console.error(`[ Invalid Order Id ]: Order does not belong to user: ${user}`);
            res.status(401).send({
              error: `[ Invalid Order Id ]: Order does not belong to user: ${user}`
            });
          } else {
            const orderItems = at(order, 'items').map((item) => item.id) || [];
            const reqItems = at(req, 'body.releaseUpdateEntry.linesToBeCancelled').map((item) => item.id) || [];
            const validRequestItems = reqItems.every((item) => orderItems.indexOf(item) >= 0);

            if (validRequestItems) {
              Client.put(url, req.body)
                .end((error, response) => {
                  if (error) {
                    console.error('Error [OMS Server CancelItems]:', error);
                    res.status(error.status).send({
                      error: error.error
                    });
                  } else {
                    res.status(200).send(response.body);
                  }
                });
            } else {
              res.status(403).send({
                error: 'forbidden: All items does not belong to user'
              });
            }
          }
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);


router.put('/cancelPacket', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('gateway')}user/packet/storePacket/${at(req.body, 'packetId')}/cancelPacket`;

    if (user) {
      Client.put(url, req.body, { 'x-mynt-ctx': `storeid=2297;uidx=${user}` })
        .end((error, response) => {
          if (error) {
            console.error('[Error: Cancelling packet]: ', error);
            res.status(error.status).send({
              error: response.body.message
            });
          }
          res.status(response.status).send(response.body);
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.get('/packageDetails', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('apify')}?page=1`;
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');

    if (user) {
      Client.get(url, { xid })
        .end((error, response) => {
          const orders = response.body.orders;
          const packet = {};
          packet.items = [];
          orders.forEach((order) => {
            const items = at(order, 'items');
            packet.items.push(items[0]);
          });
          res.status(200).send(packet);
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/orderrelease/changeAddress', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    let url;
    let headers = {
      Authorization: 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU=',
      'Content-Type': 'application/json'
    };
    if (req.query.gateway) {
      url = `${config('user')}order/storeOrder/${req.body._storeOrderId}/changeAddress`;
      headers = { ...headers, 'x-mynt-ctx': `storeid=2297;uidx=${user};nidx=${xid};` };
    } else {
      url = `${config('oms')}oms${req.url}`;
    }

    const data = {
      user,
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      storeOrderId: req.body._storeOrderId,
      payload: {}
    };
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    ['USER_TOKEN', '_storeOrderId'].forEach(e => delete req.body[e]);

    if (user && clientUserToken === serverUserToken) {
      omsServiceGateway.getOrder(data, {}, (err, resp) => {
        if (err) {
          res.status(err.status).send({
            error: err.error
          });
        } else {
          const order = at(resp, 'body.order') || {};
          if (user !== at(order, 'createdBy.uidx')) {
            console.error(`[ Invalid Order Id ]: Order does not belong to user: ${user}`);
            res.status(401).send({
              error: `[ Invalid Order Id ]: Order does not belong to user: ${user}`
            });
          } else {
            const orderReleases = at(order, 'items').map((item) => item.orderReleaseId) || [];
            const reqRelease = at(req, 'body.addressChangeRequestEntry.releaseid') || '';
            const validRequestRelease = orderReleases.indexOf(+reqRelease) >= 0;

            if (validRequestRelease) {
              console.log('payload: ', req.body.addressChangeRequestEntry);
              Client
                .post(url, req.body, headers)
                .end((error, response) => {
                  if (error) {
                    console.error('Error [OMS Server UpdateAddress]:', error);
                    res.status(error.status).send({
                      error: error.error
                    });
                  } else {
                    res.status(200).send(response.body);
                  }
                });
            } else {
              res.status(403).send({
                error: 'forbidden: Release does not belong to user'
              });
            }
          }
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/markDelivered', [middlewares('oms'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const markDeliveredUrl = `${config('orchestrator')}/wms/getItemDetails/${req.body.warehouseId}/${req.body.shipmentId}`;
    const data = {
      user,
      xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
      storeOrderId: req.body.storeOrderId,
      payload: {
        getStyle: 'true'
      }
    };
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;

    if (!req.body.warehouseId || !req.body.shipmentId) {
      res.status(200).send({
        status: 'error',
        message: 'Could not verify barcode number. Please try after some time'
      });
    } else {
      if (user && clientUserToken === serverUserToken) {
        omsServiceGateway
          .getOrder(data, {}, (e, r) => {
            if (at(r, 'body.order.createdBy.uidx') === user) {
              Client
                .get(markDeliveredUrl)
                .end((err, resp) => {
                  if (err) {
                    console.error('Error [OMS Server]:', err);
                    res.status(err.status).send({
                      error: err.error
                    });
                  } else {
                    const barcodes = at(resp, 'body.data');
                    if (!barcodes) {
                      res.status(200).send({
                        status: 'error',
                        message: 'Could not verify barcode number. Please try after some time'
                      });
                    } else {
                      const index = findIndex(barcodes, (barcode) => (at(barcode, 'barcode') === req.body.barcode));
                      if (index !== -1) {
                        const mdPayload = {
                          tripOrder: {
                            tripOrderStatusValue: 'Delivered',
                            remark: 'DELIVERED',
                            createdBy: user,
                            orderId: req.body.packetId,
                            deliveryReasonCode: 'DELIVERED',
                            shipmentType: 'DL',
                            mode: 'Customer',
                            deliveryTime: `${new Date().toISOString().slice(0, 19)}+05:30`
                          }
                        };
                        Client.post(`${config('lms')}trip/updateTripOrderOnTheFly`, mdPayload, {
                          Authorization: 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU=',
                          'Content-Type': 'application/json'
                        }).end((error, response) => {
                          if (error) {
                            console.error('error in marking delivered: ', error);
                            res.status(200).send({
                              status: 'error',
                              message: 'Unable to process your request. Please try after some time'
                            });
                          } else {
                            const respToSend = at(response, 'body.tripOrderResponse.status.statusType') === 'ERROR' ? {
                              status: 'error',
                              message: 'An error occurred. Please try after some time.'
                            } : {
                              status: 'success'
                            };
                            res.status(200).send(respToSend);
                          }
                        });
                      } else {
                        res.status(200).send({
                          status: 'error',
                          message: 'Invalid barcode number'
                        });
                      }
                    }
                  }
                });
            }
          });
      } else {
        res.status(403).send({
          error: 'forbidden'
        });
      }
    }
  });
}]);

export { router, omsServiceGateway };
