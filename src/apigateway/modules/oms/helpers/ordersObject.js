import at from 'v-at';

/**
 * @param {List Of Orders} OrdersObject
 *  Successive addition of requests to be done in this Object only.
 */
const OrdersObject = class {
  constructor(orders) {
    this.orders = orders;
  }

  getUpdatedOrders() {
    return this.orders;
  }

  getUniqueStylesForRatings() {
    const styleIds = [];

    this.orders.forEach((order) => {
      const items = at(order, 'items') || [];
      items.forEach((item) => {
        if (at(item, 'status.code') === 'D' || at(item, 'status.code') === 'C') {
          styleIds.push(at(item, 'product.id'));
        }
      });
    });
    return [...new Set(styleIds)];
  }

  attachRatingsDataToProduct(ratings) {
    const ratingsMap = {};

    if (ratings.length) {
      ratings.forEach((rating) => { ratingsMap[rating.key] = at(rating, 'value.userRating'); });

      this.orders.forEach((order) => {
        const items = at(order, 'items') || [];
        items.forEach((item) => {
          if (at(item, 'status.code') !== 'IC') {
            const product = at(item, 'product') || {};
            product.ratings = ratingsMap[product.id];
          }
        });
      });
    }
    return this.orders;
  }
};

export default OrdersObject;
