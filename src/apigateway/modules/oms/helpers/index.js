import async from 'async';
import { ratingsApiGateway } from '../../ratings';
import at from 'v-at';
import OrdersObject from '../helpers/ordersObject';
const isEmptyObject = (obj) => Object.keys(obj).length === 0 && obj.constructor === Object;

/**
 * Based on the criticality of the info attached.
 * Next Steps: Add a citicality field, and then call with the callback.
 */

export default {
  resolveOrderDependencies(orders, user, dependencies, callback) {
    const tasks = [];

    if (isEmptyObject(dependencies)) {
      callback(null, orders);
    } else {
      const ordersObject = new OrdersObject(orders);
      const ratingStyles = ordersObject.getUniqueStylesForRatings();

      if (dependencies.getRatings && ratingStyles.length) {
        const ratingsData = {
          user,
          styleIds: ordersObject.getUniqueStylesForRatings()
        };

        tasks.push((done) => {
          ratingsApiGateway.getUserRatings(ratingsData, done);
        });
      }

      if (tasks.length) {
        async.parallel(tasks, (error, response) => {
          if (error) {
            callback(null, orders);
          } else {
            const ratingsResponse = at(response[0], 'body.userRatingMap') || [];
            ordersObject.attachRatingsDataToProduct(ratingsResponse);
            callback(null, ordersObject.getUpdatedOrders());
          }
        });
      } else {
        callback(null, orders);
      }
    }
  }
};
