import express from 'express';
const router = express();
import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

import { omsServiceGateway } from './oms';

const serviceGateway = {
  getData: (data) => {
    const user = at(data, 'user');
    const expiredCards = data.type === 'active' ? false : true;
    const url = `${config('giftcard')}user/giftcard/${user}?isExpired=${expiredCards}&offset=${data.offset}&limit=${data.limit}`;
    return Client.get(url);
  },

  getBalance: (data) => {
    const user = at(data, 'user');
    const url = `${config('giftcard')}user/giftcard/balance/${user}`;
    return Client.get(url);
  },

  getTransactionDetails: (data) => {
    const user = at(data, 'user');
    const expiredCards = data.type === 'active' ? false : true;
    const url = `${config('giftcard')}user/giftcard/transaction/${user}?isExpired=${expiredCards}&offset=${data.offset}&limit=${data.limit}`;
    return Client.get(url);
  },

  getDataFromGcid: (gcid) => {
    const url = `${config('giftcard')}giftcardpurchase/config/${gcid}`;
    return Client.get(url);
  }
};

router.post('/getData', [middlewares('giftcard'), function (req, res, next) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const data = {
      user,
      type: req.body.type,
      offset: req.body.offset,
      limit: req.body.limit
    };

    if (user) {
      serviceGateway
        .getData(data)
        .end((error, response) => {
          if (error) {
            console.error('Error [GiftCard Server]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.get('/getGCBalance', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    if (user) {
      serviceGateway
        .getBalance({ user })
        .end((error, response) => {
          if (error) {
            console.error('Error [GiftCard Server]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/getTransactionDetails', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      type: req.body.type,
      offset: req.body.offset,
      limit: req.body.limit
    };
    if (user) {
      serviceGateway
        .getData(data)
        .end((error, response) => {
          if (error) {
            console.error('Error [GiftCard Server]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/addCard', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('giftcard')}user/giftcard/add`;
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;

    if (user && clientUserToken === serverUserToken) {
      Client.post(url)
        .send(req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error [GiftCard Server]:', at(error, 'error'));
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/showMoreTransactions', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const data = {
      user: at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login'),
      type: req.body.type,
      offset: req.body.offset,
      limit: req.body.limit
    };
    if (user) {
      serviceGateway
        .getTransactionDetails(data)
        .end((error, response) => {
          if (error) {
            console.error('Error [GiftCard Server]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/getDataFromGcid', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const url = `${config('giftcard')}giftcardpurchase/config/${req.body.gcid}`;
    Client.get(url)
      .end((error, response) => {
        if (error) {
          console.error('Error [GiftCard Server]:', error);
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
}]);

router.post('/resendGiftCardEmail', [middlewares('giftcard'), function (req, res) {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;

    if (user && clientUserToken === serverUserToken) {
      omsServiceGateway
        .getOrder({
          user,
          xid: at(req, 'myx.session.xid') || at(req, 'myx.xid'),
          storeOrderId: req.body.storeOrderId,
          payload: {}
        }, {}, (error1, response1) => {
          const order = at(response1, 'body.order');
          const _userOrder = at(order, 'createdBy.uidx');

          if (order && _userOrder.toLowerCase() === user.toLowerCase()) {
            const url = `${config('giftcard')}giftcardpurchase/reset/${req.body.releaseId}/${req.body.skuId}?email=${req.body.email}`;

            Client
              .put(url)
              .end((error2, response2) => {
                if (error2) {
                  console.error('Error [GiftCard Server], Resending Email:', error2);
                  res.status(error2.status).send({
                    error: error2.error
                  });
                } else {
                  res.status(200).send(response2.body);
                }
              });
          } else {
            res.status(403).send({
              error: 'forbidden'
            });
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export {
  router,
  serviceGateway
};
