import at from 'v-at';
import isEmpty from 'lodash/isEmpty';

function handleAuthTokenUpdate(req, res, apiResponse, clearAllTokens) {
  const updatedTokens = at(apiResponse, 'updatedTokens') || {};
  const mcookies = require('@myntra/myx/lib/server/cookies')(req, res);

  if (clearAllTokens) {
    mcookies.del('at');
    mcookies.del('xid');
    mcookies.del('sxid');
    mcookies.del('rt');
    return;
  }

  if (!isEmpty(updatedTokens)) {
    const defaultTokenExpirySecs = 186 * 24 * 60 * 60; // default 6 months expiry
    const loggedInSessionExpirySecs = at(req, 'myx.kvpairs.sessionExpirySecs');
    const nonLoggedInSessionExpirySecs = at(req, 'myx.features.sessionExpirySecs');
    const atTokenExpirySecs = at(req, 'myx.kvpairs.atTokenExpirySecs');
    const rtTokenExpirySecs = at(req, 'myx.kvpairs.rtTokenExpirySecs');

    const atExpiry = new Date(new Date().getTime() + (isNaN(atTokenExpirySecs) ? defaultTokenExpirySecs : atTokenExpirySecs) * 1000);
    const rtExpiry = new Date(new Date().getTime() + (isNaN(rtTokenExpirySecs) ? defaultTokenExpirySecs : rtTokenExpirySecs) * 1000);

    const atCookieConfig = {
      'encode': String,
      'expires': atExpiry
    };

    const rtCookieConfig = {
      'encode': String,
      'expires': rtExpiry
    };

    const sxidCookieConfig = {
      'secure': true,
      'encode': String
    };

    const loggedInXidConfig = {
      'expires': new Date(new Date().getTime() + (isNaN(loggedInSessionExpirySecs) ? 7 * 24 * 60 * 60 : loggedInSessionExpirySecs) * 1000)
    };

    const nonLoggedXidConfig = {
      'expires': new Date(new Date().getTime() + (isNaN(nonLoggedInSessionExpirySecs) ? 7 * 24 * 60 * 60 : nonLoggedInSessionExpirySecs) * 1000)
    };

    const atToken = at(apiResponse, 'updatedTokens.at');
    const rtToken = at(apiResponse, 'updatedTokens.rt');
    const xid = at(apiResponse, 'updatedTokens.xid');
    const sxid = at(apiResponse, 'updatedTokens.sxid');

    if (updatedTokens.rtExpired && atToken && xid) {
      // deconste rt and sxid
      mcookies.del('sxid');
      mcookies.del('rt');
      mcookies.set('at', atToken, atCookieConfig);
      mcookies.set('xid', xid, nonLoggedXidConfig);
      mcookies.del('ilgim');
    } else if (atToken && xid && rtToken && sxid) {
      mcookies.set('at', atToken, atCookieConfig);
      mcookies.set('rt', rtToken, rtCookieConfig);
      mcookies.set('xid', xid, loggedInXidConfig);
      mcookies.set('sxid', sxid, sxidCookieConfig);
    } else if (atToken && xid) {
      mcookies.set('at', atToken, atCookieConfig);
      mcookies.set('xid', xid, nonLoggedXidConfig);
    }
  }
}

module.exports = handleAuthTokenUpdate;
