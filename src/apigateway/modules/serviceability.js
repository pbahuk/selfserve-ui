import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.post('/checkServiceability', [middlewares('serviceability'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const xid = at(req, 'myx.session.xid') || at(req, 'myx.xid');
    const url = `${config('user')}order/serviceability`;

    console.log('********** Serviceability payload start **********');
    console.log(req.body);
    console.log('********** Serviceability payload end **********');

    if (user) {
      Client
        .post(url, req.body, {
          'x-mynt-ctx': `storeid=2297;uidx=${user};nidx=${xid};`
        })
        .end((error, response) => {
          if (error) {
            console.error('Error [CSS Server, Check Serviceability]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
            console.log('********** Serviceability response start **********');
            console.log(response.body);
            console.log('********** Serviceability response end **********');
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}
]);

export { router };
