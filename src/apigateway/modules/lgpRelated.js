import Client from '../../utils/Client';
import config from '../../config';
import express from 'express';
const router = express();
import at from 'v-at';
import middlewares from '../middlewares';
import when from '@myntra/myx/lib/when';

router.get('/fetchProducts/:productId', [middlewares('emptyState'), (req, res) => {
  when(req).all(() => {
    const lgpUrl = `${config('lgpRecommended')}`;
    const relatedUrl = `${config('related')}${req.params.productId}/related?colors=false`;
    const headers = {
      xid: at(req, 'myx.session.xid')
    };

    Promise.all([
      Client.get(relatedUrl).catch(err => err),
      Client.get(lgpUrl, headers).catch(err => err)
    ])
    .then((response) => {
      const apisFailed = response.every((resp) => resp instanceof Error);

      if (apisFailed) {
        console.error('Error [Empty State]: Failed to fetch products from both empty state apis');
        return res.status(500).send({
          error: 'no products found'
        });
      }

      return res.status(200).send(response.map((resp) => {
        if (resp instanceof Error) {
          return {};
        }
        return resp.body;
      }));
    })
    .catch(err => {
      console.error('Error [Empty State]:', err, 'Some error occured');
      res.status(500).send({
        error: 'Something went Wrong'
      });
    });
  });
}]);

export { router };
