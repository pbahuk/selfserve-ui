import Client from '../../utils/Client';
import config from '../../config';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import at from 'v-at';

router.get('/fetchProducts/:productId', (req, res) => {
  when(req).all(() => {
    const url = `${config('related')}${req.params.productId}/cross-sell?maxCount=100`;
    Client.get(url)
      .end((error, response) => {
        if (error) {
          console.error('Error [Cross Sell Server]:', at(error, 'error') || 'Failed to fetch Recos');
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
  });
});

export { router };
