import express from 'express';
const router = express();
import at from 'v-at';
import config from '../../config';
import when from '@myntra/myx/lib/when';
import Client from '../../utils/Client';
import middlewares from '../middlewares';

const ratingsApiGateway = {
  getUserRatings(data, callback) {
    const styleIds = at(data, 'styleIds').join(',');
    const url = `${config('ratings')}/user?uidx=${at(data, 'user')}&styleIds=${styleIds}`;

    Client.get(url, { 'x-mynt-ctx': `storeid=2297;uidx=${at(data, 'user')}` })
      .end((error, response) => {
        if (error) {
          console.error('Error [Ratings Server getUserRatings]:', error.response.error);
        }
        callback(error, response);
      });
  },

  getEligibleStyles(uidx, callback) {
    const url = `${config('ratings')}/eligible?uidx=${uidx}`;

    Client.get(url, { 'x-mynt-ctx': `storeid=2297;uidx=${uidx}` })
      .end((error, response) => {
        if (error) {
          console.error('Error [Ratings Server getEligibleStyles]:', error.response.error);
        }
        callback(error, response);
      });
  }
};

router.post('/getRatings', [middlewares('ratings'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    if (user) {
      const data = {
        user,
        styleIds: at(req, 'body.styleIds') || ''
      };
      ratingsApiGateway.getUserRatings(data, (error, response) => {
        if (error) {
          console.error('Error [Ratings Server, Get Ratings]:', error);
          res.status(error.status).send({
            error: error.error
          });
        } else {
          res.status(200).send(response.body);
        }
      });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);


router.post('/', [middlewares('ratings'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('ratings')}`;
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;

    if (user && serverUserToken === clientUserToken) {
      Client.post(url, req.body, { 'x-mynt-ctx': `storeid=2297;uidx=${user}` })
        .end((error, response) => {
          if (error) {
            console.error('Error [Ratings Server, Set Ratings]:', error);
            res.status(error.status).send({
              error: error.error
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export { ratingsApiGateway, router as ratingsRouter };
