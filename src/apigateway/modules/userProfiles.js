import Client from '../../utils/Client';
import express from 'express';
import config from '../../config';
import middlewares from '../middlewares';
import when from '@myntra/myx/lib/when';
import at from 'v-at';

const router = express();

const serviceGateway = {
  getProfiles(data) {
    const user = at(data, 'user');
    const url = `${config('userProfiles')}profile/${user}`;

    return Client.get(url);
  },

  getProfileById(user, profileId) {
    const url = `${config('userProfiles')}profile/${user}/${profileId}`;

    return Client.get(url);
  }
};

router.post('/saveProfile', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = req.body.userId;

    if (user) {
      Client.post(`${config('userProfiles')}profile/${user}`, req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error in saving profile: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.put('/updateProfile', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = req.body.userId;
    const profileId = req.body.profileId;

    if (user) {
      Client.put(`${config('userProfiles')}profile/${user}/${profileId}`, req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error in updating profile: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/addOrderToProfile', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');

    if (user) {
      Client.post(`${config('userProfiles')}order`, req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error in adding order: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/getSizeProfileOrder', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');

    if (user) {
      Client.post(`${config('userProfiles')}order/get`, req.body)
        .end((error, response) => {
          if (error) {
            console.error('Error in fetching order: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/getProfiles', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = req.body.userId;
    if (user) {
      serviceGateway
        .getProfiles({ user })
        .end((error, response) => {
          if (error) {
            console.error('Error in fetching profiles: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

router.post('/deleteProfile', [middlewares('userprofile'), (req, resp) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');

    if (user) {
      Client.delete(`${config('userProfiles')}profile/${user}/${req.body.profileId}`)
        .end((error, response) => {
          if (error) {
            console.error('Error in deleting profile: ', error);
            resp.status(error.status).send({
              error: error.error
            });
          } else {
            resp.status(200).send(response.body);
          }
        });
    } else {
      resp.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export {
  serviceGateway as uspServiceGateway,
  router as uspRouter
};
