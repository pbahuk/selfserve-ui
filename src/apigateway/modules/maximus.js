import maximus from 'maximus-node';

const maximusServiceGateway = {
  getPdpDataBulk(styleIds, callback) {
    const options = { timeout: 1000 };
    const service = new maximus(options);

    service.fetchData('getPdpDataBulk', styleIds, 0)
      .then(data => {
        if (data.statusResponse.statusCode === 200) {
          callback(null, data.data);
        }
      })
      .catch(err => {
        console.error('[Error: Maximus getPdpDataBulk]: ', err);
        callback(err, []);
      });
  }
};

export { maximusServiceGateway };
