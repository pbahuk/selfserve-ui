import Client from '../../utils/Client';
import config from '../../config';
import at from 'v-at';
import express from 'express';
const router = express();
import when from '@myntra/myx/lib/when';
import middlewares from '../middlewares';

router.post('/createExchange', [middlewares('css'), (req, res) => {
  when(req).all(() => {
    const user = at(req, 'myx.session.identifiers.C.login') || at(req, 'myx.session.login');
    const url = `${config('user')}exchange`;
    const xMyntCtx = `storeid=2297;uidx=${user}`;
    const serverUserToken = at(req, 'myx.session.USER_TOKEN') || '';
    const clientUserToken = at(req, 'body.USER_TOKEN') || '';
    delete req.body.USER_TOKEN;

    if (user && (serverUserToken === clientUserToken)) {
      Client
        .post(url, req.body, { 'x-mynt-ctx': xMyntCtx })
        .end((error, response) => {
          if (error) {
            console.error('Error [Gateway Exchange, create exchange]:', error);
            console.error('create exchange payload: ', JSON.stringify(req.body));
            res.status(error.status).send({
              error: error.message
            });
          } else {
            res.status(200).send(response.body);
          }
        });
    } else {
      res.status(403).send({
        error: 'forbidden'
      });
    }
  });
}]);

export { router };