// TODO: split into js , css , js objects 
export function push(req, str){
  if(!req || !str){
    return;
  }
  else{
    req.__pump = req.__pump || [];
    req.__pump.push(str);
  }
}

export function pump(req) {
  return (req && req.__pump && req.__pump.join && req.__pump.join(' ')) || '';
};