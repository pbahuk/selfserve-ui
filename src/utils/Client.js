// Singleton service client
// const Promise = require('promise');
const agent = require('superagent');

// import Track from '../utils/track';

let instance = null;

class Client {
  constructor() {
    if (instance === null) {
      instance = this;
    }
    return instance;
  }

  get(url, headers, _opts) {
    const options = _opts || {};
    const _timeout = options.timeout || 15000;
    return agent.get(url).set({
      ...{ 'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU='}, ...headers })
    .timeout(_timeout)
    .send();
  }

  post(url, params, headers, _opts) {
    const parameters = params || {};
    const options = _opts || {};
    const _timeout = options.timeout || 15000;
    return agent
    .post(url)
    .withCredentials()
    .set({
      ...{ 'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU='}, ...headers })
    .timeout(_timeout)
    .send(parameters);
  }

  put(url, params, headers, _opts) {
    const parameters = params || {};
    const options = _opts || {};
    const _timeout = options.timeout || 15000;
    return agent.put(url).set({
      ...{ 'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU=' }, ...headers }
    )
    .timeout(_timeout)
    .send(parameters);
  }

  delete(url, params, headers, _opts) {
    const parameters = params || {};
    const options = _opts || {};
    const _timeout = options.timeout || 15000;

    console.log('delete url is: ', url);
    return agent.del(url).set(
      { ...{ 'Accept': 'application/json', 'Content-Type': 'application/json' }, ...headers }
    )
    .timeout(_timeout)
    .send(parameters);
  }
}

export default (new Client());
