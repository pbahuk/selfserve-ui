'use strict';

const fs = require('fs');
const path = require('path');
const conf = {
  accessKeyId: 'AKIAIRS3IZDGU4ZMXCCQ',
  secretAccessKey: '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',
  region: 'ap-southeast-1'
};
const knox = require('knox');
const s3 = knox.createClient({
  key: conf.accessKeyId,
  secret: conf.secretAccessKey,
  region: conf.region,
  bucket: 'myntrawebimages'
});


var push = function(file, params, callback) {
    if (!fs.existsSync(file)) {
        callback("Source file not exists");
        return;
    }
    file = fs.realpathSync(file);
    var fileName = path.basename(file);

    // get the file extenstion (with optional gzipped)
    var matches = fileName.match(/\.(.*?)(?:\.(css|js))(?:\.(gz|jgz))?$/);
    var fileType = matches[2];
    
    var contentTypes = {
        json: 'application/json',
        js  : 'text/javascript',
        css : 'text/css',
        jpg : 'image/jpeg',
        png : 'image/png',
        gif : 'image/gif'
    };
    var contentType = contentTypes[fileType];
    if (!contentType) {
        callback("File type not supported");
        return;
    }

    // set the headers
    var headers = params.headers || {};
    headers['Content-Type'] = contentType;
    if (!!matches[3]) {
        headers['Content-Encoding'] = 'gzip';
    }
    //console.log(headers, s3path);

    var uri, prefix;
    var days = params.days || 365;
    var maxAge = days * 24 * 60 * 60;
    var expireDate = new Date();
    expireDate.setTime(expireDate.getTime() + maxAge * 1000);

    if (params.uri) {
        uri = params.uri;
    }
    else {
        prefix = params.prefix ? path.join(params.prefix, fileType) : fileType;
        uri = path.join(prefix, fileName);
    }
    s3.getFile(uri, function(err, res) {
        if (err) { callback(err); return; }
        if (!params.replace && res.statusCode == 200) {
            callback("Already Exists: " + uri);
            return;
        }

        headers['x-amz-acl'] = 'public-read';
        headers['Cache-Control'] = 'max-age=' + maxAge + ', public';
        headers['Expires'] = expireDate.toUTCString();

        s3.putFile(file, uri, headers, function(err, res) {
            if (err) { callback(err); return; }
            if (res.statusCode == 200) {
                callback(null, {uri: uri});
            }
            else {
                callback("S3 put failed: " + uri);
            }
        });
    });
};

var check = function(uri, callback) {
    s3.getFile(uri, callback);
};

exports.push = push;
exports.check = check;

