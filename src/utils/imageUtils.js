import get from 'lodash/get';

const imageQualityBasedOnNetwork = () => {
  const navigator = get(window, 'navigator') || {};
  const connection =
    navigator.connection ||
    navigator.mozConnection ||
    navigator.webkitConnection ||
    {};
  const effectiveType = connection.effectiveType || '';
  if (effectiveType === 'slow-2g' || effectiveType === '2g') {
    return 40;
  } else if (effectiveType === '3g') {
    return 60;
  } else if (effectiveType === '4g') {
    return 100;
  } else {
    return 'auto';
  }
};

const getCloudinaryImage = (src, w, h) => {
  if (src) {
    src = `${src.replace(
      '($qualityPercentage)',
      imageQualityBasedOnNetwork()
    )}`;
    src = isNaN(w)
      ? `${src.replace('w_($width)', '')}`
      : `${src.replace('($width)', w)}`;
    src = isNaN(h)
      ? `${src.replace('h_($height)', '')}`
      : `${src.replace('($height)', h)}`;
  }
  return src;
};

const getRandomBackgroundColors = () => {
  let backgroundColors = ['#ffedf3', '#fff2df', '#f4fff9', '#e5f1ff'];
  let randNum = Math.floor(Math.random() * 1000) % 4;
  return backgroundColors[randNum];
};

const getProgressiveImage = (src, attr = {}) => {
  const imgSize = get(attr, 'w', 800);
  const searchKey = 'assets.myntassets.com/';
  const startPos = src.indexOf(searchKey) + searchKey.length;
  // Default Value.
  const imageQuality = imageQualityBasedOnNetwork();
  const stripQ_AutoParamFromUrl = src => {
    if (src.indexOf('q_auto/') !== -1) {
      let key = 'q_auto/';
      let pos = src.indexOf(key);
      return src.slice(0, pos) + src.slice(pos + key.length);
    }
    return src;
  };
  // if URL contains q_auto, remove it,
  src = stripQ_AutoParamFromUrl(src);

  let cloudinaryProps = Object.keys(attr).reduce(
    (url, key) => (url += `${key}_${attr[key]},`),
    ''
  );
  if (!attr.q) {
    cloudinaryProps += `q_${imageQuality},`;
  }
  if (!attr.w) {
    cloudinaryProps += `w_${imgSize},`;
  }
  if (!attr.c) {
    cloudinaryProps += `c_limit,`;
  }

  cloudinaryProps += `fl_progressive/`;
  return src.substr(0, startPos) + cloudinaryProps + src.substr(startPos);
};

export { getCloudinaryImage, getRandomBackgroundColors, getProgressiveImage };
