import crypto from 'crypto';

const sortFlatAndAppendValues = function(obj) {
  var values = [];
  if (obj !== null) {
    var sortedKeys = Object.keys(obj).sort();
    for (let key in sortedKeys) {
      var value = obj[sortedKeys[key]];

      if (Array.isArray(value)) {
        let idxOrder = getStringSortedIndexOrder(value);
        idxOrder.forEach(idx => {
          values.push(value[parseInt(idx)]);
        });
      } else if (typeof value === "object") {
        values.push(...sortFlatAndAppendValues(value));
      } else if (value === null || typeof value === "undefined") {
        // pass
        continue;
      } else {
        values.push(value);
      }
    }
  }
  return values;
};

const getStringSortedIndexOrder = arr => {
  arr = Array.apply(null, { length: arr.length }).map(Number.call, Number);
  arr = arr.map(i => i.toString());
  arr.sort();
  // arr = arr.map((i) => parseInt(i, 10))
  return arr;
};

const generateAuthorizationToken = function(
  payload,
  config
) {
  var sortedPayload = sortFlatAndAppendValues(payload);
  var payloadString = sortedPayload.join("|");
  var authorisationString = `${config.client}|${config.version}|${payloadString}|${config.key}`;
  var auth = crypto.createHash('sha256')
    .update(authorisationString)
    .digest("hex");
  return auth;
};

export default generateAuthorizationToken;