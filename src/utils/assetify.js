'use strict';

const prefix = 'web/assets';

module.exports = function resolve(filename, protocol) {
  if (process.env.NODE_ENV === 'development') {
    return filename;
  }
  let path = 'http://myntra.myntassets.com/' + prefix + '/';
  if (protocol === "https") {
    path = 'https://cdn.myntassets.com/' + prefix + '/';
  }
  const filenameWithoutGzip = filename.replace('.gz', '');
  const extension = filenameWithoutGzip.substring(filenameWithoutGzip.lastIndexOf('.') + 1);
  path += extension + '/';
  return path + (filename.substring(filename.lastIndexOf('/') + 1)) + '.gz';
};
module.exports.prefix = prefix;
