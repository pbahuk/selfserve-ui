'use strict';

var fs = require('fs');

function isAlive() {
	return fs.existsSync('./.live');
}

var alive = isAlive();

fs.watch('.', function(e, file) {
	if (file === '.live') {
		alive = isAlive();
		console.log(new Date(), alive ? 'joining' : 'leaving', 'the load balancer');
	}
});


module.exports = function(req, res, next) {
	if (req.path === '/') {
		alive ? res.send(200, 'success') : res.send(404, 'Not found');
	} else {
		next();
	}
};
