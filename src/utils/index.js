export const handleUnauthorized = ( error, nodeRes) => {
  if (error.status === 401 || error.status === 403) {
    console.log('REASON=[UNAUTHORIZED REQUEST REDIRECTED]');
    const path = `/login?referer=${nodeRes.req.get('host') + nodeRes.req.originalUrl}`;
    nodeRes.redirect(path);
    return true
  }
  return false;
}