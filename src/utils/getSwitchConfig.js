const getDefaultSwitchConfig = map =>
  Object.values(map).reduce((defaultConfig, config) => {
    config.key && (defaultConfig[config.key] = config.defaultValue);
    return defaultConfig;
  }, {});

const getSwitchConfig = map =>
  ({
    list: (Object.values(map).map(val => val.key) || []).filter(key => key),
    tenantId: 'myntra',
    options: {
        username: 'checkout',
        password: 'checkout@123'
    },
    defaultConfig: getDefaultSwitchConfig(map)
  })

export default getSwitchConfig