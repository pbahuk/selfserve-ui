'use strict';

const assets = require('../../webpack-assets');
const cdn = require('./cdn');
const prefix = require('./assetify').prefix;

const pages = Object.keys(assets);
const files = [];
pages.forEach((page) => {
	Object.keys(assets[page]).forEach((type) => {
		files.push(assets[page][type]);
	});
});

const params = { prefix : prefix , replace : false };

files.forEach((file) => {
	const filename = __dirname + '/../..' + file+ '.gz';

	cdn.push(filename,params,(err, data) => {
		if (err) {
			console.error(err);
			return;
		}
		console.log('DONE: ' + 'http://myntra.myntassets.com/' + data.uri);
	});
});
