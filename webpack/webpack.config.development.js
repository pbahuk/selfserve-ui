const config = require('./webpack.config.base');
const ExtractTextPlugin = config.ExtractTextPlugin;
const webpack = require('webpack');
// const entryMain = config.entry;

config.devtool = 'source-map';
config.output.filename = '[name].js';
// config.entry = {};
// config.entry.main = [entryMain];
// config.entry.main = ['webpack-hot-middleware/client?reload=true'].concat(config.entry.main);
config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new ExtractTextPlugin('[name].css', { allChunks: true })
]);


module.exports = config;
