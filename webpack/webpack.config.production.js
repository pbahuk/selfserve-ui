const config = require('./webpack.config.base');
const ExtractTextPlugin = config.ExtractTextPlugin;
const webpack = require('webpack');
const CompressionPlugin = require("compression-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

config.output.filename = '[name].[hash].js';
config.plugins = config.plugins.concat([
  new webpack.optimize.DedupePlugin(),
  new ExtractTextPlugin('[name].[hash].css', { allChunks: true }),
  new UglifyJsPlugin({
  	uglifyOptions : {
		mangle: {
        	safari10: true,
        }
    }
  }),
  new CompressionPlugin({
    algorithm: "gzip",
    minRatio: 0.8
  })
]);

module.exports = config;

// new MinifyPlugin({}, {
//   test: /\.(js)$/,
// }),

// new webpack.optimize.UglifyJsPlugin({
//   compress: {
//     warnings: false
//   },
//   sourceMap: false
// }),

// new CompressionPlugin({
//   algorithm: "gzip",
//   minRatio: 0.8
// })