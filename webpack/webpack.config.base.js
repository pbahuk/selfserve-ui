const path = require('path');
const webpack = require('webpack');
const AssetsPlugin = require('assets-webpack-plugin');
const assetsInstance = new AssetsPlugin();
const autoprefixer = require('autoprefixer');
const constants = require('postcss-constants');
const COLORS = require('../src/app/resources/colors');
const fonts = require('../src/app/resources/fonts');
const fontWeight = require('../src/app/resources/fontWeight');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  entry: {
    main: ['babel-polyfill', './src/app/pages/app.js'],
    headerfooter: ['./src/app/components/Header/entry.js', './src/app/components/Footer/entry.js']
  },
  node: {
    fs: 'empty'
  },
  output: {
    path: path.resolve(__dirname, '../public'),
    publicPath: '/public/'
  },
  module: {
    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'eslint-loader'
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ],
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        exclude: /(dist)/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?-url&modules&importLoaders=1&localIdentName=[name]-[local]!less!postcss-loader')
      },
      {
        test: /\.(css|less)$/,
        include: /(dist)/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?-url!less!postcss-loader')
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: "file-loader?name=images/[name].[hash].[ext]"
      }
    ]
  },
  postcss: [
    constants({
      defaults: {
        colors: COLORS,
        fonts: fonts,
        fontWeight: fontWeight
      }
    }),
    // clean prefixes
    autoprefixer({ add: false, browsers: [] }),
    // all prefixes, dont remove legacy prefixes
    autoprefixer({ browsers: ['> 0%'], remove: false })
  ],
  plugins: [
    assetsInstance,
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.EnvironmentPlugin([
      'NODE_ENV', 'NODE_SUBENV'
    ]),
    new webpack.DefinePlugin({
      'process.myntcontext': JSON.stringify('browser')
    })
  ],
  ExtractTextPlugin
};

module.exports = config;
